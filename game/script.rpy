################################################################################
## Initialization
################################################################################

## Splashscreen ################################################################
##
## The splashscreen shown as the game launches

label splashscreen:
    scene bg black
    play sound "126756_sbordage_london-drinking" fadein 1.0
    pause 0.5
    scene splash with fade
    pause 3.0
    stop sound fadeout 1.0
    scene bg black with fade
    pause 1.0
    return

label act_transition(act):
    call reset
    play music "eisai.mp3"
    if act == "1":
        window hide
        show text "Shotglass Studios Presents{w=5.0}{nw}" at truecenter with Dissolve(3)
        pause 3
        scene bg black with Dissolve(3)
        show text "A Visual Novel by Eisai Project{w=5.0}{nw}" at truecenter with Dissolve(3)
        pause 3
    window hide
    if act == "0":
        $ title = "Prelude"
    elif act == "1":
        $ title = "A New Everything"
    elif act == "2":
        $ title = "Springtime of Life"
    elif act == "3":
        $ title = "Civil Twilight"
    else:
        $ title = "Finale"
    scene bg black with bg_change_days
    # Workaround to prevent a strange crash
    python:
        tmp = str(act)
    show text "Act [tmp] — [title]{w=5.0}{nw}" with Dissolve(3)
    pause 3
    window auto
    call audio_fadeout
    return

label show_line(line, hold, end):
    show text "[line]" with Dissolve(1)
    pause hold
    if end == 'true' or end == 'good':
        scene bg white with Dissolve(1)
    else:
        scene bg black with Dissolve(1)
    pause 0
    return

label weather(rain, wind):
    if rain != 0:
        if rain == 1:
            play ambient "401277_inspectorj_rain-moderate-a_1" fadein 0.5
        elif rain == 2:
            play ambient "401277_inspectorj_rain-moderate-a_2" fadein 0.5
        elif rain == 5:
            play ambient "401277_inspectorj_rain-moderate-a_5" fadein 0.5
        elif rain == 10:
            play ambient "401277_inspectorj_rain-moderate-a_10" fadein 0.5
    if wind != 0:
        if wind == 2:
            play ambient2 "405561_inspectorj_wind-realistic-a_2" fadein 0.5
        elif wind == 10:
            play ambient2 "405561_inspectorj_wind-realistic-a_10" fadein 0.5
    return

label stop_weather:
    stop ambient fadeout config.fade_music
    stop ambient2 fadeout config.fade_music
    return

label ending(end, thanksonly=False):
    if not thanksonly:
        play music "eisai_loop.mp3" fadeout 1 fadein 1
    $ quick_menu = False
    hide screen quick_menu
    if end == 'true' or end == 'good':
        scene bg white with Dissolve(3)
    else:
        scene bg black with Dissolve(3)
    pause 3
    window hide
    if not thanksonly:
        call show_line('The end', 5.5, end)
        call show_line('{u}Directed by{/u}\nWolvenfire86\n\n{u}Written by{/u}\nWolvenfire86\nThemocaw\nUwasaWaya', 4, end)
        call show_line('{u}Art by{/u}\nAnagram-Daine{space=30}fatti{space=30}Koeniginator{space=30}Llangil013\nMikaturtle{space=30}porogi_fur{space=30}Potat0Master\nRtil{space=30}Wyne\n\n{u}Music by{/u}\nShirli Ainsworth (Borealis)\nJ.R. Omahen', 4, end)
        call show_line('{u}Programming and editing by{/u}\nLeonhart231', 4, end)
        call show_line('{u}Sounds by{/u}\nAbolla{space=30}AshFox{space=30}Bassmonkey91\nBenboncan{space=30}bennstir{space=30}bone666138\nchancelombardo{space=30}crashoverride61088{space=30}deleted_user_2104797\ndeleted_user_4772965{space=30}Fewes{space=30}goldkelchen\nHerbertBoland{space=30}InspectorJ{space=30}Jack_Hase\nJakeEaton{space=30}joedeshon{space=30}Macif\nMedartimus{space=30}morgantj{space=30}Nabito{space=30}Podsburgh\npotentjello{space=30}sbordage{space=30}skyumori\nTHE_bizniss{space=30}tosha73{space=30}Vampirella17', 4, end)
        call show_line('{u}Shotglass Studios founded by{/u}\nUwasaWaya\nKoeniginator', 4, end)
    if end != 'true' or thanksonly:
        call show_line('Thank you for playing my game.\nAnd special thanks to the very talented people who helped with production.\n—Wolvenfire86', 15, end)
    stop music fadeout 3
    scene bg black with Dissolve(3)
    if end != 'true' or thanksonly:
        window auto
    $ quick_menu = True
    return

## Start #######################################################################
##
## Executed when the player starts the game

default called_someone = False
default true_end_available = False

label start:
    call a0
    return
