##--------------------------------------
## Act 1: A Brighter Future
## Scene 11: Chilling, Relaxing, Outside
##--------------------------------------

label a1s11:
    scene bg inou sunset
    show kevin neutral sides open
    with bg_change_minutes
    "The sun is just starting to set as Kevin and I leave the science building."
    "Kevin gestures me to come close to a nearby bench, one that overlooks campus. He sits down and reaches into his plastic bag. He pulls out two silver cans from the bag, both dripping with moisture."
    Kevin smile "Thirsty?"
    Shun "Is it … beer?"
    Kevin "Nah. That comes later. This is just juice. Hope you like watermelon. I’m a cherry guy myself. Wink-wink."
    "Kevin pops the can open and sits down."
    Kevin neutral crossed closed "Sit down. My favorite time of day is coming."
    Shun "Is the girl’s swim team coming out from practice?"
    Kevin smirk open "No, that’s during the week at like Thursday at around 4:35 or something. Sit down, sit down! You’ll see it in a second."
    stop music fadeout config.fade_music
    "I sit down next to him. He sips from his can, kicks his feet one on top of the other, and lets out a loud, relaxed sigh. He looks at the time on his phone and after a few seconds … nods."
    Kevin "See it?"
    "I look around and see … {w}nothing.{w} An empty campus. A setting sun. A sky painted with dark blue and burning orange."
    Shun "See what?"
    play music "decompression_loop.mp3"
    Kevin "Shun, Shun, Shun. If you don’t slow down to appreciate beautiful things, you’re going to let the best parts of life pass you by. Take a look around. Like, really look around."
    Kevin "This is perfect! It’s like the whole world was made for us right now. No noise, no work, just a pretty picture for us to admire."
    "Kevin gestures to the world around us and I take a second look. The campus {em}was{/em} beautiful this time of day. {w}The leaves on the trees dance as the wind tickles them, and I can see the colors fading gradually from the sky as night closes in."
    Shun "You relax a lot, don’t you?"
    Kevin "Life is a vacation."
    Kevin "I try to sit here on this bench at least once a week. And remember things."
    Shun "Like what?"
    Kevin "Important things. How to be myself. How to be okay being here, right now, for now.{w} How to enjoy moments that won’t come a second time."
    Kevin "It’s really easy to forget to appreciate something when you have it. Like, really easy. Scary-easy."
    "He rests his head back against the bench and sighs, looking for all purposes like a king upon his throne, surveying his kingdom."
    "I try to relax too, but that leisurely feeling doesn’t come as easily to me."
    "Still, now that he points it out … {w}this place is beautiful."
    "I gently sip my juice, much slower than Kevin does. He finishes his a long time before I do, but still relaxes, still soaks the sun and enjoys the relaxing sound of the wind."
    "When he finishes, he crushes the juice can and throws it at a garbage bin far away. It lands in flawlessly, without touching the rim."
    "When I am finished, I try to copy him … but miss … by a lot. Kevin snickers and leans his head back again, while I stand up to fetch my can."

    show risa neutral at right with config.say_attribute_transition
    "As I reach for it … a pair of thin fingers with purple nail polish touch my own."
    "Startled, I pull my hand back, and see a girl looking back at me, eye-to-eye as both of us try to pick up the same piece of garbage."
    "Her hair and eyes are both a striking violet shade, and her skin is pale like the moon. I am not sure if that is her natural skin tone or if she is wearing makeup … but I can see her lips also match her color scheme. They are purple and the same hue of her bangs."
    "She’d look pretty if not for that stone-cold, disinterested look. She stares at me in complete silence, with an unsure look."
    "She slowly picks the can up for me, and throws it out. Then, not making eye contact, she stands in front of me with her hands in the pockets of her leather jacket, and does nothing."
    Shun "Thank you. Sorry, I didn’t mean to litter. I was just about to pick it up."
    Risa "…"
    "She turns around and walks away in the direction opposite to Kevin, towards the girl’s dormitory."
    hide risa with config.say_attribute_transition
    "I walk back to Kevin, who is busy stretching and yawning."
    Kevin "Well … we should get back. Hideki is probably flipping out."
    Shun "About what?"
    Kevin "I don’t know, but it’s always something."
    Shun "Okay. Um, so … you said you know everyone, right?"
    Kevin "Well, almost everyone."
    Shun "Who was that girl?"
    Kevin "What girl?"
    Shun "The girl with the purple hair and the really pale face."
    "I point to where she was. She is still in view, and Kevin looks. He examines her from afar for a few seconds before answering."
    Kevin "… Nah, I don’t recognize her. Maybe she’s a freshman like you."
    Kevin "Go say hi to her."
    Shun "I tried to. She was … quiet."
    Kevin "Quiet is fine. It’s better than what is probably waiting for us back home."
    "He jumps off the bench and cracks his neck, then scratched his belly like a caveman."
    stop music fadeout config.fade_music
    Kevin "Come on little buddy. We might as well get this over with. I’m sure Hideki won’t be too angry."

    return
