# Scene: Bonding with Beers

label a1s17:
    stop music
    scene bg takuya_far night with bg_change_minutes
    play music "happy_hangout_loop.mp3"
    "These roads seem less daunting now that I know I won’t get lost. I try to memorize the names of the roads this time as Kevin guides the two of us to."
    "Kevin takes us to a bar with a bright neon sign above the entrance. The last letter flickers every few seconds. I can see people moving around inside. Their silhouettes look threatening through the amber-stained glass."
    "But the man guarding the front door looks even {em}more{/em} threatening. His arms are thicker than my thighs, and his rock-hard jaw has a scar on the underside."
    "Kevin walks up to this man without fear, and pats his shoulder. They shake hands and greet each other. Kevin whispers something in his ear and the big man laughs."

    Shun "How does he do that?"

    show hideki hips bored with config.say_attribute_transition
    Hideki "Do what?"

    Shun "Just … talk to people like it is no big deal."

    Hideki "Animals move in packs, I suppose."

    Kevin "Good to see ya again big guy. Think we could get in tonight? My friend is new in town. He’s been in college a full day and hasn’t had a drink yet. Please, for god sake’s, help! Hehe."

    Shun "He-hello."

    "The bouncer looks me up and down and says nothing for a moment. Then smiles at Kevin and, with a grumbling voice, says we can all go inside."

    scene bg bar night with bg_change_seconds
    "The inside of the bar is just like the places my mother told me to avoid. It smells like beer and cigarettes, though I don’t see anyone currently smoking. The lamps hang low over booths but barely illuminate the dark walls and blackened tables."
    "A vintage jukebox plays loud music from the 80’s, but not loud enough to cover the sound of a retro pinball machine howling every few minutes."
    "There is a stage at the end of the bar with an empty stool and unused mic. A number of patrons wear leather jackets and punk-rock style shirts. I start to panic, wondering where Kevin has taken me."
    "I sit with my hands on my lap, trying my hardest to not make eye contact with anyone, not even Hideki, who sits across from me at our booth. {em}His{/em} arms stay crossed and he glowers constantly, like he blames me for … something."
    "I see dozens of symbols carved into our wood table. The finish is completely gone, and left are markings that say profanity, names of lovers, bands and declarations that someone was once “here”."
    "Kevin comes to our table with three tall glasses filled with golden liquid that bubbles with froth. He places them down, then pats the table like a drum-roll."

    show kevin laugh sides closed at leftish
    show hideki crossed glare at rightish
    with config.say_attribute_transition
    Kevin "Who’s ready to kill some brain cells?"

    Hideki "Simpleton. As if you had any brain cells to kill."

    Kevin smirk sides open "Someone’s grumpy! You sound like you needs a driiink …"

    "Kevin lifts one glass and shakes it in front of Hideki’s face, who pushes his wrist away. A small splash of beer falls on Kevin’s knees."

    show kevin frown with config.say_attribute_transition
    Hideki armsup argue "Feh! First I must meet this new person, then am obligated to attend that pointless orientation, now I must cater to you and your social rituals … I would have had Fermat’s figured out by now if I didn’t have so many distractions!"

    Kevin smile "Well, if you don’t want your part of the booze, I can just give it to Shun."

    show hideki crossed glare
    show kevin frown
    with config.say_attribute_transition
    Shun "I actually don’t drink."

    "Kevin and Hideki look at me like I just said I was from another planet."

    show kevin neutral sides open with config.say_attribute_transition
    Shun "I mean, I’ve never drank before. I’m not, uh, there is nothing wrong with you two drinking."

    play sound "350630_jack_hase_pounding-one-time-on-a-wooden-table"
    "Hideki angrily throws his open palm on the table in protest to my admittance of sobriety."

    Hideki armsup argue "Well, what are you waiting for you fool!?"

    Shun "Is he talking to me or you?"

    Kevin mad "Hey!"

    play sound "350630_jack_hase_pounding-one-time-on-a-wooden-table"
    Hideki "If it will make you drink faster, I am speaking to both of you!"

    "Hideki pulls out a crisp bill from his smooth leather wallet and stuffs it in Kevin’s shirt-pocket."

    Hideki "Lucky for you, it is I who ordered the first round and not Gallagher. If he had done it, your first sample would be the watered-down horse-piss he enjoys."

    Kevin smirk sides open "As opposed to the watered-down horse-piss you enjoy? Just cause you’re a whiskey guy—"

    Hideki "Drink! Drink so I don’t have to listen to your incessant chatter!"

    show kevin crossed closed with config.say_attribute_transition
    "Kevin shrugs and happily does as he is told."

    Hideki "New Person. I will permit you this sampling at my expense this once. Once! Do not get into the habit of expecting charity in the future."

    "Hideki pushes one glass to me. It makes a distinct sound as it scratches over the carved-up wood table, bumping over the “I ❤ Kimichi” marking someone etched into it."
    "Hideki then takes his glass and clinks it against Kevin’s. Kevin throws the glass back and half of his beer is gone is a flash. He sighs and smiles happily afterwards."

    Kevin laugh sides closed "Oh man. I needed that. Between jet-lag and the paperwork, this has been one long week."
    Kevin neutral sides open "Your turn. Try some! Don’t worry, I know the people here. They won’t toss you out."

    Shun "Uh … I’m okay. Really, it’s fine."

    Kevin "Come on, drink it. You’re in college now. Not drinking in college is like not eating on a cruise. Or not dancing at a club."

    Shun "So just because I’m in college I’m supposed to enjoy drinking?"

    Kevin "Of course not, don’t be silly! That would imply you can’t enjoy drinking after you graduate."

    Hideki armsup argue "Silence! Shun, do you have any moral objections to consuming alcohol?"

    Shun "Moral objections? Not really. Just …"

    show hideki crossed pissed with config.say_attribute_transition
    "I can still imagine my mother passed out at the kitchen table with a half-empty bottle of whiskey in her hand as I smell the alcohol now in mine. It makes my stomach twist."
    "But I remind myself that I came to college to forget all that, and to become someone new. I was branching out just by being in this place. So why not at least try it?"
    "I look at the cool drink, noting the way the moisture drip over my fingertips, and take a deep breath. And look at it some more."

    Kevin "It’s not going to give you an invitation Shun!"

    Shun "I know, I know, just … uh …"

    Kevin smile crossed "Come on! Drinking is one of mankind’s oldest pastimes. It’s like, every culture in the world drinks and drinks together. So in a way, it’s like you’re becoming part of the World Culture. We’re talking about sophistication here Shun …"
    Kevin "{em}buuurp{/em} … Excuse me."
    Kevin "But seriously, drink it or I will."

    show hideki crossed glare with config.say_attribute_transition
    "I lift the glass up to my lips and let the alcohol run over my tongue."

    Shun "… BLEH! This is so bitter! People actually like this stuff?"
    Kevin smirk sides open "You get used to it. Like a new pair of pants, or a roommate."

    "Kevin pushes his elbow into Hideki’s side. Hideki doesn’t dignify him with a response. Hideki nurses his beer, with a poise in complete contrast with the American."

    show hideki crossed pissed with config.say_attribute_transition
    Kevin neutral sides open "Try taking a big swig. Just push it back, don’t worry about the taste right now."

    Shun "… Umm …"

    Kevin "Just do it."

    "I can still taste the bitterness of my first gulp. I pinch my nose and take a large gulp as Kevin suggests. I hear him laugh at me as I do this."

    Kevin laugh crossed closed "Give that a minute. You’ll feel more relaxed soon."

    scene bg bar night
    show kevin laugh sides closed at leftish
    show hideki crossed glare at rightish
    with bg_change_minutes
    "Kevin orders the next round, even though I have not finished my first drink. My beer got warm, so Kevin ordered a glass of water for me so I can drink with them."

    Kevin "Aaaaah! Not bad. Japanese beer is cool, but Belgium makes the best beer on earth. Hands down."

    Hideki "It disgusts me to see you gulping it down like water."

    Kevin "I can handle my booze. Unlike you, Professor."

    show kevin neutral sides open with config.say_attribute_transition
    "Hideki twitches when Kevin drops that nickname, but he quickly recomposes himself and tries to act like it doesn’t bother him."

    show hideki hips bored
    Hideki "On the contrary. I’ve carefully built a intolerance to alcohol. This way, I can enjoy the intoxicating effects faster and with less fluid. I save much needed funds during my time in school. And with a college student’s budget, that is a skill most certainly worth having."

    Hideki "Advice for the new partaker. New Person … to properly enjoy alcohol, you should buy good drinks, imbibe them slowly, and savor every sip."

    Kevin "Yeah. That works. If you’re a pussy. Sometimes you just want to drink to get drunk, know what I’m saying? Then you can buy whatever you want."

    "He punctuates this opposing opinion by burping again."

    Kevin "I might want another."

    Hideki armsup argue "Here. Take it, you lush."

    "Hideki passes Kevin his half-full glass and Kevin gladly takes it. He finishes it in another big gulp, sighing contentedly when it is emptied."
    "I slosh the warm fizzy amber liquid I still have, turned off by the sour flavor now made worse by its warmth. I don’t feel like drinking more after my first half, though I do feel slightly calmer as Kevin promised."

    Shun "I think I’ve had enough."

    Kevin laugh sides open "Dibs!"

    "Kevin grabs it and finishes it in a flash."

    Kevin "This is what it is all about man! Classes might be a nightmare. Studying might be a chore. But spending time with your bros over beers makes it worth it."

    Hideki crossed pissed "If that phrase came from anyone except you, it might have been inspiring.{w} Can we go now?"

    Kevin laugh sides closed "Fine, fine, fine. We’ll head back in a minute."

    "Kevin stands up, swaying a bit, and piles the empty glasses on top of one another. Hideki huffs and starts to wipe down our table with a napkin."
    "Despite their constant bickering, I notice they do little things to help each other out. Maybe they are being polite in public to clean up the mess so the waitress doesn’t have to, but they still do something nice without drawing attention to it."

    stop music
    scene bg dorm with bg_change_hours
    play music "decompression_loop.mp3"
    "We were gone for more than two hours, but I don’t mention that. Kevin looks far too happy."
    "Hideki bolts into his room as soon as we get back and slams the door."

    show kevin neutral sides open with config.say_attribute_transition
    Kevin "Well, it’s almost time to head to bed. Don’t wanna be late on day one of the new year, right?"

    Shun "Isn’t it bad to go right to bed after you drink?"

    show kevin smirk sides open with config.say_attribute_transition
    Kevin "You didn’t even have one full drink. You’ll be fine. Worst case … drink some water before going to bed. In fact, get into the habit of drinking water when you drink alcohol now."

    "Kevin stretches, cracking his neck and then his fingers as he sits down on the couch."

    Shun "Are you going to bed?"

    Kevin "I have to do a little something-something first. Don’t worry. Go get some shuteye. You wanna be rested for your first day."

    Shun "Yeah. Okay. Thank you for … everything."

    Kevin "Hey. What are friends for?"

    "He says that so casually that it is a little annoying. Does he actually value friendship if he calls me one so easily? Or is it really that easy to make new friends? I ponder that thought as I grab a bottle of water from the fridge."
    scene bg room_shun night with bg_change_seconds
    stop music fadeout config.fade_music
    "I’ve just had my first beer, with two strangers, on my second night in college, miles away from my old life. It was like an initiation of sorts. Another new experience in a new world. Part of me liked it. Part of me was afraid that I would turn into a delinquent."
    "But the way that Kevin accepts me so quickly … the way he treats me like we had known each other for years … who does that? Why put your trust in someone so easily? Why treat anyone like family without knowing who they really are?"
    play music "unrest_loop.mp3"
    "A knot in my stomach grows as I remember Higa-sensei."
    "He never gave up on me and I was mean to him so often. Maybe … it really {em}has{/em} been me all this time. Maybe I’ve been treating people the wrong way. But what’s the right way? Kevin’s way? Being overly open and excessively honest?"
    "I hear shuffling outside my door, followed by the sound of Kevin’s voice. The walls are thin enough for me to eavesdrop."

    Kevin "{en}… Mom? Hi Mom! Do you miss me yet? Oh man, did Dad cry again? Ha! He always does that when I am not there. Tell him he’s a big wuss!{/en}"

    "I hear Kevin speak to someone in English. I understand only every other word, but I know he is talking to his family. Thousands of miles away, and they still keep in touch."

    Kevin "{en}No, Koji moved out. We got this new kid … he’s pretty cool. A little shy, but I like him.{/en}"
    Kevin "{en}All right, Mom. Okay … Mom, it’s late so I gotta … I know, I love you too. Tell dad I miss him. And call him a wuss … okay. Bye mom. …{/en}"

    "I hear Kevin hang up. Then he sighs. The living room light turns off and I hear his door close …"
    "I lay down on the bed. That knot in my stomach twists."

    return
