##-------------------------
## Act 1: A Brighter Future
##-------------------------

label a1:
    call act_transition("1")
    call reset
    call a0s8
    call a1s1
    call a1s2
    call a1s3
    call a1s4
    call a1s5
    call a1s6
    call a1s7
    call a1s8
    call a1s9
    call a1s10
    call a1s11
    call a1s12
    call a1s13
    call a1s14
    call a1s15
    call a1s16
    call a1s17
    call a1s18
    call a1s19
    call a1s20
    call a1s21
    call a1s22
    call a1s23
    call a1s24
    call a1s25
    call a1s26
    call a1s27
    call a1s28
    call a1s29
    call a1_5
    return
