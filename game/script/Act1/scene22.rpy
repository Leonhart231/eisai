# Scene: Week One Event

label a1s22:
    scene bg class_okada with bg_change_minutes
    stop music fadeout config.fade_music
    "As class ends, everyone continues to give Risa nasty looks. She hurries out the second the bell rings, disappearing mysteriously again."

    scene bg hall_school with bg_change_seconds
    "I have a free period and figure it would be best to look around. It could help clear the negative thoughts out of my head."
    "I explore the building with paced footsteps, admiring the statue of Inou Tadataka, inhaling the cool air a few times, counting my blessings silently."
    "Classes continue as I walk through the science building. I am alone in the silent air conditioned hallway. Nice and alone. My footsteps are the only sound in the hallway."

    Shun "This is what I missed."

    "Most of the doors are shut, signaling that lectures have begun. Only one door was open. Naturally, I look inside."

    scene bg lab_maryanne with bg_change_seconds
    "I find my way back to the lab I went into before. This time, there are a few upperclassman doing work, moving from here to there."
    "One paces back and forth among the glassware, rubbing his chin as he does. A few sit down at the long tables and diligently write on notepads."
    "It reminds me of the science club back in high school, like this was their own club house. Nerds only, no jocks allowed."

    play music "romantic.mp3"
    "And Maryanne, as she said she would be, is here too. Once again the only blonde head to be seen. Looking through a microscope with her hair tied behind her neck, as focused on her work as the others."
    "She jots down something on her open laptop, then looks back into the tubes."
    "I let myself inside. A few people look up at me, saying “what is he doing here?” with their eyes. I feel like an outsider again."
    "Maryanne is so fixated on whatever she is working on that she doesn’t notice me even as I stand next to her."

    Shun "A-hem."

    "I cough. She waves her hand in the air dismissively."

    Maryanne "Not now Yuki. I told you, I’d help you with your homework after I finished."

    Shun "I’ll tell her that if I see her."

    show maryanne coat confused circles with config.say_attribute_transition
    Maryanne "Oh Shun! Hello. Sorry, I was … preoccupied."

    show maryanne happy with config.say_attribute_transition
    Shun "I just wanted to stop by and say hello."

    "She seems happy to see me, but sleepy. A pair of dark rings hang under her eyes."

    Shun "I had fun last night. Next time, we should do it earlier though."

    Maryanne ecstatic "Yeah! I mean, yeah. It was fun."

    Shun "You are helping someone with their homework?"

    Maryanne happy "I help anyone who needs it. Yuki is a new student as well and she’s being extra careful with her assignments. Some of this work can seem daunting. … But it’s all doable. You just need to try hard and ask for help when you need it."

    "Not one to overlook an opportunity, I figure that it would be best to ask for help now rather than later."

    Shun "Speaking of which … I don’t need help right now, but Okada’s class looks like it is going to be harder than I thought. Would you mind helping me out later if need be?"

    Maryanne "Not at all. Just let me know when you need it. I’m usually right here."

    Shun "Thank you."

    show maryanne with config.say_attribute_transition
    "I nod to her. She smiles again. I hear someone clear their throat. I glance to my right and see the other scientists are still staring."

    Shun "Well, I’ll leave you to go back to your work."

    Maryanne embarrassed "Okay. I need to get this done but uh …"

    hide maryanne with config.say_attribute_transition
    "She doesn’t finish her sentence, and ends our discussion with a wave goodbye."

    scene bg hall_school with bg_change_seconds
    "I go back to my quiet walk alone. My head feels clearer already."

    return
