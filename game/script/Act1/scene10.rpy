##-------------------------
## Act 1: A Brighter Future
## Scene 10: Shione - Meet
##-------------------------

label a1s10:
    scene bg lab_shione with bg_change_seconds
    "I exhale very slowly, then open my eyes."
    "I’m in a slightly chilled laboratory … this must be the one Kevin mentioned earlier. Maryanne’s Sanctuary!"
    "Glassware, burners, jars, chemicals, lights, heat lamps and strange tools decorate the laboratory like pictures on the walls of an old mansion."
    "Soft sounds come out of various oddities and devices. Tinkering tubes, mechanical gears and … splashing water? I see something moving … fish?"
    "There are several fish tanks with fish in them. Beautiful ones. And coral, rocks, each tank with an odd device on their rims."
    "This is how the main character gets killed in the beginning of horrors movies … oh god, I’m playing the role to a tee!"
    play sound "416710_inspectorj_splash-small-a"
    "I jump as I hear one of the hideous creatures … oh, wait … never mind …"
    stop music
    scene bg lab_shione:
        xalign 1.0
        yalign 0.7
        zoom 2
    with bg_change_seconds
    "A barefooted girl stands on a ladder, precariously balancing with her head dipped into the fish tank in the back."
    play music "eisai_loop.mp3"
    "In her right hand is some kind of pistol-shaped instrument connected to a long rubber tube … which is connected to a motor, the source of the sounds I heard from the hallway."
    "She shoots globs of goop out of the pistol, into the water, which is filled with jellyfish."
    "My heart jumps as she balances on the tips of her toes, barely staying still on the metal ladder."
    Shun "Uh, hello?"
    "I call to her but she doesn’t respond. I cautiously come a little closer, looking through the glass."
    "She sways back and forth, dancing on the tips of her toes."
    "A mask covers her face, with a blue and white striped snorkel hovering just past her ear like a colorful stovepipe."
    "Her short-cropped, turquoise-dyed hair floats around her face, as she uses the gun-like tool to … do something in the water."
    "Her bright hair is decorated with plastic jellyfish clipped on near her ear."
    Shione "Ishy, ishy, ishy …"
    "A live jellyfish bumps into the lens of her goggles, then latches on."
    "I stare at the wobbly image of her for a while, perplexed, until the sound of the stool rocking back and forth ceases."
    "The girl’s face, distorted through small waves, looks at me. Like two cats unsure if the other will be friendly or hostile, neither of us move as our eyes meet."
    "An explosion of water puffs out from the snorkel and she pulls herself out of the tank, sending droplets flying everywhere."
    "The stool clicks loudly and swayed again … this time too much."
    "She squeals and throws out an arm and a leg, barely avoiding a fall into the fish tanks."
    Shun "Be careful!"
    "I reach to grab her, but she jumps off her stool and lands gracefully on the wet ceramic."
    play sound "559069_jakeeaton_foot-stomp"
    "Her arms stretch out like a proud gymnast, then she nonchalantly pulls her snorkel off before using her free hand to wring out her wet green hair."
    "Briefly walking like a mummy, with her hands stretched outward, she finds the table nearest to her and feels around, eventually grabbing a pair of thick, dorky glasses."
    "They magnify her once tiny eyes and project their blueness."

    scene bg lab_shione
    show shione hips excited
    with bg_change_seconds
    stop music fadeout config.fade_music
    "She is very short, the tip of her head barely passing my shoulders. Her cargo shorts make her look even more diminutive."
    "I take notice of a scar on her arm, one that looks almost like a tattoo but the fleshy-texture gives it away. I force myself not to stare at it."

    Shione "That’s better. Now I can see you. Before you looked all blurry and wobbly and then you looked like a shadow and now you look like a person. That’s a much better look on you."

    Shun "… Uh … thanks."

    Shione "You’re welcome."
    play music "decompression_loop.mp3"
    "She pats the side of her head hard, getting water out of her ears, then yawns and shakes her head like a dog, sending water droplets everywhere."

    Shione "That fall was a close one, right! If I’d broken a tank, I’d have had to make the survivors share houses and none of them really get along with their neighbors."

    Shione finger pompous "I think the hemigrapsus takanoi—that’s a crab by the way—keeps them up at night and blames it on the anguilla japonica."
    "I stare at her, confused. A huge grin ripples across her face."

    Shione hips exclaim "I’m kidding! Crabs can’t lie to other sea creatures! They aren’t smart enough. Not like eels. They’re the real jerks!"

    Shione excited "… Heh heh!"

    show shione pompous with config.say_attribute_transition
    "Her blue eyes gleam with excitement, even through that dorky mask. The snorkel flops around, loosely hanging from the rubber strap over her ear as she waited for my reaction."
    "And waited."
    "… And waited."
    Shun "… Uh … okay. Good to know?"

    show shione wonder with config.say_attribute_transition
    "The girl pouts and looks down at her feet, wiggling her toes in the puddle under her. I guess she wanted me to play along?"
    Shun "What were you doing? In the tank. I thought you were in trouble."

    Shione excited "I wasn’t in trouble. I was feeding my jellies."
    Shun "Were you talking to them?"

    Shione finger confused "What?! No way! That would be weird."

    Shione excited "I was singing to them."

    Shione hips "And feeding them! You have to squirt food at them with something like this gun here …"
    "She picks up the strange pistol, and clicks the trigger a few times. The greenish ooze spills out of the tip."

    Shione finger exclaim "So I put on my mask and turned the food-feeder on and I went in there and fed them but the gun is kinda old so I only could get a bit out at a time so that took a while …"
    Shione "… And after that I wanted to hang out with them and I was having fun watching them swim … so I started blowing bubbles."
    Shione hips excited "Then I realized you can blow bubbles when you sing. So I did both! And I fed them a bit more too! So I did three things at once!"
    "This has been a very weird day."
    Shun "Where are your shoes?"
    "She scratches her head like she doesn’t know what I am talking about. Then lifts up one foot and wriggles her toes."

    Shione confused "I’m barefoot because my socks were slipping on the footstool."
    Shun "Um, right. But where are your shoes?"

    Shione finger pompous "If I wear stockings, that would have been fine cause it would have been like wearing nothing at all anyway."
    Shun "Yeah, okay. But your shoes?"

    Shione hips confused "Huh? I’m not wearing shoes."
    Shun "I know that but …"

    show shione excited with config.say_attribute_transition
    "She smiles and giggles again, fiddling with the pistol as I get frustrated."
    Shun "I’m asking … wouldn’t your shoes have a firmer grip on the ladder?"

    Shione finger pompous "…"
    Shione hips confused "… …"
    Shione wonder "… … … oooooh. Yeeeeah. I … I guess that would have worked."

    Shione exclaim "I don’t know … I kind of like being barefoot. It’s like … when you have a grassy lawn and it rains and you walk barefoot through it just to remember what wet grass feels like."
    Shione "Or how you can wrap your toes around a rock or a step or something."
    Shione "I don’t know, it just feels nice to feel something real under your feet sometimes."
    Shione "Don’t you ever like doing different stuff?"
    Shun "I do, but—"

    Shione excited "Me too. I like it a lot. It’s fun to have fun, right?"
    "She balances on the tips of her toes, gliding through a small puddle."

    Shione exclaim "So, what’s your name?"
    Shun "I’m Shun."

    Shione excited "Hi Shun! I’m Shione."
    $ shione_name = "Shione"
    Shione finger "And behind you is Andy, Jasper, Rollo, Mariko and Jinzo."

    "She points to each Jellyfish in the tank by my shoulder, then proceeds to introduce me to the crabs in the other tank. One of the crab points his big claw at me and it looks like he is waving."
    Shione "And this is Carl, Cammy, Chris and Camelle. Carl is a jerk. He is harassing Chris, and Chris doesn’t want to hurt anyone so it’s not fair!"
    Shun "You named your fish?"

    Shione hips pompous "Okay, first of all, these are Arthropoda and Chrysaora and they are … like standing right in front of you. Rude! And second, haven’t you ever given a dog a name?"
    Shun "Um … I prefer cats."
    Shione "Same difference. Ever name one of those?"
    Shun "Well … yeah, okay. I guess I see your point."

    Shione excited "Good! I’m glad you get it. Some people don’t and they think I’m weird. But I have a giant tank of sea creatures and they don’t … so they can envy me all they want."
    "Shione picks up the feeding pistol, grinning as she prepares to feed … Carl. She clicks the trigger twice but the green goo doesn’t come out."

    Shione wonder "Oh no! Not again! Another broken-thingy?!"
    "She clicks it a few times, smacking twice, hoping that it will spring to life if she presses the same button over and over or is mean to it."
    Kevin "Shione!"

    show shione hips excited at leftish
    show kevin neutral sides open at rightish
    with config.say_attribute_transition

    "Kevin barges into the room and gives both of us a stare like we’re misbehaving children."

    Shione finger exclaim "Hi Kevin! My feeding gun is broken again. Fix it! Fix it! Fix it!"
    "Kevin eyes the feeding gun in her hand and sighs. He has a plastic bag in hand, which he puts down by the door."
    "The pistol’s tube is connected to a small mechanical box. Kevin walks over to it and casually flips a switch. With a low rumble and a hum, the device turns on."
    "Shione clicks the trigger, and a spray of gooey green muck spurts out onto the table beside us."

    Shione hips confused "Ack! No! The food!"

    "She fumbles with the gun and sets it to one side."

    Kevin laugh sides closed "It wasn’t fully turned on Shio. We talked about this; you gotta remember to turn stuff on. Or off. Or just … remember things."

    Shione pompous "I forgot you told me not to forget. But you remembered, so that was good."

    Kevin smirk sides open "And don’t forget to clean this one! You didn’t clean that squirt bulb thing and you had to throw it out."

    Shione excited "Maryanne used to help me clean."

    Kevin "Maryanne used to clean up for you."

    Shione finger pompous "I always said thank you! Just like I did for you 10 seconds ago."
    "Kevin reaches over and tugs on her cheek. Shione squeaks and squirms and flails around."
    "He has to bend over to holder onto her; she looks extra tiny under his towering frame."
    "He lets go after a few seconds of teasing her. Shione rubs her cheeks and pouts."

    Shione finger wonder "That wasn’t very nice!"
    Kevin "Clean your stuff up!"

    Shione hips pompous "Fine!"
    Kevin "Good!"
    Shione "Okay!!"

    Kevin serious crossed open "So Shun … I see you’ve met our resident mermaid."
    "Shione sticks her tongue out at Kevin and makes a childish sound."

    Shione excited "Oh, I wish! That would be so cool! All of that swimming … and you’d save a ton of money on clothes since you’d never need socks or pants again. Oh wait, I wouldn’t be able to walk in the grass anymore."
    Kevin "Shun, Shione is studying marine biology. She likes fish the way some girls like horses or shoes or whatever. I don’t get it, but it’s a problem she refuses to acknowledge."
    Shione "It’s not a problem! Fish are amazing!"

    Kevin neutral sides open "Shun’s my new roommate. I’m giving him the tour, introducing him, all that."

    Shione finger "Oh, how cool! Gimme a second and I can come …"
    Kevin "No! Remember? Cleaning up?"

    Shione hips wonder "No fun!"
    "She stamps her foot and pouts again. Kevin pinches his fingers together and she jumps back."

    Kevin "We’ll hang out again later. Promise."
    "Kevin leans in close to me."

    show kevin serious crossed open at rightishnear with config.say_attribute_transition
    Kevin "Leave now while you still can."
    "He stands up and grins at Shione, waving to her."

    show kevin neutral sides open at rightish with config.say_attribute_transition
    Kevin "See you around Shione."

    Shione finger excited "All right! Nice to meet you, Shun!"
    Shun "Nice to meet you too, Shione."
    "Shione waves as we leave."

    scene bg hall_school
    show kevin neutral sides open
    with bg_change_seconds
    "Kevin and I walk through the hallway, in silence for a few moments."

    Kevin smirk sides open "So …"
    Shun "So …?"

    Kevin laugh sides closed "She’s cute, huh?"
    Shun "She’s … something."

    Kevin neutral sides open "Yeah. Shione’s a goofball but she’s cool when you get to know her. It’s a good thing I pulled you away, or she would have talked your ear off for hours."
    Shun "She seems like a bit of a scatterbrain."

    Kevin serious crossed open "Yeah. A bit. But she’s a lot of fun. Maryanne balances her out … oh, they’re roommates."

    Shun "Oh geez. That must be … interesting."

    Kevin "Interesting how?"

    Shun "Well, they seem very much opposite to each other. Like complete opposites. Living together? I don’t know, how would that work?"

    Kevin "…"

    Shun "…"

    Kevin neutral "… Well, Shione goes to the beach almost every other weekend. Maybe that helps."
    Kevin "And she’s got tons of energy. On and off land. She’s also a lot smarter than she lets on."

    Shun "So she’s only pretending to be that way?"

    Kevin neutral sides open "Oh, no, she’s totally an airhead. But it would be easier to explain that the gun is broken rather than explaining to a stranger that she just likes to feed jellyfish with her face."
    Shun "I don’t think I understand."

    Kevin serious crossed open "If Shione had a fan club, “I don’t think I understand” be their motto."

    return
