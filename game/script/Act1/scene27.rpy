# Scene: Pregame

label a1s27:
    stop music
    scene bg hall_dorm_boys with bg_change_days
    play music "decompression_loop.mp3"
    "The week ends at last! By now I have figured out the politics of each individual class. The courses themselves were, for the most part, easy. The difficult part was managing all of them, and their homework, and my own personal life on top of that."
    "No one was cleaning my room or my clothes anymore. It was all on me. That was freeing, but daunting."

    scene bg dorm
    show kevin neutral sides open
    with bg_change_minutes
    "I get back to my dorm to see Kevin cleaning the dishes. He greets me without looking away from his work."

    Kevin "Yo! How’d ya day go?"

    Shun "It was fine. I’m ready to relax though. I need this weekend."

    Kevin smile crossed "I hear that. And what better way to unwind than to go partying?!"

    "In all that has happened this past week, I forgot about Shione’s party tonight. My legs ache and I debate if going would be worth it or not. But Kevin starts to talk about it like he has been looking forward to it all week."

    Kevin laugh sides closed "Shione gets really, really good food for her parties! Oh, and her surfing buddies are coming, I haven’t seen them in months. And there’s gonna be girls there. Last year, I met this Mayoko girl … really cool. Pretty as hell … hmmm … I should call her some time."

    Kevin neutral sides open "You’re gonna come right? It’s so much fun!"

    Shun "I … guess so."

    Kevin laugh sides closed "Great! And hey, if you wanna leave early, you can do that too. No pressure."

    "He says that, but I worry that I will let him down if I decide not to come."
    hide kevin with config.say_attribute_transition
    "He turns off the faucet and leaves his frying pan out to dry on a towel, then hurries to his room."

    Kevin "I’m going to get dressed! I got just the shirt. Ooooh man, this’ll be great! I missed this place. It feels so good to be back in Japan!"

    "He grabs a hand full of toiletries and runs into the bathroom, dropping them all in the sink loudly. With the bathroom spoken for, I decide to get ready in the privacy of my own room."

    scene bg room_shun sunset with bg_change_seconds
    "I manage to assemble a decent ensemble. A simple button-down shirt, slacks, and a little gel in my hair to make my hairstyle stick in place after I comb it down."
    "As I smooth out my clothes with my hand, nice clothes on, Kevin knocks."
    "Before I can say “come in”, he throws my door open."

    show kevin smile crossed open with config.say_attribute_transition
    "His hair is barely combed and he reeks of cheap cologne. He seems to be wearing the same clothes that he usually does, except these are ironed. I look significantly more dressed up than he does."

    Shun "Is it supposed to be casual?"

    Kevin neutral sides open "It’s whatever you wanna go as. Don’t you worry little buddy. You look slick."

    Shun "Uh, thanks."

    Kevin smile closed "Don’t sweat it. Let’s just worry about having fun tonight, okay? It’s you and me. Two gentlemen on the prowl tonight!"

    show kevin frown open with config.say_attribute_transition
    Hideki "Plus one."

    show kevin at leftish
    show hideki formal glasses adjust at rightish
    with config.say_attribute_transition
    "Hideki appears behind us. He runs his hand over his combed hair and coughs. His blazer tightly fitting his shoulders, like it was a size too small."

    Kevin "… Pinch me, Shun. I’m obviously dreaming."

    Hideki formal armsup argue "Why? Because I am attending a social gathering?"

    Kevin mad sides "No, because you’re doing it willingly. Who paid you? Did Ai pay you? Is it blackmail?"

    Hideki formal crossed glare "Tch. Idiot …"

    Kevin serious crossed "What’s the catch? Why are you crawling out of your hole?"

    Hideki formal crossed pissed "I too can enjoy sociable company … on occasion. When I choose. I researched what I am required to do at such events. Etiquette, mannerisms, anecdotes. I am confident that it will … be … fun."

    "Hideki and Kevin stare at each other. Hideki’s eyes narrow suspiciously. Kevin leans back and waits for him to spill the beans."

    Hideki formal hips bored "… Also, Shione has a reputation for making fantastic buttered crab legs."

    Kevin serious crossed open "Figures. The best way to get a college kid to show up to something is offer free food."

    Hideki formal glasses adjust "You two morons need only lead the way and not worry about my involvement. I will arrive with your group, not part of your group. You “gentlemen on the prowl” will hunt without my assistance."

    Kevin laugh sides closed "Just make sure you get home before morning or the sunlight will burn you to a crisp, Count."

    show hideki formal crossed pissed with config.say_attribute_transition
    "Kevin rubs Hideki’s combed head, messing up his hair. Hideki clenches his fist, visibly turning red again. I think the only thing stopping Hideki from hitting Kevin is the law and half a foot."

    Kevin "Okay, let’s go. The sooner we get there, the sooner we can grab that food Hideki was talking about."

    hide kevin with config.say_attribute_transition
    "Kevin grabs his keys and leaves the room first, hustling to the stairwell."

    Hideki formal hips bored "Shun."

    Shun "Uh … yes?"

    Hideki "… May I please borrow your brush?"

    "His question takes me by surprise, but I hand it to him."
    "He quickly fixes up the mess that Kevin caused."

    Hideki "Do I look … presentable?"

    Shun "Umm … I …"

    Hideki formal crossed glare "Well?!"

    Shun "You look nice."

    show hideki formal hips bored with config.say_attribute_transition
    "Hideki’s expression goes neutral. He nods. That might be the closest to a “thank you” I’ll get from him."

    Kevin "Are you coming or not?"

    "I follow my roommates. One sulks, the other gloats. I sense a long night."

    scene bg hall_dorm_girls with bg_change_minutes
    "I can hear a crowd of people behind the door. Kevin knocks twice and the door opens right away."
    stop music
    scene bg dorm_maryanne with bg_change_seconds
    play music "happy_hangout_loop.mp3"
    "Hideki beelines for the kitchen, pushing people slightly to the side and he hurries to get a free meal."

    show kevin neutral sides open with config.say_attribute_transition
    Kevin "Wow, look at him go."

    Shun "Does he eat a lot?"

    Kevin laugh sides closed "Nah, but bless him, he will try his best if it’s free. You’d better grab some while you can."

    show shione hips excited at rightish
    Shione "Kevin Kevin Kevin! How are yoooou!?"

    "She jumps up and down as she talks in her loud, high-pitched voice. Kevin pats her head."

    Kevin "Little mermaid! Thanks for the invite."

    Shione "Shuuun! Hiii! I’m glad you’re here too!"

    Shun "Uh, hi. Thank you for—"

    Shione "Kev, you will {em}not{/em} believe who is here! Koji stopped by!"

    Kevin "Get out! He’s seriously here? Shun, sorry man, I gotta see this guy. You’re good right?"
    stop music fadeout config.fade_music
    hide kevin
    hide shione
    with config.say_attribute_transition
    "Shione pulls Kevin away, tugging his wrist. She looks so tiny next to him that’s comical to watch her try to move him."
    "I try to grab something to drink, only to be pushed away by taller people who accidently don’t see me. I end up just standing awkwardly by myself around the refreshment table as Hideki haplessly pushes freshmen to the side to get his takoyaki."

    return
