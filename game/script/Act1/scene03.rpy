##-------------------------
## Act 1: A Brighter Future
## Scene 3: Tour of Campus
##-------------------------

label a1s3:
    stop ambient
    stop music
    scene bg dorm with bg_change_seconds
    play music "contemplative_loop.mp3"
    "The size of this room takes me by surprise. For a college dormitory, it’s practically a palace!"
    "There are no windows in the main room, and it smells a bit like sweat and old socks."
    "A large TV sits against one wall across from a worn-out couch that looks like it might have been bought from a yard sale. There is a cabinet under the TV filled to the brim with DVDs in a vast array of different languages."
    "I spot a few Hollywood blockbusters, and a section with nothing but Akira Kurosawa films."
    "A small fridge is shoved into a cramped kitchen with pots and pans piled high in the sink."
    "All in all, it’s a bit cluttered, but not dirty. Disorganized, but not disgusting. Not a deal breaker …"

    "There are three doors, all leading to different rooms. The door nearest to the kitchen has a sign hanging on the knob."
    "The sign says, “DO NOT DISTURB!” in thick, printed letters."
    "Written under that, in bold, red marker, is “UNLESS HIS BOYFRIEND CALLS”, in sloppy handwriting."
    "I suspect my other roommate is behind this closed off door. Maybe I should say hello?"

    menu:
        "Maybe I should say hello?{fast}"
        "Knock on the door.":
            $ hideki_annoy = True
        "Don’t knock.":
            $ hideki_annoy = False
            "…"
            "If the sign is there, it must be there for a reason. Best to leave it alone."
            "I take a seat on the couch and wait patiently for my new roommate to return."
            "… {w}… {w} …"
            scene bg dorm with bg_change_minutes
            Kevin "I’m back! Did ya miss me?"
            return

    play sound "194365_macif_door-knocking-angry"
    "Ignoring the warning, I knock on the closed door."
    "A muffled curse word blurts from inside. After a few moments, the door opens a crack."

    play sound "104533_skyumori_door-open-02"
    scene bg room_hideki with bg_change_seconds
    "A flash of light reflects off of a pair of sinister glasses."
    Shun "… Um … hi. I’m Shun Takamine. I’m your new roommate."

    show hideki crossed glare with config.say_attribute_transition
    play music "bickering_loop.mp3"
    Hideki "You are a student at this university, yes?"
    Shun "Yeah, actually I start …"

    Hideki crossed pissed "So I assume that means that you graduated High School?"
    Shun "Yes, I—"
    Hideki "Because I find it hard to believe that you managed at minimum four years of schooling before today and are still unable to read {b}a goddamn sign{/b}."
    Shun "But I—"

    Hideki chin gendo "{w=1.0}Let me explain this to you in simple terms, since you seem to lack the mental faculties for simple reading comprehension."
    Hideki "This will be the last time you ever knock on my door."
    Hideki "You will not enter my room without permission."
    Hideki "You will not eat any food with my name written on the container."
    Hideki "You will not use any video game controllers or play any video games or watch any movies with my name printed on their case."
    Hideki "You will not touch any of my property. If you see property that is not yours or The Dog’s, assume it is mine until proven otherwise."

    Hideki crossed glare "Current evidence indicates that you are a blithering moron with the social graces of a gorilla, and therefore I do not trust you to handle any of my belongings without trying to either eat them or use them to dig for tubers or break open nuts."

    Hideki crossed pissed "Good day."

    play sound "80928_bennstir_door-slam-1"
    scene bg dorm with hpunch
    stop music fadeout config.fade_music
    "He slams the door in my face before I can think of a response, leaving me gawking at the closed door."

    Kevin "Yeah, he’s like that. Don’t worry about it."

    return
