##-------------------------
## Act 1: A Brighter Future
## Scene 6:
##-------------------------

label a1s6:
    play music "bickering_loop.mp3"
    if not hideki_annoy:
        "He turns back to Hideki’s room as we leave."
        Kevin "Hideki! We out!"
        Hideki "FUCK OFF, GALLAGHER!"
        Hideki "Wait! Are you going to go get some food?"
        Kevin "Maybe."
        Hideki "Pick me up a sandwich on your way back!"
        Kevin "You’ll get nothing and like it!"
        Hideki "SCREW YOU, GALLAGH—"
    else:
        Kevin "Hideki, We out!"
        Hideki "Good! And take that idiot of a new roommate with you!"
        Hideki "In fact, why don’t the two of you just leave forever and never come back! Then I can finally get some work done in peace and quiet!"
        Kevin "By the way, we’re having a party here tonight! A few guys, some girls, some strippers …"
        Hideki "WHAAAT?!"
        Kevin "It’s cool! I’ll buy you a lap dance! No need to thank me!"
        Hideki "YOU HAD BETTER BE LYING, GALLA—"

    play sound "80928_bennstir_door-slam-1"
    scene bg hall_dorm_boys
    show kevin laugh sides closed at center
    with hpunch
    "Kevin leaves the room and shuts the door before the angry voice can finish his ranting."
    Kevin "Heh. Bugging him never gets old."
    Shun "Who was that?"

    Kevin neutral sides open "{em}That{/em} was Hideki Ayanokoji. My arch-nemesis."
    $ hideki_name = "Hideki"

    Kevin laugh sides closed "One day, I will slay him in mortal combat. One day, I will take his head, and with it, his power. For there can be only one …"
    Shun "… What’s he like?"

    Kevin serious crossed open "Shun, I speak seven languages fluently and I have yet to find a word to describe Hideki, though can think of a few four-letter words that come close."
    stop music fadeout config.fade_music
    Kevin neutral sides open "Let’s just say he’s one of a kind. Like me! But not in a good way."
    Kevin "You’ll see him later. But for now, let’s get that tour started!"
    Shun "You don’t have to do this if you don’t want to."

    Kevin laugh sides closed "But I do want to. Think of me as your personal welcome committee."
    Shun "All right. Thanks for your help."
    Kevin "Hey, that’s what I’m here for. If you need help from anyone, just ask. People are friendly here man. If you need to know where to find me …"
    Kevin "… Just ask the ladies."
    "His voice deepens for that last part."
    Shun "…" (multiple=2)
    Kevin "…" (multiple=2)
    Kevin "All right, the first stop on our Grand Inou Tour is Floor 3 of the North Men’s Dormitory."
    Kevin "The guys on this floor are like one big, distant family. Not everyone likes their family."
    "No kidding."
    Kevin "There’s a vending machine on the first floor if you want a drink or a snack. Tip Number One: Save your money and buy in bulk from an off-campus store. Food is …"

    "As we approach the stairwell, Kevin sniffs the air, then pinches his nose."
    Kevin "Ugh! Not again!"

    play sound "194365_macif_door-knocking-angry"
    "He bangs on the door to 3-A loudly."
    Kevin serious "Yo! Fusao! Are you cooking that squid thing again?"
    Fusao "Yeah! Sorry, gotta eat it before it goes bad!"
    Kevin "Well cover the pot! You’re funk-i-fying the whole floor!"
    Fusao "Sorry!"
    Kevin "Geez. Last time it was octopus …"
    Kevin neutral "Anyway, that’s 3-A. I lived used to live there when I was a first year. Fusao lives there now. He’s the RA."
    Kevin "Fusao’s cool, but he’s real busy so don’t expect to see a lot of him."
    return
