##----------------------------------------
## Act 1: A Brighter Future
## Scene 2: The American Welcome Committee
##----------------------------------------

label a1s2:
    stop sound
    play ambient ["81966_benboncan_foyer-crowd"] loop fadein 2.0
    scene bg hall_dorm_boys with bg_change_minutes
    "I check my paperwork for the sixth time … room 3-G. {w}I suppose that means it’s on the third floor."
    "I sling my luggage forward, step by heavy step up the stairwell."

    "While the outside of the dorm sports a sleek and modern look, the inside is the exact opposite. The interior is cozy, simple, and feels lived in. I feel like I’m in a cheap apartment in an expensive neighborhood."
    "It has a homey feel to it, like it’s been here a while."
    "The place smells of fresh carpet shampoo. They must have just cleaned everything for the new students."

    play sound "234263_fewes_footsteps-wood"
    "The stairs are stuffed with students and family members, carrying luggage, furniture, and various other items up and down each floor."
    "A few of the residents offer greetings. I nod and smile politely, but don’t stop to chat."
    "… Damn it, this bag weighs a lot."
    "I see an older brother carrying a suitcase for his younger sibling, the two of them laughing as they jokingly insult each other.{w} A pang of jealousy bubbles up in my stomach."
    "I give up and leave my heaviest bag in the stairwell. I doubt anyone will take it, and I can come back for it after I find my dorm. Besides, maybe when I catch my second wind it’ll feel lighter."

    stop sound fadeout 1.0
    stop ambient fadeout 1.0
    scene bg black with bg_change_minutes
    "I reach the third floor and wipe the sweat off my forehead."
    "No one else seems to be moving into the rooms on this floor. Am I in the right place?"
    "The door by my left reads 3-A.{w} … So I guess this is the right floor."
    "The carpet shampoo smell is not as strong here. I smell sunscreen and salt water and, for some reason fish? Weird."

    scene bg dorm_door with bg_change_seconds
    "I find the door to my new home at the far end of the hall. “3-G” is written on the bottom of the door in golden letters … looks like this is it."
    "I’ll be sharing a dorm room with two other people. At least that’s what my acceptance letter said."
    "The idea of sharing my space with two strangers makes me a bit nervous, but excited too. Given Inou’s reputation, I imagine they’ll be a couple of studious, quiet introverts like myself."
    "They’re probably the kind of guys who like to sit around and read a lot. {w}Which is good. I could use the peace and quiet. Maybe the quietness on this floor is a sign!"
    "Before I can overthink myself into any number of what-ifs, I take a deep breath. I calm myself."
    "I lift my fist … and knock."

    play sound "194365_macif_door-knocking-angry"
    pause 2
    Hideki "Gallagher! Get the door!"
    Kevin "YOU GET IT!!"
    Hideki "NO! YOU GET IT!!"
    Kevin "FINE!"

    "Two angry voices shout from behind the door. I feel a flutter of nausea as memories from home flash back."
    Kevin "Hold on! I’m coming!"
    "I hear heavy footsteps, then see the doorknob abruptly twist."

    play sound "104533_skyumori_door-open-02"
    pause 1
    play music "happy_hangout_loop.mp3"
    scene bg dorm_door open
    show kevin neutral sides open
    with bg_change_seconds
    Kevin "What?!"
    "The door opens up so fast I stumble backwards."
    Shun "Whoa!"

    Kevin "Yikes!"
    "The tall young man swiftly catches me by my shirt, pulling me back up before I can fall."

    Kevin "You okay, little guy?"
    Shun "Um, yeah. I think so."

    Kevin "Whew! That’s good. Sorry about that."
    "He brushes my shoulders off and pats my back. He’s so tall that he blocks out the ceiling light behind him."
    "His light-red hair is short and spiky and his shoulders are nearly twice as wide as my own."
    "Despite his intimidating appearance, he wears a big, easygoing smile."
    $ kevin_name = "Tall Foreigner"

    Kevin laugh sides closed "Oh hey, are you our new roommate?"
    Shun "I think so. This is 3-G right?"
    Kevin "That’s what it says on the door!"
    "He sticks his head back into the dorm room."

    Kevin neutral sides open "Hey Hideki, get out here!"
    $ hideki_name = "Hideki?"
    show kevin with vpunch
    Hideki "UP YOURS!"
    "He rolls his eyes."

    Kevin serious crossed open "Ignore that guy. He’s just not a morning person. {w}Or a night person. {w}Or a day person."

    Kevin neutral sides open "… Now that I think about it, I don’t know if he’s ever happy. {w}He doesn’t even look happy when he sleeps. A crowbar couldn’t pry his frown off him."
    Shun "Um … o—okay."

    "I swallow the lump in my throat. The twinge of nervousness I’d felt when I heard these two yelling turned into “GET OUT WHILE YOU STILL CAN!”"

    Kevin "Whatever. Nice to meet you bro, I’m Kevin Gallagher. What’s your name?"
    $ kevin_name = "Kevin"

    Shun "I’m … my name is Shun Takamine. I’m from Tokyo."

    Kevin laugh sides closed "Hey, Tokyo. A city boy, nice!  Welcome to your new home buddy!"
    "He offers to shake my hand. I indulge him, and he almost crushes my fingers as he wildly bobs my wrist up and down."

    Kevin smirk sides open "Haha, this is great! Good to have you here man."
    Shun "Yes. Thank you. Thank you for welcoming me here."

    Kevin "Heh … yeah, so, I’m a third year student. I know you’re new, so let me bring you up to speed."
    Kevin "First thing you have to know is, I’m quite the big deal on campus."
    Hideki "NO YOU’RE NOT!"

    Kevin closed "DON’T LIE! IT’S A BAD HABIT!"
    Hideki "I WILL POISON YOUR FOOD!"
    Kevin neutral open "Anyway … me. Big deal. And seeing as how I’m bigger than almost everyone in this side of the globe, no offense, I guess you could say I’m the biggest man on campus."
    Shun "Well … um … if you say so."
    "This has to be some kind of mistake. This has to be a joke. This guy can’t be my roommate?"

    Kevin laugh sides closed "That’s not all you brought, is it?"
    Shun "I left my biggest bag in the stairwell. It was kind of heavy, and I—."

    Kevin neutral sides open "Say no more! I’ll get it for you!"
    Shun "You don’t need—"

    Kevin serious crossed open "Nonsense. You’re tired, you’re new, you need a break. Go inside and get comfortable. Leave the heavy lifting to me, new best buddy."
    "Oh god."
    Shun "Well … o—okay, I guess if it’s—"

    hide kevin with config.say_attribute_transition
    "He rushes past me and down the hall … not waiting for me to tell him what the bag looks like. I’m sure he’ll figure it out.{w} I hope."

    return
