##-------------------------
## Act 1: A Brighter Future
## Scene 5:
##-------------------------

label a1s5:
    if not guess_kevin:
        Kevin "I’m actually American."
        Shun "I should have known."
        Kevin "But you didn’t, did ya?"

    Kevin laugh closed "New York is awesome … millions of things to do, pizza, the traffic is bad but no one drives anyway and … {w}let me know when I am rambling, okay?"
    "I really, really want to tell him he’s rambling, but I also don’t want to be rude to someone I’ll be living with for a year."
    Kevin smile open "But enough about me."
    "Thank God."
    Kevin "What’s your gift?"
    Shun "I don’t really have a gift. Good grades, I guess."
    Kevin laugh closed "Dude, “good grades” are a gift!{w} I’ve traveled all over the world and met a lot of people, and the one thing I’ve learned is that intelligence is in short supply everywhere you go."
    Shun "I don’t know. It doesn’t seem like a big deal to me."
    Kevin smile open "Don’t worry, you just gotta find your niche. Then you’ll get it."
    "Kevin continues to shuffle through my belongings, and tosses them on my bed so I can put them away. He moves quickly, forcing me to keep up with his motions as I actually put things away."
    "He jabbers on about a few random topics as he “helps”. What the school is like, what the teachers expect, which places in town are safe at night …"
    "Mostly, though, he talks about women. Where to find them. What they’re like. Ones he’s “been with” …"
    Kevin "… During sports events, it’s really easy to meet pretty girls. Especially during swim meets. The swim team here is like Olympic-class or something, and they are all in great shape."
    Kevin laugh "Hooking up is a bit tricky cause they’re always on swim team-mode, but the chase is half the fun, am I right?"
    Shun "I guess."
    "Not that I’d know all that much about women to begin with."
    Kevin smile "So, how about you? Got a girlfriend back home?"
    Shun "Unfortunately, no."
    Kevin closed smile "Unfortunately!? Dude, that’s a good thing!"
    Kevin open smirk "You’re in college now! You have so many options.{w} Plus, college girls are way better looking. They’re smarter, and they can drink!{w} That’s like fish throwing themselves into the net for you. High-five!"
    "He puts his hand up in the air and waits. I don’t reciprocate."
    Kevin "Guess I have to teach you how to high-five."
    Shun "I know what a high-five is."
    "I try to hint that I’m ignoring him, but he goes right on to blabbering about another subject, cyclically going back to women every few minutes."

    "I try to keep track of the important things he mentions, like the inner workings of the school and the names of buildings."
    "Separating what he thinks are jokes and what is actually legitimate advice is a bit of a chore."
    "I’m not sure if he’s incredibly cocky, incredibly stupid, or if he’s just acting that way for kicks. And frankly I can’t tell if I like him or not."
    "We finish unpacking in less than half an hour, even with his excessive talking. My clothes are put away, my books are on the shelves, and the once-heavy suitcase is finally light and easy to move."

    Kevin "That was fun."
    Kevin "So, now that the work is out of the way, how about a tour of campus?"
    Shun "Um …"
    Kevin smile crossed closed "And lunch. I’m getting hungry."

    Kevin serious crossed open "There is a cafeteria here and … {w}well, it’s okay. Takuya’s got it all though! It’s way better there. What do you say?"
    Shun "I guess so."

    Kevin neutral sides open "“Guess so?” Come on Shun, be more assertive! You’re living on your own now! You’re your own man, in the big world! How old are you again?"
    Shun "I turned eighteen earlier this year."

    Kevin laugh sides closed "Just the right age to live it up."
    Shun "Shouldn’t I study to get ready for classes?"

    Kevin smile crossed open "Schoolwork? Wow. You’re still thinking like a high-schooler. You poor, poor boy."

    "His face gets serious quickly. His voice deepens in tone."
    Kevin "Look man, schoolwork will always be there. But your youth is only here for a short time and college is only for four years. {w}{size=-7}(… unless you drop out or take forever to finish. Don’t do that though.){/size}"
    Kevin "Every day is an adventure waiting to happen! But you gotta go out and find it. Sure, you get your work done, but there’s more to life than just work."
    Kevin "Make something happen! Work will be waiting for us when we get back.{w} So let’s go around the campus and see what we can find, what do you say?"
    Shun "I …"
    Kevin "… And it would be a good idea to get acquainted with the campus before classes start on Monday. That way, you won’t get lost."
    "I’m tired from the drive over and the mental stress of everything that’s happened. All I want is a nap.{w} But Kevin’s persistence is hard to ignore. And his reasoning makes sense."
    Shun "Okay, I’ll go."
    Kevin "That’s the spirit!"
    Shun "But only for a little while. I’m kind of tired."
    Kevin "Fair enough. We can take it slow tonight. Maybe play some video games, watch a movie … We’ll save the big welcoming party for later."

    scene bg dorm with bg_change_seconds
    "Kevin swings my door open and pulls me to the living room with a quick lurch."

    return
