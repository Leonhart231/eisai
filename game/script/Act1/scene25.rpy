# Scene: The Paper

label a1s25:
    stop music
    scene bg class_okada
    show okada neutral
    with bg_change_hours
    play music "decompression_loop.mp3"
    "Okada passes back our pop-quizzes like a mortician passing out black tags. I can see nervousness on everyone’s pale faces. I suspect Okada moves slowly intentionally to draw this out."
    "But he doesn’t hand me back mine. When he finishes handing back the assignments, I am still empty handed."

    Okada "There, that is everyone."

    Shun "Um, sir?"

    Okada "Yes Mr. Takamine?"

    Shun "I did not get my test back sir."
    stop music fadeout config.fade_music
    Okada "No. You did not. Please see me after class."

    "My heart plummets like a stone in a pond. The remaining 40 minutes of class feels like an eternity."

    scene bg class_okada
    show risa concern at right
    with bg_change_minutes
    "Everyone else shuffles out of the classroom in a hurry, some jamming up the door on their way out. I stay seated and wait for the room to empty."
    "Risa, the last one to leave, puts her hand on my shoulder."

    Risa "Shun … it probably has bad news. I mean, uh … it could be worse. I mean … good luck."

    hide risa
    show okada neutral
    with config.say_attribute_transition
    "Okada sits at his desk, reading through his notes with a stern grimace. When at last the classroom was vacant he glances up at me, then back to his paperwork. Not a word is spoken."

    Shun "You … wanted to see me sir?"

    "He nods and gestures me to come towards him. He pulls my test paper out from the bottom of a stack of papers when I am close."

    Okada "This was acceptable."
    play music "decompression_loop.mp3"
    Shun "A 95? I got a 95?!"

    Okada "Yes Shun. A 95. Five points less than perfect."

    Okada "Are you happy with this grade young man? Does it meet your standards? According to your record, this is a little lower than you normally get."

    "He stares at me without a single emotional queue on his face. This was obviously a challenge, or dare, or something like that."

    Shun "Well, I … think it is pretty good considering it’s the first test of the year."

    Okada "Perhaps it is. And perhaps it is not. It depends on what you expect from yourself."

    "He searches through his briefcase and pushed a few folders inside."

    Okada "I would like you to write a paper for me. Twenty pages. The topic is illness. You can pick any fatal disease you want. And I would like it by this Friday."

    Shun "What!? Friday!?"

    Okada "Yes, is that a problem?"

    "He looks away and starts squiggling down notes before I could tell him how unfair that assignment was! His class wasn’t the only one I had to worry about and a paper that large in two days’ time is absurd."

    Okada "If you fail or choose to not do it, I will not count it against your grade. I will only count a good grade. It will help boost your average if you do a good job. That much, I can promise."

    Shun "Did anyone else get this assignment?"

    Okada "Yes. I assigned it to your friend Mr. Gallagher, when he was a first year student. He did … exactly what I expected him to do."
    Okada "You are dismissed. Good luck."

    Shun "Um, sir, I …"

    Okada "Goodbye Mr. Takamine. You better get to work."

    scene bg hall_school with bg_change_seconds

    "I leave the room, trying to figure out how best to do this assignment."
    "I need help to finish this …"

    return
