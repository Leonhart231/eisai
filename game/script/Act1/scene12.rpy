##--------------------------------------------------
## Act 1: A Brighter Future
## Scene 12: Swords and Dudes (Hideki First Meeting)
##--------------------------------------------------

label a1s12:
    scene bg hall_dorm_boys
    show kevin neutral sides open
    with bg_change_minutes
    Kevin "Well, then … any questions?"
    Shun "Not really."
    Kevin "No problem. But since the tour is done, wanna grab a bite?"
    Shun "You’re still hungry?"
    Kevin "Hungry for dinner, yeah. You can’t stay Number One on only a 2,000 calorie diet."
    "Kevin holds his hand up for another high-five. By now, I decide it’s best not to acknowledge it. I don’t want to encourage any more bad jokes."
    Kevin "You really need to relax more, Shun."

    hide kevin with config.say_attribute_transition
    "We climb the stairwell in the boys dorms. Kevin continues ranting about the different kinds of people who live in each room, throwing in facts about girls here and there."

    scene bg dorm
    show kevin neutral sides open
    with bg_change_seconds

    "The door next to Kevin’s is ajar. I can hear the sound of a whirring computer fan coming from it."
    Kevin "Oh, cool. Door’s open. That means he’s accepting visitors. C’mon Shun."
    "Kevin knocks on the door’s frame a couple of times before pushing it open."
    Kevin "Yo! Hideki!"
    Hideki "Hold on a moment …"
    Kevin "Hey buddy. I was just wondering if you—"
    play music "bickering_loop.mp3"
    Hideki "I said, SILENCE!!"
    Kevin "No you didn’t."
    Hideki "Yes I … oh wait, I didn’t.{w} … SILENCE!!"
    "Kevin rolls his eyes, shakes his head, and walks inside anyway."

    scene bg room_hideki with bg_change_seconds
    "The room is dark, lit only by the dull lamp standing over the bed and the fading sunlight peeking in through the blinds. It is meticulously tidy, down to the last book on the tightly-packed shelf."
    "Two large white boards hang on the wall, both decorated with advanced mathematical formulas. Some I can comprehend, but most are so complicated that I strain my eyes just looking at them."
    "Some have symbols that don’t even look like they belong in mathematics at all."
    "A computer with dual flat-screen monitors and a back-lit keyboard glows from the desk, humming loudly, proudly singing about how much money it must have cost."
    "Lounging on the bed, with a large drawing pad propped up against his knee, is a lanky young man with short ink-colored hair and round wire-rimmed glasses."
    "His body is rigid as a statue, save for his right hand, which spins a mechanical pencil like an airplane propeller through his thin fingers."
    "His eyes are locked onto a spot on the pad, though I can barely see his eyes through his thick lenses."
    "Kevin watches the focused young man like he is an attraction at a circus."
    "After a few seconds of this, he writes something across the white surface of the drawing pad with lightning speed. Symbols and equations fall on it like rain. "
    "Then he abruptly stops, scans the page carefully, but a growl of frustration as he doesn’t seem to like what he sees. He tosses the drawing pad across his room, followed by his pencil."

    show hideki crossed glare at leftish
    show kevin neutral sides open at rightish
    with config.say_attribute_transition
    Hideki "Damn it! I thought I had it. But I’m still so far away. I blame you, Gallagher."
    Kevin smile crossed "Don’t. If you didn’t want visitors, you should have closed the door."
    Hideki "I did close the door."
    Kevin "No you didn’t. It was wide open."
    Hideki armsup argue "The door must have been closed. You knocked."
    Kevin serious crossed open "I knocked on the door frame. I was being polite. Asshole."
    "The two of them glare at each other."
    show kevin neutral sides open
    show hideki hips bored
    with config.say_attribute_transition
    "Then they smirk, at the same time. In an instant, the tense atmosphere between them dissipates, while I’m still adjusting to it."

    Kevin smile crossed "What are you working on now? Fermat’s Last Theorem again?"
    Hideki "Pounding my head against a brick wall would yield more effective results. I am beginning to think Fermat was either a filthy liar or an ascended being beyond the reach of mere mortals."
    Shun "Didn’t Andrew Wiles prove Fermat’s theorem a long time ago?"
    show hideki crossed pissed with config.say_attribute_transition
    "Hideki glares at me, as though I had accidentally insulted him."
    Hideki "If you are referring to the “proof” Andrew Wiles submitted using his modularity theorem, then you’re as stupid as you look!"
    Hideki "Modularity theorem … ha! Tell me, if you took a machine gun back to the Feudal Era and shot Miyamoto Musashi, are you the superior warrior?"
    Kevin laugh sides closed "Oh my god! I just thought of an awesome idea for a movie! It’s like … that one part in Army of Darkness—"
    Hideki "Shush you!"
    Kevin open "They could call it “Feudal Error”. Get it? It’ll write itself!"
    Hideki "… That is not one of my worst ideas. But that’s beside the point!"
    Kevin mad crossed open "What? It’s wasn’t your idea. It’s my idea!"
    Hideki "The point that I am TRYING to get to is this … the TRUE proof of Fermat’s Last Theorem lies in entering the mindset of Fermat himself. I seek to recreate a proof of Fermat’s theorem using only 17th century mathematics."
    Hideki "In so doing, I will be like Sasaki Kojiro, defeating the Musashi that is this equation with my skill alone!"
    Shun "Didn’t Sasaki Kojiro lose that fight?"

    if hideki_annoy:
        "Hideki’s glasses flash as he shoots me a look of disdain."
        Hideki chin gendo "Hey! You’re the moron who disturbed my rest earlier, aren’t you? Get out!"
        Kevin neutral "Hideki, down boy! This is our new roommate."
        Hideki "You mean a new distraction? That’s just perfect! Perhaps Koji’s replacement will vacate our apartment faster than he did."
        Kevin "Shun, this is Hideki Ayanokoji. Part-time genius and full-time jerk. He doesn’t get paid for either of those jobs, so try to bear with whatever charity he throws your way."
        Hideki crossed pissed "I’m only a genius in comparison to knuckle dragging simpletons like you two idiots."
        Kevin smile "He’s a real charmer, ain’t he?"
    else:
        "Hideki’s glasses flash as he shoots me a cocky smirk."
        Hideki hips bored "What’s this? Did another young pup follow you home, Gallagher? Snapping at your heels like an infant clinging to his mother’s apron strings?"
        Kevin neutral "He’s our new roommate. Be nice."
        Hideki "You mean a new distraction? Koji’s replacement … perfect. Another one. I give him two months before he also decides to leave."
        Kevin "Shun, this is Hideki Ayanokoji. Part-time genius and full-time jerk. He doesn’t get paid for either of those jobs, so try to bear with whatever charity he throws your way."
        Hideki "I’m only a genius in comparison to knuckle dragging simpletons like you two idiots."
        Kevin smile "He’s modest too."

    "I take note that Hideki didn’t deny being a jerk."
    Kevin "Hey Four-eyes, we’re going out to Yamada’s. Want to come?"
    Hideki hips bored "If you had asked me a half hour ago, I would have said yes. Sadly, I have already dined. I suppose poor Yamada will have to serve his exquisite wares to a foreigner who cannot truly appreciate it tonight."
    Kevin mad "I appreciate their offers just fine, thank you very much."
    Hideki "Of course you do. All you have to compare to it is that greasy, fatty, high-calorie salt-slop back home."
    Kevin "The grease has all the flavor! Anyway, you’re one to talk. At least we take the time to cook our fish back home …"
    "Hideki turns to me, still looking cross."
    Hideki chin gendo "Shun. You’re from here. What do you like better? Japanese food, or god awful, high-calorie Western food?"
    Kevin "Don’t engage him Shun. If you give him attention, he’ll just follow you around all da—"
    Hideki "Your hair looks stupid."

    "Kevin gasps dramatically and touches his head."
    Kevin mad sides "My … my hair … is not … stupid! You monster!"
    Hideki "And … and … your legal guardian dressed you funny this morning! And I think your eyes are too far apart!"
    Kevin "… Are those supposed to be fighting words?"
    Hideki "What if they are?"
    "They stare at each other for a few tense seconds{w} … then Kevin rolls up his sleeves dramatically. I step back."

    Kevin "Hang on a minute, Shun, I gotta teach this boy a lesson."
    Hideki "You couldn’t teach me anything except how to lose!"
    stop music fadeout config.fade_music

    scene bg dorm with bg_change_minutes
    Hideki "Oh look. You’re down again."
    Kevin "Damn it! Stop spamming the low kick! You always do that."
    Hideki "It’s a legitimate strategy!"
    Kevin "Don’t use that argument! Spamming is cheap."
    Hideki "“Cheap” is the excuse of the loser. If I can defeat you with only—HEY! Give me back my glasses!!"
    Kevin "Inability to see is the excuse of the loser!"
    "And with that, their video game duel devolves into a wrestling match."
    "This is stupid. I’m out of here."

    return
