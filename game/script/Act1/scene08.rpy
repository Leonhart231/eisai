##----------------------------------------------------------------------------
## Act 1: A Brighter Future
## Scene 8: Maryanne - Enter Maryanne: The Brilliant Biologist (First Meeting)
##----------------------------------------------------------------------------

label a1s8:
    scene bg inou with bg_change_minutes
    "Kevin leads me back through town and past the front gate. He guides me through the main campus field without stopping."
    "A blossomless cherry tree waits for spring in the center of the field. The artificially planted bushes and trees are aesthetically placed where Mother Nature would not have picked, all trimmed to be as inviting as possible."

    show kevin neutral sides open with config.say_attribute_transition
    "I admire the pretty landscape as Kevin yammers out another story about himself."
    Kevin "… And that’s the bench where this one time I made out with this girl from Yodaki University during a swim meet. She had a thing for foreigners. High-five?"
    "Kevin raises his hand over his head and waits."
    Kevin "High-five? Yes? No? No high-five? Okay."
    "He puts his hand back down and keeps walking."
    Shun "This campus is gorgeous."
    Kevin "You should see this place in the summer. It’s even better in the summer, when the cherry blossoms are blooming and the girls are wearing less."
    "Kevin takes a shortcut through the field to a two-story building with a bell tower on its summit. A flight of marble stairs leads up from the cobble stone walkway to its front door."
    Kevin "This, my vertically challenged friend, is the science building. All of the science and medical eggheads study here."
    "Vertically-what now?!"

    Kevin "They have not one, but two laboratories. And … and …"
    "Kevin looks at his phone and checks the time."
    Kevin "… Actually, I need to run inside for a second. Do you mind making a short detour?"
    Shun "No, it’s fine. I’ll get to see the interior that way."
    Kevin "Good thinking."
    "He jogs to the building and I walked behind him. Wherever Kevin has to go, he seems to want to get there fast."

    hide kevin with config.say_attribute_transition
    show maryanne happy coat with Dissolve(2)
    play music "romantic.mp3"
    "Just as my foot touches the first step, a girl walks out the front door. Another Westerner, not surprisingly. I was running into a lot of those today."
    "Her blonde hair stands out, as it practically starts glowing when the sun hits her. She’s a bit tall for a girl, but that seems to be normal with Western women. At least it’s true for the ones on TV."
    "She looks slightly older than the girls back home, but perhaps I am imagining that because of the way she is dressed. The lab coat over her shoulders gives her a professional look."
    "Despite her exotic aura, she looks stressed out. She rubs two fingers on the brim of her nose as she walks outside. I hear her sigh as the door closes behind her."
    Kevin "Hey, look who it is!"
    "Kevin waves to her and climbs the stairs faster. The girl jumps when he shouts at her, then quickly becomes excited."
    Maryanne ecstatic "Kevin! Welcome back!"

    play sound "194081_potentjello_woosh-noise-1"
    show kevin neutral sides open at midleft behind maryanne with config.say_attribute_transition
    "Without hesitation, they share a warm hug.{w} Is this Kevin’s girlfriend?"

    show kevin at leftish
    show maryanne at rightish
    with config.say_attribute_transition
    Kevin "{en}So, is that old coot still running you ragged?{/en}"

    Maryanne worried "{en}He’s not such a bad man Kevin. He’s just strict.{/en}"
    "I sort of understand what they’re saying … once again, I wish that I had a better grasp of English."
    Kevin "{en}Oh! Come here, you gotta meet this guy!{/en}"
    "Kevin gestures towards me. The girl sees me and smiles politely."
    "Before we can speak, Kevin leaps down the stairs and enthusiastically pulls me closer."

    play sound "234263_fewes_footsteps-wood"
    show kevin at leftnear
    show maryanne happy at rightnear
    with config.say_attribute_transition
    Shun "Hey, don’t pull! I’ll come over!"
    Kevin "I believe you, yeah, but you’re not coming over fast enough."

    play sound "234263_fewes_footsteps-wood"
    "He stops me just short of a few inches, when we are all at the base of the stairs."
    Maryanne ecstatic "Hello. Are you Kevin’s friend?"
    "Her smile is as warm as her welcoming voice. She barely has an accent, and I can’t decide if that seems noteworthy, or refreshing compared to Kevin’s heavy inflection."
    Shun "We just met. I’m his new roommate."
    Maryanne happy "Oh! Well welcome to Inou. It’s a pleasure—"
    Kevin laugh crossed "Oh! I’m sorry, let me introduce you two. Where are my manners?"
    "Good question."
    Kevin smile "Maryanne, this is Shun Takamine. And like the little guy said, we’re gonna be roommates this year! And it’s gonna be awesome!"
    $ maryanne_name = "Maryanne"
    Kevin "Shun, this is Maryanne Sawyer. She’s one of those science eggheads I was talking about.{w} She’s an old friend, as smart as she is pretty and guess what? She’s single."
    Shun "W—what?"

    Maryanne worried "Geez, Kevin, calm down. Don’t scare him off so quickly. Not like Koji, that poor boy."
    Kevin "Sorry, sorry. I won’t do it again."
    "Maryanne commands and Kevin listens. Interesting."

    Maryanne happy "Pleased to meet you Shun."
    "She bows gently. I bow back to thank her. I definitely feel more comfortable around Maryanne than Kevin."

    Maryanne worried "Is Kevin treating you right? I know he can be a bit … overzealous sometimes."

    Shun "Kevin has been informative. He’s … um … very good at telling stories."

    Maryanne neutral "He’ll be more polite from now on. Right Kevin?"
    "Maryanne glares at Kevin again, the way a mother glares at a son when he has misbehaved."
    "Kevin rolls his eyes, says nothing for a moment as Maryanne continues to shoot him daggers, then generally nods. Maryanne’s bright smile comes back when he does."

    Maryanne happy "So, Shun, do you like your new home so far?"
    Shun "So far, yes. It’s very nice. We just got lunch at that place … Yamada’s. It was good …"

    Maryanne worried "Mmm. Food sounds good right now."
    "She rubs her smooth belly."

    Kevin serious crossed open "Did you forget to eat again? I keep telling you, girl, you work too hard. You have to take better care of yourself."

    Maryanne happy "I’m fine Kevin. Really. I just left my lunch in my dorm. That’s all."

    Kevin smirk sides open "Yeah right! What kept you busy this time? Cancer, genetics, or PlayStation?"

    Maryanne confused "None of the above. I volunteered to help Okada-sensei get his classroom ready for the new students."

    Kevin neutral sides open "Oh crap, Okada. I keep forgetting about that old mule."

    hide kevin with config.say_attribute_transition
    "Kevin bolts up the flight of stairs and opens the door. He turns and shouts back before ducking inside."

    show kevin neutral sides open at midleft behind maryanne with config.say_attribute_transition
    Kevin "Sorry dude, but I’ll be five, ten minutes tops. Maryanne, keep an eye on this one for me, okay?"

    hide kevin with config.say_attribute_transition
    "And he runs inside, leaving me with … his friend? His girlfriend? His female friend?"

    show maryanne happy at center with config.say_attribute_transition
    "Maryanne stands quietly and grins at me as we share an awkward silence."
    "The silence is broken by a loud rumbling coming from Maryanne’s stomach. She blushes."

    Maryanne ecstatic "Heh heh … Sorry."
    Shun "You sound very hungry."

    Maryanne worried "I actually {em}did{/em} forget to eat this morning. Don’t tell Kevin. He can overreact sometimes."
    "She groans softly. I remember the rice ball in the pocket by my side."

    menu:
        "She needs it more than me.":
            Shun "Here."
            "I pull the rice ball out and hand it to her. Maryanne gasps softly when she sees it."
            Maryanne confused "Oh no, I couldn’t. I’m fine."
            Shun "You just said you haven’t eaten all day. Go ahead, it’s yours. It’s just a rice ball. It’ll go bad if someone doesn’t eat it anyway."
            "Her stomach growls again before she could protest."
            Maryanne "Are … you sure?"
            Shun "Absolutely."
            "Maryanne glances at the rice ball, then at me, then back and forth a few times. There is a mix of colors in her eyes … a hazel and green blend."
            "Then suddenly, Maryanne snatches the morsel out of my hand and devours half of it in a single bite. I flinch as she gulps."
            Maryanne ecstatic "Oh wow … That tastes sooo good."
            "She finishes the other half with a second shark-sized chomp. She kisses one finger after it’s all gone."
            Maryanne "Mmm~. I really needed that Shun. Thank you."
            Shun "It’s … fine."
        "I should save it for myself.":
            "It is my snack to begin with. And she did say she has her lunch back at the dorms."

    show maryanne ecstatic with config.say_attribute_transition
    "Maryanne then stretches her arms far up into the sky."
    "Maryanne yawns lightly, arching her back and reaches towards the sky. I notice that she is curvier than most girls, though I try not to make it obvious that I’m looking."
    Shun "So … um … how do … er … how do you know Kevin?"

    Maryanne happy "Hm? Oh, it’s a long story. Very long. We met back when we were kids. Once."
    Shun "Oh, you’re American?"
    Maryanne "That I am."
    Shun "Are you from New York too?"
    "She laughs softly, then lowers her arms."
    Maryanne "I’m from California. That’s on the other coast. New York and California are so different, it’s sometimes hard to believe that they are in the same country."
    Shun "I have heard there is a lot to see in America."
    Maryanne "There is. So much. It’s so big. But there is a lot to see in Japan too. I really like it here."
    "Her eyes dart back and forth from the top of my head to the bottom of my chin. She smiles softly when she sees me noticing."
    "I remember that Kevin and she had shared a hug. Kevin doesn’t seem like the kind to have a steady girlfriend, though."
    Shun "So you and Kevin are just friends?"
    "I immediately regret my choice of words. It sounds like I’m checking to see if she’s single."
    Maryanne "Yep. Just friends. He’s like my big brother."
    Shun "Kevin actually has platonic female friends?"

    Maryanne confused "Ah. You noticed that he likes women. A lot."
    Shun "He doesn’t hide that fact."

    Maryanne worried "That’s for sure. Oh, that boy, he has a lot of … energy. It’s going to get him in trouble one of these days."
    Maryanne "But he is a loyal friend and a good person. I’m sure he’s been helpful to you so far, right?"
    "I think about that for a few seconds."
    "Yeah, Kevin has been a big help today … in his own way. Kind of."

    show maryanne neutral with config.say_attribute_transition
    "I catch Maryanne’s hazel eyes glancing over me again."
    Shun "Um. So. You were working in Okada-sensei’s laboratory?"

    Maryanne happy "The real thing is much less impressive than it sounds. It’s really just a classroom with functioning laboratory equipment stuffed in near the walls. But it’s mine."

    Maryanne ecstatic "Mine, haha. It’s not {em}mine{/em} mine, but it’s mine like … it’s like my sanctuary!"

    Maryanne worried "I wanted the smaller lab in the basement, but Shione claimed that one before I could."
    Shun "Who is Shione? Another science major like you?"

    Maryanne confused "What makes you think I’m a science major?"
    "I cough and gesture to her coat."

    Maryanne neutral "Uh … well I was. I—I mean, I am still."
    Maryanne "Actually, I am not an undergrad. I’m in grad school."
    "She gulps and stutters, like she is ashamed to admit her accomplishments."
    Shun "You got your bachelor’s degree already?"

    Maryanne confused "… I have two."
    Shun "Two what?"

    Maryanne neutral "Two degrees. I am … studying for my Master’s."
    Shun "A master’s? But … how old are you?"

    Maryanne confused "Well, I um …"
    "She inhales and straightens her posture, trying to look as confident as possible."

    Maryanne neutral "I … I skipped two grades. And I am nineteen years old."
    "She feigns conviction. I wonder how many times she’s had to tell this fact to different people and how many different reactions she’s received."
    Shun "That’s incredible."

    Maryanne confused "You really think so?"
    Shun "Yeah. Of course I do. That much success is something to be proud of."

    Maryanne ecstatic "I … well … thank you!"

    Maryanne happy "Most people … my seniors still think I’m just a teenager, but my colleagues think I’m some genius and they …"

    Maryanne ecstatic "Thank you! Heh heh … Thank you."

    show maryanne happy with config.say_attribute_transition
    "She’s so thrilled that I complimented her I almost feel bad. You’d think she’d be tired of praise, not flattered by it."

    Maryanne neutral "I wish it was the weekend. I need a vacation."
    Shun "It is the weekend."

    Maryanne happy "Oh? Well … good!"

    show maryanne confused with hpunch
    "{snd}BANG!{/snd}"

    show kevin neutral sides open at left with config.say_attribute_transition
    "The front door to the science building flies open. Kevin reappears."
    Kevin "I’m back baby! I hope Shun didn’t bore you too much."

    Maryanne happy "Not at all. He’s been on his best behavior.{w} Didn’t make any comments or jokes. He acted like a real gentleman, unlike some people I am used to."
    Kevin "… Are you talking about Hideki?"

    show maryanne worried with config.say_attribute_transition
    "Maryanne rolls her eyes."
    Maryanne "Yes. Yes, I am talking about Hideki."
    Shun "Did you get what you needed?"

    show kevin serious crossed open at leftish
    show maryanne neutral at rightish
    with config.say_attribute_transition
    Kevin "Nope. Okada is not in his office. He’s probably off terrorizing a child or a village or making plans to destroy Tokyo again."

    Maryanne confused "He’s not {em}that{/em} bad!"

    Kevin neutral sides open "I’ll find him later. He can wait another hour."

    Kevin smirk sides open "Want to come with us Cali-girl? I have to show this kid the inside of the science building, and then we’re going to figure out what we’re doing."
    Kevin "Shun here is gonna find out why our swim team is known throughout all of whatever the hell this particular island is called."

    Maryanne neutral "Honshu."

    Kevin serious crossed open "Bless you."

    Maryanne confused "No, Kevin … that’s not …"

    show kevin smirk sides open
    show maryanne worried
    with config.say_attribute_transition
    "Kevin laughs. Maryanne shakes her head."
    Kevin "It was a joke, lighten up.{w} So you wanna come?"

    Maryanne happy "Thanks for the invite. But I am still really hungry."

    Kevin neutral sides open "No problem. Hey, if you aren’t doing anything later, wanna get sushi? I could really go for some sushi tonight."

    Maryanne ecstatic "Anything edible sounds good right now. That sounds—"

    show maryanne happy
    show kevin smirk sides open
    with config.say_attribute_transition
    Kevin "Like a plan!"

    show kevin neutral sides open with hpunch
    "Kevin pats my back so hard that I nearly fell over."
    Kevin "Come young one. To the science wing! Follow me Shun. We have so much time and so little to do! Or something like that."
    Kevin "Some of what I say gets lost in translation. {en}Vamanos, mi amigo!{/en}"

    hide kevin with config.say_attribute_transition
    Shun "It was nice meeting you."
    Maryanne "Nice meeting you too Shun."
    "She waves as I leave."

    show kevin neutral sides open at center
    hide maryanne
    with config.say_attribute_transition
    stop music fadeout config.fade_music
    Kevin "She’s cute ain’t she?"
    "Kevin asks when we were barely three feet away from her."
    "I look back.{w} Maryanne waves timidly as she walks away. I smile and wave back."

    return
