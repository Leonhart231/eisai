##-------------------------
## Act 1: A Brighter Future
## Scene 7: Tour of Campus
##-------------------------

label a1s7:
    stop music
    scene bg inou
    show kevin neutral sides open
    with bg_change_minutes
    play sound "234263_fewes_footsteps-wood"
    play music "decompression_loop.mp3"

    "Kevin pulls me out of the dorms by my shirt. Even with my feet sliding against the ground, he moves me easily, only letting go when we are outside."
    Kevin "So that was the men’s dorm. I’m sure you knew that since you’re a man and you’re dorming there."
    Kevin laugh closed "The ladies’ dorms are that-a-way! You can find me there on the weekends. Afternoons and nights. And some mornings. Am I right?"
    Shun "Right about what?"
    Kevin neutral open "…"
    Kevin "… So, the official rules say that curfew is 10:30 on the weekdays. {w}Fridays and Saturdays, there’s no curfew, and Sunday it’s midnight.{w} But they’re pretty flexible about breaking that rule as long as you don’t get caught."
    Kevin "Takuya Town Center’s behind you and straight down. It’s got all you need for college life. Cheap food, cheap clothes, a mall, a bar, movies, a puppy store … You like puppies, right?"
    Shun "I prefer cats."
    Kevin smile crossed "And so does the Internet, hence why most of the people online are single."
    Kevin "See, people with cats hang out online all day.{w} People with dogs go outside and walk around. Puppies are great conversation starters."
    Kevin "My point is, guys with puppies get laid more than guys with cats. Get it?"
    "The way this guy talks about women, I have to wonder if they like him back or not."
    Kevin laugh closed "Come on! We’re burning daylight!"
    "He marches onward, and I follow behind him. I feel more like a mule following a farmer than a tourist."

    scene bg inou_gate
    show kevin neutral sides open
    with bg_change_seconds
    "Kevin shows me to the white stone gates from earlier, pointing to it with energy."
    Kevin "This is the front gate. They call it Shiro Torii."
    Kevin "Dumb name, right? You’d think they’d give it something more clever.{w} I tried calling it Black Gate to be ironic, but no one got the joke.{w} Then I tried calling it Stargate, but no one got the reference."
    Shun "What reference?"
    Kevin frown "There you go again. None of you geniuses ever get my references."
    Shun "I—{nw}"
    Kevin smile "But it’s okay. I got plenty more jokes. Ever hear the one about the dead cow and the lady leprechaun?"
    "Before I can answer, Kevin freezes and turns red. He puts his hand on my back and pushes me away from the entrance."
    Kevin neutral "You know what? I can tell you the joke later. Let’s get food."
    "As he pushes me away, I can hear a girl’s voice laughing. It sounds kind of familiar …"

    scene bg takuya
    show kevin neutral sides open
    with bg_change_seconds

    "Kevin leads me through Taukya, walking in a pace that I can just barely keep up with. The town looks peaceful, with plenty of apartment buildings and shops lining the streets."
    "There are far more young people than old, but that is to be expected with a college right next door. It’s clean and peaceful, and vaguely reminds me of home."
    "As I get lost in admiring the town, Kevin abruptly pulls my shirt and to a side-road that is heavy with the fragrance of boiling broth."

    scene bg yamada with bg_change_seconds
    Kevin "All right, so this is Yamada’s! Best ramen joint in town. They serve sushi too. The good stuff. And they have these great lunch specials … you’ll fall in love with this place soon."

    "He hurries inside and gestures for me to follow him. With a sigh, I follow."
    "Despite the size of the building, the ramen shop is … surprisingly cozy.{w} A middle-aged man smacks a ladle on his wok as flames burst from underneath it, and a young girl carries steaming hot bowls to a couple in the corner."
    "Advertisements for Kamitsu beer decorate the wood walls and glass windows, and it is mildly noisy as a few college students take up most of the available seats."

    show kevin neutral sides open with config.say_attribute_transition

    Kevin "You find us a table. I’ll get us some grub. This one’s on me, I know what to get."

    hide kevin with config.say_attribute_transition

    "He darts away before I can say that I’d be willing to pay for my own food. Sighing again, I find a small table by a window and sit down."
    play music "contemplative_loop.mp3"
    "I watch people pass by, all of them young and probably going to this school. Instead of uniforms, everyone is dressed in a unique way. Inside this shop, I hear several soft-spoken conversations, people bandying opinions around, and sputtering out the occasional laugh."
    "I feel … odd, now that I can breathe. Alone. For the first time in a while …"

    show kevin neutral sides open with config.say_attribute_transition
    play music "happy_hangout_loop.mp3"
    "The feeling abates very quickly as Kevin pushes a tray with a bowl of ramen towards me, accidentally spilling a bit of broth as he moves it. He also bought four rice balls and a second, noticeably bigger bowl with peppers and shrimp."
    "My bowl has noodles and a few vegetables. Both smell fantastic."
    Kevin smile crossed "Here we go! This is my usual. The spicy-sea bowl. And I got you the regular. That should be everyone’s starter."
    Shun "It smells nice."
    Kevin "Wait till you taste it. I love Japanese food plenty, but the portions … a guy like me needs bigger sizes. Know what I mean? That’s another perk about Yamada’s. They aren’t stingy with their sizes."
    "I take my spoon and scoop up a mouthful of the broth. I sip it gently … as Kevin promised, it is wonderful. Very rich flavor, and not too hot."
    "Kevin devours a rice ball like he has been waiting to taste it all day."
    Shun "So, Kevin … what brought you to Inou? It’s a long way from New York."
    "Kevin pauses in the middle of wolfing down his meal."
    Kevin "Well for one, I wanted to learn Japanese. And studying abroad looks awesome when you set your sights high."
    Shun "And where are your sights set?"
    Kevin "Everywhere! Ever since I was a kid, I’ve wanted to travel and see … everything. I want to be a citizen of the whole world! But if I don’t have money, I can’t do that. So I asked myself “Kevin … what job will let you do that?” The answer … diplomat."
    Kevin "My dream is to work for the UN. Maybe as a general translator, or even  a private translator, or maybe something with politicians. I don’t care, as long as I get to travel a lot."
    Kevin "A degree from Inou  would look sooo good to people out there."
    Shun "I couldn’t imagine doing that. I’ve never been outside of Japan."
    Kevin neutral closed "You should! Go somewhere, anywhere, sometime while you still can. You won’t regret it."
    Kevin serious open "And what’s your story? You here for the degree too?"
    Shun "In a nutshell yes. It’s … a bit more complicated than that …"
    "I’d hoped that he wouldn’t pry. I don’t want to talk about … that."
    "Kevin chews his food and waits for me to speak up. Patiently. When I stay quiet, he shrugs and moves on, seemingly unconcerned by my unwillingness to talk about myself."
    Kevin smirk sides "… Okay. So, finish up and I’ll show you the rest of campus. And take the rice balls. I bought them for us."
    "He hands a rice ball wrapped in plastic to me, smirking knowingly. I roll my eyes and put it in my pocket for later."

    scene bg yamada_street with bg_change_seconds
    show kevin smirk sides open with config.say_attribute_transition
    "Kevin finishes eating at the same time I do.{w} A few people smile at us on the way out of the ramen shop."
    "One of them waves to Kevin as he passes by … a beautiful older woman, dressed like a teacher, looks furtively in Kevin’s direction just before we leave, and blushes when Kevin winks at her."

    Kevin "Did I mention I was popular?"
    "Of course he’s popular. He stands out like a sore thumb."
    Shun "How many friends do you have?"
    Kevin "What, in college or in total everywhere?"
    Shun "Er … I meant here but …"
    Kevin "It doesn’t matter, I lost count a long time ago."
    Kevin "It’s safer to assume everyone knows me. Which is good for you. I’ll hook you up with some new buddies soon and you’ll be Mr. Popular before you know it."
    Shun "I prefer a small group of close friends."
    Kevin "Everyone does in their own way, but you can’t make that small circle if you don’t have lots of people to pick from."
    Kevin "Anyway, the tour! This way!"

    return
