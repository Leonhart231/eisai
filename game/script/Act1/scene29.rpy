# Scene: Set to Study

label a1s29:
    stop music
    scene bg hall_school with bg_change_days
    play music "romantic.mp3"
    "The weekend rolls by uneventfully. A new week starts and classes come and go as usual. Throughout the day, I hear a few rumors about me and Maryanne drifting between groups of people. They’re not terrible ones, but I try my best to ignore them."
    "Since I handed the paper in, Okada hasn’t tried to instigate me. When I raise my hand, he calls on me. When I hand in my work, he says “thank you”. That’s pretty much it. He doesn’t pester me after class or assign me any additional work."
    "Perhaps I earned his respect, or perhaps he didn’t care anymore. But either way, I am satisfied with the B+ average. For now."
    "I was warned in high school that college would be a lot harder. And for the most part, that was a lie. Okada was the hardest class I had, but the others are surprisingly easy."
    "What {em}is{/em} difficult is managing the many non-schoolwork related tasks I am given. Of the five classes I am taking, all of them expect me to put their assignments before all others."
    "Add that to laundry, buying food, finding time to sleep and balancing friendships … college was proving to be more of challenge of scheduling than actual work."
    "When I actually need to get work done, Kevin and Hideki are annoyances at the very least, and distractions at worst. I think that studying in my dorm is not going to be a great idea in the long term."
    "As I walk between classes, I try to think if the library is a better option for studying …"

    show maryanne happy with config.say_attribute_transition
    Maryanne "Hi Shun."
    hide maryanne with config.say_attribute_transition

    "Maryanne squeaks a greeting as she passes me. I wave and smile. She smiles back, than quickly studies to her laboratory."

    Shun "… Wait a minute, Maryanne! Come back!"

    show maryanne confused with config.say_attribute_transition
    Maryanne "Yeah? What is it?"

    Shun "Are those study sessions still an option? You know, regular ones? Like maybe once a week?"

    Maryanne ecstatic "S—sure! I would like the extra company. Um, I study in my lab every afternoon for a—at least a little bit."

    Shun "That’s great! I’ll see you then some time."

    Maryanne happy "Oh! You never told me how things went with Okada. Did you get an A?"

    Shun "Not … exactly. Okada didn’t even grade it. He only said he knows where I stand now."

    Maryanne ecstatic "… He likes you."

    Shun "Huh? Why would you think that?"

    Maryanne happy "Because he challenged you and you didn’t back down. You showed him you can handle yourself as a student."

    Shun "But I didn’t feel like I accomplished anything. It felt like he gave me the paper for the hell of it."

    Maryanne "Yeah. And you did a great job still. That means something. If you try your best and no one ever notices, it means you have more character than people who do things for attention."

    Maryanne "It’s a personal philosophy of mine. Most of what we do in school will never been seen by anyone. But I try to do my best. Trying is important, because one day I will have to look back on what I did. I don’t want to be disappointed."

    "Her words strike a cord. My old self, the parts I didn’t like, suddenly feel further away."

    Maryanne uncomfortable "A—about studying … the next couple of weeks are a bit busy but … we can start with a regular schedule after we all get from the trip."

    Shun "The trip? Oh, to Shodo Island? Yeah, okay. I’m going there too, so maybe we’ll catch up there."

    Maryanne happy "Great, maybe! And when we get back—"

    "The bell rings before she can finish. She hurries off, not wanting to be late."
    hide maryanne with config.say_attribute_transition
    "Weird, I think, how Maryanne still has work to do even though she has been working in it relentlessly since school started. Oh well. I guess that is graduate school for you."
    "I tell myself that as long as I get some time out of my dorm every now and then, I’ll be fine. And spending it with a friend who isn’t loud or rude … that would be nice."

    return
