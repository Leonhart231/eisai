##-----------------------------------
## Act 1: A Brighter Future
## Scene 9: Science Building Interior
##-----------------------------------

label a1s9:
    scene bg hall_school
    show kevin neutral sides open
    with bg_change_seconds
    stop music fadeout 1
    "The interior of the science building is refreshingly cool. A wave of conditioned air puffs across my back."
    "I can hear people moving inside a few classrooms. It is probably staff or maintenance getting the school ready for incoming students and a new year of classes."
    Kevin "This is the science building. Some of the best minds in Japan have studied here.{w} Doctors, biologists … Maryanne. Don’t let the hair fool you, she’s a total Egghead."
    Shun "She seems smart, if that is what you mean."

    Kevin serious crossed open "Yeah, but a blonde? I guess there are exceptions to every rule. Some people think Americans are obnoxious and loud and rude, but I’m fine!"
    "I can’t tell if he is joking or if he really is that oblivious. Instead of answering, I turn and look around the building some more."

    Kevin neutral sides open "But yeah, Maryanne . But don’t tell her I said that. We’re old friends and that’d be like saying my sister was hot or something."
    Shun "Uh … s—sure. I won’t say anything."

    Kevin "Right. So, if you have a science class it’ll be in here. There’s like … I think a dozen rooms in total—"
    Okada "MR. GALLAGHER!!!"
    "Someone yells at the top of their lungs in our direction, causing both of us to flinch."
    "An aging man with barely any silver hair left marches up to us. “Infuriated” doesn’t begin to describe how angry he looks. If volcanoes had faces, they would look like he did just before erupting."
    "A blood vessel on his forehead pulsates with stress. Next to Kevin he looks tiny but his stature doesn’t make him any less terrifying."

    show kevin at leftish
    show okada angry at rightish
    with config.say_attribute_transition
    play music "bickering_loop.mp3"
    Kevin "Oh, there you are."
    Okada "In my office NOW, young man!"
    Kevin smirk sides open "Oh, yes sir, General Old Person ssssir."
    "Kevin mocks him with a salute. I am shocked by his attitude. In high school, when a teacher raised his voice, it meant that you were in a lot of trouble. Kevin isn’t even phased by his threatening outburst."
    hide okada with config.say_attribute_transition
    "The teacher walks away, stamping his feet loudly. Kevin leans in to me before leaving."

    Kevin neutral sides open "You better wait out here little buddy. Okada-sensei is kind of a hard-ass, but I know how to handle him."
    $ okada_name = "Okada"
    Shun "Um. O-okay."

    hide kevin with config.say_attribute_transition
    "Kevin follows Okada down the hallway to (I assume) his office. Still he grins confidently, even as the old man begins to scream again."

    Okada "You were supposed to be in my office TEN minutes ago! TEN! I leave for ONE moment, to use the restroom, and …"
    "A few people stick their heads out the classrooms to see what’s causing the noise. Kevin has the gall to wave to some of them."
    "They both disappear into a door at the far end of the hall.{w} I am alone again."
    "My footsteps echo as I aimlessly walk down the hall. I’m not sure where I’m going. I don’t mean to go anywhere, just to explore."
    "I quietly walk past the door Kevin entered. It is open just a crack so I can clearly hear the old professor yelling at him."

    Okada "Young man, when are you going to shape up and stop being such an immature child?"
    Kevin "It’s on my to-do list Okada-sensei, I promise you. I just have to get through a lot of other stuff first. {w}For one, I have to get my test back from you. Was I right? Wasn’t the grade supposed to be a teeeeeny bit higher?"
    Okada "One day that arrogance will be your downfall! I don’t care how impressive your academic record is! {w}Talent means nothing without effort and you never put effort into anything you do!"
    Okada "You are a cocky, lazy boy who doesn’t study and spends all of his free time chasing girls!"
    Kevin "What else do you like about me?"
    Okada "… Here is your test. 100 percent, as you … predicted."
    "Okada-sensei voice is filled with a deep resentment as he announces Kevin’s flawless grade."
    Kevin "Thank you, my good professor. Thank you, thank you. I am glad we got that little clerical error cleared up. Grades are important, after all."
    Okada "Hmph. Now, that aside, on to our next matter …"
    "Dear God, Kevin has no respect for this man. The lecture continues, and a memory of the arguments from back home seeps into my mind."
    scene bg hall_school with bg_change_seconds
    "I duck down the nearby stairway to get away from the noise, down to the bottom floor. I don’t stop until I hear beautiful silence."
    return
