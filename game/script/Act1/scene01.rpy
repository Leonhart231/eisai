##--------------------------
## Act 1: A Brighter Future
## Scene 1: Arriving at Inou
##--------------------------

label a1s1:
    play sound "59622_morgantj_japaneseairportbusstopping" fadein 1.0 fadeout 2.0
    window show
    Bus "Excuse me sir, we’ve arrived at your destination."
    "The driver’s voice pulls me out from the nap I needed. Hours of on-again, off-again rest through the bumpy country roads, between bus stop after bus stop … I might as well have not slept at all."

    scene bg bus with bg_change_minutes
    Shun "Uh … Where am I?"
    Bus "We have arrived at Inou Tadataka University. This is the final stop."
    "He bows and gestures to the door with a gloved hand."
    Shun "Oh. Yes. Thank you."
    "Still in a haze, I stagger to the front. The driver is kind enough to help me unload my luggage."
    Bus "Are you a student here?"
    Shun "Hm? Oh, yeah. I’m just starting, actually."
    Bus "Well, best of luck, young man. You’re lucky to be attending such a prestigious school."
    "I thank the bus driver and stumble onto the sidewalk, dragging my giant bag with me, blinking away the lingering effects of my long nap."
    "I’m finally here. College."

    stop music
    scene bg inou_gate with bg_change_seconds
    play music "decompression_loop.mp3"
    "A white marble gate marks the entrance, a brick wall on both sides. I can hear birds singing as the sun rays hit my eyes."
    "The words “Inou Tadataka University” are etched in silver near the entrance. The school’s slogan is engraved on a bronze plaque near the front … “Mapping out a brighter future” … well that is fitting. I suppose it’s a good sign."
    "A campus map given to me when I was accepted shows me the entrance, a dotted with a bright red circle that assures me that I am in fact Here."
    "I grab my heavy suitcase, yawning as I boldly lug it through the welcoming gate."

    scene bg inou with bg_change_seconds
    "The photos in the brochures don’t do this place justice, I think as I look around. The greenery is carefully trimmed and styled, planted and spaced deliberately and professionally."
    "Everything feels … safe. The brilliant sun shines down on crowds of students walking along the winding stone walkways that criss-cross the grounds. The warm breeze smells like the  blooming trees."
    "I hear bittersweet laughter from families saying goodbye to sons and daughters, friendly chatter between students, and the cadence of athletes jogging through the quad."
    "I’m not the only one struggling with my luggage, but it seems like I’m the only one who doesn’t have family or friends to help."

    "The buildings are as diverse looking as the student body. Some are rounded and low, some stretch across campus, one is covered with shimmering walls of reflective glass. Whoever designed this place cared about their work."
    "Many buildings have a modern and clean feel, a distinct character to their designs that help to showcase their purpose. {w}Others carry the weight of their age, as if they are old soldiers watching over this place in silent vigil."
    "I step off the path, both to catch my breath and to allow a trio of female students to pass by: apparently, on their way to the women’s dormitories."
    "… Where the heck {em}is{/em} my dorm, anyway?"
    Shun "Um, excuse me!"
    "I wave to the nearest person passing by, a girl who energetically scans the crowd."

    show ai proud with config.say_attribute_transition
    Ai "Hey there! Did you need some help?"
    "The girl’s inflections are odd, though I’m having trouble pinpointing exactly why."
    Shun "Do you know where the boys’ dormitories are?"

    Ai talk "Ah! A freshman!"

    Ai smile "Welcome to Inou! {w}The boy’s dorm is that big building right there. You can’t miss it."
    "She points to a large, brick building not too far away.{w} Thank God I don’t have to drag my bags much farther."
    Shun "Thank you."

    Ai talk "No problem! Anything else I can help you with?"
    Shun "No, I think I’m okay now. Thanks."

    Ai proud "Great. Good luck to you this year!"

    hide ai with config.say_attribute_transition
    "She rushes towards a group of girls and starts chatting with them excitedly about something."
    "That was my first meeting with another student. It didn’t go as badly as it could have."

    return
