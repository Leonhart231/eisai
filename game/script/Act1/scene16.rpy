label a1s16:
    stop music
    scene bg class_okada with bg_change_minutes
    "I quietly open the door, trying to draw as little attention to myself as I can without getting caught."

    scene bg class_okada with hpunch

    "But then Risa throws the door open, stopping the conversation in the room and drawing attention to us."
    play music "bickering_loop.mp3"
    show ai yell at center
    Ai "You’re both late. Get in quickly!"

    "Ai, speaking at the front of the room, talks down to us."
    "The meeting has already started. The members of the Eisai club are divided into two groups, one for boys and one for girls. There are surprisingly fewer members than I thought there’d be, but among them I spot familiar faces."
    "Kevin waves to me and gestures towards an empty chair next to him. Like a pariah, the eyes of the fidgeting and impatient crowd bore into me as I trudge to my designated spot. My skin crawls from the unwanted attention."
    "I slink to the empty chair, head hanging low, like a child caught after curfew."
    "I shuffle past Risa, who doesn’t care at all."

    Ai "Good! Now we can continue!"
    hide ai with config.say_attribute_transition

    "Ai prattles on, in front of everyone. I would have been surprised to see her conducting the meeting if I wasn’t so embarrassed."
    "I also see the grumpy old professor sitting in a chair next to the podium, watching the crowd like a hawk as Ai goes over several topics."

    show kevin smirk sides open at center
    Kevin "Hell of a first impression."

    "Kevin whispers to me slyly as I sit down. Hideki’s glasses-covered eyes burn with disappointment."

    Shun "Why is Ai on the stage?"

    Kevin "She is the key organizer for Eisai. Kind of like the mascot or the club president."

    Shun "Good for her. So what did I miss?"

    Kevin "Not much. They are about to go over the vacation in a second. Pay attention little buddy. The trip is something you’ll want to hear about."
    hide kevin with config.say_attribute_transition

    "I try to listen, but my attention drifts away from a long-winded monologue and shifts to catching glimpses of the students I’ll be spending the year with."
    "Most of my peers look to be older than I, which doesn’t surprise me, but there are several students who look too young to even be in college. One girl in particular looks like she’d snuck in from middle school. Lord knows how many grades she skipped."
    "I easily spot Maryanne’s yellow hair, and her strange green haired friend by her side. Shione tries to whisper something to Maryanne, despite the taller girl’s determination to pay attention to the lecture."
    "I see a girl with a worn-out smock covered with paint, and with really pretty hair who looks too nervous to speak."
    "Risa sits by herself, looking like she doesn’t care. I can’t believe she is part of Eisai. I wonder how she got a scholarship to this school."

    show ai talk with config.say_attribute_transition
    Ai "Last year, our Eisai fundraiser earned enough money to get us the trip we wanted! The previous seniors did a fantastic job. Like the old saying goes, when old men plant trees whose shade they know they shall never sit in, right?!"
    Ai "So even though they won’t be coming, we get to visit Shodo Island!"
    Ai "Sign-ups have to be completed a week before the trip. We have plenty of room so don’t make us fill the roster with non-Eisai members again! Haha. And bring your smiles to the beach everyone!"

    "Shione is practically bouncing off her chair as Ai talks about this beach trip."

    Ai "The university is encouraging all of you to make the Eisai program better than it has ever been before. We are counting on the upperclassmen to make the new students feel welcome and teach them all they need."

    "Kevin shoves me gently and smiles."

    Ai "Don’t be afraid to ask for help! That is the point of coming to school, to get help and get better! And to make wonderful memories!"

    hide ai
    show okada neutral
    with config.say_attribute_transition

    "Ai finishes her speech, then trades places with Okada, the angry science teacher I have yet to formally meet. He lurches over to the podium, showing his age as he walks, and grips the edges like a vulture grips a bone."

    Okada "Remember! If you need help with science classes, I am free anytime in the afternoon during the week. And if you have questions throughout the year, please ask."

    "He glares at Kevin, who flashes a cocky smile back at him."
    hide okada with config.say_attribute_transition
    "His speech ends right there. A man of few words I suppose. Okada-sensei steps away from the podium to leave the room. No one so much as flinches until he is at the door and steps outside."
    "But the second he is out of sights, the room bursts out in conversation."

    show kevin neutral sides open at leftish
    show hideki crossed glare at rightish
    with config.say_attribute_transition

    Kevin "Phew! I thought he’d never leave."
    Hideki "You’re in for either a very long year or a very short one, Gallagher."

    Kevin laugh sides closed "Please! If Okada really wanted me expelled, he would have done it a long time ago."

    Hideki "You’re just lucky. Luck is wasted on those who have nothing to be proud of."
    Kevin "And I bet that just drives you crazy, doesn’t it Math-boy."
    show kevin neutral sides open with config.say_attribute_transition

    "Hideki stands up, just before Kevin can poke him."

    Hideki "I will see you back in the lair. Do {em}not{/em} be late. I will {em}not{/em} restrain my patience for any reason."

    Kevin "Yeah, yeah, whatever. Get going, you creepy little gremlin."
    hide hideki with config.say_attribute_transition

    "Hideki mechanically marches away."

    Shun "Where is he going?"

    Kevin laugh sides closed "You’ll find out later. It’s a surprise. For you."

    Shun "Is it something that will get me in trouble?"

    Kevin smirk sides open "Only if you get caught. So don’t get caught."

    "Kevin’s cryptic answers fail to put me at ease. He shoves a folded piece of paper to me before I can inquire further."

    Kevin laugh sides closed "Here. They passed out your schedule for the semester. You can change it around a bit if you talk to admissions fast, but they usually give new students good classes."
    show kevin neutral sides open with config.say_attribute_transition

    "I unfold the paper and Kevin moves his head to get a closer look. I pull the page closer to my face so he cannot pry."
    "I have Okada’s Intro to Biology for my first class Monday morning. The other classes seem like a standard setup for a college freshman. A math course, a history course, one Japanese Literature course."

    Shun "I have your favorite professor. First on the list, day one."

    Kevin "Ha! Sucks to be you. I still have my old notes from when I was in his class. I’ll give you a good discount for them."

    Shun "Dis … count?"

    Kevin "Note-selling is big money in college. So is test selling. If you’re good, you can make enough cash for half a semester."

    Shun "You’re serious?! Isn’t that illegal?!"

    Kevin "Only if you get caught. So don’t get caught."

    "He jokes again, I think."

    "Since I will be stuck with Okada for at least a semester, and since I knew how important first impressions were, I decide it would be best to introduce myself now and leave a positive memory while I have the chance."
    hide kevin with config.say_attribute_transition
    "I spring out of my chair before Kevin could ask me where I was going, and hurry outside the building."

    stop music
    scene bg inou sunset with bg_change_seconds
    play music "contemplative_loop.mp3"
    "Okada stands meters from the science building, fondling a cigarette between his decrepit boney fingers. He stares at the sky, deep in thought. His spine curves between his shoulders, a sign of age and years of hard work. He sighs softly as he exhales a cloud of smoke."

    Shun "Um, sir. Okada-sensei! Excuse me."

    show okada neutral
    Okada "Hmm? Yes?"

    Shun "Um, hello. My name is Shun Takamine. I am a first year and member of Eisai."

    "He turns slowly. He stares at me for a moment, his aging eyes squinting, then looks back at the sunset."

    Okada "These moments are beautiful …"
    Okada "Sorry. My mind was somewhere else. Good evening young man. I trust that you enjoyed orientation?"

    Shun "I wanted to say … I look forward to your class this semester sir."

    "He nods a few times, then waits for me to say more."

    Shun "I … I can’t promise perfection, but I can promise effort. And you’ll see plenty of that from me."

    Okada "Good for you! There’s not enough initiative in young people these days. Hard work, a resilience spirit, respect … these are dying traits. It is good to see someone has them."

    "He isn’t the monster Kevin warned he would be, nor does he seem unreasonable in his expectations. He is merely old fashioned and expects more than baseline average from people."

    Okada "Where are you from?"

    "Before I can answer, Kevin steps outside. The content look on Okada’s face vanishes. An ugly gargoyle now stands where the respectable elder was a moment ago."

    show kevin neutral sides open at rightish
    show okada at leftish
    with config.say_attribute_transition
    Kevin "Heeeey, where’d you g … oh."
    play music "bickering_loop.mp3"
    Okada "… Mr. Gallagher. I’m surprised you’re still here now that the lecture is finished. I always expect you to run off to play the moment you’d get your chance."

    Kevin "Stranger things have happened old man. After all, it’s been another summer and you’re still here. What are you up to now, a hundred and forty?"

    Okada "I could ask you the same question but in reverse. What are {em}you{/em} up to … seven, six years old? You have all the maturity of a child after all."

    Kevin "Yeah, yeah. You can belittle my perfect GPA later. Shun, come back inside."

    Okada "You two know each other?"

    Shun "No!" (multiple=2)
    Kevin "Yes!" (multiple=2)

    Shun "No we don’t!"

    Kevin "Sure we do! He’s my new roommate. I’m taking him under my wing and showing him how to act in this school."

    Okada "Is that so."

    Kevin "Yes sir, Okada sir! This little guy will be just like me before you know it!"

    "Kevin pulls me close to him and smiles. I try to push him off, but Okada’s eyes tell me the damage is done."

    Okada "Your name was Takamine, right? Thank you for introducing yourself. I will remember you."

    hide okada with config.say_attribute_transition

    "The old man steps on his cigarette which may as well be the good impression I wanted to leave. Then walks away."

    show kevin smirk crossed open
    Shun "Why did you do that?!"

    Kevin "Cause, you vanished and we have a surprise to get to."

    Shun "No, I mean why did you … forget it. It’s too late now. But geez, Kevin!"

    Kevin neutral "Whaaat? What’d I do? Come on, we gotta go do stuff. Hideki is waiting for us."

    scene bg hall_dorm_boys with bg_change_minutes

    "We go back in the lobby, dodging the students who are on their way out. Everyone seems to be in a hurry. I wonder if it is because they want to be prepared for classes tomorrow."
    "I see Ai in the corner of the room talking to Risa, who rolls her eyes as Ai lectures her. It is probably about her tardiness. Risa says something I can not hear, and Ai slams her foot on the ground angrily."

    scene bg dorm with bg_change_seconds

    "When we are in the safety of our dormitory, Kevin jabs my side and laughs loudly."

    show kevin laugh sides closed at leftish with config.say_attribute_transition

    Kevin "Way to show up late on your first day buddy."

    Shun "Okada was mad, didn’t you see? When I was late and when you told him we were roommates."

    Kevin serious crossed open "Yeah, but so what? He’s always mad at something. Just endure it. Or ignore it. I do."

    "Hideki walks out of his room, looking mildly frustrated as he counts bills through his fingers. Once he finishes counting, he pulls out a gray wallet that looks brand new and gently stuffs money into the thin folds."

    show hideki hips bored at rightish
    Hideki "I have funds for the evening. If you wish to depart, we must go now."

    Kevin neutral sides open "Good! Shun, get ready for a night out man. It’s gonna be fun."

    Shun "You’re going out on a Sunday?"

    Kevin "Of course not. {em}We{/em} are going out on a Sunday."

    Shun "No, no, no, this is not smart. We have class tomorrow."

    Kevin "Exactly. Which means we currently have no homework. Now is the perfect time to go out."

    Shun "Kevin! This is—"

    Kevin "Just an hour or two."
    Kevin laugh sides closed "Look man, work will always be there. Always. But college is only for four years. Every day here is an experience bro. An adventure! You just gotta go out there and find it! Make something happen! Work will be waiting for us when we get back."
    Kevin "I promise we will not be out late. Promise. Scout’s honor."

    Hideki "You are not a scout."

    Kevin neutral sides open "Shh!"

    Hideki crossed pissed "SHHHH yourself, Dog!!"

    Kevin "Come on little buddy, we’ll have fun. Just a little bit. And you’ll be back before bedtime."

    "His persistence is admirable. And though my gut tells me I am going to regret going along with him later, I am interested discovering what he has planned though."

    Shun "Alright, fine. But only an hour!"

    Kevin "One hour, yeah! Or two."

    Shun "No. One hour. Seriously."

    Kevin "Okay. You got it. Maybe one hour."

    "He opens the door and waits for us to exit. Hideki bumps into me as he hurries out."

    stop music
    scene bg inou night with bg_change_seconds
    play music "decompression_loop.mp3"
    "Our trio hurries out of the boy’s dormitory and through the main campus. We see Ai hurrying by and Kevin waves to her."
    "Hideki grumbles and fixes his glasses as he takes his wallet out again and counts the bills for a second time."
    "Ai sees Kevin and with a wiry grin comes over. Kevin clears his throat."
    show kevin neutral sides open at center
    show hideki hips bored at right
    show ai smile at left
    with bg_change_seconds
    Kevin "Oi!"
    Ai "Where are you all off to."
    Kevin smile crossed open "We’re going to sneak Shun into a bar."
    stop music fadeout config.fade_music
    "… Wait, hold on. What are we doing?"
    Ai "Kevin! What have I told you about doing things that might get your roommates in trouble?"
    play music "bickering_loop.mp3"
    Kevin serious crossed open "… Try to cut down a little?"
    Ai "Just don’t go overboard. We don’t want another Koji incident."
    Kevin neutral sides open "Alright mom. You want to come with us? There’s room for one more."
    "… He says as Hideki is looking through his wallet."

    Ai "Sorry I can’t play children, but I am needed elsewhere. The burdensome life of being popular comes with high demands, you know?"

    Kevin "Alright, no worries. Next time. I’m planning something else soon, we’ll keep you posted."

    Ai "Looking forward to it! Good night, everyone."
    hide ai with config.say_attribute_transition

    Hideki glasses adjust "{w=1.0}Pathetic …"

    Kevin "Shut up. Let’s get going."

    return
