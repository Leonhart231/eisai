# Scene: Second Day of Classes

label a1s21:
    stop music
    scene bg inou with bg_change_minutes
    play music "happy_hangout_loop.mp3"
    "The morning sun makes my eyes squint. It’s warm out. A nice breeze blows by me as I round the corner of the buildings. I join the flood of students who make their way into the science building."

    scene bg hall_school with bg_change_seconds
    "I am in the hallway … when the bell rings, prompting me to run as fast as I can to the door. I am exactly three seconds late as I step inside."

    scene bg class_okada
    show okada neutral
    with bg_change_seconds
    Okada "Mr. Takamine. You are late."

    "I freeze. Okada is right next to the door, as if he was waiting for me to enter. I feel the frightened gazes of all of the underclassman on me and flash back to elementary school."

    Okada "Well? Are you going to take a seat or just admire the view?"

    hide okada with config.say_attribute_transition
    "My face burns red as I walk to the empty chair, and take out my books … my books? Fantastic, I forgot my books. Just what I need, a bad second impression."
    "I hear the sound of a seat moving behind me as Okada wobbles to his own desk at the front. Risa is in the seat next to mine. She waves gently, still looking at the front of the room."
    "I wave back, and she leans towards me as I sit down."

    show risa neutral at left with config.say_attribute_transition
    Risa "No one can make you angry. He can push your angry buttons, but you’re still the one driving."

    Shun "Uhh … o—okay …"

    Okada "Ms. Hayase … is there something important going on over there? Something more important than my lecture that could mean the difference between you failing or passing this semester?"

    Risa happy "No, sir! My apologies, sir! I was just updating Takamine on what he missed in the first few seconds of class, sir!"

    Okada "… You can do that after class. For now, please pay attention."

    Risa "Yes, sir!"

    show okada angry with config.say_attribute_transition
    "She salutes him and sits down. The class is stunned. Okada looks like he is about to explode."
    "But he doesn’t. Instead, he just continues."

    show okada neutral with config.say_attribute_transition
    Shun "Are you …"

    "I lean in a bit, lowering my voice."

    Shun "Are you mad at him or something?"

    Risa neutral "Maybe. Why?"

    "For the rest of class, Okada directs all questions to Risa, who answers them with one sarcastic remark after another."

    Okada "Miss Hayase, what temperature does gold melt?"

    Risa happy "Hot, sir."

    show risa neutral with config.say_attribute_transition
    "After three or four mocking responses, Okada drops a bomb … on all of us."

    Okada "Everyone take out a pen. Pop-quiz time."

    "He starts to pass out a test paper. The room grow nervous, myself included, and we all get mad at Risa."
    "But Risa shows no signs of emotion. Her hand goes up."

    Okada "Yes?"

    Risa "The syllabus does not show a quiz, sir. Is the syllabus reliable at all?"

    show okada angry
    with config.say_attribute_transition
    "Okada’s face crumples like an ogre."

    Okada "Pop-quizzes are pop-quizzes for a reason. This is an initial assessment, to find out who knows what. If you don’t understand the basics, this will be nearly impossible. Did you have any other questions?"

    Risa "No, sir!"

    Okada "Then take out your pens, please. You have twenty minutes to complete this test. Do not ask your partner for help."

    "The classroom glares at Risa, blaming her for this test. She yawns as she gets her test paper."

    return
