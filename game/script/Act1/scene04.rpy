##-------------------------
## Act 1: A Brighter Future
## Scene 4:
##-------------------------

label a1s4:
    show kevin neutral sides open with config.say_attribute_transition
    play music "happy_hangout_loop.mp3"
    "My tall roommate returns with my bag over his shoulder. He plops it down near me effortlessly."
    Kevin "Oof. What are you packing for, World War 3? If that actually happens, I bet Germany gets involved. They tend to do that."
    Shun "D—did I pack too much? I brought my books just in case."
    Kevin smile crossed "Well, the fewer things you keep, the better. There’s not much space here to begin with so we gotta keep it simple."
    "He thinks this place is small? Maybe he’s just lived here for so many years that he’s gotten used to it."
    Shun "Sorry. I can send some stuff home later."
    Kevin "It’s cool, bro. We can use stuff as a footstool until then. Nothing says “comfy” like a leather suitcase with a t-shirt on it."
    "His simple-mindedness is a bit much, but he doesn’t seem like a bad guy. Maybe loud, but not bad. Regardless, this was not how I imagined our first encounter to go down."
    Kevin smirk "Have you been around campus yet, freshman?"
    Shun "No. I haven’t."
    Kevin "Not at all?"
    Shun "I mean, I walked through the front door and this place, but that’s about it."
    Kevin "Skipped the summer tours, did you?"
    Shun "I wasn’t able to get here in time for those. Because of … well … stuff."
    stop music fadeout config.fade_music
    "Stuff like having to go to court for my parents. {w}Or having to scramble to get my paperwork together to finish my application in time for the deadline. {w}Or having to go to court for the custody hearings."
    "Kevin says nothing for a moment, waiting for me to elaborate. When I don’t, he takes that as permission to keep talking."
    play music "happy_hangout_loop.mp3"
    Kevin laugh closed "Heh. It’s cool dude. We all got problems. I got 99. Know what I’m sayin’?"
    Shun "… Um … that … sounds like a lot?"
    Kevin serious open "… You don’t listen to the rap music, do ya?"
    Shun "Sorry, no."
    Kevin smile sides "Great. References are like half of my jokes. I’m funny back home."

    if not hideki_annoy:
        Hideki "According to whom?"
        "Someone yells at us from behind the closed door."
        Kevin "According to your mom! Shut up!"
        Hideki "You shut up!"
        Kevin "Make me!"

    Kevin "Anyway, do you need help unpacking? I’m in high demand but I’m free right now. You better grab me before someone else does."
    Shun "If you are willing to help, sure. That’d be nice."
    "Still holding my suitcase, he moves to the door in the middle of the other two, jiggles the knob and opens it for me."
    Kevin "This one is yours."

    play sound "104533_skyumori_door-open-02"
    scene bg room_shun
    show kevin neutral sides open at center
    with bg_change_seconds
    "He throws the door open, revealing a sunlit, empty bedroom."
    "The school has provided a bed, a desk, a small wardrobe, and two bookshelves, but nothing else."
    "The walls remind me on an empty canvas. My optimism returns, as I imagine painting a year I can enjoy."
    Kevin "Not bad, huh? {w}Word of advice, though: if you have a black-light, don’t use it here. I’m pretty sure the last guy to use this place used to jerk off a lot."
    "… And the optimism goes away."
    Shun "… Really?"
    Kevin "Yeah, that last guy … I think his name was Koji … he moved in last year, but requested a single-dorm after the first semester.{w} Imagine that! Who’d wanna live on their own when they can live with me?"
    "Kevin closes the door and opens up my luggage without asking permission. Before I can think of what to say, he’s already starting to unpack my things onto the bookshelf."
    Kevin "Oh! Almost forgot to ask … are you a scholarship kid?"
    "He takes out a couple of my books and flips through them quickly. The way he holds them makes it clear that he’s not good at reading Japanese … or possibly books in general."
    Shun "Uh, ye—yes. Please be careful with those."
    Kevin smile "Nice. Another Eisai kid. What are you in for? Are you one of those super-brains that skipped five grades? Lots of money maybe?"
    Shun "Good grades, I guess. And a good recommendation."
    Kevin "Right on.{w} You probably guessed this already, but I happen to be one of those super smart Eisai people myself!"
    Shun "Really?"
    Kevin "Yep!"
    Shun "… Really?"
    Kevin frown "… Yeah. Why does everyone always ask that twice?"
    Shun "So you’re gifted?"
    Kevin smirk "I have two gifts. One of them I can’t show you cause it would be a bit gay, but I can whip the other one out easy. Check this:"
    "Kevin clears his throat."

    Kevin laugh sides closed "{en}Hallo mein kleiner japanischer Freund. Sie sprechen kein Deutsch, oder?{/en}"
    Shun "What?"

    Kevin neutral sides open "Heh. {en}Qu’en est-il français? Les filles en Amérique aime cette voix.{/en}"

    show kevin laugh sides closed with config.say_attribute_transition
    "Kevin flashes me a confident, self-satisfied grin."
    Kevin "Ta-da! I’m a walking translator. I speak and read seven languages fluently, including Canadian. Still working on my Dutch and Japanese, though."
    Shun "… Are you Canadian?"
    Kevin "Ha! Canadian! No, no, I’m far too interesting to be Canadian."
    "He wipes a tear away from his eye. I’m not sure why he thinks that’s so funny."

    Kevin neutral sides open "I’ll give you another chance. Take another guess."

    $ guess_kevin = False
    menu:
        "If I had to guess …{fast}"
        "The first language he spoke was {b}German.{/b}":
            Shun "You spoke German pretty well. Are you German?"
            Kevin "Pffft. Nah, not German. But I do love their beer. And that schnitzel stuff."
            Kevin "I {em}was{/em} in Germany for a student exchange for about nine months when I was sixteen. {w}I went for the exchange program, but I stayed for the frauleins, know what I mean?"
            "He snickers again."
            Kevin "Blondes dude. I’m talking about blondes."
        "Canada used to be a {b}British{/b} colony.":
            Shun "Are you from England?"
            Kevin "I have some ancestry somewhere in the U.K., hence the last name. But I’m not officially from there. See my teeth? Look at this perfect smile! I’d never fit in over there."
            "Another inappropriate laugh from behind his grin."
        "He speaks {b}French{/b} pretty well.":
            Shun "You spoke French very well. Are you from France?"
            Kevin "Nope. I did stop by France once when I was in Europe, though. {w}I tell ya, I am so thankful you Japanese know how to take baths. That whole country smelled … odd."
            Kevin "The cheese tasted good though. But after I ate it, I smelled like the cheese. Go figure."
            "…"
            Kevin "Oh and dude? Three words: French Bath Houses. So worth it."
            Kevin "Well, that was six words in two sentences, but whatever."
        "I have no idea. {b}Belgian?{/b}":
            Shun "Are you Belgian?"
            Kevin "Waffles?"
            Shun "Huh?"
            Kevin "What?"
            Shun "What what?"
            Kevin "I was asking what."
            Shun "I was asking you what."
            Kevin "What?"
            "…"
            Kevin "No, I’m not Belgian."
        "He’s obnoxious enough to be an {b}American.{/b}":
            Shun "Are you American?"
            Kevin laugh closed "Hell yeah! Born and raised in New York. Land of the Free. Home of the Brave. Dicks to the entire world … sorry by the way."
            $ guess_kevin = True

    return
