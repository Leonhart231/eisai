label a1s18:
    stop music
    scene bg room_shun with bg_change_days
    play music "decompression_loop.mp3"
    "I wake up to the sound of something sizzling and the wafting scent of meat cooking."

    scene bg dorm with bg_change_seconds

    "Kevin is behind the little stove, flicking a frying pan subtly over a blue flame. He yawns like a lion then shakes his head to shoo away sleepiness. Loose pajama pants cover his lower half."

    show kevin neutral sides open with config.say_attribute_transition

    Kevin "Morning dude. Break-fast is almost ready."

    Shun "You can cook?"

    Kevin smirk sides open "Oh yeah. Chicks dig that in a guy. Hope you like eggs and sausage."
    Kevin "Do me a solid and get some orange juice. It’s in the fridge."

    hide kevin with config.say_attribute_transition
    "I open the fridge and see a collection of nicely stacked plastic trays, all arranged asymmetrically by size, color and shape. They all have labels that read “KEEP OFF”."

    Kevin "No, not there. The one in my room. His royal pain-in-the-ass likes his stuff exactly where he keeps it, so I got my own mini-fridge."

    "I cringe as I brace myself, ready to expect the worst."

    scene bg room_kevin with bg_change_seconds
    "My assumptions of how his room would look are as accurate as I expected. It’s messy with old clothes lying on the floor without care … even though he has only been in this room for, what, two days?"
    "Posters of beautiful women decorate his walls, though they are also unevenly placed and the cheap tape he used to hang them barely holds them in place. His bed is unmade, and looks like an animal had run through his sheets in a hurry."
    "There is a picture of New York City next to an open window, which lets light generously pour inside."
    "His little laptop is on, glowing with images of obscure films buffering at 13 kb per second. Gotta love that “free” campus internet."
    "Nothing in this room is organized, say for a stand that holds three colorful flags on his desk, none of which are of the United States."
    "I find that to be ironic, as it looks like Kevin has almost no regard to the neatness of his own place of living but respected the symbols of places that were not his home."
    "I quickly open the small fridge, pushing some empty soda cans to the side as I do, and pull out the orange juice he promised."

    scene bg dorm with bg_change_seconds
    "Kevin continues cooking, and before long a creaking noise comes from down the hallway."

    show hideki hips bored at leftish with config.say_attribute_transition
    Hideki "I am awake."

    "Hideki announces himself as if he owns the place. His head turns mechanically as he follows Kevin, who carries the cooked meal to the small table in the middle of the living room. Sausage, eggs, cooked onions and peppers, roasted in some kind of seasoned oil."

    Kevin neutral sides open "I made breakfast."

    Hideki crossed pissed "Why?"

    show kevin smirk sides open at rightish with config.say_attribute_transition
    Kevin "Because I’m a nice guy and I’m trying to get that to rub off on you. Sit down and eat. And say thank you for once!"

    "Hideki shrugs and takes a seat. He picks up his chopsticks and lifts the edge of an omelet off his plate."
    "For the few minutes, Hideki pokes and prods his breakfast, using his chopsticks like they were surgical blades examining a patient, muttering under his breath the whole time and making comments about the quality."

    Hideki chin think "… Egg through to the center. No runny bits. No browning. Even layers …"

    Kevin neutral sides open "You gonna eat it, or put your dick in—"

    "Hideki makes a shushing gesture with his right hand. Finally, he folds his chopsticks in his hands and claps them together."

    Hideki hips bored "It is acceptable. Gratitude."

    Kevin serious crossed open "Yeah, yeah, you’re welcome."

    "Kevin rolls his eyes as Hideki starts eating. Kevin take three sausages right before passing the plate to me."
    "I push it towards Hideki next, but instead of a thank you I get a brief hiss."

    show kevin neutral sides open
    show hideki crossed pissed
    with config.say_attribute_transition
    Hideki "Keep those phallic symbols away from me."

    Kevin "Oh Jesus Christ, here we go … if there is one thing I didn’t miss it was homophobic shit you’re always on about, or whatever you want to call it."

    play music "bickering_loop.mp3"
    Hideki armsup argue "{cps=*1.5}I call it a higher level of thought! I notice you have no problem putting a spicy phallus in your mouth. You wanton profligate.{/cps}"

    Kevin smile crossed open "First of all, just because I don’t have a word-of-the-day calendar doesn’t mean I’m what you just called me. And second, sex in general doesn’t freak me out. And third, pass the peppers back here."

    Hideki crossed pissed "{cps=*1.5}And fourth, unlike {em}you{/em} sex isn’t the only thing {em}I{/em} think about. Don’t judge me for being more enlightened than you.{/cps}"

    Kevin smile crossed closed "Listen to this Shun. Hideki is about to passive-aggressively admit that his junk doesn’t work."

    Hideki armsup argue "{cps=*1.5}Your obsession with sex … humanity’s obsession with sex is detrimental to progress. The prospect of reproduction has brought down empires and ruined great men!{/cps}"
    Hideki "{cps=*1.5}And you … you throw away your time pursuing an act of transitory pleasure which, despite all that modern society does to try and glamorize it, is objectively silly.{/cps}"
    Hideki "{cps=*1.5}Sex is not beautiful or magical. It is a compulsory act, a habit! Have you ever stopped and objectively thought of how disgusting it is?!{/cps}"

    show hideki crossed glare
    show kevin laugh sides closed
    with config.say_attribute_transition
    Kevin "I just close my eyes and think of England."

    "Kevin snickers and takes peppers off the plate Hideki casually passes to him. Then Hideki continues barking his dogma into Kevin’s ears as they engage in an otherwise normal meal."

    show kevin neutral sides open
    show hideki armsup argue
    with config.say_attribute_transition
    Hideki "Physical intimacy is a vestige of our evolutionary heritage from the days when we were senseless animals squatting in the bushes. The sooner mankind embraces in-vitro fertilization and develops the artificial womb, the better."
    Hideki "The sooner we throw off the shackles of “instinct” and embrace the unbiased calculations of intellect, the sooner we can focus on higher goals!"
    Hideki "Like enlightenment, star travel … hell, curing diseases is better than just making new humans. Any two drunks with the right plumbing can make another human. And yet our modern society constantly treats sexual congress as if it’s the end goal of all life …"

    scene bg dorm
    show hideki glasses adjust at leftish
    with bg_change_minutes
    Hideki "… And therefore, sex is bad!"

    Kevin "Cool story bro. Help me clean this up."
    play music "happy_hangout_loop.mp3"
    show hideki crossed pissed with config.say_attribute_transition
    "Kevin takes my empty plate and Hideki the forks, grumbling angrily. They pile them into the sink and Kevin cleans them as Hideki grabs a towel and rubs down our table."
    hide hideki with config.say_attribute_transition
    "Despite their polar opposite dispositions, they seemed to work well together. By the time everything is clean, Hideki had burned off whatever anger he had been bottling up that morning."

    show kevin laugh sides closed at rightish
    show hideki crossed glare at leftish
    with config.say_attribute_transition

    Kevin "So Shunnie … first day of classes today, huh? It feels like just yesterday you were going to Kindergarten. They grow up so fast!"

    Hideki "Ignore the peasant. Give me your class schedule."

    show kevin neutral sides open with config.say_attribute_transition
    Shun "Why?"

    Hideki crossed pissed "Positive feedback from an experienced elder is a value to all. Your schedule! Present it!"

    show hideki crossed glare with config.say_attribute_transition
    "I quickly fetch the print-out of my classes, and run them over to Hideki as he commanded. He snatches it and scans over the paper, glaring through his narrow glasses."
    "Eventually, he points to one part of the schedule, loudly slamming his finger on the crumpled page."

    Hideki "Okada … you have Okada. Here is your free advice: Okada will work you to the bone. He is a man who expects the best from his students. All of the time. He is a proud teacher. Intelligent, determined …"

    Kevin serious crossed open "He’s a massive prick."

    show kevin neutral sides open with config.say_attribute_transition
    Hideki crossed pissed "Who’s giving the advice here, you or me? I admit that he is somewhat difficult to work with. But that’s because he demands excellence from everyone. Himself included. He values effort more than anything. Do what it takes to show him you are trying."
    Hideki "And {em}never{/em} prove him wrong in front of the class. Ever! Rumor has it that that’s how Gallagher got on his bad side."

    Kevin serious crossed open "I was right and he was wrong and he threw a fit over it!"

    Hideki armsup argue "I’m not about to get into another discussion over your utter lack of tact. Moving on … now, you’ve got Himura for Honors Calculus. He’s new, but he knows what he is doing …"

    hide kevin
    hide hideki
    with config.say_attribute_transition
    "Hideki continues giving me advice on how to survive an apparent gauntlet of lectures. While Kevin covered the big picture, Hideki focused on the nuts and bolts."
    "He goes through one class at a time, meticulously picking them apart and giving me their teaching strategies like he has studied them."

    scene bg dorm
    show hideki hips bored at leftish
    show kevin neutral sides open at rightish
    with bg_change_minutes
    Hideki "So, there are your teachers in a nutshell. Study hard, do well."

    Kevin laugh sides closed "And ask Hideki or me if you need tutoring."

    show hideki crossed glare
    show kevin neutral sides open
    with config.say_attribute_transition
    Hideki "Why are you enlisting me as his tutor?"

    Kevin "Cause … you know more about math than anyone we know and you enjoy stroking your ego more than your own fun parts."

    Hideki crossed begrudging "… … Fine. Shun, if you need assistance this year, I can give you some."
    Hideki crossed pissed "But I don’t waste time on idiots. If you waste my time, I’ll kick you to the side of the road. Understand?"

    Shun "Got it …"

    show hideki hips bored with config.say_attribute_transition
    Kevin "Speaking of wasting time, we gotta go. It’s almost nine."

    Hideki "Hmm … yes, a new year. I wish you two luck. I do not require luck so both of you can have any I have acclimated thus far."

    hide hideki with config.say_attribute_transition
    "Hideki heads to his room with his nose in the air, slamming his door loudly as he disappears."

    Kevin "You’ll be fine Shun. Just be yourself."

    return
