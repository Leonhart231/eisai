# Scene: The Paper - Maryanne

label a1s26:

    "Maryanne! She’s a science major. She offered to help me before. This sounds like a good time to take her up on that offer."
    "I hurry down the hallway, hoping Maryanne would be in her laboratory."
    "I go to the advanced biology lab and poke my head inside."
    "I see Maryanne talking to the professor some questions. I overhear them and see the teacher look worn down as he struggles to keep up with Maryanne’s inquiries."
    "I wait until she is finished before stepping inside."

    scene bg lab_maryanne
    show maryanne coat confused
    with bg_change_seconds
    Maryanne "Shun? What are you—"

    Shun "I’m sorry but … I need your help. It’s important."

    Maryanne embarrassed "If you need help, you need help. What’s wrong?"

    Shun "Okada gave me a twenty page essay on … uh … fatal diseases and he wants it in two days."

    Maryanne worried "… Really?"

    "She pauses, then sighs. She sounds tired, maybe a bit annoyed."

    Maryanne "Of course Shun. I’d be happy to help you."

    Shun "Thank you! Thank you so much Maryanne. I can’t—"

    Maryanne "Don’t thank me yet. I’m not going to write the paper for you. I am just going to help you when … when you need it."

    Shun "Yeah, okay. But still, thank you."

    Maryanne mad "Yeah. Sure."

    "She sounds not as enthused as when she first promised to help me study. She leaves the room and I follow, guilt bubbling up inside me like a bad meal. Did she say she’d help me just to be nice?"

    "I thank Maryanne a twelfth time as she guides me to her bench. The other teacher leaves, leaving the room empty for just the two of us."

    Maryanne uncertain "I have some work to do on my own. You get started on the paper. Do anything you can. Just tap me on the shoulder when you need help. And when I am done, I’ll give you my full attention."

    Shun "Yes, okay. Thank you!"

    Maryanne happy "You’ll be okay Shun. It’s just another assignment. You’ll get a lot of those."

    Shun "Uh-huh. It’s way more than that. Okada has it out for me!"

    Maryanne confused "I’m sure he doesn’t hate you anymore than all his other students."

    Shun "No, I mean it. I think he really hates me. He didn’t give this assignment to anyone else today."

    Maryanne "… Come again?"

    Shun "Okada didn’t give this assignment out to anyone but me. He said it was technically optional, but in a threatening—"

    show bg lab_maryanne
    show maryanne mad
    with vpunch
    "Maryanne abruptly smack her open palm on the table."

    Maryanne "Again? He’s still doing that?! That is absurd! That’s … ugh!"

    Shun "What do you mean again?"

    Maryanne "Never you mind. And when is it due? Two days? Unbelievable!"

    "Maryanne grabs my wrist and forces me out of my chair."

    Shun "What are you doing?!"

    scene bg hall_school with bg_change_seconds
    "With a surprising amount of strength, she pulls me out of her lab, leaving our book-bags and books unattended."

    play music "bickering_loop.mp3"
    Maryanne "I know what he is doing and I am going to give that … that … unfair not-nice man a piece of my mind!"

    Shun "What?! No, stop it. Do all of that without me!"

    hide maryanne with config.say_attribute_transition
    "Much to my protest Maryanne drags me down the hallway, towards Okada’s classroom."
    scene bg class_okada with bg_change_seconds
    "She pushes open the door and marches in without fear. The class is empty except for Okada, who diligently works on grading homework assignments. He flinches as Maryanne barges in, dumbfounded by her audacity."

    show okada neutral at leftish
    show maryanne mad coat at rightish
    with config.say_attribute_transition
    "She finally lets go of my wrist when we are mere feet from his desk. Maryanne announces herself by slamming her foot on the floor. Okada jumps again."

    Maryanne "Okada! Why are you picking on another freshman!? It isn’t fair to pile on work to someone who is still getting used to this school! "

    Okada "Wha—what? Ms. Sawyer—"

    Maryanne "And it’s not fair to give a new student such a heavy workload. What, do you want to scare another person? Do you want students to dislike you, is that it?"

    Okada "Ms. Sawyer, if you dare—"

    Maryanne "His friend gets perfect grades so you can’t flunk him. That doesn’t give you the right to target Shun because you have to teach someone you don’t like!"

    Okada angry "Young lady! Do not take that tone with me!"

    stop music fadeout 0
    Maryanne "When are you two going to bury the hatchet? You act like Kevin is such a problem, but you are just as stubborn and immature as he is!"

    "They lock eyes, neither of them flinching. And I slooowly back away from them."

    Okada "I will give assignments … to whomever I wish. Whenever I wish."

    Maryanne "{en}#$\%^&*{/en}"

    show okada neutral with config.say_attribute_transition
    play music "bickering_loop.mp3"
    "A mess of heated English words blurt of Maryanne’s mouth. Okada grins as she loses her cool."
    "With red cheeks she grabs my wrist and pulls me again, this time {em}leaving{/em} the classroom."

    scene bg hall_school with bg_change_seconds
    "She doesn’t stop marching, or pulling me, until we are back in her laboratory."

    scene bg lab_maryanne
    show maryanne mad coat
    with bg_change_seconds
    Maryanne "That settles it! You’re just going to have to get a perfect grade."

    Maryanne "We’re going to get this paper done tonight! And I’m going to stay with you all the way through it!"

    Shun "Really?!"

    Maryanne pissed "Yes! And it’ll be so good that Okada-sensei will be speechless. I’ll get sources and revise each page {em}too{/em}."

    Shun "You’re getting very worked up over a paper. …{w} I like it, keep going!"

    Maryanne mad "This isn’t about the paper! This is about getting this stupid cycle of his to end!"

    Shun "You’re going to have to explain that to me."

    Maryanne "Later. Okay! What do you want to write about?"

    Shun "I haven’t decided. I guess since it’s on an illness … maybe cancer?"

    Maryanne "Too easy! Let’s get something more challenging."

    "She grabs a textbook out of her backpack and thumbs through like a crazed wizard searching through a spell book, her finger eventually stopping on a page."

    Maryanne happy "Creutzfeldt–Jakob disease! Let’s do that. It’s complicated so it will make you look confident."

    "Without even asking if I wanted to do that illness, she pushes the book into my chest and turns on her laptop. She moves like a one-woman assembly line."

    Maryanne ecstatic "Start reading! I’ll find some sources."

    Shun "Uh, okay. But I just had a thought. Okada is going to know you helped me now."

    Maryanne "Good! Let him know. You said it yourself, the paper is optional. What is he going to do, turn it away because I helped you? No way."

    Maryanne happy "This will be a hundred percent paper! A hundred and one percent! Maybe more!"

    show maryanne blush with config.say_attribute_transition
    "Maryanne’s cheeks flush as her stomach growls."

    stop music fadeout 0
    Maryanne embarrassed "… Uh … hehe. Do you mind if we get something to eat first?"

    scene bg yamada with bg_change_minutes
    play sound "234263_fewes_footsteps-wood"
    pause 2
    stop music
    scene bg lab_maryanne with bg_change_minutes
    play music "happy_hangout_loop.mp3"
    "I come back twenty minutes later with a few sandwiches and drinks. Maryanne stayed behind so she could research articles on the disease in question, trusting me to get something she’d like."

    show maryanne happy coat with config.say_attribute_transition
    Shun "I’m back. I got you a salmon sandwich lunch special. It was all they had."

    Maryanne ecstatic "Great! Thank you. We can start working as soon as …"
    Maryanne happy "{i}CHOMP!{/i}"
    Maryanne "… {i}gulp{/i} … we’re done."

    "After devouring one sandwich like a shark, she grabs the other sandwich in the bag and consumes that just as fast."

    Maryanne "… Oops. Sorry, was this one yours?"

    Shun "No, I ate mine on the way over here."

    Maryanne "Oh, thank goodness. I’d feel awful if I ate your dinner by accident."

    "I laugh … and feel my stomach ache. Thankfully I bought a bag of chips to eat for a snack. That was dinner for me I guess."
    "Maryanne places her pink laptop on her knees and runs her finger over the glowing screen. A few stickers decorated the case, bearing words and names I can’t read."

    Maryanne "I found lots of information on this sickness. There was a big outbreak in England years ago when I was too little to remember it. It spread via meat, so—"

    Shun "What are those stickers?"

    Maryanne confused "Excuse me?"

    Shun "There are stickers on your laptop. I was wondering … what are they?"

    Maryanne happy "Wow. I forgot these were here. It’s funny … I see them every day, but I … they are travel stickers. I collect them."

    Shun "What do they say?"

    show maryanne ecstatic with config.say_attribute_transition
    "A look of relief comes across her face."

    Maryanne "This one says “Viva Las Vegas”, which means “Long Live Las Vegas.” That’s the city in America with all of the gambling and shows. My mom was from there."
    Maryanne "And this one is a picture of the Empire State Building. Kevin gave me that. I’ve never been to New York, but I hear it’s wonderful. I’d go if I had the money."
    Maryanne ecstatic "And … this one is from Disneyland! Ooooh, Disneyland! I remember that! I went there ten years ago. I can’t believe it’s been that long. I had such a good time!"

    show maryanne happy with config.say_attribute_transition
    "She pointed to them one at a time, her face lighting up with nostalgia as she touched each."
    "I point to one that is shaped like a Chinese building with a red roof."

    Shun "Did you get this from Beijing?"

    Maryanne "That’s Mann’s Chinese Theater in Hollywood. My grandparents took me there when I was eight. We walked up the Hollywood square and I got to put my hands in Robin Williams’ hand prints."

    Maryanne "Sorry. You … probably don’t know who he is, do you?"

    Shun "Um … I don’t. Sorry."

    Maryanne "It’s okay. He was a celebrity. A great one! I remember his hands were really big."

    play music "romantic.mp3"
    "She sighs sadly as she recalls a bittersweet memory. I can tell she is thinking of better days."

    Maryanne uncertain "I haven’t been back home since I first moved here. Too much work I guess. School takes priority, right?"

    Shun "I guess so, but Kevin would disagree."

    Maryanne uncomfortable "Yeah. He would, I suppose."

    "She looks homesick, a feeling I’m not familiar with. But if it was anything like loneliness, I could sympathize with that."

    show maryanne neutral with config.say_attribute_transition
    Shun "This is the first time I’m away from home. Ever."
    Shun "I’m not really … used to all of … this."

    "I point to the stickers on her laptop screen. She looks as me contemplatively, then smiles, then turns back to her laptop."

    Maryanne happy "We should probably get to work."

    "Her voice sounds softer now. I imagine that her thoughts are overseas, back with whatever family she left behind."

    stop music
    scene bg lab_maryanne
    show maryanne coat happy
    with bg_change_minutes
    play music "happy_hangout_loop.mp3"
    "An hour later, we have an outline! It was basic and to the point, and desperately needed to be filled with ripe facts, but it was a start."
    "I could tell I was going to need to stretch a few paragraphs here and there, add some excess details as buffer text, but that would be fine. As long as I could hand it in on time and get rid of this, I didn’t mind if it had a bit of fluff."
    "Still, Maryanne insisted I was going to get a perfect score."

    Maryanne neutral "Okay, now to get to writing. That’s going to take a while, so get right into it. Don’t stop unless you need to rest your eyes."

    Shun "This is going to take all night."

    Maryanne happy "I’m not going anywhere."

    "She smiles as the printer in the corner chugs a few sheets of facts and statistics about our mysterious disease."

    Maryanne "You type, I’ll research."

    Shun "Yes captain!"

    hide maryanne with config.say_attribute_transition
    "Using the public computer in the lab’s corner, I type out the introduction as fast as I could, spitting out any and every idea I can think of while Maryanne reads facts out loud. She thankfully says them in rudimentary terms that makes the dictation much easier."
    "Maryanne was a better teacher than the ones I was paying for! Go figure."
    "She had a habit of pacing around the room as she spoke, words pouring out of her mouth constantly. Sometimes without looking at the pages, she would just print them!"
    "Sometimes she’d go on long tangents of other diseases and scientific breakthroughs, claiming they were things she read recently or had been studying."

    scene bg lab_maryanne night with bg_change_minutes
    "Hours pass. Pages fly onto the computer. Maryanne reread my work the second a page was completed."
    "I’d print out a page one at a time and she’d correct it, highlight areas where I needed improvement and write down where I could add more facts. Then she went back to lecturing me about the disease as I added her revisions."
    "It was like a nerdy bucket-line."
    "We worked so well together that it was hard to keep track of the time. She would occasionally ask me if I was okay, adding in words of motivation when she thought I needed it."

    show maryanne happy coat hairdown with config.say_attribute_transition
    Maryanne "It’s been four hours. How are you feeling?"

    Shun "My head is spinning."

    Maryanne ecstatic "Don’t give up Shun! Look how far you’ve come."

    scene bg lab_maryanne night
    show maryanne neutral coat hairdown
    with bg_change_minutes
    Shun "Can we take a break?"

    show maryanne worried with config.say_attribute_transition
    "I asked as I complete the twelfth page of the final draft. Maryanne pouts at this request."

    Maryanne "Okay, but only fifteen minutes. We’ve made a lot of progress. It would be bad to slack off now when we’ve got such a great rhythm going."

    "I stretch my arms over my head and yawn. Maryanne keeps on typing. It was inspiring to see such a hard worker, but it also seemed a tad obsessive."

    show maryanne happy with config.say_attribute_transition
    Shun "Are you here every night?"

    Maryanne "Mostly."

    "She doesn’t look at me when she answers."
    stop music fadeout config.fade_music
    "I spot a metal bar tucked away under one of the tables, with a green wool sheet over it. I kicked it and it rings with a hollow noise that echoes through the room."

    Maryanne confused "What was that?!"

    Shun "I found something over here. It looks like a cot."

    Maryanne uncomfortable "Oh! Leave that alone. It’s mine."

    Shun "What do you mean “yours”?"

    Maryanne uncomfortable "I mean, it’s not mine-mine. But the janitor left it here because I asked him to."
    Maryanne worried "It’s … in case I stay too late and need a place to sleep."

    play music "contemplative_loop.mp3"
    Shun "You sleep here sometimes?"

    Maryanne "Sometimes. But, not often. Just … sometimes."

    "Her head sinks between her shoulders and she hides her face behind her computer screen. Her fingers rapped on the keyboard much faster."

    Maryanne mad "I don’t work too much, okay?"

    Shun "I wasn’t thinking that."

    "I was."

    Maryanne embarrassed "C—can we just go back to the assignment. It’s almost done."

    "I look at the clock on the wall. 9:38pm."

    Shun "Sure. Let’s finish this thing."

    stop music
    scene bg lab_maryanne with bg_change_days
    play music "happy_hangout_loop.mp3"
    "A bright ray of light crawls over my closed eyelids. I pulled my wool green blanket over my head and try to go back to sleep."
    "And as the gritty wool rubs on my cheek, that’s when I remember I did not own a wool blanket."
    "I sit up and see that I was still in the lab. The surprisingly comfortable cot is under me."
    "I look for Maryanne but she is nowhere to be seen. Everything in the lab has been cleaned up and put away, as if we had never been there last night. The only evidence that said otherwise was the cot … and a stack of papers on the nearby desk."
    "Stapled together was my twenty page report, with my name on the front. A sticky note is tacked on the front of the essay."

    Note "We did it! —M"

    "I check the time. I was awake much earlier than my usual. Plenty of time to shower and get dressed."
    "I put the cot away as neatly as I can and made a mental note to thank Maryanne later. I couldn’t wait to see Okada’s face."

    scene bg class_okada
    show okada neutral
    with bg_change_minutes
    "Okada skims over my paper carefully. All the pages are there, the references, the sources, all edited and revised as perfect as it could be. He couldn’t have asked for more."

    Okada "… This is … very impressive."

    "The bags under my eyes tremble as I nod."
    "He flips through a few pages. Multiple surprised looks come over him as he judges “my” work."
    "After a few minutes of skimming, he hands it back to me without grading it."

    Okada "Good job."

    Shun "… That’s it? Good job? That’s all I get?"

    Okada "What else can I give you? You completed the assignment on time."

    Okada "I know where you stand now Mr. Takamine. Good day."

    "I feel the urge to scream at him, but I decide to leave before I let my temper get the better of me."

    scene bg dorm with bg_change_minutes
    "Kevin and Hideki are playing Skullgirls in the living room when I return. The hours from my all-nighter are catching up to me fast, especially after going to the rest of my classes today. I yawn as they finish a round."

    show kevin neutral sides open at rightish
    show hideki crossed glare at leftish
    with config.say_attribute_transition
    Kevin "Hey dude. Wanna get the next match?"

    Shun "No thanks. I’ve had a long day."

    Hideki crossed pissed "No doubt gallivanting and misusing your free time."

    Kevin smirk sides open "Shut up Pencil-Neck. Shun was hanging out with Maryanne and didn’t come home last night. Of course he’s “tired”. Wink-wink."

    Shun "How did you know I was …"

    Kevin serious crossed open "Everyone knows."

    Hideki hips bored "Maryanne wasn’t in her lab this morning, so Shione went to go find her and Maryanne told her you were with her last night. Then Shione told Kevin who told me."

    Kevin "Word travels."

    Shun "Before you go assuming things, we were getting work done."

    Kevin laugh sides closed "Yeah, I bet you were getting work done. Nothing to be ashamed of-YES! WOOOOO! Combo Four-Eyes! Eat it! Eat it Poindexter!"

    show hideki armsup argue with config.say_attribute_transition
    "Kevin jumps to his feet and cheers himself as Hideki loses the round."

    Hideki "Rematch! I demand a rematch! I am at a handicap. I do not have the finger coordination of a chimpanzee and you do, you boisterous animal."

    show kevin neutral sides open
    show hideki crossed glare
    with config.say_attribute_transition
    Kevin "Fine. Replay. I’m still Valentine."

    Shun "Maryanne and I were just working on my paper for Okada."

    Kevin laugh sides closed "Ha! He gave you an insane optional assignment that no one else got, didn’t he? He did the same thing to me when I was a first year. I blew it off, never handed it in. Let me know what happens in class now. We’ll compare behavioral notes."

    Hideki "He is not an animal to be studied, Ape."

    hide hideki
    hide kevin
    with config.say_attribute_transition
    "Ignoring their oncoming squabble, I quietly walk by them, needing rest."
    "They both boo me as I pass the screen and interrupt their gameplay."

    return
