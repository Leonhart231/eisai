##-----------------------------------
## Act 1: A Brighter Future
## Scene 14: The day comes to a close
##-----------------------------------

label a1s14:
    scene bg dorm
    show kevin smirk sides open
    with bg_change_minutes
    "We head back. Hideki’s door is open again."
    "I can see him pacing back and forth in his dark lair, muttering to himself angrily as he glares at the whiteboards on his walls."
    "Kevin stays still and watches for a few seconds, gesturing me to not say a word."
    play music "bickering_loop.mp3"
    Hideki "… All right, back to basics. Let’s assume that 2+2 is actually 4 …"
    Kevin "{em}ahem{/em}{w} Hey, Hideki!"
    Hideki "DAMN IT, GALLAGHER! SHUT THE HELL UP!"
    Kevin "Heh heh … want me to close your door for you?"
    Hideki "My door is not open, you stupid …"
    play sound "53269_the-bizniss_door-close-2"
    "Hideki stops mid-sentence. He hurries over to his bedroom door and closes it."
    play music "decompression_loop.mp3"
    Kevin neutral sides open "You’ll learn to love that guy. Or at least tolerate him."
    "Kevin sits on the couch and stretches his legs. He snatches a magazine off the coffee table and thumbs through the pages."
    Kevin "What are you doing tomorrow?"
    Shun "I haven’t thought that far ahead."
    Kevin smirk sides open "I have a habit of sleeping in Sundays. If you want to look around on your own, go for it man. Just get back here for Eisai orientation. I think it’s at 6."
    Shun "We have orientation? I thought you said Eisai didn’t have meetings?"
    Kevin "We don’t. But the school has this private lecture to motivate us for the coming year or something."
    Kevin neutral sides open "They have free food, they have speeches for upcoming events … it’s totally stupid. Just cause we got in via recruiters and letters, we get special treatment."
    "I remember that I got in because of Higa’s letter and feel a twist of guilt in my stomach."
    Kevin smile "You’ll get your class schedule, you’ll meet the other members … some of the professors come down to say “Hi”. But it’s mandatory, so be there even if you’re late."
    Shun "I’ll be there on time. Don’t worry."
    "Kevin looks up from his magazine and smiles."
    "I stifle a yawn and do a double-take when I glance at the clock. I hadn’t realized how late it was. Now I’m really feeling the day catch up with me."
    Shun "I think I’m going to go to bed."
    Kevin neutral "Sure thing … oh yeah, though. I almost forgot."
    "He reaches into his pocket and pulls out a copper-colored key attached to a thin tin ring."
    Kevin "Here you go. That’s yours, Roomie."
    Shun "Oh. Thank you. And for everything else today."
    "Kevin says nothing as I take the key from him. He goes back to his magazine, smirking wordlessly to himself."
    Shun "Are you going to sleep?"
    Kevin "Not just yet. I have a couple of things to take care of first."
    Shun "Um … okay. Good night."
    Kevin "Night Shun."

    scene bg room_shun night with bg_change_seconds
    stop music fadeout config.fade_music
    "I step into my shadowy bedroom. The only light comes from the dim light poles illuminating the quad outside my window."
    "For the first time since I arrived at Inou University, I feel truly alone.{w} … Thank God."
    "I place my key on the bookshelf and slip into bed, welcoming a good night’s rest … but falling asleep is more of a challenge than I’d expected."
    "The mattress is hard, and the pillow is thin compared to my one at home. A twinge of homesickness bubbles up in my stomach for a second before I suppress it."
    "Today’s events replay as I look up at the ceiling. So much happened … so many things to remember. I find myself worrying that I’ll forget someone’s name, or that I’ll end up lost on my way to class. "
    "Or worse, that I’m just not cut out for this. That I’m deluding myself. That I’ll fail."
    "But Higa’s words echo in the back of my mind. Everything he said to me before I left and all he did for me … knowing, somehow, that he believes in me stills the panic that wells up within me."
    "Higa never gave up on me. He thought I could be something. He was the only one who thought that."
    "The living room light shuts off and I hear Kevin close the door to his own room."
    "I’m in a strange place, with odd people. But I’m not alone."
    "{snd}PING{/snd}"
    "A soft electronic chime interrupts my thoughts.{w} It’s my phone."
    "{tt}New Text Message.{/tt}"
    Mom "{tt}Shun: Hope you arrived safely. Please call me when you have a chance. Love, Mom.{/tt}"
    "…"
    "My thumb hovers over the “reply” button."
    "… What do I really have to say to her? I can’t really think of anything."
    "Besides, it’s late and I’m about to fall asleep … technically. I’ll text her in the morning when I wake up.{w} … If I have some time."
    "I put my phone back onto the nightstand and close my eyes, slipping into a dream."

    window hide
    scene bg black with bg_change_minutes
    nvl show bg_change_seconds
    nvl_narrator "The car ride from our home to the bus stop had been excruciating. Mom stared at the road in silence, occasionally asking a question out of nowhere."
    nvl_narrator "“Did you remember to pack your cell phone charger, Shun?”"
    nvl_narrator "“I did.”"
    nvl_narrator "“You will remember to call, right?”"
    nvl_narrator "“If I have time. I’ll be busy.”"
    nvl_narrator "“I know you will. But please call me once in a while.”"
    nvl_narrator "“…”"
    nvl_narrator "“College is an important time, Shun. It will be tough, but don’t give up. I’m sure you’ll enjoy it.”"

    nvl clear
    nvl_narrator "“…”"
    play music "unrest_loop.mp3"
    nvl_narrator "What do you say to that?{w} What did she want me to say?"
    nvl_narrator "Bitterness doesn’t go away just because you pretend it’s not there."
    nvl_narrator "“Okay, Mom.”"
    nvl_narrator "That was about all I was able to say to her.{w} My mom seemed to understand. At least she didn’t press the issue."

    nvl hide bg_change_seconds
    nvl clear
    scene cg act0_bus with bg_change_seconds
    nvl show bg_change_seconds
    nvl_narrator "“I’ve checked the schedule. You have about fifteen minutes before the bus arrives.”"
    nvl_narrator "That was the first thing my dad said to me in weeks. No “hello.” No “I missed you.” Just a cold statement of fact as he awkwardly stands in the rain."

    nvl clear
    nvl_narrator "“… Do you have a snack for the bus?”"
    nvl_narrator "“Mom gave me some snacks.”"
    nvl_narrator "“Anything to drink?”"
    nvl_narrator "“I’ll be fine.”"
    nvl_narrator "He coughed, then reached into his pocket and pulled out a bottle of juice."
    nvl_narrator "“Here. Take this. But don’t drink too much too quickly. It’s a long way between bus stations, and the toilets on these buses tend to be unsanitary.”"
    nvl_narrator "I nodded silently and took the juice from him."
    nvl_narrator "I knew what he was trying to do, but he couldn’t just say it out loud."
    nvl_narrator "We’ve never been very good at communicating. None of us ever were."
    nvl hide bg_change_seconds
    nvl clear
    scene bg room_shun night with bg_change_minutes
    window show
    stop music fadeout 0
    "…"
    "I roll over in bed and pick up my cell phone."

    Shun "{tt}Have arrived safely. Everything's fine. Thanks for the snacks and juice.{/tt}"

    "I hit the “SEND” button. Then put my phone back on my desk."
    "Only then am I able to sleep restfully."

    return
