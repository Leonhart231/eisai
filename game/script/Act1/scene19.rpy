# Social Butterfly

label a1s19:
    scene bg hall_dorm_boys with bg_change_hours
    "By the time the day is over, after walking across campus I don’t know how many times and barely finding enough time to grab a decent lunch, my feet feel like they are tied to cement blocks."
    "I don’t remember school being so … exhausting. My head aches from all of the interactions I had to endure today. So many bright, happy smiles, so many people eager to talk to you. Ugh!"
    "On the bright side, with the exception of Okada’s class, I feel confident that I’ll be able to handle and even enjoy most of my classes. I’m going to learn a lot more here than I would at any other school. But on the down side, it is only 3:45 and I feel like I had been awake for days."
    "I can’t remember the last time I was so tired."

    scene bg dorm with bg_change_seconds
    "The dorm’s a little cold, having been unoccupied all day."
    "It feels so foreign, or maybe that is just me. I am the object that doesn’t belong here right now. Everyone knows each other and everyone tries to be friendly. I feel like I stick out."

    scene bg room_shun with bg_change_seconds
    "I crawl into my bed. A nap is in order. That would give me enough stamina to finish the little homework I was assigned later tonight. The cool air chills my blankets just the way I like it, and makes my desired nap feel much more appealing."

    Shun "Just five minutes …"

    "I say as my eyelids grow heavy."

    stop music
    scene bg room_shun night with bg_change_days
    play music "bickering_loop.mp3"
    "A strong buzzing in my pocket wakes me up. I answer my phone without checking the time."

    Shun "Uh … hello …"

    Kevin "Heeeeeeeeeeey! Guess who?"

    Shun "Hey Kevin."

    Kevin "Guess what I did, little buddy?"

    Shun "… How did you get my number?"

    Kevin "Haha! Yes, I got your number! Correct!"

    Shun "… What time is it?"

    Kevin "Like eight."

    Shun "Already?!"

    "I sit up and stretch. I am physically rested once more, though slightly crabby from being woken up."

    Kevin "Where are you?"

    Shun "In the dorm."

    Kevin "Well, come over to the girl’s dorm. Remember that charming and beautiful blonde I introduced you to a while back."

    Maryanne "Shut up Kevin!"

    "I hear a girl’s voice in the background, and Kevin laughing."

    Kevin "Haha, Maryanne said she wanted to get some sushi, remember? I got us a table at Yamada’s. Private party room. Ai is gonna tag along."
    Kevin "Come on out. We need a native here or we will look like tourists."

    "I had planned on studying tonight. That homework isn’t going anywhere. But Kevin did say we were going to get sushi later."

    return
