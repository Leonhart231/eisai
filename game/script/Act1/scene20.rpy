# Go out with Kevin

label a1s20:

    "I probably should get my work done but … I left home to get away from that old life. If I make myself do new things, if I force myself into social situations, maybe they will become easier. And after the long day I’ve had, I could use a night out just for me."
    "And I could think of a number of worse ways to spend my first night in college then with Kevin. Like a night with Hideki."

    Kevin "Shun, you there?"

    Shun "Yeah, I’m here. Okay, I’ll come out with you."

    Kevin "Awesome! We’ll meet you outside the girl’s dorm in a few."

    Shun "But seriously, how did you get this number?"

    "He hangs up on me before answering."
    "I look at my backpack, which is still loaded with books, and push it under my desk. Like Kevin said earlier, work can wait. Besides, I am hungry."

    stop music
    scene bg inou night with bg_change_minutes
    play music "decompression_loop.mp3"

    "The night air is refreshing and cool. I look up at the stars. Near Tokyo, the light pollution ensures that they’re a rare sight. Here though, away from Takuya, the sky is surprisingly cluttered with gleaming white dots."
    "I see Kevin, Ai and Maryanne standing together near the girl’s dorm entrance. They’re conversing in English, but even without understanding the language I can pick up on Kevin’s joking tone, or Maryanne’s dismissive reply. Ai merely smiles."

    show kevin neutral sides open at center
    show maryanne happy at left
    show ai smile at right
    with config.say_attribute_transition

    Shun "Hi guys."

    Kevin laugh closed "Yeeeah little buddy, gang’s all here. Ai, my fair lady, will you lead the way?"

    Ai proud "But of course, Mr. Gallagher. I would be happy to guide you."

    "Kevin and Ai playfully tease each other, and Ai turns to lead the march."

    Ai pout "But don’t stare at my butt."

    Kevin "No promises."

    hide ai
    hide kevin
    show maryanne at center
    with config.say_attribute_transition
    Maryanne "… Hi Shun."

    "Maryanne speaks up as we all play follow the leader. Her voice is soft, coy even."

    Shun "Hey Maryanne. It was Maryanne, right?"

    "She nods."

    Maryanne worried "I’m sorry if Kevin woke you. We really wanted you to come out with us."

    Shun "It’s fine. I’m glad you invited me."

    show maryanne happy with config.say_attribute_transition
    "Maryanne smiles again. I can hear her stomach growling."

    Kevin "We’re going to make the restaurant rich at this rate. Especially with Maryanne’s appetite."

    show maryanne worried with config.say_attribute_transition
    Ai "Oh, leave her alone. There’s nothing wrong with a girl who isn’t afraid to eat."

    Kevin "Afraid to eat?! That’s like saying a bulldozer isn’t—"

    Maryanne "Kevin, be quiet."

    "Once again, Maryanne orders Kevin and he listens without question."

    stop music
    scene bg yamada night
    show kevin neutral sides open at center
    show maryanne happy at left
    show ai smile at right
    with bg_change_minutes
    play music "happy_hangout_loop.mp3"
    "One of the hostesses seems to know Kevin and he starts chatting her up. Maybe he’s trying to talk his way into getting us a better table?"
    "We’re led to the back, away from the open tables and the ramen bar. Ai recognizes some people at another table and stops briefly to say hello."
    "It seems like everyone knows everyone in this school or this town, in one way or another."
    "I get the horrible idea that this is a joke, but only for a moment. Like they’re going to ask me to do their homework for them or something."
    "Kevin sits across from me and nods quick, then turns to the waitress and orders for himself."

    Kevin smile crossed "Hey Shun, you want a drink? I got first round."

    Shun "Uh … ma—maybe later."

    "Kevin nods and the waitress turns to Ai."

    Ai smile "Beef katsudon with two unagi rolls. … {em}Please{/em}?"

    "The waitress scribbles Ai’s order down."

    show kevin neutral sides open with config.say_attribute_transition
    "Kevin turns his head towards Maryanne and smirks. Maryanne doesn’t look back."

    Maryanne "… The Eastern Sashimi Special please."

    Waitress "Er, excuse me ma’am. I think you may be mistaken. That is a two-person order."

    Maryanne "I know. I’d like that … please."

    Waitress "And what would you like sir?"

    Maryanne uncertain "Wait, I wasn’t finished …"

    "The waitress looks shocked. I hear a snicker coming from Kevin’s direction. Maryanne blushes."

    Maryanne uncomfortable "I … um … if you … n—never mind. I think I am okay."

    show maryanne worried with config.say_attribute_transition
    Shun "I’ll have the nikujaga please."

    "The waitress leaves after me, and returns quickly with a bottle of sake as Kevin ordered."

    Kevin crossed closed "Okay, Shun, it’s here if you want it. Girls?"

    "He passes the bottle around and they take their first cup."

    Kevin laugh open "To college."

    "They all drink and I down some water to match pace."

    Ai smile "Anyway, Shun … tell us about yourself."

    Shun "What do you want to know? There’s not much to talk about."

    Kevin "Surprise us."

    show maryanne happy with config.say_attribute_transition
    Shun "Honestly, I’d rather know about you guys. You’ve been all over the world, not me. What things have you seen? Where are you from?"

    Ai talk "Well, I’ve been to—{nw}"

    show kevin laugh sides closed
    show ai pout
    with config.say_attribute_transition
    Kevin "Oh dude, I’ve been all over! I went to Germany with an exchange program in high school, I told you that right? It was great! And I’ve been all over the US, and England once, and France for a little bit …"

    show maryanne embarrassed with config.say_attribute_transition
    "Maryanne sits patiently, with her hands on her lap, waiting for Kevin’s boisterous rambling to end. She looks so eager to share a story but refuses to do so until her friend stops talking."
    "Ai, on the other hand, watches Kevin like a hawk for an opportunity to sink her talons into the discussion. She chimes in every now and then, prodding Kevin’s claims but jokingly so."
    "Maybe to share her own story, maybe to lecture Kevin about how he’s being a loudmouth."
    "Finally, Ai chimes in as Kevin mentions something about a layover in Fiji, with her own story of her days in London."
    "I listen to her as Kevin pours a second cup of sake for himself."

    Shun "Uh … you know … maybe I’ll have … just one of those."

    Kevin "Kampai, brother."

    scene bg yamada night
    show kevin neutral sides open at center
    show maryanne happy at left
    show ai smile at right
    with bg_change_minutes
    Shun "So, how long have you lived in Japan, Maryanne?"

    Maryanne ecstatic "M—me? Well let’s see …"

    "Maryanne stumbles with her words for a second, seemingly flattered that someone asked her that question. She counts off the years on her fingers as her voice trails off and she searches through her thoughts."

    Maryanne happy "… I have been here since I was thirteen."

    Shun "Your Japanese is very good."

    Maryanne "Well, I took a few classes before coming over here with my father. He’s very fluent. He came to Japan a lot when he was younger."

    Shun "And your mother?"

    Maryanne "… Uh, she’s not here anymore."

    "Maryanne’s smile doesn’t waver as she says … that."

    Shun "I’m sorry."

    Maryanne "It’s okay. I didn’t really know her. I was really young when she left."
    Maryanne ecstatic "But my dad is awesome. He works all the time, but we talk every week."

    Kevin smile sides closed "Yeah, Mr. Sawyer is a cool guy. When’s the next time he’s coming in?"

    Maryanne "Next time he comes by, I’ll remind you but please no jokes again."

    Kevin open "Oh yeah, haha … Shun, the first time I met him, he said “hello” to me in Japanese. So I then pretended Japanese was the only language I knew. Like literally the only one."

    Kevin "Maryanne blew it though. I think I had him convinced."

    Shun "So you two like … {em}really{/em} know each other?"

    Maryanne "Yeah, we met when we were … eight. At a national science pageant back home."
    Maryanne ecstatic "It was a lot of fun. I made a mini-green house and grew beans in it during the winter. And when it was over, I got to see the Golden Gate Bridge!"
    Maryanne happy "And I met Kevin there. It was only once and very casual, and I think I spent a total of two hours with him. And that was it."
    Maryanne mad "In my first year at Inou, Kevin was in the same class. And he started … “talking” to me?"

    Kevin frown open "… What?"

    Maryanne happy "… Well, eventually we realized we had met once before. And we started being friends after that."

    Kevin neutral crossed closed "What are the odds, right? A million schools to go to and she picks this one too?"

    "The waitress comes in with our food, and another bottle opened … and a second waiter to help her carry in Maryanne’s order."

    Kevin smile sides open "If you need help eating any of that …"

    Maryanne mad "I won’t!"

    "Kevin backs off, but giggles at Maryanne as her food vanishes. Her first plate is empty in moments."

    Maryanne ecstatic "Mmmmm! This is really good! Everything is so fresh."

    Kevin laugh sides closed "Whenever I watch you eat, in my mind I hear a starving child cry."

    show bg yamada night with hpunch
    show maryanne ecstatic
    show kevin mad sides closed
    "{snd}BANG{/snd}"
    play music "decompression_loop.mp3"
    show kevin open with config.say_attribute_transition
    "Kevin jumps and bites his lip. Maryanne looks pleased with herself as Kevin rubs his shin."

    scene bg yamada night with bg_change_minutes
    Kevin "Thank you fooor everything, it was aaall wonderful. We’ll be back soon. Prrromise."

    show ai smile at right
    show kevin neutral sides open at left
    show maryanne happy at center
    with config.say_attribute_transition
    Ai "Let her do herrrr job, you blowhard."

    Kevin laugh sides closed "Ha-ha! Blowhard …"

    "Ai and Kevin both had enough, it seems. They both stumble very slightly as we leave Yamada’s."
    "Meanwhile, Maryanne’s massive smile has not left her face. She is practically skipping back to the dorm."

    scene bg yamada_street night
    show ai smile at right
    show kevin neutral sides open at left
    show maryanne happy at center
    with bg_change_seconds

    Maryanne "That really hit the spot! I have to get that plate again! Sooo goood!"

    Kevin laugh crossed closed "Yeah. You-you could keep them in business by yourssself."

    Ai pout "Sssstop makin fun o’ her."

    hide ai with config.say_attribute_transition
    hide kevin with config.say_attribute_transition
    "Ai (barely) punches Kevin in the arm, and he responds by rubbing the top of her head. Ai then runs away from him while Kevin chases her, leaving Maryanne and me to try to catch up."

    Shun "… Are Kevin and Ai always like that?"

    Maryanne stressed "What, drunk?"

    Shun "No, I mean … ugh. Never mind."
    show kevin closed sides laugh at leftish
    show maryanne uncomfortable
    with config.say_attribute_transition
    Kevin "Heeey Shun! Did—did Maryanne ever tell you about the first time I met her dad? And-and what I did? Haha … did she?"

    scene bg inou night
    show ai smile at right
    show kevin laugh crossed closed
    show maryanne happy at center
    with bg_change_minutes

    Ai "Alright, time to call it. Goodnight guys. Shun, watch that one. Make sure he doesn’t fall down the stairs on his way up."

    Shun "I’ll try. Goodnight Ai. Goodnight Maryanne."

    Maryanne "Uh … yeah. Good … goodnight."

    hide maryanne with config.say_attribute_transition
    hide ai with config.say_attribute_transition
    "Our group splits up. Maryanne walks back towards the science building instead of following Ai to the girl’s dorm."

    Kevin "Hehe … nice crowd, right?"

    Shun "They are. I’m glad I came out."

    Kevin "Yeah dude, me too. You’re-you’re gonna fit in just fine."

    "Fit in. Me. Ha! It sounds weird hearing someone else say it. The idea of me, of all people … but I guess stranger things have happened."

    stop music
    scene bg room_shun with bg_change_days
    play music "contemplative_loop.mp3"
    "The alarm shocks me awake, and I fumble around, slapping blindly until it stops. I’m still sleepy. I probably shouldn’t have stayed out as late as I did, but I had a good time."
    "None of the people last night were the kind I’d usually spend time with. I’d probably have never talked to any of them on my own."
    "It was great to tag along. But I still felt like the odd-one in that group. Given all they have done and been through, I felt … weirdly enough, more of a stranger among then."

    scene bg dorm with bg_change_seconds
    "I dress quickly, gather my things for class, and scarf down breakfast. The dorm is quiet. I suppose everyone is in class already. It sounds like another world."
    "My parents yelling was so far away now. Their problems didn’t seem so big after last night."

    return
