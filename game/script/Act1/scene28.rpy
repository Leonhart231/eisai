# Scene: Eisai Party - Maryanne

label a1s28:
    "I look over at the food … and spot a head of bright yellow hair that stands out vividly like a lighthouse among the sea of dark-haired heads."
    "Maryanne is tucked in the corner of the kitchen, her head between her shoulders, stirring the straw in her drink as she tries to avoid eye contact."
    "And despite her efforts to remain hidden, Hideki is standing next to her, talking his mouth off. I sneak in closer to listen in on their conversation."

    show hideki formal glasses adjust at leftish
    show maryanne circles neutral at rightish
    with config.say_attribute_transition
    play music "bickering_loop.mp3"
    Hideki "… So Schrödinger’s Cat is in a box, right? Fifty percent chance is dead, fifty-percent chance it’s alive. Some say the cat is both alive and dead at the same time."

    Maryanne "Mmm. Yeah."

    Hideki "Except that the cat has a point of view too. The cat is observing itself. It knows it can die at any moment."

    Maryanne uncertain "… Do you … like cats?"

    Hideki "Irrelevant. If a cat is dead and alive at the same time, it needs to be studied. Not petted."

    Maryanne confused "Umm … o—okay …"

    Hideki "Anyway, if you yourself are Schrödinger’s cat, you have a fifty percent chance of being killed each second …"

    "Maryanne looks visibly unwell as she was forced to listen to Hideki ramblings. I figure that that is a cue. Maybe not mine, but I take it anyway. I hurry over to her, and say the first thing that pops into my mind."

    Shun "Are you guys talking about cats? I like cats. More than dogs anyway."

    Hideki formal crossed glare "CATS are NOT the subject of our conversation. I am lecturing the biologist on nuclear physics … and dead cats."

    show maryanne happy with config.say_attribute_transition
    Shun "Like a radioactive zombie cat? That sounds kind of cool!"

    Hideki "No! Not like a radioactive zombie cat! You and the other one are perfect for each other, you know that!?"

    Hideki formal armsup argue "Bah! Waste of time. Waste of air!"

    hide hideki with config.say_attribute_transition
    "Hideki’s stuffs a crab leg into his mouth and leaves, grumbling to himself about eyeballs on top of pyramids or something. Maryanne sighs in relief after he is beyond earshot."

    Maryanne happy "Thank you! That was … disturbing."

    Shun "It’s okay. I know Hideki can be a bit … much. How are you doing Maryanne?"

    Maryanne "Good. Very good."

    "She says with a half-hearted smile, then sighs."

    Maryanne uncertain "I … really wasn’t planning on such a big crowd."

    Shun "What do you mean? It’s your dorm."

    Maryanne uncomfortable "Yeah, but … Shione … she didn’t really … I mean, she told me we were having people over a few hours ago and she looked so excited that I couldn’t say no … but I didn’t think it was going to be this crowded …"

    Shun "Oh. I see. Um, yeah, that would bother me a little too."

    Maryanne neutral "I’m not … bothered. Exactly."

    Shun "Um, did you try the food yet?"

    Maryanne "No. I’m not hungry."

    Shun "… You?"

    Maryanne worried "I don’t always need to eat a lot, you know. Besides … I ate a while ago. I’m full, don’t worry."

    stop music fadeout config.fade_music
    "Twirling her straw again, she mutters something under her breath in English. Then she yawns."

    Maryanne happy "… Excuse me … uh, how was your second week of school? Was it as hard as you’d thought it would be?"

    Shun "No … it was much worse."

    Maryanne uncertain "Oh come on. Is it because of Okada?"

    Shun "Among other things. The last two weeks have been a bit difficult. This place is … strange."

    Maryanne confused "What do you mean?"

    "She puts her hand over her mouth and yawns loudly."

    Shun "What, you didn’t notice? Everyone here is … different. And the workload, the town … this isn’t what I expected."

    Maryanne happy "Life is unpredictable? First impressions aren’t always the right ones, yeah?"

    "She swayed a little, poorly balancing on her heels, like she has trouble standing."

    Shun "You okay? You look tired."

    play music "very_sad_loop.mp3"
    Maryanne "I’m fine."

    Shun "Are you even enjoying this party?"

    show maryanne worried with config.say_attribute_transition
    "Maryanne pauses, looking … frustrated."

    Maryanne "I … I had a long day and I just wanted to sit in a quiet room alone and do nothing all night."
    Maryanne "But then I had to help Shione cook and buy things and everyone put their bags in my room so I can’t even go to sleep early if I wanted to and Shione means well and she has a lot of friends and I … I …"

    "{snd}crash{/snd} Something breaks."

    Anon "Sorry! That was my fault!"

    "The crowd of people boos and laughs at whoever dropped whatever that was."
    "Maryanne covers her ears, baring her teeth as she fights to urge to pull her hair out."

    Maryanne mad "I … need a minute."

    hide maryanne with config.say_attribute_transition
    stop music fadeout config.fade_music
    "She hurries towards the front door, trying to not push anyone as she lets herself out."
    "As a man who also values the quiet moments, I follow her."

    scene bg hall_dorm_girls with bg_change_seconds
    "The door closes and seals off most of the noise, though the muffled conversations can still be heard through the wall. Maryanne is not in the hallway. I hear the door in the stairwell open and close."

    stop music
    scene bg inou night
    show maryanne worried circles
    with bg_change_seconds
    play music "very_sad_loop.mp3"
    "Maryanne rubs her eyes as she sits down on the nearby bench. The night is quiet. We can barely hear anything coming from her dorm room."

    Shun "You okay?"

    Maryanne "I don’t like loud noises. Or big crowds. Or both in my dorm at night when all I want to do is go to bed."

    Maryanne mad "This was a mistake. I should have just said told Shione that I didn’t want people over. But nooo, I had to be nice again. I have too much work to do and need to call my dad and do my laundry and … and it’s … {em}god{/em}, I’m so tired!"

    "She kicks a pebble. The skin under her cheeks brightens as stress comes out from behind her usually calm face."

    Shun "You don’t have to go back up, you know."

    Maryanne worried "Yes I do. I have to, or I’ll have to explain to everyone why I left tomorrow and … I don’t want to do that."

    Maryanne "… I just want to sleep …"

    "She leans her head back over the bench and sighs again. I’ve seen what wits-end looks like plenty of times before. She looks like I did a few months ago when my parents finalized their divorce."

    Maryanne "I overdid it a bit this week. I—I thought … I don’t know, maybe this party would … help me feel … better."

    Shun "You know what, I’m not in a partying mood either. I kind of want to stay away from all of … that … for a while. You can tell Shione I convinced you to step out later."

    Maryanne "… Okay."

    Maryanne "I don’t know why I thought this would work out. Why did I think it would be different this time? I just … everyone was going to be here. They wanted to come. I didn’t want to ruin everyone’s night to save mine."
    Maryanne "Why does it seem like everyone else can handle these things and I can’t?"

    Shun "Hey, you’re handling a lot more academically. I bet more people are jealous of you."

    Maryanne uncomfortable "Jealous? Yeah right."

    "She rolls her eyes, and sighs softly again."
    "The crickets chirp and our conversation evaporates. Maryanne stays seated, unwilling to share more at the moment. I stay quiet too, hoping that she feels better … and wanting to stay away from the giant crowds."

    show maryanne neutral with config.say_attribute_transition

    Maryanne "Um … maybe I’m … hungry. {em}Now.{/em} A bit."

    Shun "I thought you said you ate earlier?"

    Maryanne uncertain "…"

    "She looks away from me."

    Shun "I’ll get you something if you wait here. I’m going to grab something too."

    show maryanne happy with config.say_attribute_transition
    stop music fadeout config.fade_music
    "Maryanne tries not to hug me for volunteering to reenter that social war zone. Instead, she gently nods and modestly thanks me."

    scene bg dorm_maryanne with bg_change_minutes
    "I maneuver through the crowd around the concession stand, toward the kitchen. It feels like more people showed up since I arrived."

    show kevin laugh sides closed
    Kevin "Boo!"
    play music "bickering_loop.mp3"
    "Kevin tries to sneak up on me but fails to scare me. Balancing two cups and two plates of food preoccupies me too much to be startled. He sways constantly, even though he’s standing still. The cup Kevin is holding … well, it doesn’t smell like just soda."

    Kevin crossed open "Dude, have you seen these girls? They are sooo pretty. Japan … dude, Japan knows how ta make great videogames, great comic booksss and great women. You guys are really good at … both a’those things."

    Shun "Um … thank you?"

    Kevin closed "No Shun. No. Thank you. Thaaaank all of you. Hahaha!"
    Kevin neutral sides open "What’s all … this? Getting seconds before you get firsts, huh?"

    Shun "One of these is for Maryanne."

    Kevin smirk sides open "You sssly dog!"

    Shun "It’s not like that. I’m just being nice to her. Hideki made her uncomfortable."

    Kevin mad crossed "He wasn’t talkin’ about that stupid cat in the box thing, was he?"

    Shun "He was! He told you that cat thing too?"

    Kevin closed "Every week with the fucking cat dude. Every god damn week. He’s tells it to lots of girls … like they’d care about cats dying from radiation. Hehe, speaking of pussy drying up … am I right?"

    show kevin laugh open with config.say_attribute_transition
    "He raised his hand for a high-five. I gesture to the cups preoccupying both of my hand."

    Shun "Well … anyway, I’m going to go see Maryanne."

    Kevin sides closed "No problem dude. Have fffun. I’m going to go bug Ai more. Haha"

    scene bg inou night with bg_change_minutes
    stop music fadeout config.fade_music
    "It takes me a moment to figure out how to open the front door with my hands full, but I eventually get it."

    Shun "I’m back. Here’s—"

    play music "romantic.mp3"
    "Maryanne slouches on the wooden bench, eyes closed, her mouth partially open as she naps."
    "I quietly move next to her, sitting down as slowly as I can. I sit quietly, and she remains dormant."
    "As I take a bite out of my food, she jolts up from her slumber suddenly. Her eyes shoot open and in a snap her shoulders straightened out."

    show maryanne uncertain circles with config.say_attribute_transition
    Maryanne "… Did I fall asleep?"

    Shun "Only for a minute I think. Still thirsty?"

    "She looks embarrassed, covering her cheek with one hand. Then she grabs a brownie and … inhales it. Then another. And then a third."

    show maryanne uncomfortable with config.say_attribute_transition
    Shun "Do you want to sneak out of here? Maybe call it an early night?"

    Maryanne "… Yes."

    Shun "I mean … my dorm is free if you want to … I don’t know …"

    show maryanne confused with config.say_attribute_transition
    "Maryanne’s eyes widen and she gulps."

    Shun "No, no, not like that. I mean, it’s quiet and I can crash on the couch tonight. It’ll be payback for, you know, setting me up with the cot."

    Maryanne "I don’t—"

    Shun "There are snacks at the place too. Just in case."

    show maryanne neutral with config.say_attribute_transition
    "Her shoulders tense up for a few more seconds, then she relaxes."

    Maryanne happy "… Okay. Thank you."

    "I take both plates, and Maryanne grabs the drinks."

    show maryanne uncertain with config.say_attribute_transition
    Shun "I wouldn’t drink that if I were you."

    "She sniffs it, then rolls her eyes before tossing it out onto a nearby bush."

    scene bg dorm
    show maryanne uncertain circles
    with bg_change_minutes
    "Maryanne can barely keep her eyes open by the time I get her into my dorm. She yawns and walks towards the bedroom like she hasn’t slept in days."

    Shun "Uh, that room is not mine. I’m in the middle."

    Maryanne "Oh … okay. Sorry. I’ll …"
    Maryanne uncomfortable "… Are you sure you don’t mind me sleeping here?"

    Shun "No, of course not. Rest up."

    "She smiles, and lets herself into my room."

    Maryanne "… Goodnight."

    hide maryanne with config.say_attribute_transition
    "The door closes. I look around the dorm for something to do. The guys won’t mind if I’m not at the party. They’re too busy having their own adventures."
    "I turn on the TV with the volume nice and low and pop in one of Kevin’s movies. I pick at random, and end up watching a film about two roommates, one Indian and one Chinese, who desperately want to eat hamburgers before the night is over. It’s very odd, but funny."
    "I lay down on the couch, watching the film as I drift to sleep."

    stop music
    scene bg dorm with bg_change_days
    play music "contemplative_loop.mp3"
    "I wake up at 8:00, as Hideki leaves his room. He says nothing, and only opens the fridge to take out crab legs that he saved from last night, then returns to his room."
    scene bg room_shun with bg_change_seconds
    "I check my room. Maryanne is already gone. There is a post-it note on my dresser."
    "“Thank you for everything –M :)”"
    "Her little note makes me smile."

    "{snd}BING{/snd} A text goes off in my pocket."

    Kevin "{tt}Hey man. I'll be home in a bit. Can you get coffee ready? My head really hurts.{/tt}"

    "Shaking my head, I do as my friend asks …"

    Shun "… Ha. My friend."

    return
