# Scene: Shodo Signup 1

label a1s23:
    stop music fadeout config.fade_music
    "I weave through the sea of people and leave the science building, shuffling through everyone and into another sunny day."
    "Just as I step off the bottom step, I hear a peppy voice call out to me."
    "Ai has found me. Looking eager and spunky as she did when I first met her."

    show ai talk with config.say_attribute_transition
    play music "bickering_loop.mp3"
    Ai "Oi! Shun! How’s it going young traveler."
    Shun "Hi Ai. What’s up?"

    Ai "Oh, just passing through, handling the needs of others, as it is my nature to do so! And I suppose you are once again mixed up with all the wrong people, is that right?"

    Shun "I … don’t think so."

    Ai proud "Of course you don’t think you are. You are an innocent,  unlike the rest of us."

    "She smiles, flashing her chipped tooth."

    Ai talk "Speaking of handling needs, are you going to the Shodo Island trip?"

    Shun "Trip? What trip?"

    Ai yell "The trip that Eisai takes part in, remember? We talked about it during orientation … the one you were late for …"

    Shun "Oh. Right. That one. I actually haven’t decided about that yet."

    Ai "Well hurry up and decide. Golden week is not too far away, and we need papers in! Documents, signatures, affidavits, the works! The sooner you get it in, the better."
    Ai "You don’t have prior plans, do you? Spend it with family?"

    "I hadn’t thought about Golden week at all. I figured I’d stay on campus during my time off and study, or raid my roommate’s movie collection while they did their own thing."
    "The thought of going back home so soon made me sick, as did spending it on the beach with people who were not my family."

    Shun "Uh … I don’t have plans just yet."

    Ai smile "Great! Then of course you’re coming, right? Just fill out the forms and you’ll be on your way to paradise! I won’t even ask for your soul in exchange."
    Ai talk "So how about it? Are you really going to turn down a free tropical vacation? Everyone else is going."

    Shun "Who is everyone else?"

    Ai "Everyone who matters. Your roommates Tweedle-Dum and Tweedle-Dumber are going. Me too, obviously. Yumiko, Maryanne, Shione … um, who else … Modoka … Risa I think … and a bunch more. You’ll have plenty of playmates."

    Shun "I’ll … think about it."

    Ai proud "Fantastic. You go on and think about it. And … I will be in touch!"

    hide ai with config.say_attribute_transition
    "She hands me a packet of papers, then hurries away to recruit some other poor sap to her cause."
    "My next class is not till after lunch, so I decide to go back to the dorm and make sure I have all my books this time."

    return
