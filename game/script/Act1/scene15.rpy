##-------------------------------
## Act 1: A Brighter Future
## Scene 15: Risa - First Meeting
##-------------------------------

label a1s15:
    stop music
    scene bg room_shun with bg_change_days
    play music "contemplative_loop.mp3"
    "The alarm clock’s shrill ringing knocks me out of my dream. I groan and smack the snooze button, but miss. Twice. I am awake by the time I get the buzzing to stop."
    "Dazed, I look around the unfamiliar room and my heart starts thumping. A moment of panic hits me before remembering this is my dorm now. My brain was still expecting to wake up in my old room, in my old bed. Instead, I regain consciousness … here."
    "When I realize where I am, I sigh and lay back down on the narrow and unfamiliar bed, looking up at the ceiling. The paint is distorted near the edges, and there are a few broken speckles around the light."
    "The key Kevin gave me is still on my dresser. I fiddle with it, listening to the soft jingle as I do nothing."
    "After a few minutes of laying still, absorbing the warmth of a full night’s rest that still lingers between the covers, I climb off my mattress. My knees crack loudly as I stretch."
    "My phone tells me it is 9:54, and upon seeing the time my stomach tells me it is time for breakfast."
    stop music fadeout config.fade_music
    scene bg dorm with bg_change_seconds
    "Once fully dressed, I step into the living room. The sign on Hideki’s door is there, but now reads “DO NOT WAKE UNTIL:”. Stickers with numbers hang under the letter, reading “7:00 PM”. The numbers are crooked, suggesting that someone besides Hideki has swapped them."
    "I look in the fridge. There are some eggs and sausages and such. I guess I could use them and pay them back later, but I fear they belong to Hideki. I don’t think my life was worth one egg."
    "But thankfully, since Kevin bought me dinner yesterday, I have money from my parents, more than I expected to have by now. Maybe it’s time to go grocery shopping."
    "I grab my wallet, check to make sure my new key is with me, and venture out to find something to eat."

    stop music
    scene bg takuya with bg_change_minutes
    play music "decompression_loop.mp3"
    "I step off campus and walk through town. Takuya starts where Inou ends, right outside its gate. I pass a few students on my search for food. The town is full of young people, most I can tell are new students like me."
    "Most are guided by older class-men who know the town well enough to act as guides."
    "I’m fine with being by myself. It’s the first time I have been alone since I arrived, not counting sleeping. Meeting so many new people, on top of the long trip, exhausted me."
    scene bg takuya_bridge with bg_change_minutes
    "I cross a bridge over a stream, and hear the wind gently blow against wet clothes that hang on rope between windows. I smell fresh air, and hear a car’s motor only every other minute."
    "I pass by many quiet homes and sturdy apartment buildings, small shops and narrow roads. It’s nothing like Tokyo. It isn’t too crowded, isn’t too busy … everything feels calmer."
    "I think I like it here."

    scene bg black with bg_change_minutes
    "Eventually, after a good twenty minutes of walking aimlessly, I find a convenience store that reminds me of the store back home. I order a breakfast sandwich and a drink, along with some other easy meals for the next few days."
    "The cashier smiles and thanks me for my service, and I find a nearby bench and start eating my meal. I look around to see more small houses and apartment buildings. I hear birds tweeting on rooftops, and … see a bridge that looks a lot like the bridge I think I crossed earlier. Or did I?"
    "… Which street did I walk down to get here, again?"
    "… Uh oh."

    scene bg takuya_far with bg_change_minutes
    "A half an hour later, I have crossed three bridges and countless convenience stores. By now I am sure that I’m wandering in circles, but I can’t find any streets that look familiar."
    "As I walk down Nikoto Street, I pull out my phone, ready to call Kevin for help … and realize I don’t have his phone number."
    "Fatigue starts to build up in my calves. I sit down on another bench to rest, one that I am sure is different from the last one … I think."
    "I take a minute and look up at the sky, trying to gather my bearings before I dial the police."
    "Then, I hear music. One guitar string after another, plucked gently, creating a simple melody that matches the serenity of the town."
    show risa neutral with config.say_attribute_transition
    "A girl walks down the road, her head casually bobbing back and forth as she listens to very loud music through her phone. Her purple ponytail bounces and I realize … That’s the girl from yesterday!"
    "She doesn’t see me, too lost in her music to notice anyone."
    "Realizing that she must know the way back to school, I hurry after her."
    Shun "… Hey wait!"
    "She doesn’t acknowledge me as run over, not until I am feet away from her backside."
    "She does not turn her head. I have to walk in front of her to see her face."
    show risa concern with config.say_attribute_transition
    Shun "Sorry I don’t mean to be rude but … I’m lost. Do you know the way back to Inou campus?"
    "She chews gum and stares. I hear her exhale as she scans my face. She nods in a direction, but doesn’t say anything."
    Shun "… That way? Straight in that direction?"
    "She twists her head a little, cracking her neck softly. Then stares at me."
    Risa "Go north. That’s it."
    Shun "Oh. Well, thank you."
    "She nods. She keeps staring, still not showing a smile or frown or … anything."
    "She walks around me and starts going North, as she suggested. She puts her phone in her pocket."
    Shun "Are … are you going back to Inou too?"
    "She nods but doesn’t stop walking."
    show risa pout with config.say_attribute_transition
    Shun "We can go back together, right? Do you mind?"
    "I hurry to her side, partly because I don’t trust my own sense of direction. Her eyes glance at me, then back to the road in front of her. She shrugs. That’s all I get."
    "She reminds me of the popular kids back in high school, but not the ones who were popular because of good grades."
    "She reminds me more of the ones who sat in the back row and picked fights with anyone who looked at them. Her attitude makes me self-conscious of my own coolness right away."
    Shun "Okay, cool. … My name is Shun, I’m new in town."
    Risa neutral "… I saw you yesterday."
    Shun "Oh. Yeah. Just for a bit."
    Risa "…"
    Shun "Er, what’s your name?"
    Risa pout "Risa Hayase."
    $ risa_name = "Risa"
    "She answers with a soft grumble, like she didn’t want to tell me her name in the first place. I decide to stop talking and keep walking."

    scene bg inou_gate sunset
    show risa neutral
    with bg_change_minutes
    "Eventually, we get back to campus. Risa guided me safely home without another word passing between us."
    Shun "Thank you for showing me the way back."
    Risa pout "Yeah. Sure."
    Shun "Well … have a good day. I’m sure I’ll see you around."
    "She nods and awkwardly looks at me, not saying a word."
    "Just then, thankfully, my phone buzzes with a text message alert."
    Kevin "{tt}hey it is kevin where r u? ur late to orentatun.{/tt}"
    "Kevin somehow got my phone number, and managed to remind me five minutes after the fact that I am late for the orientation he told me to attend. But, this is a good excuse to leave without any awkwardness."
    Shun "Well, I have to go now. I am late for an orientation."
    "Risa nods. I turn and start to hustle towards the boy’s dorm lobby."
    "… I hear footsteps behind me. Risa is following me. When I stop walking, she walks past me."
    Risa "I’m not waiting this time. You’ll make us both late if you do that."
    Shun "… Wait. You? You’re a scholarship student too?"
    Risa "Yeah. Why is that surprising?"
    Shun "N—no reason."
    "I laugh awkwardly and she again says nothing. It prompts me to walk faster."

    return
