##-------------------------
## Act 1: A Brighter Future
## Scene 13:
##-------------------------

label a1s13:
    scene bg room_shun sunset with bg_change_seconds
    "I go to my new room … Koji’s old room apparently … and sort through the books I brought with me, leaving plenty of room for my future textbooks."
    "With all the extra space, I realize that I actually didn’t pack that much."

    scene bg room_shun sunset with hpunch
    Kevin "Yeah! Suck on it!"
    Hideki "No fair! I couldn’t see!"
    "I hear Kevin yell and assume the match is over."
    "Maybe if I sit here quietly, he’ll forget I’m in here."
    "{snd}KNOCK KNOCK KNOCK{/snd}"
    "… No such luck."

    show kevin neutral sides open with config.say_attribute_transition
    "Kevin sticks his head inside before asking permission to enter."
    Kevin "Guess who won? It rhymes with “seven” and he’s a 9 out of 10."
    Shun "… Yeah, good for you."

    Kevin serious crossed open "Is this really all you brought? No posters or anything?"
    Shun "I just brought what I thought I would need."
    Kevin "Eh. We’ll get you some swag later. Make this room feel like home."
    Kevin smile "So, dinner? The ramen shop is still open … I don’t mind a repeat."
    Kevin smirk sides "If you’re low on cash, I’ll cover you again."
    Shun "… I don’t know …"
    Kevin "Come on. Hideki’s already had dinner. You’re not going to make me go eat by myself like some loser, are you?"
    "… What’s so bad about eating by yourself?"
    Shun "Maybe something small, I guess."

    hide kevin with config.say_attribute_transition
    "Kevin bolts to his room before I can second guess myself."
    "I look at my mostly empty room. I guess it’s going to take a while for this all to become normal."

    stop music
    scene bg inou_gate night
    show kevin neutral sides open
    with bg_change_minutes
    play music "decompression_loop.mp3"
    "Kevin waits for me by the front door, raring to go. He’s practically bouncing on the tips of his shoes."
    Kevin smile crossed "Ready?"

    "Kevin leaves and I follow. Campus looks very different at night. Students are still out, but the sea of people that covered the sidewalks this morning has disappeared."
    "Gleaming lamp-posts dot the walkways, casting faded shadows here and there."
    "Every other dorm window flickers with shadows of happy young people socializing, or sitting at their desks as they prepare themselves for the upcoming week."
    Kevin smirk sides "So … what do you like to do?"
    Shun "Like to do?"
    Kevin "You know. What are you good at?"
    Shun "Uh, I’m good at calculus."

    Kevin serious crossed open "For fun, Shun. What do you do for fun? You’re killing me here man."
    Shun "Oh, sorry."
    "… Actually, what do I do for fun? Fun isn’t really something that I’ve had that much time for."

    menu:
        "What do I like to do for fun?{fast}"
        "I like to listen to music.":
            Shun "I like to listen to music."
            Kevin neutral sides open "Everyone likes music. What kind do you like?"
            Shun "Rock mostly. The old school stuff."
            Kevin smirk "Yeah, way to narrow it down."

        "I like to read books.":
            Shun "I like reading, I guess."
            Kevin "Figures you’d be a bookworm. Anything in particular?"
            Shun "I read a lot of light novels."
            Kevin "Oh yeah. Those are pretty popular over here, aren’t they?"
            Shun "In high school at least. I am not sure if college students like them."
            Kevin "Hey, back home, we have this lame book about a vampire-fairy and a werewolf with a tan. Totally not the same thing, but it was big with high school kids too."
            Shun "I read this great book once about a guy who keeps going back in time and repeating the same day."
            Kevin "Yeah! They made a movie about it back home. God, I love Bill Murray."
            Shun "Uh … yeah …"
            "… I don’t think we’re talking about the same thing here, but whatever."

        "I like movies.":
            Shun "I like watching movies."
            Kevin "There we go! So do I! I’m a huge movie buff. Quick, name your favorite, go!"
            Shun "Um, er, I don’t know, I can’t pick one. Lord of the Rings?"
            Kevin "Yeah, that was a good one. Did you ever see “Braindead”? It’s by the same director, but it’s … different."
            Shun "No, I haven’t."
            Kevin "I’ll have to show it to you. Not right after dinner, though. And never before sex."

    Shun "What about you?"

    Kevin serious crossed open "What do I do for fun? Well, there’s Tomoko, Zoey, Meiya, Cassie, Sheena …"
    Kevin "One-Ear was pretty cool. She didn’t really have one ear, it was just a nickname and my gut told me not to ask."
    Kevin laugh crossed closed "But that’s not so much a “hobby” as it is a “calling”. I’m all about film in my down time man. And I travel a lot and I—"
    Anon "Oi! Kev!"
    "A high-pitched voice shouts at us. Kevin’s eyebrows jump."

    Kevin smirk sides open "Uh … so, yeah. I’ll tell you later."

    show ai yell at rightish
    show kevin neutral sides open at leftish
    with config.say_attribute_transition
    "The black-haired girl from this morning runs to Kevin’s side, smiling brightly, showing off a chipped tooth with a wry smile."
    Kevin "Heeeey, Ai!"
    $ ai_name = "Ai"

    Ai smile "Fancy seeing you back so soon. I thought you’d have given up by now and run off to yet another far-away land."

    Kevin laugh sides closed "Er, nope. Still here and kicking. They tried to get rid of me but I refused to go."
    "He laughs softly and the girl smirks."

    Ai pout "Well, not for lack of trying. Good thing you di—"
    "Ai notices me and pauses to examine my face."

    Ai yell "Who’s this? Another stray you picked up?"
    Kevin crossed open "You’re the second person who called him a stray."

    Ai pout "You’ve got this habit of attracting the oddballs. Where’d you meet this one?"
    Kevin "My place. He took Koji’s room."

    show ai proud at rightish with config.say_attribute_transition
    Ai "Heh. Koji. He still twitches when he hears your name, last I heard. Or Hideki’s name. What did you two do to the poor guy, anyw—"

    show ai yell with config.say_attribute_transition
    "She stops mid-sentence and gives me a second look."

    Ai "Have we met before, Shun? You look … {em}familiar{/em}."
    Shun "I think I bumped into you this morning."

    Ai proud "Oh, right! You’re the new guy that didn’t know one end of campus from the other."
    Shun "Er, yeah. New. That’s me."

    Ai talk "And now this perv is showing you where to go and where to avoid. Right?"

    Kevin smile crossed open "We’re going to Yamada’s! Want to come with us?!"

    Ai proud "Absolutely! I have pictures of my Aunt back in Osaka to show ya!"

    Kevin "Oh yeah?  I stopped in Hawaii on my flight back to Queens. Spent a night there eating shrimp and watching hula girls."

    Ai pout "Ha! Classic Kevin. Never missing a chance to chase a skirt, even if it’s made of grass."
    Kevin "Hey, don’t hate the player. Especially not when he’s winning."

    Ai yell "Chasing girls ain’t a game. For one thing, “games” usually require skill or talent."
    Kevin "I do have a talent … for finding the prettiest ones around!"

    show ai pout
    show kevin serious crossed open
    with hpunch
    "Ai inexplicably punches Kevin in the arm, sticking her tongue out at him right after."
    Kevin "OW!"

    Ai yell "You deserved that. C’mon. I’ll buy you a beer at Yamada’s."

    hide ai
    hide kevin
    with config.say_attribute_transition
    "She turns and walks away, leaving Kevin and I to follow. He jogs up alongside her."

    scene bg yamada_street night
    show kevin neutral sides open at leftish
    show ai smile at rightish
    with bg_change_minutes
    "Ai keeps pushing her cellphone into Kevin’s face and showing him what appear to be photos she’s taken on vacation."
    "Kevin doesn’t seem interested, which prompts Ai to want more attention from him."
    "After a moment, Ai turns her attention to me."
    Ai smile "Do you at least want to see my vacation photos?"
    "I freeze and laugh nervously, not wanting to pick sides in any conflict these two are having."
    Kevin "Give him a break, you’re scaring him."
    Ai "Am not!"
    "Kevin turns to me."
    Kevin "Don’t worry about Ai. She’s all bark and no bite. Think of her like a small dog: No teeth."
    Ai pout "I only “bark” at you. And I wouldn’t if you weren’t so … ugh, never mind."

    "We eventually make it to Yamada’s. I can see silhouettes of customers sitting and staff moving around from outside."

    stop music
    scene bg yamada night
    play music "happy_hangout_loop.mp3"
    show kevin serious crossed open at leftish
    show ai smile at rightish
    with bg_change_seconds
    "Kevin slides the door open and lets Ai and myself go in first. A wave of mouth-watering smells assaults my nose, and my stomach rumbles in response."
    "Kevin and Ai walk in like they have been here a thousand times before. This place must make a mint off college students alone."
    "A tired, older waitress drops us off at a table by the wall. I am placed across from my two new acquaintances."

    show kevin neutral sides open
    show ai smile
    with config.say_attribute_transition
    Ai "So. Shun. What’s your story? Where are you from?"
    Shun "Uh, my family lives in Tokyo."
    Ai talk "Oh! Another city dweller, just like me."

    show ai proud with config.say_attribute_transition
    "Our waitress drops off a pot of tea and takes our order back to the kitchen."

    show ai smile with config.say_attribute_transition
    "Ai holds out her cup and Kevin pours some tea for her, before pouring some for me too. He doesn’t pour himself any."
    Ai "Ah! Everything here is just fantastic."
    Kevin "How can you drink that stuff all the time? It’s just leaf juice and water, it doesn’t even have sugar in it."

    Ai proud "I know! Isn’t it lovely?"

    show kevin smirk crossed open with config.say_attribute_transition
    "Kevin rolls his eyes."

    Ai talk "Shun, what’s Tokyo like? Do you like it there? Is it as great as the movies make it look?"
    Shun "Umm … let me think. My home was okay. Tokyo is really big and crowded. I’m actually glad to be away from it right now."
    "I struggle to answer. Meeting all of these new people is exhausting. I didn’t want to be rude but I have barely enough energy to keep a stiff upper lip after all that’s happened today."

    Ai smile "I can relate. That’s why I picked a school that’s not in a big city. Though this one reminds me enough of home to be comfortable, without being too familiar."

    show ai proud at rightish
    show kevin neutral sides open at leftish
    with config.say_attribute_transition
    "She nudges Kevin with her elbow."

    show ai talk with config.say_attribute_transition
    Kevin serious crossed open "Cut it out!"

    show ai proud with config.say_attribute_transition
    "Ai smirks and shoves his arm. Kevin only pouts back."
    Shun "… I see. Where are you from, Ai?"

    show kevin smirk sides open
    show ai talk
    with config.say_attribute_transition
    Kevin "Just outside of Osaka. Born there, raised there and proud of it." (multiple=2)
    Ai "Just outside of Osaka. Born there, raised there and proud of it." (multiple=2)

    Ai pout "Hey!"

    Kevin laugh sides closed "You say those precise words to everyone. You don’t see me telling everyone I’m American."

    Ai yell "That’s because they are painfully aware of it."

    Kevin serious crossed open "That’s because … I … um, yeah. I guess …"
    "Kevin backs down. It’s weird … after all his talk about being great with girls, Kevin’s having trouble with this one."

    Ai proud "Shun, if you ever catch Kevin giving out advice, do yourself a favor and go deaf for a few moments.{w} Something he says might accidentally sink in, and from that point on it’s a slippery slope to doom and ruination."
    Shun "Kevin’s been fine so far. Very helpful."

    Ai talk "I’m sure. But regardless, all you’ll need is a few weeks to get the hang of this place. It’s quite fun once you have a group of friends."

    Kevin neutral sides open "Oh! While we’re here.{w} Shun’s already a member of our Super Secret Special Club."

    Ai smile "Oh! You’re in the Eisai Society too? So am I! Both of us are."
    Shun "Great. But honestly … I’m just happy to be here right now."
    "{cps=*1.5}Please don’t let them ask me about my parents please don’t let them ask me about my parents please don’t let them ask me about my parents …{/cps}"

    Kevin serious crossed open "Yeah, yeah, modesty and all that. You can add it on your résumé later, it’s not a bad perk."

    Ai pout "… Well, that’s one way of putting it."
    Ai smile "Practically though, a lot of Eisai Society members end up using it as a means to keep in touch after they graduate. So it’s a good way to stay in contact  with old friends!"
    Ai "College isn’t like high school. Socializing is easier, but meeting people can be a bit tricky in your first year."
    Ai "If nothing else, you can think of Eisai as a way to meet new friends! People to make study groups, hang out with …"

    Kevin smirk sides open "… Hook up with …"

    Ai yell "Oi! Eisai Society isn’t a dating club!"

    Kevin serious crossed open "No, not offically."
    "The waitress returns, carrying our drinks and a big tray of food. A giant bowl filled with shrimp and crab, two plates of freshly-rolled sushi, leeks and onion flakes swimming in a pond of savory pork broth and thin, curly noodles."

    "I have a taste of my order and immediately my tongue lights up with flavor … this is probably some of the best ramen I’ve ever had in my life."
    Kevin smirk sides "Not bad right?"
    "I slurp down my mouthful and nod enthusiastically."
    Kevin "Heh. I knew you’d like it, everyone likes it here. I come in at least once a week. They have specials here for students, so it’s pretty affordable."

    "Kevin proceeds to gobble down his food, while Ai eats with somewhat more poise and decorum."
    "This isn’t what I expected at all. I’d expected a school full of nerds and overachievers, but these guys seem … welcoming.{w} Well … with the exception of Hideki."
    "They’re really nice to the new guy. Even if it’s just over a bowl of broth and noodles."

    scene bg yamada_street night
    show kevin neutral sides open at leftish
    show ai pout at rightish
    with bg_change_seconds
    window show
    "The staff thanks us for our patronage as we step out. {w}Kevin waves to one of the prettier waitresses, shooting her a wink and a smile. She giggles."
    Kevin "Ahhh. I love that place. I wish they had more places like that one. Yamada should expand into a franchise or something."

    Ai smile "Nah. Hidden treasures are better when they are rarities. It wouldn’t be special if it wasn’t unique."

    show ai yell with config.say_attribute_transition
    "Ai throws her hands up into the air and lets out a yawn."

    Ai pout "Bah. I’m sooo tired. I need to get my sleep schedule sorted out before classes start."
    Kevin "You think you have it bad? I’m still jet-lagged over here. Imagine jumping twenty time zones."

    stop music
    scene bg inou_gate night
    play music "decompression_loop.mp3"
    show kevin neutral sides open at leftish
    show ai smile at rightish
    with bg_change_minutes
    "They talk about how exhausted they are the whole way back. We stop at the girl’s dorm to drop Ai off."
    Ai "Well that’s it for me. I’m going to take a ten hour nap and enjoy sleeping in for one more day."
    Kevin "Kay. I’ll be seeing you."
    Ai "Right. Nice meeting you Shun. Remember what I said about listening to this one."

    hide ai with config.say_attribute_transition
    "She shoves a finger into Kevin’s chest before turning around and walking into her dorm. Kevin smiles as he watches her leave."
    Kevin "Heh. She’s cool, ain’t she?"
    Shun "I honestly can’t tell if you guys hate each other or not."
    Kevin laugh "Hate? Nooo, we’re thick as thieves. We’re just playing around."
    scene bg inou night
    show kevin laugh sides open at leftish
    with bg_change_seconds
    Kevin "Ai and me are friends because of Eisai Society. We don’t have meetings but we find the time to hang out anyway."
    Shun "Is the Eisai Society monitored by the school at all?"
    Kevin smile "The school always tries their damnedest to get members of Eisai to come to their events. We make the school look good so they grease our wheels a lot. It’s totally stupid and really unfair to the other kids going here."
    Kevin "Ah, but forget it! It’s no big deal, you’re in the club anyway. Don’t worry about it man, you are one of us."
    scene bg inou night
    show kevin neutral sides open at leftish
    with hpunch
    "Once again, he pats my back hard enough to almost knock me down. My less-than-decent suspicions of him from earlier fade away as he assures me I am part of his group."
    hide kevin with config.say_attribute_transition
    "I don’t feel like I am truly part of anything yet. But hearing someone say that I belong after years of being alone felt … good."
    Shun "If you say so."
    "I politely acknowledge what he said, trying not to look too excited."

    return
