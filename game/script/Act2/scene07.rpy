##-------------------------
## Act 2: Maryanne
## Scene 7: After the movie
##-------------------------

label a2s7:
    stop music
    scene bg dorm with bg_change_minutes
    show kevin frown crossed open at right:
        linear 0.5 left
        pause 0.5
        linear 0.5 right
        pause 0.5
        repeat
    play music "bickering_loop.mp3"
    "Kevin is scurrying from one corner of the dorm to the other like an excited puppy, tossing pillows and blankets all over the floor."

    Shun "… Did you lose something?"

    show kevin at center with move
    Kevin "Have you seen my sunglasses? I swore I left them … somewhere. Where the hell did I put those things? They were right here!"

    hide kevin with config.say_attribute_transition
    "He grumbles to himself about clothing accessories under his breath as he ducks into the kitchen to look there."

    "Hideki walks in and, upon seeing Kevin digging through the kitchen drawers, quickly shoves something into his back pocket."

    show kevin frown sides open at leftish
    show hideki chin gendo at rightish
    with config.say_attribute_transition
    Hideki "Looking for your missing bone, Dog?"

    Kevin serious "I’m looking for your mom’s phone number! And also my sunglasses. You know, the cool ones with the silver rims. Like the ones from Fear and Loathing but the colors are reversed?"

    Hideki hips bored "Oh my. Call campus security. A piece of fake silver has gone missing."

    Kevin mad crossed "I bought those back home man! They’re a piece … wait a minute … you have them, don’t you?!"

    Hideki armsup argue "Wh—what?! W—Why would I need a pair of your ugly sunglasses? I have my own glasses and they actually—"

    Kevin "You lying little shit! You totally stole them!"

    Hideki "Were you not listening to me just now?! I have my own and would never sink so low as to—"

    Kevin "Okay, here’s what’s gonna happen! I’m going to go into my room, count to thirty, and if my sunglasses just {em}happen{/em} to magically reappear before I come back out … we’ll let this go."
    Kevin "If they are not here, I’ll start looking in your room. And maybe I’ll log onto your precious PC while I’m in there and start pressing buttons."

    Hideki chin gendo "Ha! Fool! You don’t even know the passwor—"

    Kevin "Musashi."

    Hideki crossed pissed "…"

    Kevin "If I can figure out how to pronounce your crazy as hell last name, Ayanokoji, I can figure out that your password is based off your historical man-crush."

    "He taps the side of Hideki’s head with his fingertips to taunt him. Hideki looks like he is about to have an aneurysm."

    Hideki armsup argue "Hands off Dog!"

    Kevin sides "I’m watching you, Gremlin."
    show kevin:
        linear 8.0 xpos -1.0
    Kevin "I’m. Watching. You."

    "Kevin slowly backs into his room, not breaking eye contact with Hideki. His pointing finger moves from under his eyes and point to Hideki."

    Kevin "Watching …"

    "He slowly shuts his door."

    Kevin "… You …"

    show hideki crossed pissed
    hide kevin
    with config.say_attribute_transition
    "Hideki looks at me sourly as Kevin disappears, like somehow this was my fault! He grabs my wrist and places the missing pair of sunglasses in my palm."

    Hideki "I’ve got to go change my password. Do {em}not{/em} disturb me."

    hide hideki with config.say_attribute_transition
    "Hideki quickly runs into his room and loudly slams his door shut. I hear him cursing profanities behind his door."
    "Kevin reemerges from his room and chuckles."

    show kevin smirk closed sides with config.say_attribute_transition
    Kevin "Oh wow, look! My sunglasses! They just reappeared. Ain’t that something! It’s a gosh-darn miracle."
    Kevin open "Whatever. I’m already over it. Wanna play a game? My trigger finger is itchy."

    stop music
    scene bg dorm with bg_change_hours
    play music "decompression_loop.mp3"
    "Kevin’s favorite fighting game depletes an hour out of the evening before we realize it. I notice he has a fondness for the super strong characters with low defense. I beat him a few times with my balanced character, but his advantage of being the owner of the game prevails."
    "We play in relative silence, except for an occasional chatter between rounds."

    Shun "Hey uh, so … Maryanne and I went out today."

    "Kevin’s character makes a mistake and loses half his health."

    show kevin neutral crossed open with config.say_attribute_transition
    Kevin "What, like … {em}out{/em} out? Like on a date?"

    Shun "I think so. Yeah."
    Shun "She had fun. She told me she had fun."

    "I wait for Kevin to make a sexual innuendo or spit out advice on how to put on a condom properly. But instead, he waits for me to say more."

    Shun "Uh, it was kind of on impulse. But it was cool."

    Kevin "… Okay."

    Shun "Okay?"

    Kevin "Yeah."

    Shun "That’s it? That’s all? Just an okay?"

    Kevin "What do you want me to do, a magic trick?! Just see how it goes. You two aren’t officially dating … are you?"

    Shun "We went out just once, so … I don’t know. I don’t think so …"

    Kevin "Cool. Just play it cool."
    Kevin "…"
    Kevin serious "… Be nice to her, okay?"

    Shun "Huh? O—of course. Why wouldn’t I be nice to her?"

    Kevin neutral crossed closed "I know you will, but, it’s just … look, if this was another girl, I’d be talking about her boobs and legs and all the whatnots."

    Kevin open serious "But Maryanne … she’s … you know … a friend. So be nice to her. Okay?"

    Shun "Y–yeah. Sure man."

    Kevin smile "Good. I’m gonna hold you to that."

    hide kevin with config.say_attribute_transition
    "The next round starts up."

    return
