##--------------------
## Act 2 "Maryanne
## Scene 6 "Movie Date
##--------------------

label a2s6:
    scene bg class_okada with bg_change_days
    stop music fadeout config.fade_music
    "Okada hands back yesterday’s pop quiz. One by one, disappointed groans and relieved sighs spring up among his students. I know some of the students are just getting by. Risa is doing okay, at least that’s what she says."
    "Okada hands me back my test. … 82\%. I want to do better … but for now, that is enough. Thankfully, Maryanne is still willing to help me when I need it. As long as she doesn’t overdo it, like she promised."
    "I wonder if she’d be able to keep said promise."
    "Okada openly goes over the test, question by question, so that everyone knows exactly {em}why{/em} their answers were right or wrong. And, like Risa, who is spacing out as usual, my mind goes elsewhere."

    scene bg lab_maryanne with bg_change_minutes
    "I hurry out of there when class ends, rushing to the lab. Maryanne is, as usual, there already, moving stacks of textbooks from one end of the room to the other, three at a time."

    show maryanne happy coat with config.say_attribute_transition
    play music "romantic.mp3"
    Shun "Do you need help?"

    Maryanne "Hmm? Oh, no. I’m just cleaning the mess the science club left. Don’t mind me."

    Maryanne "How did the quiz go?"

    Shun "I got my test back, an 82. I have an 84 average right now. But that average will go up when the midterm comes, mark my words."

    Maryanne ecstatic "That’s the spirit! Do you want to start studying for the midterms now? Better to start early, right?"

    Shun "Okada-sensei hasn’t gone over everything that will be on it. I think I will stick to homework today. Rain check though."

    show maryanne happy with config.say_attribute_transition
    "Maryanne nods and finishes moving the last two stacks. Then sits back at her usually spot, pulling out a marble notebook and pen."

    Shun "They should pay you overtime for all the work you do in this place."

    Maryanne embarrassed "Tell me about it. Heh."

    show maryanne ecstatic with config.say_attribute_transition
    "I pull a bag of candy out of my backpack. I sit down and put the bag on the table in between us, with the opening facing Maryanne. She starts eating, smiling ear-to-ear, as I sit down next to her."
    "The way Maryanne is acting is far cry from the way she was a few weeks ago. She was a high-spirited, optimistic beam of sunshine, practically a different person now. Not that I was complaining, but the juxtaposition was a tad jarring."
    "Maybe she’s just happy to be studying, happy to distract herself from missing her dad or something sad. Maybe she’s using this lab to hide for a while."
    "I think of how my roommates watch movies so often. I remember that mom drank when things got hard. They were all ways to cope with something. Boredom, stress, solitariness … that was why Maryanne would sometimes drown herself in work."
    "I skim through my notes as Maryanne pokes her nose into her thick textbook."

    Shun "… You really come here every day? Regularly?"

    Maryanne embarrassed "Pretty much. I don’t have much of a social life during the week."

    Shun "What about on the weekends?"

    Maryanne uncomfortable "I’m … you know … sometimes Shione and I go out. You know, when she’s … not in the diving club. So, I’m uh, usually here till noon or one or two or … sometimes."
    Maryanne uncertain "I mean … Shione is a good friend but … she’s just as busy as I am. Kevin is always running around with girls or bar crawls or he’s with his weird friend so I don’t see him as often as I’d …"
    Maryanne happy "… It doesn’t matter. Even if Kevin and Shione were free more often, I’m not very good at socializing. I get … nervous."

    Shun "Do you? Really? Remember that night at the sushi place? You had a lot of fun."

    Maryanne neutral "That’s because I was with people I knew."

    Shun "You’re shy?"

    Maryanne uncertain "I’m … I mean … I’m—"

    Shun "You’re … what?"

    Maryanne sad "… Never mind."

    "Rap. Rap. Rap. Rap. Rap. Her fingers dance over the table as her eyes dart back and forth between me and her notepad. Then she starts writing again."

    Shun "What are you working on?"

    Maryanne happy "St—studying. For a midterm. I am rewriting class notes."

    Shun "Re-writing?"

    Maryanne "It’s a great way to memorize something. Write it down over and over again until it sticks. Works like a charm for me."

    Shun "Even though you study all the time, you still rewrite your notes?"

    Maryanne "Actually, most of my studying is me copying my notes. That takes up a lot of time, but it is worth it. I try to do at least one rewrite on all topics and—"

    Shun "Whoa, whoa, what? You rewrite all of your notes? So, twice in total?"

    Maryanne confused "… Y—yes?"

    "We say nothing for a few seconds. Maryanne coughs and goes back to writing in her notebook."

    Shun "Uh, you know … you don’t need to study, right? I mean, if you’re staying here to help me …"

    Maryanne happy "Ha, no. It’s … fine. I told you, I wanna finish writing my notes."

    Shun "Okay …"

    "She jots down a few more things, quickly, her handwriting becoming noticeably sloppier as she hurries through, what seems to be, the last page. She stabs the page harshly to put the final period on the page and, smiling proudly, puts her pen away."

    Maryanne ecstatic "There … I’m all caught up. See?"

    Shun "That was it?"

    Maryanne "That was it! I did most of it before you got here."

    Shun "Cool! Okay, well … I guess we both don’t have work right now."

    Maryanne neutral "Nope. I’m … all caught up for now."

    "The room gets very quiet as we both stand there. Maryanne’s proud grin gets gradually weaker, bit by bit, stopping only when she clears her throat."

    Maryanne uncomfortable "There’s … this movie I wanted to see …"

    "A voice in my head screams “take the hint” so loudly that it gives me a migraine."

    Shun "You wanna go?"

    Maryanne ecstatic "Sure! If you want to go, I’ll go!"

    Shun "Yeah, that sounds like fun. What movie—"

    Maryanne happy "I have to stop at my room first though."

    hide maryanne with config.say_attribute_transition
    Shun "Oh, yeah, Oka—"

    "Maryanne is already at the laboratory door before I can stand up."

    stop music
    scene bg inou with bg_change_minutes
    play music "decompression_loop.mp3"
    "Maryanne slows down after she realizes she was walking a bit faster than normal."
    "She stops me right outside the girls’ dorms and tells me she will be out in a minute. I sit down on a bench next to the entrance, in the quietest part of campus. And watch the few people pass by as I wait. The teachers, the athletes … the young couples."
    "It is hard to ignore all of the young couples."
    "They always smile, at least in public. They smiled a lot. They always looked … safer together. Like fewer things could hurt them or make them feel scared."
    "As I watched them go back and forth … I never noticed how couples seem to talk with just their eyes until just then."
    "Maybe because my parents never did that."

    show hideki chin think with vpunch
    Hideki "Quite an odd display, isn’t it?"

    play music "bickering_loop.mp3"
    "I jump back as Hideki speaks up. He is suddenly sitting next to me, somehow having crept up beside me without me noticing."

    Shun "When did you get here?"

    Hideki glasses adjust "We are everywhere Shun. We know everything. We are part of the same collective."

    "Hideki strikes a pose, smirking at his own “joke”. Then points to a boy and girl holding hands."

    Hideki armsup argue "As I was saying … it is a strange display. Humans tie themselves to a member of the opposite sex, usually multiple times in most instances … and for? A chance for reproduction? Physical pleasure?"
    Hideki "These common folk waste so much time and energy on mating rituals … energy that could be devoted towards more noble pursuits."

    Shun "This sounds like something you have said before. Like, almost exactly. So since I heard it already, I think I’m gonna go and—"

    Hideki "If we redirected half the enthusiasm we spend chasing each other around towards finding, say, a solution to world peace … we could put an end to war altogether. Of course, once all war was ended, the common folk would celebrate by fucking."
    Hideki chin gendo "Miserable single-minded fools! That is why I should be in charge!"
    Hideki "Look! Look at how silly they all look. How … blissfully imbecilic they are with their frivolous distractions."

    Shun "That “distraction” is called “happiness” Hideki."

    Hideki hips bored "There are many names for it. It all comes down to wasting time."

    Shun "Yeah, this all sounds really familiar …"

    "Trying to escape, I try to inch away from him. Hideki only inches closer to me as his rant continues."

    Hideki chin think "And speaking of hypnotically overpowering … women."

    Shun "Women?"

    Hideki chin gendo "Women!"

    Shun "You don’t like women?"

    Hideki chin think "I appreciate the idea of women."

    Shun "What does that even mean?!"

    Hideki crossed grin "Oh Shun … I would like to say that there is hope for you, but some days … I am not so sure."
    Hideki crossed pissed "The things the females of our species do for attention … preposterous! Intelligent women will prance around like circus ponies in the name of, survey says … {em}ding-ding{/em}! “Love.” Bleh!"

    Hideki "Look at them. Just look at them!"

    "I look at the girls he villainizes and see … short skirts, combed hair, big smiles. And Hideki was just “looking” at them. I don’t think he is “seeing” what other men see."
    show hideki crossed glare with config.say_attribute_transition
    "Hideki fixes his glasses, which had fallen crookedly over his nose during his rant. Then crosses his arms, saying “I rest my case” with his body."

    Shun "Are you gay?"

    Hideki hips bored "No. But I appreciate the efficiency of such a lifestyle."

    Hideki "On a related note … you have fallen for the simple snares of the opposite sex as well, I see."

    Shun "Fallen?"

    Hideki "Are you or are you not about to go on a date with the mad scientist with the golden hair?"

    Shun "How did you know we … were you following us?"

    Hideki glasses adjust "I was keeping tabs on you. For your own safety."

    Shun "Please don’t do that in the future. And it’s not a date."

    "… Was it?"

    Shun "We’re just going to hang out a bit."

    Hideki chin think "You seem to “hang out” with her a lot."

    Shun "Sure, but I hang out with you and Kevin a lot too."

    Hideki "… That is true."
    Hideki "Are {em}you{/em} gay?"

    Shun "No! And Maryanne and I are friends."

    Hideki chin gendo "So … if you had the opportunity to see your blonde “friend” naked, would you pass on it?"

    Shun "… That’s not a fair question!"

    Hideki chin think "Of course it’s not. {em}sigh{/em} Of course it’s not. You let me down again Mr. Takamine."

    Shun "Look! Maryanne is … whatever man, she’s interesting."

    Hideki crossed begrudging "Interesting?"

    Shun "Yes! Interesting! She’s got interesting—why am I even … what do you want Hideki?"

    Hideki "If you want interesting things, you could partake in an afternoon indulgence. I would like to go the bar for a quick happy hour treat and would not like to drink alone. The Dog is busy watching, in his words, “an orgy of CGI and explosions”."

    Shun "For your information, I’m going to see a movie now. I don’t know how long I’ll be out … start without me."

    Hideki crossed glare "I see … and how long should we wait before we are to assume you are not coming back?"

    Shun "Um, I mean, I will come back."

    Hideki "Are you sure? Are you sure you’re not going to sleep in someone else’s bed tonight?"

    Shun "Stop doing that!"

    Hideki crossed grin "Doing what, Shun?"

    "He shrugs mockingly."

    Hideki crossed glare "Claiming you are “just friends” is exactly what someone would say if they were more than just friends."

    Shun "It’s also what people would say if they were just friends and that is all they are."

    Hideki "That is exactly what someone would say to cover their tracks."

    Shun "Stop it!"

    Hideki glasses adjust "Denial is the first sign of being in denial Shun."

    Shun "… What?!"

    show hideki glasses adjust at leftish
    show maryanne happy hairdown at rightish
    with config.say_attribute_transition
    Maryanne "I’m ready."

    show maryanne neutral with config.say_attribute_transition
    "Oh thank god, Maryanne is back."
    "She looked excited … until she saw my roommate."

    Maryanne uncomfortable "Oh, uh … hello … Hideki. How are you today?"

    Hideki hips bored "I’m … adequate."

    Maryanne uncertain "That’s … good?"

    Maryanne confused "… Shun and I were about to go out to the mall. You’re not coming, are you?"

    Hideki "Sorry, but I unfortunately have some things I need to take care of for tonight. And they involve various unspeakable acts that I will discuss with Shun … tonight."

    Maryanne neutral "… Good luck with all of that?"

    Hideki armsup argue "Luck? Luck was never an option!"

    hide hideki
    show maryanne uncomfortable
    with config.say_attribute_transition
    "Hideki flips out a thick a pair of dark sunglasses and walks away."

    with hpunch
    play sound "198876_bone666138_crash"
    Hideki "Dammit! How does Gallagher see in these things!"

    "At last, he is out of sight. Maryanne looks uncomfortable as she watched him go."

    Shun "Sorry about that guy. He just kind of … shows up sometimes."

    show maryanne happy at center with config.say_attribute_transition
    play music "happy_hangout_loop.mp3"
    Maryanne "Thank you so much for not inviting him. I know he is your roommate, but he … gives me the creeps."

    Shun "Me too. He’s got a lot of hang-ups. He’s really weird around women."

    Maryanne confused "Is he gay?"

    Shun "No, but he appreciates the efficiency of that lifestyle."

    Maryanne "… Uh …"

    Shun "Let’s just go."

    Maryanne happy "Yes, please!"

    "I guide her out of campus, looking around for Hideki, just in case. When I am sure he is not there, I stop walking so quickly."

    Shun "To the mall!"

    Maryanne ecstatic "To the mall!"

    scene bg mall with bg_change_minutes
    "The mall appears before I could say “we should go that way”. … Not that I was lost or anything."
    "Maryanne still has her hands wrapped around my arm. She pulls me to go inside, wanting to explore. I wasn’t sure if she was happy to be there with me, or if she was just happy to be there. But she did seem eager."
    "The first floor of said mall has toxic levels of estrogen. Two shoe stores, four clothes shops, a Hello Kitty kiosk next to a luxurious Confectionery Factory. Best of all, at the end of the strip there is a pet store where you could come in and pet the puppies!"
    "Though she does not buy anything, Maryanne spends a good minute window shopping and admiring the things she wants. I join in only when we get to the puppy store."
    "The second level was the counterbalance to the floor below it. It had a bowling alley and a suit store on opposites sides of the elevator, and other commodities that men like stretched down the way."
    "An arcade is tucked away in a far corner, with the movie theater adjacent to that. A big digital sign above the entrance to the movie theater shows the show-times and which film is playing."
    "Maryanne points to said sign while failing to hold back her enthusiasm."

    Maryanne "Look! The next showing is at 5:35!"

    Shun "Alright, alright, we’ll get that one. Wanna go get some of those candy swirl things back near the entrance?"

    Maryanne ecstatic "Sure … maybe a few other things too …"

    scene bg theater
    show maryanne happy hairdown:
        xalign 1.0
        pause 2.0
        linear 0.5 yanchor -0.2
    with bg_change_minutes
    "Maryanne and I sneak to our seats just as the trailers start, juggling four boxes of chocolate covered popcorn and a bag of marble candy. I had a hunch “a few” snacks were not going to be enough for Maryanne."
    "There are maybe a dozen other people scattered throughout its seats. Most are college students like we are, but I hear a few mothers telling their kids to sit still and be quiet."

    Shun "So, what is this movie about?"

    Maryanne ecstatic "It’s about two siblings that find a crashed spaceship in their backyard. And they have to hide it from their dad."

    Shun "Is it a kid’s film?"

    Maryanne happy "I think it was made that way, but I don’t care. I love sci-fi stories. Even when I was a kid—"

    Anon "Shhhhh!"

    Maryanne embarrassed "Sorry."

    hide maryanne with config.say_attribute_transition
    "As the first trailer starts, Maryanne leans close to me and whispers. I assume as to not bother anyone again."

    show maryanne happy hairdown at right with config.say_attribute_transition
    Maryanne "Pick a number, one through five."

    Shun "What for?"

    Maryanne "It’s a game I play whenever I go to the movies. You pick a number … like, I’ll pick five … and if the fifth trailer shown is the best out of all of them, you win."

    Shun "Oh. Okay, I pick number one. What do you get if you win?"

    Maryanne ecstatic "It’s just for fun. They make so many commercials these days … I try to find ways to get through them."

    "The first trailer starts up. It advertises an alien invasion movie that takes place in the ocean and giant robots are involved somehow."

    Shun "That looks okay."

    Maryanne happy "I don’t think I’d see it."

    Shun "Why not?"

    Maryanne confused "A portal from the middle of the ocean? Please. Water would flood in and drown their world. And giant robots? You know how easily their own weight would crush themselves?"

    Shun "Uh … it’s just a movie. It’s not real, you know?"

    Maryanne worried "… I—I know that … I just wanted to make sure you know that."

    show maryanne happy with config.say_attribute_transition
    "The second trailer starts. It is a samurai film that took place in Nobunaga’s court, a story of betrayal. I sense an unneeded romantic subplot."

    Shun "Hideki would like that one."

    Shun "By the way, how do you know him? How’d you two meet?"

    Maryanne mad "Ugh … everyone knows him …"

    "Maryanne suddenly looks ill. And stuffs her face with chocolate popcorn, making her physically unable to talk about it."
    "The third trailer begins. It announces a classic Kaiju movie being re-released in theaters for a limited time only. In 3D! The trailer made it look like something children and parents could see together."
    "A smile crawls on my face. I remember how my dad would take me out to the movies on my birthdays … every year until I turned thirteen."

    Maryanne stressed "Tch …"

    "Maryanne scoffs like a stuffy film critic, not seeing the old school charm that I saw."

    Maryanne "How could an animal ever get that big? Even at half that size, it wouldn’t be able to move. It would have too much mass. Its bones would snap under the weight of its own muscles."

    Shun "Um … they do move very slow …"

    Maryanne "And what do giant monsters even eat? Maybe whales or giant squid, but they’d need to eat all day just to stay alive. Existing would cost them about a million calories an hour, and who knows how many calories they’d need to fight other big monsters."
    Maryanne neutral "They’d have to eat a year’s worth of a country’s food in a week to not starve."

    Shun "I … never thought …"

    Maryanne stressed "Where do they sleep?"

    Shun "I … what? Sleep?"

    Maryanne "They have to sleep, right? Why doesn’t the army just wait until they go to bed? It’s not like they are going to lose track of it. Just follow it when the sun is going down and shoot it when it goes to bed."
    Maryanne "Or shoot it so it doesn’t get a chance to go to bed and exhaust it."

    Shun "… That … kinda makes sense."

    Maryanne confused "And I hate when they build a giant robot to fight the giant monster. That would be impossible to maintain! It would fight for maybe a minute before something broke. Not to mention cost and resources. And there is still the problem with its weight."

    Shun "The … trailer is over you know?"

    Maryanne "Oh, and the whole “laser beams out of the eyes” thing … yeah, you’d be blind while firing."

    Shun "Maryanne. The trailer. It’s over."

    Maryanne uncomfortable "I … o—oh. Sorry. I sometimes get a little caught up with theories."

    Anon "Shhhhhh!!"

    Shun "Sorry." (multiple=2)
    Maryanne "Sorry." (multiple=2)

    "The fifth trailer starts. It starts with an abandoned house at night. Scary music plays. Bolts of lightning crash in the sky behind the ominous home. There are pictures of ghosts in the windows of the house. A dead girl with long black hair crawls across the floor."
    show maryanne worried with config.say_attribute_transition
    "Maryanne covers her eyes through the entire trailer, even though one eye pokes through her slender fingers. She jumps when the dead girl looks at the audience."

    Shun "Are you okay?"

    "Palms over her eyes, she shakes her head. She shivers and trembles with her hands covering her face. Her fingers practically dig into her forehead to keep her eyes covered."

    Shun "Are you scared of ghosts?"

    Maryanne "… Is it over?"

    "The trailer ends. Maryanne peaks through her fingers as the “Feature Presentation” card fades in."

    Maryanne worried "Thank god. I don’t like ghosts."

    Shun "You believe in ghosts?"

    Maryanne uncomfortable "No. But in movies, ghosts are always real. And they’re really scary."

    Maryanne mad "Don’t ever scare me with ghosts. I swear, if you or anyone pulls a ghost-related prank on me ever, we’re not friends anymore."

    "I nod and make a mental note to not tell Kevin about her phobia."
    "She calms down as the film starts up, repeating whatever her English mantra was a few more times as the screen goes black."

    stop music
    scene bg takuya_bridge overcast with bg_change_hours
    play music "romantic.mp3"
    "The sky is dark by the time we leave the theater. Black clouds on one side of the horizon, a burning orange sun just barely peeking just past the skyline."

    "Maryanne raves continuously about the film, almost skipping home as she munches on the last bag of candy."

    show maryanne ecstatic hairdown with config.say_attribute_transition
    Maryanne "… And the alien looked pretty real … except for the ice breath thing. You know how implausible that is? And then there was the ship, which looked great, but there is no way a ship like that could leave the atmosphere. It was shaped too … it just wouldn’t work."

    "She thankfully didn’t talk during the film, despite warnings from my roommates. Maybe it was the threats from the man behind us that got her to stay quiet."
    "We cross an old stone bridge over a river, taking the scenic route home. Maryanne stopped walking in the middle of the bridge and looked over the edge, giggling as she watched the water run underneath us."
    "She inhales as a breeze picks up. The wind makes her hair dance and pulls her perfume into my nostrils."

    Maryanne happy "I used to come here whenever I really needed a break. I came here a lot during my first year."

    "She stares at her reflection in the rippling waves. I look over the edge with her. She smiles, and moves in a little closer to me."

    Shun "Admit it. Playing hooky is fun."

    menu:
        Maryanne "Yeah. It was. I had a good time."
        "I had fun too.":
            Shun "I had fun too."
            "She nods, her smile beaming."
        "Well … you can pay me back whenever.":
            Shun "Well … you can pay me back whenever."
            Maryanne ecstatic "Ha. You can send me the bill."
            "I laugh weakly at her response. Oh, my poor wallet …"
            "… Well, I’d have to think of something cheaper next time."

    scene bg inou_gate night
    show maryanne happy hairdown
    with bg_change_minutes

    Maryanne "Well … I need to …"

    Shun "Finish some work, right?"

    "She says nothing for a second. She glances at her shoe quickly, then back at me. Her smile weakens and becomes a nervous half grin."

    Shun "You okay?"

    Maryanne "Uh … I’ll … see you later then?"

    Shun "Okay. I’ll see you when I see you."

    Maryanne "Okay. Good night."

    Shun "Good night."

    Maryanne "O—okay. Bye!"

    hide maryanne with config.say_attribute_transition
    "She hurries off. As I walk away, I look to my left, then my right. Hideki is (thankfully) nowhere to be found."

    return
