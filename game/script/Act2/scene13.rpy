##------------------------
## Act 2 "Maryanne
## Scene 13 "Phone number
##------------------------

label a2s13:
    stop music
    scene bg room_shun with bg_change_days
    play music "decompression_loop.mp3"
    "I stayed cloistered in my room for a few hours, reading through a plethora of scientific facts and equations as I try to absorb something from Okada’s most recent lesson. Though despite wanting to study, I can’t stay focused."
    "Last night felt more … important than school work."
    "It was like there was a magnet in my brain, one that would pull my thoughts back to Maryanne every few seconds. The way she’d smile, how she’d scold Kevin, how she smelled or how her hair flicked around when she walked fast …"
    "And yet somewhere in that soup of happy thoughts,  there was the persistent, condescending voice that kept repeating itself, like a thorn poking my rib …"
    "“What if you screw up?”"
    "Then I’d go back to trying to study and that weird cycle would repeat."
    "Eventually, I give up on studying, close my books … and leave."

    scene bg hall_dorm_girls with bg_change_minutes
    stop music fadeout config.fade_music
    "I sneak past the front desk of the girls’ dorms while the receptionist is distracted. I fix my hair as I hurry to Maryanne’s room, though I do not have a mirror."
    "Without overthinking it, I knock twice. No response. The condescending voice chimes in and says that that is probably for the best."
    "But just as I am about to leave, the door creaks open."

    show maryanne confused with config.say_attribute_transition
    Maryanne "Hello?"

    show maryanne happy with config.say_attribute_transition
    Shun "{em}cough{/em} Hey."

    "My voice deepens without me meaning to. I only realize how stupid I sound after the words come out."

    Maryanne "Hehe, hey."

    play music "romantic.mp3"
    Shun "So … I forgot to get your phone number the other day."

    "Maryanne steps back and lets me in."

    Maryanne "Well, I can give it to you now. If you’d like."

    "I step inside."

    scene bg room_maryanne
    show maryanne happy
    with bg_change_seconds
    "I feel my face get hotter when Maryanne closes the door behind us. She smiles at me, and I feel … nervous. I try not to gulp, try not to say something stupid. So … I say nothing. A few seconds pass and the longer I say nothing, the longer she says nothing."
    "And the longer nothing happens, the louder that doubtful voice shrieks at me."
    show maryanne embarrassed with config.say_attribute_transition
    "Maryanne fiddles with her fingers, and looks down at the floor. Then back up at me. And softly bites her lip. My nervousness goes away really fast."

    Shun "How about … I get your number later. Okay?"

    Maryanne happy blush "O—okay."

    scene bg black with Dissolve(2)
    "I gently take Maryanne’s hand and move in a little closer to her. And kiss her. Quickly at first. Then she kisses me, a bit slower."
    "She touches my shoulder. My knees feel more fragile, like I am going to fall over."
    "I force myself to kiss her slowly, or maybe faster? I try one then the other, trying not to move too fast … or too slow. I can feel her ample chest pressing on mine and I take a chance."
    "She kisses me again. I keep rubbing her chest but softer, just enough for her to make that really cute sound. I keep rubbing her chest."
    "She claws at my back a little, and starts breathing heavily. I rub her chest more. She grabs my butt at one point and it makes me jump."

    scene bg room_maryanne
    show maryanne embarrassed
    with Dissolve(1)
    Maryanne "Uh … there … are other places to touch you know."

    "She laughs nervously. I am found out! She knows I am new at this! My head races as the voice of doubt starts to win the tug of war. It causes me to choke and stop moving. And I feel guilty again for being … a bit behind."
    "But then Maryanne starts shaking too."

    Maryanne "It’s … it’s okay Shun. You don’t … uh, there’s nothing wrong with … being … that."

    Shun "… U—uh …"

    Maryanne neutral "Do … you want to sit down?"

    Shun "S—sure."

    "I nod after stuttering. She nods back. And we both do nothing for a moment."

    scene bg black with Dissolve(1)
    "She sits down first. I promptly do the same."
    "Maryanne moves in closer to me, our hips touching. I touch her knee and kiss her again. It’s easier this time. I try to be careful, and not squeeze too hard when I grab her thigh. And eventually, trying not to take too long this time, I slide my hand up her leg."
    "I feel up higher, toughing her curves, and she pulls me down on top of her."
    "I grab her thigh, but remember to touch more than just one place. My hands explore."
    "I count the seconds, trying to make sure I don’t spend too much time in open place. And trying to kiss her at the same speed … trying not to make it so obvious that I am winging this. Eventually … I find a rhythm."
    "I get the urge to kiss her neck, and obey that urge without thinking. She squeals and her fingers dig into my skin."
    "She thanks me by biting my ear. Her teeth press down slowly and awkwardly like she is worried I wouldn’t like it. But it sends a spark over my skin. She grabs me down there and my body jumps as a much stronger spark shots through me."
    "She fidgets with my belt, nervously. I can’t help but suspect that she is playing up her nervousness to make me feel better. My heart beats like a loud drum as I try to pull her top off. We’re both at the mercy of a powerful urge. I can hear It telling me what to do."
    "I try not to stare at her bra, only moving my hands to the strap behind her after I get the nerve to move again. I try to unclasp it … and try … and try again. No one told me that bra claps were like miniature Rubik’s Cube!"
    "Maryanne feels me struggle to take it off, and reaches behind her back to help."
    "In seconds we’re both naked. I’m on top of her. She lies half naked in front of me. Blushing. Bouncing a little as she breathes heavily. Still shaking, I unbutton her pants and slide them off her smooth, full hips. She fumbles with the buttons of my shirt."
    "I’m made immobile again, but not by my lack of experience … but by how she looks. She doesn’t try to cover herself up and it excites me beyond belief."
    "She grabs my hip again, urging me to move forward. I slip inside her … and she flinches."
    "Her hands wrap around my neck hard enough to pull me in. I can’t describe how good being with her feels."
    "I start moving, and she starts panting softly. Then it gets louder. Her nails hurt, but I kind of like it."
    "We get hotter, louder, we move faster. Her legs wrap around my hips as I push. We kiss and hold each other, wrapped up in a ball of each other’s sensations."
    "I lose track of how long it goes on for, but somewhere in all the heat and sweat, an intense, hot wave washes over me."

    scene bg white with Dissolve(2)

    "Maryanne keeps her arms around me long after it is over. I hold the small of her back as she rests her head on my shoulder. She doesn’t “sleep”. … It’s not even 3pm yet. Her clothes stay off, her hair stays down, and her arms stay around my shoulders."
    "A huge smile won’t leave my face."
    "I don’t know how much time passes. There’s a lot of touching and kissing. I forget there is a world outside her window. There’s a time when we get dressed, only to kiss playfully and undress again."
    "And we talk. Like we would with our clothes on. Not about anything special … but it felt special. More than usual."
    scene bg room_maryanne with Dissolve(2)
    "But eventually, the rest of the world reminds us that it is, in fact, there. Her phone buzzes and our little private world goes away … for now."

    show maryanne happy with config.say_attribute_transition
    Maryanne "That was my schedule alarm. Uh, I have to … get to class in a bit."

    Shun "I’ll walk you there."

    Maryanne ecstatic "Okay!"

    "She nods, smiling, and grabs her bag in a hurry. I kiss her neck as I hug her from behind. She does that squealing noise again before she playfully shoves me off."

    scene bg inou
    show maryanne uncomfortable blush
    with bg_change_minutes

    "I walk with her, outside, and it becomes … awkward again. Almost immediately. Or it feels awkward. I didn’t know if I should hold her hand or ask her if we were dating or saying nothing at all."
    "Maryanne looked nervous too, and held her bag close to her chest."

    Shun "So … maybe …"

    "She looks at me when I start talking, still red in the face."

    Shun "… We should do something later? After studying I mean. We have to be ready for midterms, right?"

    show maryanne happy noblush with config.say_attribute_transition
    "The redness fades fast and she nods."

    Maryanne "I … kind of wanted to maybe get something to eat for dinner. I have been craving yakiniku all day."

    Shun "Okay! Yeah, sure! We can go get that together."

    Maryanne ecstatic "Okay! Together."

    "She lets one of her hands fall free and I pick up on that sign. I grab it gently. She casually leans into my arm and I wrap my fingers around hers."

    hide maryanne with config.say_attribute_transition
    "She holds onto my fingers, drawing out our goodbye by a few more seconds. A few people see us; I don’t care. Maryanne heads off to the science wing, smiling, turning her head towards me before going inside. The grin on my face is so wide that it hurts."
    "I know my roommates will have something to say about this, or the onlookers who will no doubt spread rumors within the hour. But I don’t care. Not even a little. For once, I’m happy to be just … go with it."
    "I’m happy being … me."
    return
