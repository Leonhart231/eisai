##------------------------
## Act 2: Maryanne
## Scene 12b: Taco Night 3
##------------------------

label a2s12b:
    Shun "…"

    Hideki crossed pissed "Of course. He left you alone, haha. Of course he did!"

    "Hideki grabs the plate Kevin put aside for him, sits down where Kevin was sitting, and starts eating. His glare drills through his glasses."

    Maryanne "You sound mad …"

    "Before he can go off again, he sighs. And sits down."

    play music "decompression_loop.mp3"
    Hideki "I have had a long day and this does not make it better …"

    "He hides a smile as he wipes his chin."

    Hideki "Phone calls all morning, no peace and quiet here, and I cannot go outside because campus security knows my face now."

    Maryanne uncomfortable "Security? Uh, I mean, that … must be hard."

    Shun "I don’t suppose you have more work you could go do?"

    Hideki armsup argue "At least I am trying to get work done. You are all out here making noise and wasting time when you should be studying."

    Shun "We just wanted to have a little fun before things get really busy."

    Hideki "Ha! You say that … but look at how many distractions are in this very room right now! Food, other people, the TV is one button away from being on, and the Dog is probably off collecting something to drink."

    show maryanne worried with config.say_attribute_transition
    "Maryanne’s fingers rap, rap, rap on her knee."

    Hideki "And with women! The ultimate distraction! No diversion, no excuse, no use of time could be more damaging to one peace-of-mind than women."

    Maryanne "How Shun, or anyone spends their free time, is none of your business."

    show hideki crossed glare with config.say_attribute_transition
    "Hideki’s eyes shoot at her. She took the bait! No!"

    Hideki "Oh, is that so? I try to spare Shun future headaches by giving him valuable insight and that is not my business? The very thing I discourage occurs in my own living room and it is not my business?"

    Maryanne "Your “insight” is rude and unwarranted. Shun didn’t ask to be pestered, and neither did I."

    Hideki "And yet, here you are. Speaking of giving unwarranted advice, what exactly are you doing right now?"

    Maryanne "I don’t know what you’re talking about—"

    Hideki crossed pissed "Few who are wrong ever do! It’s so difficult, being the morally sound person in every frivolous argument that goes on in this dorm! You’d think I’d be used to it by now!"

    Maryanne mad "What’s your problem?!"

    Hideki armsup argue "And it comes back to me, of course! Another thing I am expected to get used to!"

    Maryanne "… Why are you acting this way?"

    Hideki "Because I am allowed to be! I am in my home, in my castle, and I did not invite you nor did I get a warning beforehand! But hard days and long hours don’t mean crap when someone makes plans without asking!!!"

    show hideki crossed pissed with config.say_attribute_transition
    "Hideki slams his fist on the table. His eye twitch. His face is glowing red."

    Maryanne confused "You had a bad day?"

    Hideki "Theeee worst!"

    Maryanne "… Did you want to have a night alone with your friends?"

    Hideki glare "…"

    Maryanne "… And those plans got ruined. Because you didn’t know we were coming over."

    Hideki "… Now … now hang on …"

    Maryanne uncertain "I’m sorry. I didn’t know you were looking forward to spending time with your friends."

    Hideki "…"

    "Hideki suddenly looks … a lot smaller. He stares in silence for another moment … then goes back to eating his meal."

    Maryanne embarrassed "Shun, can you walk with me somewhere? I think Hideki needs some time alone."

    Shun "S—sure!"

    hide maryanne
    hide hideki
    with config.say_attribute_transition
    "I get up and follow her. Hideki stares at the wall, glancing at Maryanne once with a humbled look on his face. Maryanne is one foot out the door when he finally speaks."

    Hideki "This does not taste awful! The Dog must have spent all day preparing it!"

    Shun "I’ll tell him you liked it when I see him."

    Hideki "… Hmph …"

    scene bg hall_dorm_boys
    show maryanne neutral hairdown
    with bg_change_seconds
    Shun "I can’t believe you got him to shut up!"

    show maryanne happy with config.say_attribute_transition
    "She smiles. Then grabs my hand as the door closes and guides me down the hall."
    play music "romantic.mp3"
    "We pass Kevin and Shione, who are walking back to the room with beer and vodka in hand."

    show shione finger excited at right with config.say_attribute_transition
    Shione "Maryanne! The dictionary said—"

    show kevin neutral sides closed behind shione:
        xalign 1.8
        linear 1 xalign 1.4
    pause 1.5
    hide shione
    hide kevin
    with moveoutright

    "Kevin pulls Shione into our dorm before she can get started, then moves to the side as we pass. I wave. He gives me a thumbs-up."

    scene bg inou night
    show maryanne happy hairdown
    with bg_change_seconds
    "It’s a cool night. Very quiet. I can hear only crickets and Maryanne giggling softly. She sits down on the bench outside the dorm."

    Maryanne "Okay. {em}Now{/em} we’re alone."

    "I sit next to her without protest. And this time without hesitating, I kiss her."

    hide maryanne with config.say_attribute_transition
    "It’s much easier the second time, nowhere near as nerve-wracking. Especially without the chance of someone walking in on us. Our lips dance and I feel the breath come out of her nose slightly faster than normal. I’m covered in the sweet smell of her again."
    "I move in very close, and she lets me. My tongue slides in her mouth and I feel her smile. I put my hand on her shoulder and gently pull her closer. Her hair falls on me a bit and my heart leaps."
    "It feels like we’re there for a long time. It’s fun. It’s exciting. This experience would be really hard to describe if I tried to explain it to myself yesterday."
    "Everything, from the scent of flowers coming off her hair down to the tiniest bead of sweat that rolls down my neck felt amazing."

    show maryanne happy blush hairdown with config.say_attribute_transition
    "Eventually, slowly, we stop. Her cheeks are really red and her smile is big. She laughs, then rubs her finger on my chin to wipe off some of her lipstick. I feel my own face flushed with redness."
    "I try to play it cool, but my smile is so big I can feel my face stretch."
    "I don’t know what to say. And I don’t think she did either. So she put her head on my shoulder and we both say nothing."
    "After some time passes, she finally speaks."

    Maryanne "We … might want to go back inside."

    Shun "Well … okay."

    "I kiss her once more, quickly. She smiles, and kisses me back."
    "Soon, our arms are around each other again, and another stretch of time goes by. She touches my leg. I touch her back and pull her in really close. I don’t know or care if anyone knows we’re gone."

    stop music
    scene bg dorm
    show maryanne embarrassed hairdown at center
    with bg_change_minutes
    play music "happy_hangout_loop.mp3"
    "As I open the door, Shione comes zooming towards me, holding her arms out like an airplane and making buzzing noises."

    show shione hips confused at rightish with config.say_attribute_transition
    Shione "Bzzzzzz! Look out! I’m coming in hot! Hahaaa~!"

    Maryanne "Oh geez, not again."

    "Maryanne rolls her eyes and proceeds to wrangle Shione down to the couch. She squirms like a kid being forced to leave a playground, and Maryanne suddenly looks like her stressed out mother."

    show kevin neutral crossed open at leftish with config.say_attribute_transition
    "Kevin sits very still and watches them, snickering as her cheeks glow red."
    "Hideki is face down on the table with one hand on his head and the other wrapped around an empty glass. How long were we outside?"

    Maryanne "Oh god Kevin. How much did you give her?"

    Kevin smile sides "See, counting stuff is shomething sober people do."

    Maryanne worried "Uuugh! We have to be up tomorrow! Shione, get your things. We need to get you to bed before you crash."

    Shione finger "Pffft, square! That’s the least fun shape out there! Feel bad Maryanne, feel bad I say!"

    Maryanne worried "Okay, okay. I’m a square. But it’s time for you to leave."

    Shione wonder "Booo! Square!"

    Kevin smirk closed "Yeah, boo! Square! Boo!"

    hide maryanne
    hide shione
    with config.say_attribute_transition
    "Maryanne wraps her hand around Shione’s waist and carried her across the room."

    Maryanne "Thanks for everything Kevin. Except for this part."

    Kevin smile open "Welcome!"

    Maryanne "B—bye Shun."

    "She smiles at me as she pulls her tiny friend out the door."

    Shun "Bye. I’ll call you later."

    show kevin smirk closed with config.say_attribute_transition
    "The moment the door closes, Kevin makes a mocking “Ooo” noise like a studio audience."

    show hideki crossed begrudging at rightish with config.say_attribute_transition
    Kevin open "Our little Shun is growing up Hideki! Pretty soon he’s going to get hair in funny places and start listening to bad music."

    Shun "Is Hideki drunk?"

    Kevin smile "Completely. You’ll love him when he is really drunk. He gets super deep and profound."
    Kevin "Hey Professor! Tell Shun something thought-provoking!"

    "Kevin grabs the back of Hideki’s hair and lifts him up. Hideki looks around, wobbling in his seat, eyes half open. For once, he is smiling. Then points at me and says …"

    Hideki "We … are all made … of ssstardust. From a bunch of galaxies … from aaall over the place."

    "He spreads his hands out, simulating a blooming rainbow. Then Kevin lets go of his hair and Hideki nearly slams his chin on the table."

    scene bg dorm
    show kevin smile sides open at leftish
    with vpunch
    play sound "164664_deleted_user_2104797_door_slam_1"
    Kevin smirk closed "Aaand, he’s gone! Far, far away. Haha!"
    Kevin smile open "So, what happened?"

    Shun "I kissed her."

    Kevin laugh sides "Yeeeeaaah! Up top!"

    "He holds his hand up. Hideki, face still down on the table, high-fives Kevin before I can."

    Kevin smile "Ahhh, this guy! Congrats Shun. You’re totally in."

    Shun "I know. And it’s … it’s exciting."

    Kevin "Ah, got the butterflies? Tell them to stop flapping. This is no time to be nervous! She’s going to expect you to follow through man. Don’t flake. She’s too hot to flake!"

    Shun "Okay, okay, I won’t. I’ll call her tomor …"
    Shun "… Oh."

    Kevin frown "What? … Oh Jesus, really man?"

    Shun "I forgot to get her number {em}again{/em}!"

    Kevin laugh closed "How the hell did you get in school?"

    Shun "Do you have her number?"

    Kevin open "Yeah, but I am not giving it to you. You gotta get it yourself."

    Shun "What?! Oh come on, please?"

    Kevin smirk "There’s a “5” in it. That’s the only hint you’re getting."

    "Kevin finishes another beer and burps so loudly it makes my ears shake."

    Kevin smile "Alright, night-cap swallowed. Time for bed. Come on Professor, back to the lair."

    "Kevin wrangles Hideki and the limp limbs flapping all over. Hideki can barely walk straight. Eventually, Kevin just picks him off the ground and carries him over his shoulder."

    Hideki "Your body has {em}hiccup{/em} … enough iron … to make a three inch nail."

    Kevin "Yes, yes, you’re making great use of that fact-of-the-day calendar. Now go to bed."

    hide kevin
    hide hideki
    with config.say_attribute_transition

    $ renpy.pause(1)
    show bg dorm with hpunch
    play sound "164664_deleted_user_2104797_door_slam_1"
    "I heard a thunderous thud from his room as he throws Hideki on his bed. Then he closes his door slowly and softly."

    show kevin smirk closed crossed with config.say_attribute_transition
    play music "contemplative_loop.mp3"
    Kevin "I don’t want to wake him. Hehe."

    "Kevin plops back down and starts cleaning up the messy table with the grace of a blind penguin. I help him clean up out of pity."

    Kevin smile open "Aw, thanks man. You—you’re alright. You fit in here with me and the Professor. He’s—he’s a little harsh and stuff, but he just needs to let it out, you know?"

    Shun "Yes Kevin, I know."

    Kevin "And you … man, you’re one of us. My buddy!"

    Shun "Um, thanks."

    Shun "But Kevin, um … seriously … how do you put up with that guy? Hideki’s rude and mean and … and he’s not like you at all. How do you put up with that?"

    Kevin smirk "Dude, everyone in this world is going to drive you nuts. You just have to figure out who is worth tolerating."

    Shun "And Hideki is worth tolerating?"

    Kevin smile "Yeah."

    Shun "… Why?"

    Kevin serious "… Because. He tolerates me."

    "When he says that, for some reason … I think of home."

    Shun "I’m going to bed. If you wait, we can clean this up in the morning, together."

    Kevin smile "Kay. Night Romeo!"

    hide kevin with config.say_attribute_transition
    "He leaves. I turn off the lights and go to my room."

    scene bg room_shun night with bg_change_seconds
    "I can barely close my eyes. I can still taste Maryanne’s lipstick and smell her perfume on my clothes."
    "Those damn butterflies won’t stop moving around. Nor do my thoughts stop racing."
    "I end up thinking of how to make the next time I see her better than tonight. For hours."
    return
