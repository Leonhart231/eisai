##------------------------
## Act 2: Maryanne
## Scene 1: Back in School
##------------------------

label a2s1:
    stop music
    scene bg dorm with bg_change_minutes
    play music "decompression_loop.mp3"
    "A lazy weekend welcomes us when we get back home, the kind that drags by nice and slowly. After the trip to Shodo, I felt like a couple of days of doing nothing."
    "Word around campus is that everyone else felt the same way. There are no plans, no parties … no worries. Spurts of rain throughout the day encourages me and my roommates to stay in."

    Kevin "Ha! Three to one!"

    "Hideki growls as Kevin beats him again. Their next game starts as the gloomy sky softly rumbles."

    "I thumb through a book I have been meaning to read while those two duke it out. It’s a story about a guy who gets lost in the woods after a plane crash and has to find his way back without even knowing what country he is in."
    "He is a fish out of water, figuring things out as he struggles to get from one point to another. I finish chapter 6 as Kevin taps my knee with his controller."

    show kevin neutral sides open at rightish
    show hideki crossed glare at leftish
    with config.say_attribute_transition
    Kevin "You wanna play Shun? This game is starting to get too easy."

    Hideki crossed pissed "The bet is not over yet! I am merely allowing you to win so that I … grrr."

    show kevin smile closed with config.say_attribute_transition
    "Kevin rubs the top of Hideki’s head like he’s a little boy. Hideki snarls under his ruffling hair."

    Shun "I’m okay. This book is really good. You guys play."

    "Kevin nods and they starts up yet another round. Hideki looks like he is about to explode as another round starts up … and he loses very quickly. Adding insult to injury, Kevin slams his controlled down on the table and throws his hands up in the air."

    Kevin laugh sides closed "Game! Pay up Four-Eyes!"

    "Grinding his teeth, Hideki pulls out his wallet."

    Shun "You bet on who’d win?"

    Kevin "It wasn’t a bet so much as it was an investment. And it’s paying off. … {em}Riiight{/em} now."
    Kevin smile open "Most wins out of nine buys lunch. Yamada’s has this Rainy-Day To-Go Special … it’s a cheap teriyaki beef bowl with leeks and those little pickled onion thingys. They only serve it when it rains. It’s really good!"

    "Hideki passes Kevin a 2,000 yen bill. Another thunderous roar bellows from outside and the wind picks up. Like nature was responding to Hideki’s violent aura."

    Kevin "Thank you for your generous donation, good friend. I’ll put the order in on my card. You go get it and—"

    Hideki armsup argue "I am not going out in that storm!"

    Kevin neutral sides open "Yeah you are. You lost, and now you have to follow through with your word. That’s how a bet works."

    Hideki "I cannot and {em}will{/em} not endanger myself just so you can stuff your face!"

    Kevin serious crossed open "What’s wrong? You afraid if you get wet you’ll multiply and make more green, scaly monsters?"

    Hideki chin think "If only reproduction could be so simple …"

    Kevin "You really, really need to get laid man."

    Shun "I’ll go get the food!"

    "I shout out before a fight escalates."

    Kevin neutral sides open "Thanks man. That’d be very nice of you. Isn’t that very nice of him, Gizmo?"

    show hideki crossed pissed with config.say_attribute_transition
    "Hideki stabs me with his eyes, like I had insulted him by stopping an argument. He stands up with a huff and hurries back to his room, without a thank you or a complaint."

    hide hideki with config.say_attribute_transition
    Kevin "So … are you used to that guy yet?"

    Shun "Uh … I’m getting there. Don’t worry about it."

    Kevin "I never do. My bowl will be the one with extra peppers. Hideki likes his with none."

    Shun "You know, I could use one too. Put in an order for a regular bowl."

    Kevin laugh sides closed "No prob dude."

    scene bg yamada_street overcast with bg_change_minutes
    call weather(5, 0)
    "Mother Nature can’t decide how she is feeling today. The rain turns on and off like a broken shower head. One minute it’s pouring, the next it’s a light drizzle that wouldn’t justify a need for an umbrella."
    "I wait outside Yamada’s for our meals to finish. Apparently this is a very popular order on rainy days so I decide to wait outside, away from the crowds. I admire the view of town as I bask in the delicious smell perfumes out from the kitchen."
    "I’ve always thought that rainy days makes the world look more … alive. In subtle ways. Wood becomes darker, grass looks richer, the air gets cool, things slow down."
    "I can hear the wind, the nearby cooks scraping things off their knives … but outside, the world is quiet. Rain, that is all I hear outside. It’s soothing."
    "Since college started up again, I haven’t had many days like this. Days when you can do nothing. Where you can slow down and watch things. Listen to things."
    "I know work is coming again, and soon. That’s life. But today is … nice. I’m starting to understand what Kevin meant back when we first met."

    scene bg inou
    show kevin smirk crossed open
    show flashback
    with bg_change_seconds
    Kevin "{flashback}This is perfect! It’s like the whole world was made for us right now. No noise, no work, just a pretty picture for us to admire.{/flashback}"
    Kevin "{flashback}Life is a vacation.{/flashback}"
    Kevin "{flashback}It’s really easy to forget to appreciate something when you have it. Like, really easy. Scary-easy.{/flashback}"

    scene bg yamada_street overcast with bg_change_seconds
    call weather(2, 0)
    "The silence is broken as the rain slows down to a very light drizzle. A vivid pink umbrella snatches my attention as Maryanne walks up to the delivery window, not seeing me at first as she puts her umbrella down by her leg."

    show maryanne happy with config.say_attribute_transition

    Shun "Nice weather we’re having."

    Maryanne "Shun! Let me guess, you’re here for the Rainy Day Special too?"

    Shun "Yeah. I’m picking up food for the … you know, the guys. They’re waiting for me back at the dorm."

    "“The guys”? That doesn’t sound right. I mean, I didn’t dislike my roommates but … I don’t know, it’s strange to talk about them like they were close friends … wait, were they? Now?"

    Maryanne ecstatic "Great! Me too, I never miss their special."

    Shun "I guess I will find out what the fuss is about. We’re having an “indoors day” today. Kevin said something about movies tonight."

    Maryanne happy "Oh, those are the best! Shione never wants to stay in and relax like that. Unless it’s with … lobsters or something."

    "{snd}ding-ding!{/snd} The cook rings a bell, signaling that he has finished an order."
    "My order appears in the pick-up window, neatly tied in a plastic bag. I can see steam escaping from the plastic edges of the huge to-go bowls."
    "Maryanne makes a move for it at the same time as I do."

    Maryanne "I think that is my order."

    Shun "Yours? It couldn’t be. There’s food for three people."

    Maryanne embarrassed "… Uh … yeah."

    "There is a receipt stapled to the side of the bag. “Customer: Mari and So-yaw” is written on the top."
    "Maryanne snatches the bag, looking mildly self-conscious."

    Maryanne worried "I skipped breakfast, okay!?"

    Shun "O—okay."

    "She stares at me, still suspecting that I am judging her."

    Shun "… Hey, why don’t you come eat with us? You can join in all the nothing we’re doing."

    Maryanne neutral "Oh, uh, that sounds … nice. But … I can’t."

    Shun "I’m sure the guys wouldn’t mind."

    Maryanne confused "It’s not that. I have work to do."

    Shun "Still? Really?"

    Maryanne "I’m … I’m getting a jump on a presentation for Professor Namura’s class. Our trip set my schedule back a little."

    Shun "Is that the presentation for one of your grad classes?"

    Maryanne happy "{em}cough{/em} … Y—yeah."

    "Word around campus was that there teachers were holding back on homework for now, since they too had just gotten back from Golden Week and needed to focus on writing up the midterms."
    "I can’t imagine that Maryanne has more work to do, especially since she has been relentlessly tiring away every afternoon and weekend since the year began."
    "{snd}ding-ding!{/snd} A second large bag appeared with an order almost identical to Maryanne’s. This time, the receipt reads “Hideki”."

    Shun "Okay, this is mine. You sure you won’t reconsider coming over?"

    Maryanne "S—sorry. Tell Kevin I said Hi and … I’ll … uh …"
    Maryanne "… B—bye."

    hide maryanne with config.say_attribute_transition
    "She hurries off, carrying the heavy order."
    "I grab my order, but almost trip. Maryanne’s umbrella gets caught between my legs."

    call weather(5, 0)
    Shun "Wait, Maryanne, you forgot your—!"

    "Almost on queue, the rain starts to pour again."

    Maryanne "{em}EEEP!{/em}"
    "Maryanne shrieks as she gets caught in the sudden downpour. She runs towards me and grabs the umbrella out of my hand."

    show maryanne happy with config.say_attribute_transition
    Maryanne mad "Th—thank you."

    hide maryanne with config.say_attribute_transition
    "She hurries off again, holding the heavy order with her free hand."

    scene bg dorm with bg_change_minutes
    call stop_weather
    "Kevin and Hideki stopped playing their game sometime during my food-run, and moved on to their true passion: movies. I walk in just as the title credits for an 80s action film finish rolling and the film starts."
    "Kevin looks like an excited child, sitting at the edge of his seat, an over-eager grin on his face and eyes glued to the screen."
    "Hideki is on the floor by his knee, scribbling numbers onto his electronic notepad. He glances between the screen and the device every few seconds."

    show kevin laugh crossed open at rightish
    show hideki glasses adjust at leftish
    with config.say_attribute_transition
    Kevin "Yes! Lunch! Gimme-gimme-gimme! And sit down, the movie just started."

    "I sit, untie the bag, and hand Kevin his beef bowl with extra peppers, which he impatiently grabs and rips open like a child tearing into a birthday present."

    Kevin "It don’t get better than this. No parents, no work … just friends, good eatin’, and the sultry sexiness of 1980s’ pyrotechnics."

    Hideki chin think "Is this film going to be like the one with the man stealing machine guns in the skyscraper?"

    Kevin laugh sides closed "No, no, this one has time-travel and cyborgs! You’re welcome."
    Kevin neutral sides open "But don’t do that thing where you try to actually do the math for science fiction movies. Star Trek is called Science-{em}Fiction{/em} for a reason."

    Hideki armsup argue "I don’t care if it {em}is{/em} fiction! You cannot move the entire universe around you via a control panel!"

    Kevin "Shush you! Either watch the movie, or go in your room and count Pi or whatever it is you do when you’re bored."

    show hideki crossed glare with config.say_attribute_transition
    "Hideki’s shoulders arch. He grumbles, hiding his face behind his notepad. I hear him say a few passive-aggressive curse words behind the device, and a short comment about how counting Pi can be fun sometimes."

    Shun "Maryanne says hi by the way."

    Kevin smirk sides open "You saw the Egghead?"

    Shun "Yeah, at Yamada’s. She wanted the same thing we wanted."

    Kevin "Ah man, why didn’t you invite her over?"

    Hideki armsup argue "Excuse you?! No! Not the blonde one again! Need I remind you what happened the time you invited that bimbo over to watch a science fiction show?"

    Kevin "Oh … {em}Ooooh{/em}, right. Yeah. Okay. But I would have still liked to see her. She’s cool. We could have the movie another day."

    Shun "Wait, what happened last time?"

    Kevin mad "Don’t worry about it, I’ll tell you later. But back up … Maryanne is {em}not{/em} a bimbo! She’s smart and stuff. Blonde and chesty is not … uh, it isn’t the same thing as bimbo-ish."

    Hideki "Uh-huh. Why don’t you watch the movie, or imagine naked breasted females or whatever it is perverts do when they realize they have a double standard?"

    Kevin serious crossed open "Fine, maybe I will! Maybe I’ll have fun with it too and keep my double standard intact without critically thinking about it!"

    Hideki "Fine, I hope you do!" (multiple=2)
    Kevin "Okay, then I will!" (multiple=2)

    show hideki crossed pissed
    Hideki "{em}Grrr!{/em}" (multiple=2)
    Kevin "{em}Grrr!{/em} … That’s what you sound like." (multiple=2)

    Shun "… Anyway, I {em}did{/em} invite her. But she said she had to work on a presentation."

    Kevin neutral sides open "The one for Nodori?"

    Shun "… Yeah. How did you know?"

    Kevin "She told me about that thing the week before we left for Shodo. She almost didn’t come on the vacation because of it."

    Shun "Is it that important?"

    Kevin serious crossed open "I don’t know. It’s not due till the end of the year."

    Shun "End of the year?! Why is she working on it now?"

    "Kevin takes a big bite from his lunch. And doesn’t swallow it for a long time. Like he is thinking of the right thing to say, or waiting for me to answer the question for him."

    Kevin "Beats me man. She is probably just an Egghead."

    Hideki armsup argue "Enough talking! I am prepared to eat. You will now start the movie again."

    Kevin smirk sides open "Yesh shhhir commander shhir!"

    "Kevin says mockingly, saluting Hideki before hitting play."

    scene bg dorm
    show kevin neutral sides open at rightish
    show hideki hips bored at leftish
    with bg_change_minutes
    "Ten minutes pass. The giant muscly man in the film kills a few blonde women while driving a motorcycle. Hideki jots something on his notepad every few minutes, ending each memo with a loud “pat” as he hammers in an exclamation point."

    Kevin "You’re hanging with Maryanne a lot lately. Right? You two are studying, sometimes hanging out … that stuff?"

    Shun "Yeah. Every now and then. It’ll probably get more frequent next week. Why?"

    Kevin "I don’t know. Maryanne and I … we’re friends but we’re also really different, you know? You two have more in common. You both study a lot, you’re quieter than I am—"

    Hideki crossed pissed "That’s a confession! Record that! Admitting to being loud, guilty as charged!"

    menu:
        Kevin "Look, Shun … can you do me a solid?"
        "Maybe.":
            jump a2s1_maybe
        "Sure man!":
            jump a2s1_sure

    label a2s1_maybe:
        Shun "Uh, maybe. Depends on what it is."
        Kevin "Okay, fair answer. But hear me out."
        jump a2s1_common

    label a2s1_sure:
        Shun "Sure man, name it."
        Kevin laugh sides closed "That’s what I like about you Shun. You’re agreeable."
        show hideki armsup argue with config.say_attribute_transition
        "Kevin pokes the back of Hideki’s head. Hideki spazzes out, waving his hands around like he’s trying to hit a bee."
        jump a2s1_common

    label a2s1_common:
        Kevin neutral sides open "Maryanne’s gotta get out more. And she likes you enough for you to meet her dad. You think could, I don’t know, pull her away from all that studying sometimes?"

        Shun "What is wrong with her studying?"

        show hideki crossed glare with config.say_attribute_transition
        Kevin "Nothing, nothing. But too much of anything isn’t good for you. Even book-reading. She knows that biology stuff like the back of her hand. … She doesn’t need to study {em}every{/em} single day. Not for that much. It’s like she doesn’t know what else to do with her time."

        Shun "Maybe she knows that all of that stuff so well because she studies? It’s not bad to be studious. Especially in college."

        Kevin "Sure, studious, but she forgets to have a life sometimes. All work and no play, you know that saying. You gotta do some random stuff every now and then. See friends. Have fun! You know, enjoy being alive!"

        Hideki crossed pissed "Must you go on sanctimonious rant every time someone implies that you should be studying more? It is so obvious you are redirecting the topic—"

        Kevin laugh "{em}Oh!{/em} Here comes the best part!"

        "Kevin points to the TV. Hideki’s neck snaps back towards the screen, pen in hand, ready to jot down notes."
        "Kevin has a point … when he said he was different from Maryanne anyway. I think he {em}wanted{/em} to help, but he only knew how to help in {em}his{/em} way, by being his overly-extroverted self."
        "Still, I remember her at her wits end at the Eisai Party. Seeing her like that once was enough."
        "I wait for the action scene to end before speaking up."

        Shun "I’ll see what I can do about Maryanne."

        Kevin smile "Awesome. Thanks. And hey, who knows? If you two get close, it might lead to somewhere good … like under her shirt."

        Hideki armsup argue "How can you claim to be the girl’s loyal platonic friend and still objectify her sexually?"

        Kevin "I have a dick, that’s how. And helping my man use his is not the same as seeing my hot friend as just an object."

        Hideki "It’s in the same ballpark!!! {em}God!{/em}"

        Shun "I wasn’t planning on making a move or anything."

        show hideki crossed glare
        show kevin serious crossed open
        with config.say_attribute_transition
        Kevin "Of course you weren’t. Who plans on good things, really? Sometimes, you gotta say “fuck it” and do something no one expects you to do."

        Kevin smirk sides open "You’ll know what to do when the time is right, young padawan."

        Shun "Young what?"

        Kevin "It means “freshman” in English."

        Shun "Oh. Okay."

        "The TV screen lights up with another explosion. A shotgun is fired and the muscly man flies through a window, destroying a table. Kevin squeals as he tells me why that actor was an underrated politician."

    return
