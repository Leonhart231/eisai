##----------------
## Act 2: Maryanne
## Scene 10: Tacos
##----------------

label a2s10:
    "Kevin eventually stops flipping the channels when he lands on a weird prank show, one that was clearly staged because there was no way anyone could legally get away with the things we saw."
    "In the middle of some poor old man getting scared by a ghost jumping through a wall, Hideki comes back in. He looks tired, like a long day of pulling petty pranks on innocent pedestrians had taken a lot out of him."
    play music "bickering_loop.mp3"
    show kevin neutral crossed open at leftish
    show hideki hips bored at right
    with config.say_attribute_transition
    Hideki "What is there to eat?"

    Kevin "Shun bought eggs. Oh yeah, hang on …"

    "Kevin pulls money out of his pocket and pays me back for the eggs I bought this morning."

    Kevin "… There you go. Keep the change. So yeah, eggs in the fridge bro."

    Hideki "Fine. I shall cook eggs, if that is all we have."

    hide hideki with config.say_attribute_transition
    "There is some dirt on Hideki’s face. I can see that his notepad is partially ripped. He tried to conceals it under his arm."
    "He starts cooking … in silence, not making eye contact. I say nothing. Neither does Kevin. The only sounds in the room are the knife on the cutting board and Kevin chewing."

    Hideki "…"

    Kevin "…"

    Hideki "… So … did you know we had campus security?"

    Kevin "… Yeah."

    Hideki "…"

    Hideki "… No one informed me of this."

    "More silence. My imagination fills in the blanks."
    show hideki hips bored at rightish with config.say_attribute_transition
    "Hideki joins us when his eggs are cooked. Kevin eats slowly. The tension … hurts. Kevin glances at Hideki every few seconds, waiting for him to say something."

    Kevin "…"

    Hideki "…"

    Shun "…"

    Shun "… So, Maryanne—"

    Hideki armsup argue "ARGH! Again with the blonde girl! Why must you two always talk about girls?! It’s sex-this, dating-that, look at the breasts on her, look at the hair on that one. Why does everyone in our graduating year only care about the opposite sex!!"

    scene bg dorm
    show kevin neutral crossed open at leftish
    show hideki armsup argue at rightish
    with bg_change_minutes
    "And off he goes! Trembling, spitting a little, a lovely vein on his forehead throbbing, loud enough to drown out the TV. For a solid minute, at least, he rambles and rants and criticizes targets that aren’t there."
    "Eventually, and with a loud sigh, he stops, falling back onto the couch with a thud, letting his body sink deep into the cushions."

    show hideki hips bored with config.say_attribute_transition
    Kevin serious "… You okay?"

    Hideki armsup argue "And another thing! I’m sick of you always prying! It’s either sarcastic jokes or you not minding your own business! It’s not as cute as you think it is! It’s like your whole damn country!"

    scene bg dorm
    show kevin neutral crossed open at leftish
    show hideki armsup argue at rightish
    with bg_change_minutes
    Hideki "… And that’s why we should be putting more money, all the money, into the space program!!!"

    show hideki hips bored with config.say_attribute_transition
    "Another few minutes of endless ramblings, another loud sigh, and Hideki plops down into the couch. Exhausted from his own venting."

    Kevin serious "… Bad day?"

    Hideki "… Terrible. I received a letter from my progenitors."

    Kevin mad sides "Ahhh, shit. Your parents again?  They don’t just call you like normal people?"

    Hideki "No, as I prefer it."

    Kevin serious crossed "What happened this time?"

    Hideki "Does it matter?"

    Kevin closed "I mean, you’re being so quiet about it, you know. I thought I’d ask."

    Hideki crossed pissed "Bite me!"

    Kevin "Chill out, man. Everyone’s parents are headaches. The last time Taco over there talked to his parents, it bugged him too."

    Hideki "What?! What on earth did you say?!"

    Kevin smile "Taco. I’m trying out nicknames for Shun."

    Shun "Oh my god, stop that right now!"

    Kevin open "Come on! Takamine … Taco … it works!"

    Shun "No it doesn’t. I’m stopping that nickname. Immediately."

    Hideki "Isn’t a Taco like … a meat?"

    Kevin smirk "No dude, it’s … you know, it’s like … a corn pancake thing with meat and cheese. It’s from Mexico."

    Hideki armsup argue "… What the hell are you talking about?"

    Shun "I’ve had them. They sell them in Tokyo, in some shops. It looks weird but it’s pretty good."

    Hideki crossed pissed "I’m not interested in what you two consider to be “good”."

    Kevin serious closed "Whatever, forget it. I can’t get my nickname to stick and I have to educate you about Mexican cuisine."

    Hideki "The fact that you are legitimately upset denotes even higher levels of stupidity than previously estimated."

    Kevin open "I might have to shut you both up by making some of them sometime."

    Shun "What did I do!?"

    Hideki armsup argue "Do whatever you want! I don’t care!"

    show kevin serious closed sides with config.say_attribute_transition
    "With a scowl, Kevin devours the last of his food, tosses his tray out, and then stomps out of the apartment. Still in his pajama pants, his jacket over his t-shirt. Not another word."

    hide kevin with config.say_attribute_transition
    Shun "What was that about?"

    Hideki hips bored "I do not care enough to follow up with it."

    "Hideki takes Kevin’s spot on the couch and flips the channel to the World News."

    Shun "You have issues with your parents too?"

    Hideki "…"

    Shun "Uh … I know you usually don’t … discuss personal things … but if you wanted to talk about it—"

    Hideki chin gendo "I will find ways to make you permanently stop yammering."

    Shun "Yeah, that’s what I thought you’d say."
    return
