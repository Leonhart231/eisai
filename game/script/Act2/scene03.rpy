##-----------------------
## Act 2: Maryanne
## Scene 3: Couch Crasher
##-----------------------

label a2s3:
    scene bg dorm with bg_change_minutes
    "I poke my head into an empty dorm. Hideki’s “Do Not Enter” sign is on his doorknob, so for all intents and purposes no one is home. I hold the door open for Maryanne, who enters slowly."

    show maryanne happy circles tangled with config.say_attribute_transition
    Shun "Come on in. Make yourself comfortable."
    Shun "I’ll heat up the left-overs. Do you want something to drink too?"
    hide maryanne with config.say_attribute_transition
    play music "contemplative_loop.mp3"
    "She shakes her head, eyes half open. She sits on the couch, her body tilting back and forth. And tries not to yawn … at least not until my back is turned."

    "I pull out what is left of my Yamada’s special and put it in the microwave. A few loud beeps siren as I program the time."

    Shun "You can have it all if you want. I can cook more if you are really hungry. Would you like that?"
    Shun "Maryanne?"

    "I turn around and see Maryanne laying on her side, her face buried into the corner of my couch. Her ankles resting on top of one another."
    "I stop the microwave. And sit down by the side of the couch, next to her. She looks … calm. At last. She snores very, very softly, sharply inhaling every few seconds."
    "I quietly grab my book and start reading, sit by her side as I let her rest for a little while."

    scene bg dorm with bg_change_hours
    play music "decompression_loop.mp3"
    "Hours pass. Maryanne’s face stays buried in the couch. Every so often, she’d coo or her leg would twitch or I’d hear her whisper something in another language."
    "My book, about the lost traveler in the woods, gets better as it goes on. The longer he stays in the woods, the more and more capable he becomes and the less threatened he is by his new surroundings."
    "And by the time he has to leave the forest near the end, he feels sad because it is like he has to leave his home all over again."

    Maryanne "… {em}zzz{/em} … {em}zzz{/em} … {em}zzz{/em} … {em}znnt{/em}! Wha—?"

    "At last, as the sun started to set and I approach the last few chapters, Maryanne wakes up with a familiar jolt."

    show maryanne confused circles tangled with config.say_attribute_transition
    Maryanne "What happened? How did I—"

    Shun "It’s not big deal … you just fell asleep, that’s all."

    "She rubs her forehead as she struggles to sit up. She pinches her elbow, like she needed to “feel” if she was still dreaming."

    Maryanne "What time is it? How long was I asleep for?"

    Shun "Almost four hours."

    Maryanne worried "No! Oh, no, no, no! I’ll never get caught up now. Stupid …"

    Shun "You’re already two months ahead of everyone else. What harm is there in taking one nap?"

    Maryanne "Because if I take one nap now, then the next time won’t seem so bad and then before you know it I’ll be taking a whole day off!"

    Shun "… So what?"

    Maryanne "… So what? So what?! So what, he says?!! Never mind! I need to get back to work before—"

    Shun "Dinner! Eat first!"

    Maryanne "…"

    Shun "Because … it’ll help you focus."

    Maryanne "… Okay."

    "I hand her the TV remote before going back to start the microwave again, taking care to move as slowly, hoping she’d get distracted with television and forget about leaving."

    show maryanne neutral with config.say_attribute_transition
    "A few prolonged minutes later, I hand her my leftovers."
    "But as expected, the moment she has the bowl in her grasp, she devours it with the pacing of a starved animal."

    Shun "Um … enjoy?"

    Maryanne happy "Frank uou!"

    "The left-overs vanishes in a few bites."

    Maryanne "That was really good! Thank you again. But I … um … should get going."

    Shun "Um, are you sure you don’t want more? I’m kind of hungry now. We could order in."

    show maryanne confused with config.say_attribute_transition
    "Maryanne looks very uncomfortable at that request. I might have pushed my luck a bit far with that last suggestion."

    Maryanne "I … s—sorry. I don’t—"

    Kevin "Come on Egghead! Hang out with us."

    play music "bickering_loop.mp3"
    "We both jump when we hear Kevin chime in. He stands at the door of his bedroom, scratching his chin and yawning deeply. Given his messy hair, he looks like he just woke from a twelve hour nap."

    show maryanne at leftish
    show kevin smile sides open at rightish
    with config.say_attribute_transition
    Shun "You’ve been here the whole time?"

    Kevin smirk crossed "Day off. I had a lot of sleep I needed to catch up on. I had this dream … you were in it … I was Dr. Jones. Shun, you were the little kid with the baseball cap."
    Kevin smile "What’s Blondie doing here?"

    Maryanne worried "I—I was just leaving!"

    hide maryanne with config.say_attribute_transition
    "Maryanne, scurries to the front door, covering her cheeks."
    "She hurries to get her shoes on … then take a few extra seconds to turn the door knob. Like maybe she was thinking of staying. Or she wanted one of us to convince her to think twice about leaving."
    "But when no one speaks up, she runs out the door, not closing it all the way after she bolts down the hall."

    show kevin at center with config.say_attribute_transition
    Shun "I think you scared her off."

    Kevin smirk "Why? Cause I wasn’t conscious for 80\% of the day? Geez, talk about high standards."

    "He jumps over the coffee table and throws himself onto the couch. The remote bounces into the air after his impact, and Kevin catches it with a flick of his wrist. He changes the channel with another flick."

    Kevin "{em}Hey Hideki! Get out here!{/em}"

    "Kevin shouts at the top of his lungs, pauses for ten seconds, then shrugs."

    Kevin neutral "I guess he’s busy."

    "I sigh and sit down next to Kevin, not in the mood to lecture him about lowering his voice or being civil."

    Shun "So … you were right about Maryanne overworking herself."

    play music "happy_hangout_loop.mp3"
    Kevin smirk "I told ya! I never understood workaholics. The poor girl is always just “one more” project or test or paper away from being done. She’s going to burn out at this rate. Again."

    Shun "Ah. You’ve seen her burn out before huh?"

    Kevin frown "Yeah. A few times. I try as best as I can to be there for her but … I don’t know, sometimes I think I’m just being paranoid. Everyone burns out in college some times … well, except art majors cause … you know."

    Kevin "But Maryanne has a habit of bottling it all up and then … {em}pa-kow{/em}! Krakatoa man. Know what I mean?"

    Shun "Yeah. I do. I’ve seen a few people do that."

    "That was how my dad used to handle stress. And admittedly, I sometimes engaged in the same unhealthy habit."

    Kevin neutral "But hey … if you’re there and I’m there and Shione is there … sooner or later she’s bound to figure out people care about her. Maybe she will wise up and let us in."
    Kevin smile "Oh, before I forget … tomorrow night I’m bringing a “friend” back for the night. Can you, you know, disappear until late-late? I’ll owe you one."

    Shun "I don’t mind, but what about Hideki?"

    Kevin smirk closed "I’ll take care of him. Trust me."

    "I trust Kevin, against my better judgment. He presses a button on the remote and yet another old movie starts up, waiting for him to come back it seems."

    scene bg dorm
    show kevin smirk sides open at leftish
    with bg_change_minutes
    "In the middle of the movie, the second the clock clicked to 6:30pm, Hideki came out of his room. He robotically marched to the fridge, opened it and grabbed a prepacked lunch in a neat container."

    Kevin smile "Hey Mr. Social. Did you cure cancer yet?"

    show hideki crossed pissed at rightish with config.say_attribute_transition
    Hideki "No …"

    "He growls. He had bags under his eyes, his hair was messy. He also looked like he hadn’t left his room all day."

    Kevin "You look like you need some rest brother."

    Hideki "I disagree …"

    "Another snarl."

    Kevin "I had a dream where I was Dr. Jones and you were that cobra in the pit."

    Hideki hips bored "I don’t appreciate that joke Kevin."

    Kevin serious crossed "Do you ever?"

    hide hideki with config.say_attribute_transition
    "Not looking at us, Hideki went back into his room. He slams it shut, as usual."

    Kevin "Speaking of needing to relax right?"

    Shun "Ye—yeah. I guess you are used to dealing with workaholics."

    Kevin neutral "Eh. “Used to” is a term for it … hey! What’s this?"

    "As he repositions himself, Kevin pulls out a scrunched up black ball from the corner of the couch."

    Kevin "Huh. Look, a scrunchie."

    Shun "Is it … yours?"

    Kevin smile closed "Nah. Mine are pink. Black clashes with my hair."

    Shun "You know what I meant!"

    Kevin neutral open "Haha, it was a joke dude. Maybe Maryanne left it?"

    Kevin smile "Oh wait, you take it! Give it back to her whenever. Then you have an excuse to hang out again without work being involved."

    Shun "Oh … yeah, that’s not a bad idea. I mean, I’ll see her anyway but—"

    Kevin "You got this man."

    "He pats my back, stands up and walks to the fridge without pausing the TV."

    hide kevin with config.say_attribute_transition
    Kevin "Dinner time, dinner time … we still have cereal right? Or do we need to go to the cafeteria to steal some more?"

    "Ah … college."

    return
