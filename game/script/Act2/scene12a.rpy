##------------------------
## Act 2: Maryanne
## Scene 12a: Taco Night 2
##------------------------

label a2s12a:
    Shun "H—hi. Hideki. Uh, how are you?"

    play music "bickering_loop.mp3"
    Hideki crossed pissed "Let me guess … he stepped out for a minute? This is typical. Soooo typical."

    "Hideki grabs the plate Kevin put aside for him, sits down where Kevin was sitting, and starts eating. His glare drills through his glasses."

    Maryanne uncertain "Kevin and Shione had to go get something."

    Hideki "… Hmmm …"

    Maryanne "They—they should be back soon."

    Hideki "That is sooo something he would do. He’s about as clever as we all think he is. Well, at the very least he is consistent. I guess what they say about dogs learning new tricks is true."

    "One loud crunch after another comes out of his mouth as he scoffs down his portion."

    Maryanne mad "He’s not a bad person."

    "Hideki stops eating. I stop breathing."

    Hideki glare "I beg your pardon?"

    Maryanne "I said he’s … not a bad a person."

    Hideki "Yeah, says someone who doesn’t have to live with him for months on end."

    Maryanne "He put food aside for you because he wanted to be nice. Without you asking."

    Hideki "Bleh. He got better. Now instead of tasting like rancid garbage it tastes like regular garbage."

    Maryanne worried "You never have anything good to say about anyone, do you?"

    Hideki armsup argue "How dare you?! My roommates know exactly how I feel about them. I might not be spitting out daisies and rainbows, but I have never lied to them. Not once. Can you say the same with your friends?"

    Maryanne mad "I don’t need to be mean to be honest with my friends. And I don’t need to be blunt or … weird … for people to notice me."

    Hideki "Weird?! Weird?!!!!"

    Hideki crossed pissed "No, of course you don’t. You’d never need to. You just need to be a woman, period, and people will notice you all by themselves, don’t they? It’s so much easier when you’re born with a default positive trait."

    Maryanne "Hey! Don’t be sexist just because you have problems with women."

    "Their argument gets more heated as Hideki pulls in facts and information not related to the original disagreement. Maryanne holds her ground. I stay the heck out of it."
    "I grab the bottle of wine and drink right out of the bottle. Fast. I pray to any and every higher being I can think of that they do not turn to me and ask for my input."

    Maryanne mad "What is your problem!?"

    Hideki armsup argue "My problem is that everyone else acts like it is all normal! No one wants to deal with all the crap they put up with, but no one wants to change it."
    Hideki "They just want to complain about it. No one criticizes the things that cause the problems, or the people who cause the problem, they act surprised when their lives run out of control and—"

    stop music fadeout config.fade_music
    Maryanne "I don’t know who broke your heart … but not everyone is like them."

    Hideki crossed glare "…"

    Maryanne "The way you treat people … and girls … it’s not fair. It isn’t right."

    Hideki crossed pissed "… Hmmph …"

    "He stops talking, crosses his arms and looks away. He doesn’t grumble or make a sly response, nor does he finish his meal. He just stays quiet … for once."
    "Maryanne mimics his body language, crossing her arms as well. Only she doesn’t look away. She keeps her eyes locked on him, ready to verbally pounce on him again at the drop of a hat."
    "…"
    "… …"
    "… … …"

    show maryanne worried
    show kevin smile crossed closed at center
    show shione finger excited at right
    with config.say_attribute_transition
    Kevin "Hey—o! We’re back!"

    "Thank God."

    play music "bickering_loop.mp3"
    Shione "We didn’t find the “thing” or the Fusao-Thing, but we found these things. See?"

    "Kevin returns with large cases of beer under both of his arms, and a bottle of vodka in his grip. Shione carries a tub of ice cream. Both look icey cold."
    "I jump to my feet to help them. Maryanne stays put, not looking away from Hideki. Hideki doesn’t look away either."

    Kevin open smirk "What, did we miss anything?"

    show maryanne neutral
    show hideki hips bored
    with config.say_attribute_transition
    Maryanne "… Nothing." (multiple=2)
    Hideki "… Nothing." (multiple=2)

    Kevin laugh closed "Righteous. Let’s get drunk … er!"

    Shun "We just ate man!"

    Kevin smile open "Yeah, so that means now we can drink more now cause we have food in our stomachs. Don’t you know how science works?"

    Shione exclaim "He’s right Shun. Science says that. It’s in the book. If you read the Science Book, you’d know."

    "Kevin cracks a can open. He throws it back and it is gone in seconds. He drops a can on the table and pats Hideki on the back."

    Kevin "Munching down on the Mexican grub, huh?"

    Hideki "… Yes. It is … very good. Thank you for making it."

    Kevin "… Holy shit, that sounded like a compliment."

    Hideki "It … was. I can be appreciative when I want to be. Is that so strange?"

    Kevin "Nah dude, nah. If you wanna be nice for once, I won’t stop you."

    Hideki crossed glare "You know, I take it back. I was only attempting to make you feel better. It tastes like dog food! Right up your alley!"

    Kevin laugh closed "Hey, it’s better than what you made last time I left you alone with that stove. He tried to feed me horse-meat with road tar. I didn’t know that was a thing!"

    Hideki crossed pissed "You stupid punk!"

    Kevin mad crossed open "Stubborn dickhead!"

    Shione hips pompous "Jesus, why don’t you two get married already! You already live together and cook for each other. Wake up!"

    Kevin "…"

    Hideki "…"

    Shun "…"

    Maryanne ecstatic "… Drinks!"

    Shun "Yes. Many." (multiple=3)
    Kevin "Yes. Many." (multiple=3)
    Hideki "Yes. Many." (multiple=3)

    stop music
    scene bg dorm
    show maryanne happy hairdown:
        zoom 1.0
        xalign 0.1 yalign 0.0 yanchor -0.2
    show kevin crossed smile open:
        zoom 1.0
        xalign 0.3 yalign 0.0 yanchor -0.2
    show hideki crossed begrudging:
        zoom 1.0
        xalign 0.7 yalign 0.0 yanchor -0.2
    show shione hips excited:
        zoom 1.0
        xalign 0.9 yalign 0.0 yanchor -0.2
    with bg_change_minutes
    play music "happy_hangout_loop.mp3"
    "The tension is diffused gradually as each drink is drunk. Shione and Kevin get into a small drinking contest halfway through their second helpings."

    Kevin "This is fun … you buzzed yet?"

    Shione "Bzzzzzzzzzzzzzz~!"

    Kevin "Haha! Yeah, you’re buzzed."

    "Hideki is the only one to touch the bottle of vodka, and goes through several straight shots without flinching. His cheeks are rosy before long and he sways a little in his seat, but he still look annoyed and stays quiet."
    "For once, Hideki smiles … kinda."

    Kevin "Hey … hey. Professor, hey."

    "Kevin taps Hideki’s shoulder."

    Shione wonder "Who is the professor?"

    Kevin "Shh, shhh, shhh. When Hideki gets drunk on hard liqour, he getz in his head space and becomes all philosophical and deep. Watch."

    show hideki crossed smile with config.say_attribute_transition
    "Kevin taps Hideki’s shoulder again. Hideki abruptly turns his head, his body bobbing back and forth. He finally smiles, though it looks incredibly awkward on him."

    Kevin "Professor. Professor! Share your wisdom with us."

    Hideki "We … are all made … of ssstardust. From a bunch of galaxies … from aaall over the place."

    "He spreads his hands out, simulating a blooming rainbow, nearly slamming his chin on the table as his head falls off his palm."

    Kevin "Mar. E. Anne! Why are you not drinking more?"

    Maryanne "… I’m fine."

    Kevin "Come oooon. You only had likes a one. Or two. I think."

    Maryanne embarrassed "I’m not in the mood to get drunk. And I’m stuffed."

    Kevin "Pffft. No fun. Shione, drink another with me."

    show shione finger excited at p4_4 with config.say_attribute_transition
    Shione "Yes shher!"

    "Shione stands and salutes. Then stumbles on her way to the fridge, her legs flicking around as she walks."

    hide shione with config.say_attribute_transition
    Shun "Uh … is she okay?"

    Maryanne uncertain "Probably not. She’s not good at—Shione!"

    "Shione nearly falls over, but catches herself on the open refrigerator door."

    Shione "I got it! I caught me! Hooray for … me! Thank you Fridge! Hehe!"

    Maryanne happy "I think she’s had enough."

    Kevin closed "She’s fiiine! I say we ask an impartial. What do you say Professor?"

    Hideki "You … are the universe … manifesting itself … as a human being."

    Maryanne "Okay, great. Shione is about to fall over."

    Shione "No! I’m about to take off! Brrrrr!"

    "Shione holds her arms out, like an airplane, a beer can at the end of each “wing”. She comes in for a landing in front of Kevin’s knees."

    Kevin "Th—thank you piolet! You have shaved the cargo! Heh …"

    "Kevin takes her cans and Shione salutes him again."

    show shione finger excited at p4_4 with config.say_attribute_transition
    Shione "This is a lot of {i}hic{/i} fun!"

    show maryanne uncomfortable with config.say_attribute_transition
    "Maryanne rubs her forehead, concerned. She still hasn’t finished her first drink."
    "I am only on my second myself. It felt wrong drinking so close to finals, but I also did not want Maryanne to feel excluded."

    Shione hips "HEY! Hey! Mary—e … ann! What this, I’m going to do a backflip."

    Maryanne worried "NO!"

    show shione hips with config.say_attribute_transition
    "Maryanne pulls on Shione’s top and drags her to the ground. Shione giggles, wriggling as her friend tries to control her, not cooperating."

    Maryanne uncertain "I think that’s enough for … one night."

    Kevin open laugh "Booo! Buzzkill!"

    Shione "Yeah! Buzzkill! You’re killer of buzzes … you’re a … you’re a buzz murderer! I sssentence yous to buzz-jail!"

    Maryanne worried "Fine, take me to buzz-prison. But get some water."

    "Maryanne tugs Shione to her feet, then guides her to the door. Shione, being as uncooperative as a toddler in a toy store, holds her arms out like a plane."

    Shione "Weee~! I feel like I am flying!"

    Maryanne "Yeah, okay. Bring it in for a landing."
    Maryanne happy "Kevin, Thanks for the … evening. Bye Shun."

    Shun "B—bye. I’ll talk to you later."

    Shione "Byeee! See you laters and probably after that!"

    hide maryanne
    hide shione
    with config.say_attribute_transition
    "Maryanne smiles at me as she pulls Shione out to the hall."
    "The moment the door closes, Kevin punches my shoulder."

    Shun "Ow! Easy!"

    Kevin laugh closed "So, what happened? Tell me, tell me! Hehe …"

    Shun "I kissed her."

    Kevin open "Yeeeeaaah! Up top!"

    "Kevin holds his hand up. Hideki drunkenly smacks it before I can."

    Kevin smile "Congrats Shun. You’re totally in."

    Shun "I don’t know. Hideki kinda screwed tonight up for me."

    Kevin mad crossed open "What’d he do, huh?"

    Shun "He walked in on us … and … there might have been an argument. That he started."

    Kevin sides "Jesus. Hideki, what gives man?"

    Hideki crossed smile "{em}hiccup{/em} … Forgiveness … comes … the moment you accept … that you have made a mistake."

    Kevin smirk "He’s says he’s sorry. Kinda."
    Kevin "Don’t sweat it man. The little chatter box told me that Maryanne has the hots for you anyway."

    Shun "Really?!"

    Kevin "You’re {em}that{/em} surprised after she kissed you? You didn’t figure it out on your own?"

    Shun "Sure, but … it’s still nice to hear it."

    Kevin laugh closed "Hell yeah man. Anyway, Professor … tell us something profound."

    show kevin open with config.say_attribute_transition
    "Kevin slams his hand on Hideki’s shoulder. Hideki hiccups again."

    Hideki crossed begrudging "An electron … exists everywhere … and nowhere … at the same time. Just like our fears. And our … dreams."

    Kevin smile "Haha! If you think this is great, you should sssee him on whiskey. Listen to da Professor here. He know’s what—"

    Shun "AHHH! NO! NONONONO!"

    Kevin "What’s wrong?"

    Shun "I forgot to get her phone number again!"

    Kevin laugh closed "Still?! Oh my god, how-how-how the hell did you get in this ssschool?"

    Shun "Do you have her number?"

    Kevin open "Yeah, but I am not giving it to you. You gotta get it yourself."

    Shun "What?! Oh come on, please?"

    Kevin smirk "There’s a “5” in it. That’s the only hint you’re getting."

    "Kevin finishes another beer and burps so loudly it makes my ears shake."

    Kevin smile "Alright, night-cap swallowed. Time for bed. Come on Professor, back to the lair."

    "Kevin wrangles Hideki and the limp limbs flapping all over. Hideki can barely walk straight. Eventually, Kevin just picks him off the ground and carries him over his shoulder."

    Hideki "Your body has {em}hiccup{/em} … enough iron … to make a three inch nail."

    Kevin "Yes, yes, you’re making great use of that fact-of-the-day calendar. Now go to bed."
    stop music fadeout config.fade_music
    hide kevin
    hide hideki
    with config.say_attribute_transition
    "Kevin puts Hideki away. I sneak into my bed before he can see me again. I don’t feel like talking to anyone tonight."

    stop music
    scene bg room_shun night with bg_change_seconds
    play music "decompression_loop.mp3"
    "… I got a kiss!"
    "My heart is practically jumping out of my chest. I kick myself for not getting the phone number … again. And for not standing up to Hideki when he bothered her, even though it worked out. Something in my gut says the night could have gone better and I feel slightly disappointed."
    "But still … I kissed her!"
    "I lay down and try to think of a way to make sure the next time I see Maryanne … things work out a little better."
    "As I drift off, I think of how nice her hair smelled. I can still smell her on me."
    return
