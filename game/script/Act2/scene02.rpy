##------------------
## Act 2: Maryanne
## Scene 2: Wits End
##------------------

label a2s2:
    scene bg class_okada with bg_change_days
    "Okada rambles on, reviewing subjects I have thankfully already memorized. As he had warned at the beginning of the semester, half of the original class has vanished by now."
    "Leaving a class early prevented your GPA from going down. So quite a few students have dropped class to avoid a permanent blotch on their record."
    "Risa is still here … well, she is present. She’s not truly “there”. She has a consistently pent up look, like her mind is far off. At the end of every class, she gets up and leaves like a robot, mechanically and with purpose. One programmed to avoid class."
    "Okada didn’t care who left his class, and still does not. He has the respect of those who remain, but nothing else."

    show okada neutral with config.say_attribute_transition
    Okada "… Midterms are on their way. You have all missed your chance to drop this class without jeopardizing your grade. Now … you are all in it for the long haul. Sink or swim."
    Okada "I hope some of you at least know how to float."

    "{snd}riiing{/snd}"
    "I still leave his class in a hurry the second the bell rings. It’s so I don’t keep Maryanne waiting, obviously. It only {em}looks{/em} like I am running away from Okada as fast as I can, I tell myself."

    scene bg hall_school with bg_change_seconds
    "The bulk of my homework is already finished. The last assignment is Okada’s refresher to prepare us for the midterm. But I’d like to have Maryanne check it over before I hand it in. Better to be safe than … float."
    "Besides, I’d be doing Kevin a favor too. If I spend time with her, we both win."

    scene bg lab_maryanne with bg_change_seconds
    "I peek in on the lab, and see Maryanne hunched over a microscope. Her eyes glued to the device, she doesn’t hear me come in, nor does she look away as I put my bag down next to her."

    Shun "…"

    Maryanne "…"

    Shun "… Hey, I—"

    Maryanne "Ooone second …"

    "She doesn’t look up, humming softly as she continues her examination."
    "I sit down and wait. Quietly. There is a stack of empty paper cups at the edge of her table that make the room smell like coffee."
    "I notice a few strands of her gold hair are sticking out, like she hadn’t brushed it today. And her clothes look very similar to what she was wearing when I saw her at Yamada’s yesterday."
    "Finally, she turns towards me."

    show maryanne happy coat circles tangled with config.say_attribute_transition
    stop music fadeout config.fade_music
    Maryanne "… Aaand … done!"

    "She sniffles loudly twice, then coughs. Her smile is crooked. Black rings hang under her eyes."

    Shun "Are you okay?"

    Maryanne "Um yeah. Of course. I’m fine! Where’s your homework?"

    play music "unrest_loop.mp3"
    Shun "I don’t actually have a lot of homework today."

    Maryanne "O—oh. Um … that’s good! That’s very good. Very good, yeah. You’re aaall caught up. Do you still need help studying?"

    "Her eyes flutter closed for a second. Her fingers rap on the table like a metronome."

    Shun "A—are you sure you’re alright? You seem a bit … twitchy."

    Maryanne confused "I had a few shots of espresso. It keeps me focused. Alert. Wiiide awake. Nothing wrong with that right!?"

    "Out of the corner of my eye, I see the green cot peeking out of under the table. It is folded up poorly, with one of the legs sticking out and the blanket sloppily crumpled into what I think is a make-shift pillow."

    Shun "… How long have you been in here?"

    Maryanne "I don’t know. A while. Not long. Only a bit. Why?"

    "Okay, so maybe Kevin {em}was{/em} on to something when he said she worked too much."

    Shun "How about we study outside, hmm? Get some fresh air, stretch your legs a bit?"

    Maryanne happy "Nope. Nope. No. I’m okay. This chair is comfy and it’s air conditioned here and I’m a-oookay."

    "Her fingers rap and rap and rap and rap and rap and rap on the table as she tries to look like she is not tweaking on far too much caffeine."

    Shun "Oh. I’m glad you’re … okay. Then … I guess I’ll see you later."

    hide maryanne with config.say_attribute_transition
    "She nods a few times, not saying a word. And continues to rap and rap and rap and rap and rap. She doesn’t stop until I am walking out of the room."

    scene bg hall_school with bg_change_seconds
    "I wait outside the science lab door. And wait. And optimistically imagine Maryanne coming out on her own to take up my offer."
    "This was twice in less than a couple of months that I’ve seen her get this high strung. Three times if you count the incident with her necklace. No wonder Kevin worries about her."
    "I wonder if Kevin or Shione saw Maryanne go through this before, or if this was triggered by something or … and I then I realize I forgot my backpack inside! Great …"
    "I turn around and hurry back, hoping this time Maryanne will be willing to leave her lab."

    scene bg lab_maryanne with bg_change_seconds
    "I swing the door open, figuring that no matter how much noise I make, I wouldn’t be able to distract Maryanne from her work."
    "My backpack is next to her seat. She is, again,  hunched over. This time on her textbook, again not acknowledging me. She doesn’t look in my direction as I walk to her. Or move as I pick my backpack up."

    Shun "Maryanne?"

    Maryanne "… {em}zzz{/em} … {em}zzz{/em} … {em}zzz{/em} …"

    "On her open textbook, pen still in hand, Maryanne sleeps on her textbook. The light on the microscope is still on, illuminating the side of her face. She must have passed out shortly after I left."

    Shun "Maryanne."

    "I tap her shoulder gently. She mumbles, but doesn’t open her eyes."

    Shun "Maryanne. Wake up."

    "I push her shoulder, gently but hard enough to budge her a little."

    show maryanne worried circles coat tangled with config.say_attribute_transition
    Maryanne "… !"

    "Her eyes spring open and her back snaps up straight with a sudden and violent jolt. She stares forward aimlessly for a few seconds, then turns to me."

    Maryanne confused "… What happened?"

    Shun "You nodded off."

    Maryanne "…"
    Maryanne "… I need more coffee."

    Shun "I think what you {em}need{/em} is a break."

    Maryanne worried "I’m okay! Really."
    Maryanne "I wrote my notes out and now I need to type them and then I have to call the graduate school for more details and then Kyoko wanted help with the science club and—"

    "She holds up her notes, showing them to me like she needs to prove she has more to do."

    Shun "Are you hungry?"
    stop music
    "She stops talking."

    Shun "That Rainy-Day special from Yamada’s … I have some leftovers at my place. It’s yours if you leave. Now. Right now."

    "Maryanne’s eyes dart back and forth from my face to her microscope. She looks a bit more guilt-ridden every time her eyeballs shift."

    Shun "And you can come right back afterwards."

    Maryanne confused "Uhhh … I … I could eat something. Something small."

    Shun "Okay. Good."

    "In silence, she stands up. She looks like a child who didn’t want me to get her in trouble. Or maybe looked that way because she could barely stand up. She swayed a little as she walked, yawning deeply every few minutes."
