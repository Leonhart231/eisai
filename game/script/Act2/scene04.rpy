##--------------------
## Act 2: Maryanne
## Scene 4: Meltdown 1
##--------------------

label a2s4:
    stop music
    scene bg takuya with bg_change_days
    play music "decompression_loop.mp3"
    "After Maryanne left last night, Kevin and I had a feast of several slices of week old cheese and slightly stale crackers. In response to this, as of last night I volunteered to be the official food-buyer for the dorm."
    "Hideki, Kevin and I all pooled as much spare change as we could for some basic necessities that would (hopefully) last more than a week. Hideki’s math skills aid in that budget."
    "An hour after I start shopping, I proudly check off the final item on the dorm’s grocery list and start lugging the five heavy bags back to campus. After I take care of this, I have the rest of the afternoon off. The final chapter of my book is waiting for me."
    "“Nothing” was starting to become one of my favorite things to do. After going through hell back home, I was starting to enjoy … quiet."
    "Sure, doing nothing consistently sounded like a bad idea, and I doubt I ever give up my old studying habits, but I was beginning to see why those slackers in high school seemed to be relaxed. It {em}is{/em} fun to just … hang out. As long as you’re not doing it alone."
    "Maybe that was what I was missing back home. Someone to do nothing with …"

    scene bg inou with bg_change_seconds
    "Walking past the girl’s dorm reminds me I still have Maryanne’s scrunchie. A couple of days have passed since she accidentally took a nap on our couch. I hadn’t seen her since then due to an unexpected surge of homework assignments from all directions."
    "Now seemed like a good a time as any to drop it off."
    "I see Risa sitting outside the girl’s dorm with an acoustic guitar, humming and strumming softly, bobbing her head as she jams by herself. She sees me and nods. I take that as an invitation to come over."

    show risa neutral with config.say_attribute_transition
    Shun "Hey!"

    Risa "Hi."

    Shun "How are you?"

    Risa "I woke up and I am above ground. So … I’m doing great. As long as you can breathe, the day is good. That is how I figure it."

    Shun "O—oh … okay."

    Risa happy "How have you been?"

    Shun "Good. I’ve been relaxing."

    Risa "Everyone is doing that lately. Cept that blonde girl you’re dating."

    Shun "Whoa, whoa, back up there. I’m {em}not{/em} dating her. She’s a friend."

    Risa pout "Friend? Really? Hmm … that is not what some people have said."

    Shun "Well I don’t … wait, what?! Who said that?"

    Risa neutral "People. It’s always people who say things."

    Shun "Yeah but … I mean, what did you hear?"

    Risa "Oh … you know."

    Shun "… Uh, no. In fact, I do not."

    "Risa stares at me without blinking. Maybe to make a point."

    Shun "… Um, anyway … do you know if Maryanne’s around?"

    Risa "Why would I know that?"

    Shun "Because … you live here, right?"

    Risa "Only for now …"

    Shun "… Okay, thanks."

    "Risa gets up, pats my shoulder softly."

    show risa happy with config.say_attribute_transition
    Risa "Good luck … and don’t be stupid."

    hide risa with config.say_attribute_transition
    "She leaves before I can inquire further, swinging her guitar case over her shoulder like an axe."

    Shun "Yeah … well … you know … you don’t do anything stupid either. Bye."

    "Man, I really have to work on my comebacks."

    scene bg hall_dorm_girls with bg_change_minutes
    stop music fadeout config.fade_music
    "I find Maryanne’s and Shione’s room … 2D, in the middle of the hall."
    "I lift my fist to knock … and freeze."
    "I clear my throat and double check to make sure I am at the right door. Then I brush my hand over my hair. Then I second guess myself. Then a third time."
    "I raise my hand again …"
    "… Maybe she’s busy now. I should come back later …"
    "As I hesitate, the grocery bag on my wrist swings and taps the door, loud enough to constitute a knock!"
    "Why didn’t I drop this stuff off at the dorm first?!"
    "My heart races … but no one answers the accidental knock."
    "Phew, thank goodness. I turn to leave … but then I hear something from the other side of the door. It sounds like … paper being crumpled up and stepped on."
    "I inhale and gently knock, this time intentionally."
    "No answer."

    Shun "H—hello?"

    "I turn the knob and the door opens. A rush of perfumed air hits my nostrils."

    scene bg dorm_maryanne with bg_change_seconds

    "I let myself in. Compared to my dorm, Maryanne’s room feels like a different world. A fresher, less putrid, one. Organized and, oh my god … clean?! This is possible in college?"
    "However, I notice a pot of coffee and several used mugs on the kitchen counter as I let myself in."

    Shun "I—is anyone here?!"

    play music "romantic.mp3"
    Maryanne "Mmm!"

    scene bg room_maryanne with bg_change_seconds
    "I hear a familiar grunt coming from the couch. Maryanne is hunched over the coffee table, tapping a pencil on the arm of her chair like it was a drumstick on a snare."
    "A spiral notebook is on her lap, a jagged line of paper hanging off its rings. She rips off a page the notebook, makes a ball out of it, and tosses it over her shoulder."
    "I am sure she doesn’t know I am there, or doesn’t care. A voice tells me to run while I still can, but I ignore it because … why start listening to it now?"

    Shun "Uh … Maryanne?"

    show maryanne confused tangled circles with vpunch
    Maryanne "{em}Aah! What?!{/em}"

    "Her hair has about three dozen split ends spiking off in every direction. She looks (and smells) like she hadn’t slept since her catnap on my couch."

    Maryanne neutral "What … time is it?"

    "Trembling, she rubs her nose with the back of her hands. Her left eye twitches once."

    Shun "Are … you okay?"

    Maryanne uncomfortable "I was … I was just … trying to …"

    "I see a mountain of styrofoam coffee cups pack in her garbage bin, all dripping with leftover brown ooze."

    Shun "Please tell me you haven’t been working all night?"

    Maryanne worried "I … wasn’t. I got sleep. {em}Some{/em} sleep."

    Shun "Okay, that’s it! Time to stop."

    "I grab her hand and pull her to her feet, gently taking her out of her room."

    scene bg dorm_maryanne
    show maryanne worried tangled circles
    with bg_change_seconds
    Maryanne "Stop? No, no, no, no, I can’t stop. Don’t you get it? I need to get this done because—"

    Shun "Because why? Because if you don’t, you might catch your breath?"

    "She’s far to weak and spaced out to put up a counter argument as I guide her to the couch."

    Shun "Just … give me the notepad. You’ve done enough work for one day."

    "Maryanne looks at her shoes, an embarrassed frown appearing on her face. After a moment of dwelling, she hands me the book. I toss it on her bed before she could ask for it back."

    Shun "Let’s go."

    Maryanne confused "Wh—where?"

    Shun "I don’t know, out. Just come with me. You need to get some air or something."

    "I try to sound forceful but not mean and also not so terribly nervous or uncomfortable. I’m pretty sure I fail at hiding at least one of those."
    "But still, Maryanne passively nods and stands up, ready to follow me in her sleep deprived stupor."

    scene bg inou
    show maryanne sad tangled circles
    with bg_change_minutes
    "Maryanne cautiously looks around as we walk through campus, hoping no one else would see her the way she is."
    "But they do. And the whispering starts."

    Maryanne "…"

    Shun "Calm down. Just … breathe."

    Maryanne "…"

    Shun "There’s no point to put so much stress on yourself, is there?"

    Maryanne "…"

    Shun "… Uh, so … the first day I got here, Kevin said something like … work will always be there. And that’s why you need to enjoy yourself while you can. Especially since after school, you won’t get as many days off as you’d like."

    menu:
        Maryanne "… I like working."
        "You’re taking it too far.":
            Shun "I know you do. But you’re taking it too far. Way too far. It isn’t healthy and it isn’t good for you. You must know that, right?"
            show maryanne worried with config.say_attribute_transition
            "Maryanne frowns again, touching the side of her arm and looking at the ground."
        "You’ve earned a break.":
            Shun "I know you do. But … you’ve earned a break. You’re not letting anyone down by taking a breather. You can let yourself have a little down time, even for a little while."
            "Maryanne looks at the ground again, touching the side of her arm as her guilty conscience weighs her down."

    "Two girls near us whisper a bit too loudly. I can hear them say something not very nice, and move Maryanne away."
    Shun "Um, let’s … okay, this way. Let’s go … somewhere else."

    scene bg dorm
    show maryanne uncomfortable circles tangled
    with bg_change_minutes

    "Maryanne fidgets and repositions her legs for a fourth time. She sigh loudly; she is the only sound in the room."

    Maryanne worried "… This is hard!"

    Shun "You’re {em}making{/em} it hard! “Doing nothing” is literally the easiest thing in the world."

    "I say that like I am suddenly an expert at not caring. But I can empathize with what she’s going through. I know what it’s like … the pressure she must feel."
    "To succeed, to get everything right the first time … to lock yourself away and do things by yourself … not by choice, but I still get it."
    "She grumbles and fidgets, trying to stay still and do literally nothing in my room. No phone, no TV, no work. Nothing but silence. She fixes her shirt and rubs her forearm every few seconds."
    "I turn the page of my book, letting her figure out how to be still."

    Maryanne confused "Maybe I should—"

    Shun "Maryanne … relax!"

    Maryanne worried "But I … there’s … the science room needs some documents reviewed and—"

    Shun "Take it easy. You look like you’re going to explode. It’s not good for you."

    "She re-positions herself and pushes her legs under our table. Ten seconds pass before she sighs again. Who knew that teaching someone how to be lazy would be so challenging?"

    Maryanne neutral "Do you do this a lot?"

    Shun "Just when I need to."

    Maryanne uncomfortable "… How often do you {em}need{/em} to do it?"

    Shun "I don’t know. Whenever I get a chance. Between classes some days. When there is nothing else to do I mean."

    show maryanne worried with config.say_attribute_transition
    "Maryanne rubs her forehead again. She bares the face of a worried mother and a guilty child simultaneously."
    stop music fadeout config.fade_music
    "Eventually, after much resistance, silence fills the room. Maryanne looks around my living room, checking the corners, the kitchen, the TV, examining the Skullgirls poster over the couch."
    "The clock on the wall ticks as she looks around. She raps her fingers on the table several times. Over and over and over and over again, killing the quiet."

    Maryanne "… Is that long enough?"
    play music "romantic.mp3"
    Shun "Okay, you know what … let’s get some tea! I can cook up a pot in ten, fifteen minutes."

    Maryanne sad "Yeah. Tea. Okay. Tea sounds … okay."

    show maryanne happy with config.say_attribute_transition
    "She cups her hands together, lacing her fingers over one another, and nods. I put the kettle on and sit back next to her. She feigned a smile as I do."

    Shun "Well …"

    Maryanne "Well …"
    Maryanne "Where is everyone else?"

    Shun "I don’t know. Probably in class or out. I know Hideki has a night class this week."

    "She covers her mouth to cough, then re-laces her fingers over each other, like the first time she did it was incorrect."

    Shun "You’re really having trouble with this, aren’t you?"

    Maryanne neutral "I’m sorry. I just … I haven’t done this in a while and I’m used to doing stuff and …"
    Maryanne worried "I like schedules! I like plans, okay! What’s wrong with having plans?!"

    "Her tone quickly becomes defensive, so I pick my next words carefully."

    Shun "Well … look at you right now. Without plans, you don’t know what to do. I mean, planning things is fine. I do it too. But you’re so fixated on keeping yourself busy that you don’t know how to handle your own down time."

    Maryanne mad "I … I … That’s not true!"

    Maryanne stressed "… Is it?"

    Shun "What are you thinking about right now?"

    Maryanne uncertain "That I should be working?"

    Shun "See? That’s the thing. Even when you are not working, you want to work. You are worried about stuff you don’t need to worry about."

    show maryanne neutral with config.say_attribute_transition
    "Maryanne’s slouches slightly. She stares at the center of the table and says nothing."
    "More silence goes by. She fiddles her thumbs together as the clock ticks."

    Maryanne sad "… Do you think I’m weird?"

    Shun "No, of course not! You just need to remember to have fun every now and then."

    Maryanne "… Why?"

    Shun "What?"

    Maryanne "Why do I need to … never mind."

    "I stay quiet and wait for her to say something else. It feels like a long time passes before she clears her throat."

    Maryanne "I … I don’t …"

    "I wait. I say nothing. Waiting for her to finish. She knows what I’m doing. I can see it annoys her that I am not changing the subject. But eventually, she continues."

    Maryanne worried "… I don’t know why other people … have fun. More than me."
    Maryanne "I don’t get it. It just seems that … everyone is having fun and I’m not. It comes so easy for other people. They … just … have fun. Without worrying or getting busy."
    Maryanne crying "I don’t know if I’m weird or if there is something wrong with me or what. But that’s the way it always is. Me, by myself, working. I don’t know how other people do it."
    Maryanne sad "I don’t know what else am I supposed to do?"

    "Her confession echoes all too familiar anxieties."

    Shun "I don’t think you’re weird."
    Shun "It’s not easy to change. Even when and if you want to. But if you do the same thing over and over again, you’ll just get the same results. If you’ve been working all the time, maybe it is time to … {em}not{/em} work so much. For a while. And see what happens."
    Shun "You just need to practice."

    Maryanne uncertain "Practice doing nothing?"

    Shun "Exactly. It takes time to learn anything you are not familiar with. If you keep exhausting yourself, you won’t be able to do anything. You might as well enjoy yourself while you can."

    Maryanne neutral "Hmmm …"

    "Maryanne rubs her chin as the gears in her head start to turn."

    stop music fadeout config.fade_music
    Maryanne happy "Yeah. Yeah! Okay, I think I got it now!"

    Shun "Really?"
    play music "bickering_loop.mp3"
    Maryanne "If I learn how to de-stress, then I can get {em}more{/em} work done after that! {em}And{/em} I don’t have to be alone all the time."
    Maryanne ecstatic "This is great! Thanks Shun."

    Shun "N—no problem."

    "My teeth grit as I say that."

    Shun "Just promise me that you {em}will{/em} give this a shot?"

    Maryanne happy "I promise. Cross-my-heart and hope-to-not-die."

    "She draws an invisible “X” on her chest, then holds out her pinkie towards me."

    Shun "Hope-to-not-die?"

    Maryanne stressed "… Well yeah. Why would I say hope-to-die? That doesn’t make sense."

    "Seeing her point, I oblige her and pinkie-swear. She giggles softly, stands up and, with great resolve, makes a fist."

    Maryanne ecstatic "Okay! That was a good first lesson. I feel relaxed already! So, I guess I’ll see you later, right?"

    Shun "Ha-ha, nice try. Sit down. The water in the teapot hasn’t even boiled yet."

    show maryanne worried with config.say_attribute_transition
    "Maryanne grumbles and she pushes her legs back under the table. She goes back to playing with her thumbs as we wait for the tea to finish."

    return
