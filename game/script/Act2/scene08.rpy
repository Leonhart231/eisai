##-------------------------------
## Act 2: Maryanne
## Scene 8: Doubts, A Skip Forwad
##-------------------------------

label a2s8:
    stop music
    scene bg inou with bg_change_days
    play music "contemplative_loop.mp3"
    "I didn’t sleep well. My brain wouldn’t let me. And those same thoughts follow me around all morning like a stalker. The loudest and most nagging being “what should I do next?”."
    "I had never had a girlfriend before, nor had I dated a girl who was older than me. She was probably more … experienced than me. What was she expecting? What did she want me to do?"
    "… What did she even see in me?"
    "The negative thoughts festered like a flu as I try, melting over the positive ones. I get into a fight in my own head as I try desperately to convince myself that I’m over-thinking things. And not paying attention to where I’m going …"

    Shione "Ahhh! We’re going down!"
    scene bg inou with vpunch
    stop music
    "Shione tumbles over me with a loud crash. A few papers explode out of her bag and fall on the walkway, followed by a cardboard box that lands over my head. Several tools fall out of the box too, all thankfully missing my head."

    show shione hips pompous with config.say_attribute_transition
    Shione "Not my fault! I had the right of way!"

    play music "happy_hangout_loop.mp3"
    Shun "S—sorry. I wasn’t … I wasn’t watching where I was going."

    Shione finger "Well I was. I made all these hand signals and you didn’t see them!"

    Shun "Uh … you were holding a box. Your hands were full."

    Shione "That is why you didn’t see the signals!!"

    "Shione slams her, now empty, box on the ground and quickly starts repackaging it with the papers."

    Shun "In a hurry?"

    Shione hips exclaim "Always. I have to get all of these forms to the guy who does the internship thingy but I forgot his name so I have to find the person who knows his name. So now I have find that person that knows the person who knows that person’s name."

    Shun "… Oh, is that all?"

    Shione "Yes! That’s all! It’s like a Scavenger Hunt. And since I am so good at collecting people … it’ll be a snap!"

    "She snatches the paper out of my hand and shoves it in her box, her nose pointing in the air. The tools clink against each other."

    Shione "’Cuse me. I have to go find the person who knows the person who knows that other person."

    Shun "Wait! Hang on. Can I ask you a question?"

    Shione wonder "Sure you can, you can do anything you want. You can do a cartwheel, go skydiving, wrestle an alligator … if you can find an alligator."

    Shun "Let me rephrase that. Can I ask you a question that you will answer?"

    Shione finger "Maybe. Depends on the question."

    Shun "W—well, it’s about Maryanne. Has she … said anything about me?"

    Shione excited "Yeeep."

    Shun "Really?"

    Shione "Really-really."

    Shun "Can you tell me what she said?"

    Shione hips wonder "Ha! No way. I’m not falling for that trick. I promised her I wouldn’t say a word about how she liked you and stuff. Not a peep. So nice try Loverboy."

    Shun "… Darn. I thought for sure that was going to work. Oh well."

    Shione finger "Haha, yep! She … oh, there he goes!"

    hide shione with config.say_attribute_transition
    "Shione suddenly bee-lines for a guy with a thick glasses, repeating the phrase “the guy who knows the guy” as she bolts towards him."
    "And once again I am left to think about what to do next."
    "Kevin said to just let it happen, and Shione said to do what I was doing up till now. It made sense, I guess, but somehow that wasn’t enough to keep the doubts away."
    "My phone buzzes."

    Kevin "{tt}Hey cn u pick up eggs?{/tt}"

    "I roll my eyes as I get Kevin’s request … but since he usually makes free food when I buy him stuff, I decide to be a good roommate and head into town."

    scene bg takuya with bg_change_minutes

    "The walk helps a little. My head is still heavy with excessive thought. I try to force an upbeat attitude and look around for good places to take Maryanne for a second date."

    scene bg takuya with bg_change_minutes

    "The eggs cost more than I thought they would. You’d think local shops would keep simple things like eggs at a low price. But nooo, they know we will pay regardless of the price so they … god, I sound like my father."
    "I hear that all young men have to turn into their dads at some point. I hope I can put that at least off until I get a decent job."

    scene bg inou with bg_change_minutes

    "I arrive back at campus. Most students are indoors now, studying, preparing for midterms."
    "I also see an older woman stopping in the middle of the street. She bends down and picks something off the ground. After a moment, she pouts and keeps walking, hands empty."
    "I see a flash on the top of the hill. The sun reflects off a pair of thick glasses. Hideki is there, under the red maple in the center of campus."
    "He stares at the walkway, at the point where the old lady had bent over, watching through binoculars. He writes something on a notepad without turning his head."
    "Figuring that it couldn’t hurt to be nice, I scale the hill to greet him."

    Shun "Hello Hid—"

    Hideki "Shhhhhh~."

    "He hisses at me as he finishes writing something down. I wait. Eventually, he looks at me."

    show hideki hips bored with config.say_attribute_transition
    Hideki "… Shun. Good to see you are out and about again. You … bought food, is that right?"

    Shun "It’s for Kevin. He asked me to buy him eggs."
    Shun "What are you doing here?"

    Hideki chin think "Observing. Studying. Gathering information before I go back and report my findings."

    Shun "… Uh-huh. And what are you “researching”?"

    Hideki "A social experiment. Observe."

    hide hideki with config.say_attribute_transition
    "He hands me his binocular and nods towards the bottom of the hill. I see a shining speckle in the middle of the walkway. The 100 yen piece looks obnoxiously bright, like someone polished the object before putting it there."
    "A middle aged man walks towards the shiny glimmer. Hideki presses his pencil to the notepad and gets ready to write."
    "The man stops at the shiny mark, bends down to pick it up, but his finger slides off whatever it is. He tries again, then again and again, getting more frustrated with each attempt."
    "After a six or seven tries, he walks away, grumbling angrily to himself."

    show hideki chin think
    Hideki "Hmmm … 100 Yen is enough to make anyone stop. Fascinating. 5 Yen was far too little, as I hypothesized."

    Hideki "Over twenty unknowing participants helped me in today’s research of behavioral patterns regarding monetary value of inanimate objects."
    Hideki "I have also noted that more women stop for the shiny coin than men. Proving that females indeed like shiny objects as other studies have suggested."

    Shun "… This for personal research?"

    Hideki "That is the only research worth pursuing."

    Hideki "My results are … amusing. Oh, how I do enjoy my casual pursuits."

    Hideki "Oh! How good! Look, your female friend is coming."

    "Maryanne heads down the path, smiling brightly. She holds a white envelope in her hand, which she glances at several times as she walks. Almost like she wants to keep an eye on it."

    Hideki "Let us see if she falls for my trap, er, project. I predict that she will take the bait and said good mood shall be ruined, thus proving that greed is a cause for distress. As well as add additional support to my theory on females and sparkly things."

    "Maryanne crosses the 100 Yen coin. She passes by it, but looks back as she notices the sun reflecting off it. She stands over it and stares. For a few seconds. Examining it."
    "She kicks the coin, which naturally doesn’t move."
    "Maryanne shakes her head and keeps walking."
    "Growling, Hideki crumbles part of his notepad. His hair stands up on the back of his neck as his face turns red."

    Shun "Hmm, okay Hideki. Have fun pranking people."

    hide hideki with config.say_attribute_transition
    Hideki "It is a social experiment!!"

    scene bg inou_gate with bg_change_seconds
    "I hurry away, briskly rushing to see Maryanne as Hideki has another mental collapse."
    "I tap her on the shoulder twice, making her turn around quickly. She giggles softly and smiles."

    show maryanne happy with config.say_attribute_transition
    Shun "Hey."

    Maryanne ecstatic "Hi~. Slacking off again?"
    Shun "Oh, come on. I was just … you know, um, enjoying my time off."

    Maryanne happy "That’s so lucky. I wish I had a day off in the middle of the week."

    Shun "Please, you’d probably use it for studying. Again."

    show maryanne worried with config.say_attribute_transition
    "She gives me a pout, which quickly floats away. I notice she has an envelope in her hands, one slightly crumpled as she tries to cross her arms."

    Maryanne happy "Speaking of studying … I know you said that working too much is not good, but midterms are going to be here in less than two weeks. I need to get back in the lab and … you know."

    Shun "No, I understand. That’s important. Just don’t overdo it."

    Maryanne confused "And if you need … time by yourself, that’s okay too. I—if you need it! But you’re welcome to come to me too if … uh …"

    Shun "… After midterms, do you want to hang out again?"

    Maryanne happy "Yes!"

    "She says quickly, before I can say when and where or what. And not only that but she eagerly leans forward towards me, smiling again and waiting for me to say more."

    Shun "Gr—great! That would be—"

    Maryanne "That would be great! Right."

    Maryanne "Um, I gotta drop this off. One sec."

    "She presents the envelope to me. It is covered with red, white and blue stamps, and English words I cannot read."

    hide maryanne with config.say_attribute_transition
    "She hurries into the mail room, a small office tucked away on the bottom floor of the building nearest to us, and disappears for a moment."
    "I listen to the sound of the breeze running over the grass and sigh softly. The air smelt so much cleaner than Tokyo, or maybe that was just my imagination … whatever."
    "Maryanne pops back out in a few seconds."

    show maryanne ecstatic with config.say_attribute_transition
    Maryanne "Done and done!"

    Shun "What did you mail?"

    Maryanne happy "A letter to my grandpa. They don’t like computers or think cell phones are “what’s wrong with our generation” so this is usually how we stay in touch. Besides, getting a letter in the mail is just … nice, you know?"

    Shun "That’s really sweet. He’s going to love that!"

    Maryanne "Yeah. I hand wrote it too. I’m always surprised at how long it takes to write something by hand, but I like to do that intentionally sometimes. I don’t know, it’s soothing."

    "We talk for a few moments, both of us going back and forth on what we did the last few days. It wasn’t anything special but … it felt special. Better than normal. It wasn’t about anything really, not especially."
    "But I wanted to listen. And I wanted her to listen to me."
    "That lightness in my chest was getting harder to ignore, not that I wanted to."
    "We talk and talk and start walking into town. With no real plans, it was easy to just go with it, to just hang out and not worry."

    stop music
    scene bg takuya_bridge with bg_change_minutes
    play music "romantic.mp3"
    "We end up a bit off campus, over the bridge in town. I hear the river run underneath the bridge and the sounds of cars driving a few blocks by."
    "Maryanne turns and looks at the water. The wind picks up and it makes the reeds by the river bend dance."
    "She points to a stone bench by the side of the river, close to the edge of the water. As we walk towards it, things get quieter with every step, the road being blocked off by smooth grassy hills and soft running water."
    "I sit on the bench, and run my hand over the cold surface. The edges are rough but the places to sit are smooth as can be. Maryanne doesn’t sit down, but stands next to the bench, looking at the water curiously."
    "She kicks a pebble in the water and giggles as it splashes. Then she picks up a handful of wet pebbles and tosses on as far as she can get it."
    "Copying her, I pick up a stone and throw it. Mine falls significantly further than where hers do, landing with an explosive splash."
    "She cocks an eyebrow up and flicks a flat pebble over the water. It skips … skips … skips … skips … five times before gracefully sinking beneath the surface."

    show maryanne happy with config.say_attribute_transition
    Shun "How’d you do that?"

    Maryanne "You just … flick your wrist …"

    "She does it again. Skip … skip … skip … four skips this time."

    Maryanne "It’s easier when the water isn’t running fast."

    "I stand the way she stands and try to curve my wrist the way she did it. My rock flicks up high, and splashes about ten feet away from us."

    Maryanne embarrassed "No, no. It’s not about strength. It’s all about the angle. You have to toss it parallel to the water. But also, kinda downward. But just a little. And you use flat stones to …"

    show maryanne happy with config.say_attribute_transition
    "Her hand gently grabs mine. Her fingers are wet, and warm, and as her palm wraps around my knuckles I feel the dirt from those pebbles rub on me."
    "She stands behind me, her lining up her arm with mine while touching my shoulder. I try my absolutely hardest to not react when her chest touches my shoulder."
    "She guides my hand through the motion needed to pull this trick off, then steps back and lets me try it on my own. I feel guilty for only focusing on how nice it felt to have her hand on top of mine."
    "I toss my rock … but it doesn’t skip. Maryanne giggles again."

    Maryanne "You’ll get it."

    "Skip … skip … skip … her rock bounces six times off the water."

    Maryanne "I used to do this with my grandpa. He showed me how to skip rocks when I …"

    show maryanne neutral with config.say_attribute_transition
    "She stops talking, like she is worried I would be bored if she said more."

    Shun "When you what?"

    Maryanne happy "… I used to visit my grandparents every summer when I was a little girl. It was up in, uh, Trinity County."

    Shun "I don’t know where that is."

    Maryanne "It’s upstate California. There’s lots of trees and lakes. It’s really pretty …"

    Maryanne "But my grandpa would take me to the lake near his house and we’d sit down and skip rocks … and he’d tell stories about how he moved out from Maryland to Cali … and he told me stories about my mother …"
    Maryanne neutral "…"

    "Maryanne stares at the river for a moment. In silence."

    Maryanne happy "We didn’t do anything but skip rocks and talk. Oh, and eat. Grandma made us picnic-food all the time!"
    Maryanne neutral "Those were great summers."
    Maryanne uncertain "… I haven’t seen them in a long time."

    "She frowns, briefly wrestling with homesickness as she throws her last pebble across the stream. Seven skips."

    Shun "Do you ever think of going back?"

    Maryanne happy "All the time. I’ve been saving up money to go see them but … I’m just so … busy."
    Maryanne "I’ll go back. I’m sure. I hope I will … someday."

    show maryanne worried with config.say_attribute_transition
    "She frowns again. And this time nostalgia for her old country stays; I can see on her face that she misses them."

    menu:
        "I stand closer to her, ready to say something …"

        "I’m sure they miss you.":
            Shun "I’m sure they miss you too."

            Maryanne "… They do. I …"
            Maryanne "I know they do. They tell me they do."

            show maryanne happy with config.say_attribute_transition
            "She rubs under her eyes, then feigns a smile."
            "I think that upset her more."

        "You’ll see them again.":
            Shun "Don’t worry. You’ll see them again before you know."

            show maryanne happy with config.say_attribute_transition
            "She forces a smile and nods."

            Maryanne "You’re right! Before I know it."


        "Could they come visit?":
            Shun "Do you think they could come visit here?"

            Maryanne happy "In theory they could, but they hate flying. And they live on a budget … so, practically speaking, no. They can’t."

            Shun "Ah. I see."

            Maryanne "I’m sure they’d like it here though."

    "She nods a few times. Not saying anything. I pick up a smooth rock between my feet."

    Shun "This one, right here? This is the one that skips aaaall the way to the other side of the river."

    Maryanne "Hehe, oh really? You think you’d put money on it."

    Shun "… Maybe. I’ll bet a thousand imaginary-Yen that it makes it across the river."

    Maryanne ecstatic "Ha! Imaginary? Okay, I’ll take that bet."

    "I take a stance and aim carefully. My hand pulls back … then further … then further."
    "{snd}Wizzz … SPLASH{/snd}"

    Shun "… So close!"

    Maryanne "Yeah, it made a full skip downward."
    Maryanne happy "Double or nothing?"

    Shun "You’re on."

    scene bg inou_gate sunset with bg_change_hours
    "I am 64,000 imaginary Yen in debt by the time we get back to Inou. Maryanne proudly leads the march back to campus, assured that her title of Rock-Skipping-Queen was protected."

    show maryanne happy with config.say_attribute_transition
    Maryanne "Okay, {em}now{/em} I really need to study again."

    Shun "Sure, sure, but next time, I’m sure I’ll get at least one skip."

    Maryanne ecstatic "Suuure you will. Hehe."

    Shun "I’ll call you."

    Maryanne happy "O—okay. Yeah, okay! B—bye. Bye Shun."

    hide maryanne with config.say_attribute_transition
    "I finally did something right. Something that didn’t involve schoolwork."
    "I’m reeling in my happiness and think of calling her …"
    stop music fadeout 0
    Shun "Dammit! I didn’t get her phone number again!"

    play music "bickering_loop.mp3"
    "As I turn to go back to my dorm, I see Shione walk down the path, still holding her box of tools and papers."
    "She stops at the Yen coin glued to the sidewalk. Hideki watches, practically sweating as he looks through his binoculars."

    show hideki chin think with config.say_attribute_transition
    Hideki "Come on you …"

    "She tries to pick it up. Then again. Then cheeks turn red and she gets frustrated."
    "Shione pulls out one of the tools from her box, a screwdriver, and jams it under the coin. The coin flips up and Shione catches it."
    show hideki crossed glare with config.say_attribute_transition
    "Shione whistles as she passes by me, flipping the coin up off her thumb."

    Hideki armsup argue "… Grrrrrraaahh!!!"

    return
