##--------------------------------
## Act 2: Maryanne
## Scene 9: Dumplings on a Weekday
##--------------------------------

label a2s9:
    stop music
    scene bg dorm with bg_change_minutes
    play music "contemplative_loop.mp3"
    "I get back to the dorm and no one is there to greet me. Again. Just like home. My good day becomes a memory as I’m reminded what it felt like to be … alone again."
    "Why can’t I get over it? I’m out of that mess, I’m here. I live here. … I mean, it doesn’t feel like “home” yet. The dorm felt more like … a costume. Something wrapped around me that made me look and feel different, even though I was still the same person inside."
    "For some reason, I think of my mother …"
    "I sigh and try to not think about her. I put the eggs in the fridge, grab a drink and sit down on the couch."

    stop music fadeout config.fade_music
    "The door latch fidgets. Thank god, I didn’t want to be alone with my thoughts."
    play music "happy_hangout_loop.mp3"
    "Kevin pops inside, a bag of takeout in both hands and a piece of paper in his mouth. He tosses them on the table before removing the note from his teeth."

    show kevin smile sides open with config.say_attribute_transition
    Kevin "Hey man. Check it out! Lunch special, got one for dinner later. Nothing like food you didn’t cook yourself, Am I right?"

    Shun "I thought you were good at cooking, Kevin?"

    "I say that with a bit of bitterness, which I regret right away."

    Kevin "Yeah. So? Just felt like take out again cause … I don’t know, because. You know?"

    "Kevin rips the top of the container open. A cloud of steam puffs upward as he rubs his hands together."

    Kevin smirk crossed "Dumpling?"

    Shun "I’m alright. What was that thing in your mouth??"

    Kevin "Oh that? That’s the info for the Inou Gala."

    Shun "The what?"

    Kevin neutral "The Gala? The annual … you really don’t listen, you know that? First you didn’t know about the trip to Shodo, and now this."
    Kevin laugh closed "You’ll probably need someone to remind when the school year ends too."

    Shun "It’s not my fault! I’ve been busy. I’ve had a lot on my mind."

    Kevin smile open "Yeah, yeah. Freshmen always say they are busy. Whatever."
    Kevin "There is this big dance every year, about a month before finals. Sort of like a pre-hard work celebration, you know? Real fancy stuff. You wear a tux, the ladies wear dresses, they serve shrimp and wine … all the goods."
    Kevin "It’s like buying Broadway tickets. It’s a bit expensive, but you’ll definitely get laid."

    Shun "Kevin! I don’t need to hear that."

    Kevin laugh "Fine! It’s very affordable and you’ll get laid. But it’s a great night. Last year they rented out a ballroom downtown. I think it’ll be there again this year too."

    Shun "Uh … okay. I don’t know if I will go to it though."

    Kevin serious "Why not?"

    Shun "Cause, isn’t that something guys usually take their girlfriends too?"

    Kevin neutral "… Seriously, how did you get into this school?"
    Kevin smile "Okay, you know what … don’t sweat it. I’m just putting it out there … in case you decide you want to put it on your calendar for later."

    Shun "… I’ll think about it."

    Kevin "Cool."

    "Kevin sits back down and turns on the TV. I sit by the table, on the floor. Kevin flicks through the channels and pops a pork dumpling into his mouth."

    Kevin neutral "… You’ll be okay."

    "He says as he pats my shoulder. And pushes the dumpling tray towards me."

    return
