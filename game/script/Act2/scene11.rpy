##---------------------
## Act 2: Maryanne
## Scene 11: Taco Night
##---------------------

label a2s11:
    stop music
    scene bg room_shun night with bg_change_days
    play music "decompression_loop.mp3"
    "Friday night. Midterms will start Monday."
    "I read through and rewrite my science notes for Okada’s class. Maryanne was right about re-writing notes being a useful studying trick."
    "I write down a fact or two about prions, ready to take a break … when I smell something really good. Like spicy meat. And garlic!"
    "The smell pulls me from my desk. I can hear pots and pans clanging on one another., the sound of something sizzling on the stove."
    play sound "194365_macif_door-knocking-angry"
    "{snd}BANGBANGBANGBANGBANGBANGBANGBANG{/snd}"
    "Obnoxious rapping slams on my door just as I was about to step out. Kevin lets himself in."
    show kevin smile sides open with config.say_attribute_transition
    Kevin "Food’s up! Come and get it!"
    hide kevin with config.say_attribute_transition
    "I hear Kevin knock on Hideki’s door just as obnoxiously."

    scene bg dorm with bg_change_seconds
    "He hurries back to the kitchen, a few stains on his shirt, a myriad of exotic scents fuming from the stove. His wrists flick a long wooden spatula around inside a shimmering wok."

    Shun "What’s going on?"

    show kevin smile crossed closed at rightish with config.say_attribute_transition
    Kevin "Dinner’s up, man. Sit down, quick. It’ll be ready in a sec."

    Shun "What are you talking about?"

    Kevin open "Tacos man! After Count Calculus there admitted he was sheltered from good food, I needed to prove a point."

    "Hideki comes out of his room then. Slowly. His forehead red with frustration."

    show hideki crossed pissed at leftish with config.say_attribute_transition
    Hideki "What … is he … up to … this time? Surely he didn’t prepare all of this himself. That would require adult supervision."

    Kevin laugh closed "Adults? In my kitchen? Never! I don’t need a man to cook for me."

    Hideki "I am going back to work!"

    Kevin smile open "No you won’t! The girls will be here any minute! So you be polite."

    Hideki glare "… G—guests?! You did not … you invited guests without asking?! Who told you that guests were permitted?!"

    Kevin "You said I could do whatever I want and you didn’t care."

    Hideki armsup argue "That is {em}not{/em} what I meant!"

    "Three delicate knocks rap at the front door … followed by about sixteen soft, rapid ones."

    Kevin "That’s the girls. Be nice, both of you."
    hide kevin with config.say_attribute_transition

    Hideki "Girls?! You blundering oaf! I—"

    "Kevin answers the door, completely ignoring Hideki off just as Hideki looks like he’s about to pop."
    "I see Maryanne at the door! Accompanied by her small green-haired counterpart, Shione."

    scene bg room_shun night with bg_change_seconds
    "I run into my room before they see me and immediately start looking for something nicer to wear. And my hair brush. And something that smells nice."

    scene bg dorm with bg_change_minutes
    "When I return, Maryanne and Shione are sitting at our small table. Hideki is nowhere to be seen. Probably for the best."
    "Maryanne waves at me delicately with the tips of her fingers. Shione flails her arm around like it’s limp."

    show shione finger excited at center
    show maryanne happy hairdown at leftish
    with config.say_attribute_transition
    Shione "Lookie! I told you he’d be here! And if I didn’t say that, then I meant to say it earlier so I could say “lookie” now."

    Kevin "Shione, you have a way with words. You know that?"

    Shione pompous "Everyone has a way with words Kevin. Duh."

    show shione hips exclaim with config.say_attribute_transition
    "An egg timer goes off and Kevin leaps into action, hurrying into the kitchen … leaving a seat for me next to Maryanne."

    Maryanne "Hi."

    Shun "Hey."

    "Kevin pulls something hot out of our tiny oven and slides it onto a cooler pan. Seconds later, he walks over with a tray with four small bowls and quietly places them on our table."
    "One bowl has diced peppers, another has cheese, another with cooked onions. A strange red sauce sits in the last bowl. Each have a small spoon sticking out of them. And one big bowl has a mountain of yellow rice that smells way too good to pass up."

    show kevin laugh sides open behind shione at rightish with config.say_attribute_transition
    Kevin "Tacos baby! Home made, à la Kevin!"

    Shione "Whoo! Food that isn’t frozen! This is five-star review!"

    Kevin "Thank you, thank you, I take all the credit."

    Maryanne confused "This is very nice Kevin but … midterms are closing in fast."

    Shione "Yeah, almost everyone else is in their rooms reading books and doing practice tests and not eating home cooking."

    Kevin smirk "Yeah, let’s get one night of good food in before we go back on the study-train. You can jump back into study-mode tomorrow. What do you say?"

    Shione finger excited "I vote “yes” to that idea!"

    Kevin crossed closed "Seconded! That means it’s decided, right? Fun now, work later!"

    hide kevin
    show shione hips
    show maryanne worried
    with config.say_attribute_transition
    play sound "194365_macif_door-knocking-angry"
    "Before Maryanne can voice her concern, Kevin leaps up and hurries to Hideki’s door. He bangs on it four times on it, loud and hard. Enough to make Shione jump as she dumps a huge spoonful of rice on her plate."

    Kevin "Hideki! Your food is going to get cold!"

    "The door opens slowly. Hideki sticks his head out of his room, just enough so his glaring eyes peer out to see what we’re eating."

    Hideki "Bleh! You have not cooked more than an egg since you came back this year! And you expect me to ingest morsels that have not been tested by the government?! Why would I eat that garbage?"

    Kevin "It’s Mexican food! The Mexican government doesn’t even test it! Eat the—"

    "Hideki ducks back inside his cave, slamming the door."

    Kevin "He’ll come out when he is ready. Dig in guys! Just leave Hideki a plate."
    Kevin "Oh Shun, I got something real quick to show you. Come here."

    scene bg room_kevin night
    show kevin smirk sides open
    with bg_change_seconds
    "Kevin jumps up and hurries to his room, waiting for me shuffle inside before closing the door."

    Kevin "You’re welcome."

    Shun "What for?"

    Kevin crossed "For getting your lady-friend in our apartment. Now you don’t need to worry about a second date now cause, hey, we’re just hanging out right? No pressure."

    "Kevin pulls out a bottle of wine from under his bed. There’s dust on it, making me wonder how long it’s been there."

    Kevin "… What? Don’t judge me, it’s wine! It doesn’t go bad. It probably tastes better now."
    Kevin smile sides "I’m gonna make you look good tonight bro. Just don’t screw it up … err … how do you open this … oh wait, it’s a twisty-top. There we go."

    "He pats my shoulder and smirks. I smile nervously."

    scene bg dorm
    show maryanne happy hairdown at leftish
    show kevin smile open sides at rightish
    show shione hips exclaim at center
    with bg_change_seconds
    "Shione and Maryanne whispered something to each other as we come back. They giggle once before we sit back down. I notice Maryanne’s already filled up her plate with everything she can fit on it."

    Kevin laugh closed "Ta-da! The finest bottle of chardonnay from the exclusive collection at the corner quick-shop."

    Maryanne embarrassed "Big spender as usual."

    Kevin smile open "One day, I’ll make money. One day … it’s not today, but one day. Here’s to a student’s non-existent salary."

    "He pours wine into a plastic cup and passes the bottle around. Shione grabs a taco and examines it, figuring out the best way to eat it. She eventually tilts her own head to the left slight while holding the taco straight."

    scene bg dorm
    show shione hips exclaim at center
    show maryanne happy hairdown at leftish
    show kevin smile open sides at rightish
    with bg_change_minutes
    "Kevin made {em}so{/em} much food that most goes uneaten. I don’t know how anyone could eat so much in one sitting … but that seemed to be the point. There is enough leftovers for the dorm for maybe two days now."
    "Maryanne, for once, eats very slowly, savoring every bite and enjoying her time. She still ate twice as much as I do."
    "Shione’s head remains awkwardly tilted to the left for a good while until Maryanne can not take it anymore and forcibly straightens her neck for her."
    "Maryanne makes a comment about her head being barely screwed on and Shione says that a screwed on head can’t eat tacos. The topic then changes."
    "True to his word, Kevin tries to make me look good. He brings up stories about me in between jokes and anecdotes about himself. Some of the stories he shares are actually true. It is flattering, albeit unrequested. And the cheap wine probably helped."
    "Kevin pushes the trays of food by the corner of the table so Maryanne and I have to sit closer to grab what we want. It’s a subtle move, but it works. She is close by my side by the end of the meal."
    "I boldly rest my hand next to hers. And even bolder … after a long time of thinking about it … I put my hand on hers. She doesn’t look at me when I do this. But she does smile."
    "Hideki stays in his room the entire time we have our fun. Not even strokes on his keyboard are heard through his thin door. Kevin makes sure to leave his plate near his door as a sign of good faith."

    Maryanne ecstatic "That was delicious. Really, A+ Kev."

    Shione excited "Yeah! The side-ways food tasted great! Even if the rice is discolored and sickly."

    Kevin closed "What I tell ya? I know what I’m doing. And this kid doubted me, can you believe that?"

    show maryanne happy with config.say_attribute_transition
    "He rubs the top of my head like I am a puppy."
    "Maryanne and Kevin get into a small argument on who should do the dishes, both wanting to be polite to the other. They compromise and leave everything in the sink for later."

    Kevin open serious "Oh hey, Shione … before I forgot, Fusao said he wanted to see you."

    Shione confused "About what?"

    Kevin "You know, the thing. In Fusao’s room."

    Shione "… You mean Fusao? He’s not a thing you know. I mean, yeah, he is a thing technically … actually, that means I’m a thing too. Are we all things? Does “thing” even have a definition?"

    Kevin smile crossed "Good question. Let’s go look it up. I think there is a dictionary in his room so that saves time."

    hide kevin
    hide shione
    with config.say_attribute_transition
    stop music fadeout config.fade_music
    "Kevin guides Shione out of the dorm. I hear Shione mutter something about dictionaries not being as smart as they think they are."
    "Maryanne and I are alone."
    "My hand is no longer on hers. She says nothing, and only smiles at me. I guess waiting for me to say something?"

    Shun "That was … great."

    "There is a pause. I think she wanted me to say more, to carry the conversation."
    "Then I think … maybe she doesn’t want me to say more. Maybe she wants me to do something."
    "The pause lasts longer and longer. A couple of seconds, sure, but it didn’t feel that short. My heart starts pounding. I force myself not to gulp. Maryanne waits."

    Shun "… I …"

    Maryanne embarrassed "Yeah?!"

    "She answers really fast and moves in closer, wide-eyed. It’s so obvious now. And … nervousness bubbles up in me like bad oysters."
    "I force myself to push through my nervousness … and I move in a little closer too. Not consciously; instinctively."
    "Her smile grows as my hand touches hers again."

    Shun "… Uh …"

    "I bite my tongue. It is time to stop talking."
    "I go in closer. She doesn’t push away."

    stop music
    scene bg black with Fade(0.5, 0, 0)
    play music "romantic.mp3"
    "I kiss her softly …"
    "… And she lets me! So I kiss her again."
    "My heart thumps like a drum as my fingers lace around hers. Her breathing speeds up a little. Everything feels more … sensitive … more alive. Definitely warmer. Her lips are wet. She smells really good."
    "Everything else seems to freeze. Like we’re the only two people in the world. Like we are the world. Like there is nothing else anywhere except me and her."
    "My nervousness fades. This feels right."

    Hideki "Eck."
    stop music
    scene bg dorm
    show maryanne uncomfortable hairdown at leftish
    show hideki crossed glare at rightish
    with vpunch
    "We both sharply turn our heads towards Hideki as he rudely interrupts. He stares, his right eyebrow twitching with annoyance."
    show hideki crossed pissed with config.say_attribute_transition
    "He says nothing. Then he sits down and grunts, still staring, like {em}we{/em} did something wrong."
    return
