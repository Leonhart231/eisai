##----------------
## Act 2: Maryanne
##----------------

label a2:
    call act_transition("2")
    call a2s1
    call a2s2
    call a2s3
    call a2s4
    call a2s5
    call a2s6
    call a2s7
    call a2s8
    call a2s9
    call a2s10
    call a2s11
    menu:
        "Say hello.":
            call a2s12a
        "Say nothing.":
            call a2s12b
    call a2s13
    call a3
    return
