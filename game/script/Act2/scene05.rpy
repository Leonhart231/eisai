##--------------------------
## Act 2: Maryanne
## Scene 5: 4 on a Wednesday
##--------------------------

label a2s5:
    show kevin smirk sides closed at rightish with hpunch
    Kevin "Lucy, I’m hooome!"

    show maryanne happy at leftish with config.say_attribute_transition
    "Kevin barges in, throwing his grocery bag on the couch, where he usually sits. I hear something metal clink inside it when it impacts."

    Kevin smile open "Hey, Egghead! What are you doing here?"

    Maryanne "Just … passing the time."

    Kevin smirk "Is that right?"

    "His eyes dart back and forth between the two of us. Then a snicker pops out of his grin."

    Kevin smile "Well … don’t mind me."

    "Kevin jumps onto the couch, arms out, without looking behind him, landing with a thud. He pulls out a pack of beef jerky out of his bag, followed by a six-pack of Kamitsu beer."

    Kevin crossed "Wanna do something? We could break into Hideki’s room and move everything on his desk one inch to the left. He’ll freak out, it’ll be great!"

    Shun "And we have movies—"

    Kevin frown closed "No we don’t! The TV is on the fritz. No-go on the movie thing. Besides, I have … uh, homework. An essay. A big one, yep."

    show kevin open with config.say_attribute_transition
    "Kevin says sternly, clearing his throat before I can ask what happened to the TV."

    Maryanne confused "It’s fine, I’m not in the mood to watch a movie anyway. What is your paper on? I could help you with it if … you need it."

    Kevin smile "Ah, it’s a language assignment. Don’t sweat it, I’ve got it. They want me to write an excerpt from an English book and translate into a few languages, word-for-word. Symbol-for-symbol, that thing. Ain’t nothing to it but time and patience."
    Kevin "Or a little googling. Hey, you want to help me cheat?"

    Maryanne mad "If you’re serious about being a translator after you graduate, you’ll do it yourself. It’s not going to help you in the long run if you don’t practice."

    Kevin smirk "Thank you, Mom. I’ll put a pin in that after I fix the TV."

    "I hand Kevin a cup of the tea."

    Kevin smile sides "Thanks brother. {em}sniff{/em} What is this, leaf-flavored?"

    Shun "Stop doing that."

    show kevin smirk with config.say_attribute_transition
    "He snickers and smirks, passing me a beer which I decline politely."

    Maryanne "Uh, I think I’m going go now. You don’t need me distracting you from your work."

    Shun "You don’t have to leave yet. If you don’t want to."

    Maryanne "It’s okay, Shun. Don’t worry. I’m going to do … what we talked about. It’s been … {em}yawn{/em} … kind of a long week. I could … lay down for a minute."

    Kevin smile "You’re welcome to come back if you get bored. Anytime."

    Maryanne "Alright. I’ll see you both later."
    Maryanne uncomfortable "Shun … thanks for …"
    Maryanne happy "… Uh, thanks."

    hide maryanne
    show kevin at center
    with config.say_attribute_transition
    "She lets herself out, hurrying down the hallway again."
    "The moment the door is closed, Kevin pulls the remote out from behind his back and the flat screen turns on. A horror movie starting playing, paused somewhere in the middle."

    Shun "I thought you said the TV was broken."

    Kevin serious crossed "I lied. Duh."

    Shun "… Okay. Why?"

    Kevin mad "Because Maryanne. That girl can not watch a movie without challenging every {w=0.5}single {w=0.5}mistake{w=0.5}."
    Kevin sides "We watched Aliens last year … the whole time she was like “Acid can’t burn through that much metal and the alien’s growth spurt violates conservation of mass and how can she survive the vacuum of space.” Blah-blah-blah."
    Kevin serious "She even does it in Disney movies. “Why doesn’t the Little Mermaid just write the Prince a letter if she can’t talk?” She doesn’t get it man, she doesn’t get it!!"

    Shun "… That’s actually a good point though. Why {em}didn’t{/em} the Little Mermaid just do that?"

    Kevin mad crossed "Who cares! It’s a movie, that’s why! And I’m in the middle of watching it! Hideki might get annoyed with blatant liberties, but at least he waits until the film is over. Maryanne talks through the whole thing!"
    Kevin smirk "If she ever gets around to watching superhero films, I think her head might explode."

    "Kevin kicks his feet up on the table as a ghost with dripping black hair pops up on the screen. He puts his tea down, putting a beer can in his open hand instead."

    Shun "Drinking?"

    Kevin smirk "Obviously."

    Shun "It’s four o’clock on a Wednesday."

    Kevin "… Yeah? So what? It’s 5 in … um … Korea, I think. Or the ocean, whichever way the time goes."

    Shun "Don’t you have homework?"

    Kevin "“{em}Don’t you have homework?{/em}” Geez. Cut me some slack. I want to see how they stop the ghost."

    Shun "Fine. Whatever."

    Kevin smile "So what’d you two do? You were alone together for a while. Did play tonsil-hockey yet or what?"

    Shun "What?! No, come on, no! We just sat and talked. She said she wanted to try to relax more, but that was it."

    Kevin "Relax more?"

    Shun "Yes."

    Kevin "Relax … with {em}you{/em} more?"

    Shun "… I … assume so."

    show kevin smirk with config.say_attribute_transition
    "Kevin says nothing for a second as he drinks deeply."

    Kevin smile "{em}gulp{/em} … Good. Whatever you think is best man."

    "He leans back, acting like he does not care, about his “old friend” or how I was handling the situation. I hope this is a sign of trust."
    "But a nagging feeling asks if this is foolishness, either on his part for being nonchalant or on my part for not questioning it verbally."
    "Kevin watches the screen intently, ignoring me as I scan his own face for signs that I should doubt his intentions. I watch the movie and him in silence for a few minutes, waiting for him to speak up."
    show kevin laugh with config.say_attribute_transition
    "After a while, just after the ghost on the screen jumps up, he laughs. He catches his breath, then softly says …"

    Kevin neutral "… She’s a good person."

    Shun "Yeah. Yeah, I know."

    "I reach over and take that beer Kevin offered me."
    show kevin smirk with config.say_attribute_transition
    "Kevin pats my knee twice, smirking."

    return
