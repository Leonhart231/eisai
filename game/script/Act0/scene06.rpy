##-----------------------------------------------------------------
## Act 0: Prologue
## Scene 6: Sun Goes Home and Finds Only Broken Dreams and Promises
##-----------------------------------------------------------------

label a0s6:
    stop music fadeout 2.0
    scene bg shun_house_street night with bg_change_minutes
    window show
    "My father’s car is gone. At least things will be quiet now."

    stop music
    scene cg mom_kitchen with bg_change_seconds
    play music "very_sad.mp3"
    play sound "104533_skyumori_door-open-02"
    pause 1.5
    play sound "53269_the-bizniss_door-close-2"
    stop ambient fadeout config.fade_music
    "The dining room light is off. I hear my mother sigh when I enter. She’s laying on the floor to calm down, again."
    "And the bottle of whiskey is empty. Again."
    Mom "I don’t want to talk to you right now. Go away."
    Shun "It’s me, Mom."
    "She doesn’t respond at first. I step inside and step my shoes off."
    Mom "Your father isn’t here."
    Shun "I know."
    "Mom slurs a bit. Not heavily. Not as bad as other days."

    play sound "174863_ashfox_bottles-multiple-clinking"
    "I try to take the bottle away from her. She sluggishly grabs it as I do."
    Mom "… Shun?"
    "I put the bottle away. She puts her head back down on her arms and groans with nausea."
    Mom "… I’m sorry."
    Shun "I know."

    play sound ["82444_vampirella17_washing-dishes2"] loop
    "I take the glass and wash it out quickly. Mom moans and tries to sit up straight."
    Mom "I’m tired."
    Shun "Then go to bed."
    Mom "I’m tired of all of this."
    "I know what she means. I feel the same way."

    stop sound fadeout 1.0
    pause 1.1
    queue sound ["53269_the-bizniss_door-close-2", "234263_fewes_footsteps-wood"]
    scene bg shun_house_kitchen with bg_change_seconds
    "I clean up a bit and get mom some water. She groggily gets up herself. Experience taught me not to touch her when she is like this. She goes upstairs, leaning on the railing as she stumbles over her feet."

    Mom "I’m sorry … I’m really sorry …"

    "I don’t say anything. I can’t."

    play sound ["202535_abolla_books01"] loop
    "I sit down at the kitchen table and skim through my history book with my usual ease. A great distraction, from … this."
    play sound "134067_bassmonkey91_phoneringing"
    "The phone rings. I glance at the answering machine. The caller ID is a number I don’t recognize, so I ignore it."

    play sound "118227_joedeshon_phone-button-push"
    AMach "Hello. You’ve reached the Takamine Residence. We’re not home right now, but if you will please leave a message after the beep, we’ll return your call as soon as we can."

    play sound "29679_herbertboland_phonebeep"
    "{snd}BEEP{/snd}"
    Anon "Hello, Mrs. Takamine? This is Mr. Adachi, from the law firm. I’m just calling back to confirm our appointment tomorrow afternoon to begin filing your divorce papers—"
    call audio_stop
    "…"
    "Everything kind of fades for a second."
    "I wait for the message to finish then hastily press the button to make it play again."

    play sound "29679_herbertboland_phonebeep"
    Anon "Hello, Mrs. Takamine? This is Mister Adachi, from the law firm."
    "A lawyer. {w}A professional.{w} Not an empty threat from my mother or father in the heat of the moment. Divorce …"
    Shun "They’re getting … they are getting …"
    "The world starts to spin a bit …"
    call headache_start
    play music "unrest_loop.mp3"
    "… They’d promised me they wouldn’t do this. Time and time again, they promised."
    "They fought for so long and so frequently, but they promised me that they would never divorce each other."
    play sound "134067_bassmonkey91_phoneringing"
    "… {w}The phone rings again. I can’t take this right now. I don’t want to hear another lawyer."
    "I scramble to the front door and throw my shoes on. I don’t know why I think being outside will make me feel better. But I have to get out. I have to get out …"

    play sound "118227_joedeshon_phone-button-push"
    AMach "Hello. You’ve reached the Takamine Residence. We’re not home right now, but if you will please leave a message after the beep, we’ll return your call as soon as we can."

    play sound "29679_herbertboland_phonebeep"
    "{snd}BEEP{/snd}"

    stop music fadeout 0.5
    Dad "Shun?{w} Shun, are you there?{w} Shun, if you’re there, please answer the phone."
    Dad "…"
    Dad "… I guess you’re not home. Please call me back when you get this message. I’ll try your cell phone next."

    play sound "118227_joedeshon_phone-button-push"
    "{snd}CLICK{/snd}"

    play sound "60901_nabito_phone"
    "…"
    "My cellphone goes off."
    "I throw my phone at the couch. It bounces and hits the wall, falling somewhere behind the sofa."
    call headache_stop
    scene bg shun_house_door night with bg_change_seconds
    "I slam the door shut as I leave the house. I can still hear the phone ring."

    return
