##----------------
## Act 0: Prologue
## Scene 8
##----------------

label a0s8:
    stop music
    scene bg black with bg_change_minutes
    centered "Six months later …{w=5.0}{nw}"
    window show
    play music "contemplative_loop.mp3"
    "Time moves slower when you’re suffering."
    "The last few months felt like a long time-out … like my life was on “pause”."
    "Higa-sensei’s letter got me past the first stage of the application process. But after that, I was on my own."
    "The application itself was much harder than I expected. My grades and test scores were great, yes, but I wasn’t in any clubs, nor was I on a student council."
    "But I managed and pulled through."
    "I made it past the second round of applicants, then the third."
    "My acceptance letter came two days after their divorce was finalized."
    "I never thought that either of these days would ever come. One was a dream, the other I dreaded. Both were unbelievable."
    "No matter what I did to distract my anxious mind or how much I reassured myself, the day I would leave home was always a fleeting thought, a hopeful “what-if” that stuck in the back of my head."
    "But it’s real. I’m going to college."
    "The drive to the bus station is annoyingly predictable. Mom drones on and on in a conversation consisting mostly of small talk and trite advice that I pretend to listen to."
    Mom "Get lots of sleep and try not to get sick … but if you do, call us."
    Shun "Uh huh."
    Mom "Your father is going to meet us at the bus station. You’ll be civil to him, right?"
    Shun "Okay."
    "It’ll be the first time I’ve seen my Dad since the divorce. He moved into a small apartment on the other side of town."
    "In that time, I’ve never been to his new place, but he never invited me anyway, so whatever."
    "Anyway, this will all be behind me soon."

    scene cg act0_bus with bg_change_seconds
    "The silhouettes of people by the station look like smears of oil in the dismal weather."
    "One of the shadows walks up the block and waves to me. I nod absently. He stands next to Mom. They don’t look at each other."
    "I hear Dad cough and say my mother’s name in place of a greeting."
    Mom "Be safe dear."
    Dad "Call when you get settled in."
    Shun "Yeah. I will."
    "I’ve been thinking about this day for months and now that the moment is here, but surprisingly, I feel … {w}empty."

    scene bg black with bg_change_seconds
    "The bus rumbles impatiently under my feet as I climb aboard. In the wet, foggy windows, I see the shapes of my parents waving. I can’t see their eyes, though I know they’re watching."
    Bus "We will be leaving in one minute. Everyone please be responsible for your own items."
    Bus "The next stop is Kofu, then Chino. Final stop Inou University in Takuya. The first stop will be in one hour, Kofu. Final stop, nine hours overnight."

    scene cg act0_reflection with bg_change_seconds
    "A raindrop snakes down the glass, cutting a path of clarity through the fog, revealing one last glimpse of my parents. I stare at their watery faces shaded by the umbrella they share."
    "That’s it. It’s all done now, tucked away in the past."
    "A whole school year away from all this … I can’t ask for a better vacation. To have time to study in peace."
    "I take one last look at them, and the bus starts moving."
    "I’ve made my choice. I’m going. I will remake myself the way I want, become someone different. This is my second chance."
    "I wonder what I’m getting myself into."
    "I wonder what my future roommates will be like, and which teachers I will get along with. {w}I wonder who my new friends will be, what hobbies I’ll pick up and what challenges I’ll encounter."
    "Maybe Higa-sensei is right. Maybe at Inou I’ll find friendships that will last a lifetime and experiences that will inspire me. {w}But maybe he’s wrong, and all I’ll find at Inou is more loneliness and sadness."
    "My eyes ache. I can’t remember the last time I had a restful sleep."
    "I slump down in my chair, close my eyes and let sleep come."
    window hide

    stop music fadeout 3.0
    scene bg black with bg_change_seconds
    pause 3

    return
