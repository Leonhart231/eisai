##----------------------------
## Act 0: Prologue
## Scene 2: Shun’s High-school
##----------------------------

label a0s2:
    window hide
    nvl hide
    nvl clear
    scene bg black with bg_change_seconds
    centered "{cps=20}Every day is the same.{/cps}"
    window show

    scene bg a0_class_history with bg_change_seconds
    play music "unrest.mp3" noloop
    "Every morning, the same homeroom.{w}\nAnother English class.{w}\nAnother Math class.{w}\nAnother Japanese History class."

    show higa talking with config.say_attribute_transition
    Higa "The Ansei Great Earthquake occurred the same year that Commodore Perry arrived in Kanagawa.{w} At the time, many religious and spiritual factions thought that the earthquake was a terrible omen caused by the Americans and wanted the foreigners to leave."
    "Another day of listening to teachers prattle on about the past or the future instead of the present."
    Higa happy "Mr. Takamine … can you tell me who was shogun at the time?"
    Shun "What? … Oh, yeah … Tokugawa Iesada."
    Higa "That is correct, Mister Takamine."

    nvl_narrator "I answer correctly and he moves on."
    nvl_narrator "He knows I’m uninterested."
    nvl_narrator "He calls on me because he hopes one day I will get involved without him prodding me."

    window hide
    nvl hide
    nvl clear
    scene bg a0_lunch with bg_change_seconds
    centered "{cps=20}It’s just another day.{/cps}"
    window show
    "Another lunch alone, eating a meal I’ve made myself."
    "This routine has become commonplace. It has become predictable.{w} I’m not sure when I got used to it."

    window hide
    scene bg a0_stairs with bg_change_seconds
    nvl show bg_change_seconds
    nvl_narrator "I had friends once. Some graduated. Some moved away."
    nvl_narrator "Others stopped liking me because I refused to give them the answers to their homework or help them cheat on their tests."
    nvl_narrator "An academic record like mine meant frequent isolation, or getting the wrong kind of friends."
    nvl_narrator "Eventually, being alone became normal for me."
    nvl_narrator "It’s the same thing everyday."
    nvl hide
    nvl clear

    scene bg a0_class_history with bg_change_seconds
    window show
    "Another History class."
    scene bg a0_stairs with bg_change_seconds
    "Another P.E. class."

    scene bg a0_hall with bg_change_seconds
    "Another Science class."
    "Another series of questions thrown at me about my future and what I want out of life.{w} Another day of ignoring those questions."

    window hide
    nvl hide
    nvl clear
    scene bg black with bg_change_seconds
    centered "{cps=20}It’s just another day.{/cps}"
    centered "{cps=20}Another solitary walk home.{/cps}"
    nvl hide
    return
