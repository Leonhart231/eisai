##----------------------------------
## Act 0: Prologue
## Scene 5: Walking Higa-sensei Home
##----------------------------------

label a0s5:
    scene bg shun_house_street night
    show higa happy
    with bg_change_minutes
    Higa "… {w}You know, I went to a university."
    Shun "Well, you’re a teacher. I imagine you’d have to."

    Higa sad "I’ve wanted to be a teacher since I was young, so I went after what I wanted."
    Shun "You always knew what you wanted out of life?"
    Higa "Not always. I knew what I wanted to do career-wise, but there were plenty of times when I felt lost."
    Higa "Or when I didn’t know what to do. I figured it out, thankfully. But some people never do."
    Shun "Uh-huh."
    "He’s doing it again. Saying things about me indirectly. Maybe to get me angry, maybe to make me think."
    Shun "Where did you study?"
    Higa "Oh. Um."
    "He clears his throat and runs his hand through his hair, seeming a bit embarrassed."

    Higa happy "Inou University."
    stop music fadeout 1
    "I don’t think I heard him correctly …"
    Shun "Wait, what? You went to {em}the{/em} Inou Tadataka University? You went there?!"
    Higa "You sound surprised."
    Shun "Of course I’m surprised! It’s a great school. One of the top ones."
    "I’d always assumed Higa-sensei had gone to a local teacher’s college. High school teachers don’t graduate from a school like ITU."
    "And even if he did, why would he teach at regular public high school instead of an academy."
    Shun "What was it like?"
    Higa "It was … unique.{w} I specifically chose it because it has a strong program for accepting international students. I couldn’t afford to see the world, so I thought I’d let the world come to me."
    Higa "I went to school with students from Germany, Korea, England …"
    Higa "One of my best friends in college was a French engineer. Brilliant guy, we still keep in touch."
    Shun "Is your family rich? How did you afford that?"

    "Higa-sensei seems happy at last. I’m finally interested in something he has to say."

    Higa talking "I got a scholarship, took out a few loans. I paid those off awhile ago."
    Higa "Now that you bring it up, Inou University loves giving scholarships to young people with talent who wouldn’t be able to afford it otherwise."
    Higa happy "It makes the school look good. Besides, everyone who goes to college has some kind of financial support these days."

    nvl_narrator "Is he implying something again? Is he saying …"
    nvl_narrator "No. Impossible."
    nvl_narrator "Even if scholarships were an option, there’s no way I’d be able to get in to ITU. The application process and competition for those few open slots are notoriously cutthroat."
    nvl_narrator "I mean, I might be smart, but I’m not ITU smart."
    nvl_narrator "… I know I’m not."

    Shun "You were lucky."
    nvl clear
    Higa "In a way. If you think luck is showing up and not leaving … then yes, I was lucky during my time in school."
    Higa "I got in because I worked hard and knew the right people, which doesn’t hurt."
    "Damn this teacher. He’s dangling a little worm in front of my face, hoping I’ll take the bait."

    Higa sad "… There was something else …"

    Higa crossed "There are a few ways to get into Inou University. {w}One way to get into Inou University is by regular entrance exams, just like any other college."
    Higa "But another way is by formal invitation via one of the school’s scouts. Those are people who get paid to go looking for applicants. Most universities work that way?"
    Higa "But the thing is, Inou has an international focus. It would be very expensive to pay for talent scouts to go all over the world to find young people with talent."
    Shun "That makes sense. So that means … there is a third way?"

    show higa at centernear with config.say_attribute_transition
    "He moves in close to me, as if about to share a great secret that he doesn’t want anyone else to hear … though there’s no one else around besides us."

    Higa "… There’s the {em}Eisai Society{/em}."
    Shun "The what?"
    Higa "It’s kind of like the school’s … unofficial honors program. A lot of the members of this group go on to become de facto talent scouts for the university."
    Higa happy "… It’s one of the easiest ways to get into Inou University, through a letter of recommendation from a former Eisai Society member."

    call audio_fadeout
    "The world goes quiet."
    Shun "So … why tell me this?"

    Higa sad "Shun, for such a smart young man, you seem to often need things spelled out for you."
    Higa "Whatever you decide to do with your life will be up to you in the end. But like it or not, you {em}are{/em} talented. And I think if you put your mind to—"

    scene bg shun_house_street night
    show higa sides sad at center
    with hpunch
    Shun "STOP IT!"

    "Higa-sensei jolts backwards as my voice explodes."

    play music "contemplative_loop.mp3"
    Shun "I don’t care! Do you understand that? I do not care!"
    Shun "I want things to be normal!{w} I like average!{w} Average isn’t stressful!{w} I don’t care if it means that I have nothing else!"
    Shun "I want my teachers and my parents to stop expecting me to be able to do everything and anything just because I’m … me!"
    Shun "I never asked for anything! I never expect anything! Why is that so bad?! Why can’t …"
    "Don’t cry Shun. Dammit, don’t cry."

    Shun "… Why can’t things just be okay?"
    "It all comes out at once. It’s the first time I’ve ever shouted at a teacher, and I’m afraid to hear what he’ll say to me."

    Higa "… {w}I don’t know what it is like to be you."
    Higa "I don’t know what you’re feeling. I don’t know what you see at home, or who you are when you’re alone. I’m trying to understand. I’m trying to help you."
    Shun "Why?"
    Higa "Your father and I … we were school mates. {w}He sat in the same seat that you sit in now, right next to me. Your father was once in the exact same place as you are today."
    Higa "Things worked out for me. I’ve barely seen your father in the past three years. My friend, the man who had every chance that I had, someone my own age, someone I knew …"
    "His words become impassioned. He turns his head and takes a minute to calm himself."
    Higa "… He was my friend. He {em}is{/em} my friend And you’re his son. You’re just as bright and intelligent as he was."
    Higa "He wasn’t as “lucky” as I was. He didn’t have anyone to help him. I don’t want to see that repeat with you."

    "He reaches into his coat’s pocket and pulls out an envelope sealed with a gold band. It glimmers in the glow of the streetlight."
    Shun "What’s that?"
    Higa "I’ve already written a letter of recommendation for you. But I’m not going to mail it."
    "He pushes the letter to my chest."
    Higa happy "What you decide to do with your life is up to you."
    Higa "I believe in you. Even if you don’t believe in yourself."
    Higa "I believe that you have the potential to do something with your life. All you need is a chance."
    Higa "If you share that belief, then you have everything you need right here."
    "He pulls his hand back. I hold the letter and it feels heavy, like it’s made of gold. The seal of Inou University shines brightly on the front."
    Shun "Why do this?"
    "Higa-sensei says nothing for a moment, just stares at me."
    Shun "Why actually do this? What are you getting out of this? What do you want from me?"
    "Higa-sensei smiles softly, like he knows something I don’t."
    Higa sad "Do you know what happens to the people who never try?"
    Higa "Nothing. Absolutely nothing."

    "His eyes look away from me as we arrive at his front door."
    Higa happy "Thanks for the walk, Shun. Say hi to your father for me."

    hide higa with config.say_attribute_transition
    "He turns and heads towards his home."
    Shun "{em}Why{w=0.4} are{w=0.4} you{w=0.4} helping me?!{/em} I’m not even nice to you!"
    "For another moment Higa-sensei says nothing."

    show higa happy with config.say_attribute_transition
    Higa "You’ll understand some day."

    hide higa with config.say_attribute_transition
    "He walks up his walkway and goes inside his house."

    queue sound ["104533_skyumori_door-open-02", "53269_the-bizniss_door-close-2"]
    window hide
    nvl show bg_change_seconds
    nvl_narrator "I hear his wife warmly welcoming him home, and Higa-sensei greeting her back."
    nvl_narrator "There is love in that house."
    nvl_narrator "Happiness."
    nvl_narrator "Warmth …"
    nvl_narrator "I look down at the letter."
    nvl_narrator "I’m tempted to throw it away right now. Instead, I put it into my pocket and start walking home."
    nvl hide bg_change_seconds
    nvl clear
    return
