##----------------
## Act 0: Prologue
##----------------

label a0:
    call reset
    call a0s1
    call a0s2
    call a0s3
    call a0s4
    call a0s5
    call a0s6
    call a0s7
    call a1
    return
