##----------------------
## Act 0: Prologue
## Scene 1: Intro Letter
##----------------------

label a0s1:
    window hide
    nvl show bg_change_seconds
    nvl_narrator "Dear Mr. Shun Takamine,"
    nvl_narrator "It is with great honor that I extend to you an invitation to attend Inou Tadataka University."
    nvl_narrator "Established in 1867, Inou Tadataka University caters to the highest standards in education, with an emphasis on engineering and sciences. We believe a strong focus on advanced skills, in conjunction with a well-rounded general education, is the best way to prepare our students for their professional lives."
    nvl_narrator "We would also like to congratulate you on being accepted into our Eisai Society program, which focuses on the needs of the especially gifted students in our community. The Eisai Society is famous for accepting young people from all corners of the globe and helping them hone their skills to their zenith, above and beyond the already high standards of our university."
    nvl_narrator "The opportunity to become the best you can be is now yours. We hope you will answer its call."
    nvl_narrator "If you wish to pursue your education at ITU, please return the enclosed response letter no later than February 8th of this year."
    nvl_narrator "Yours truly,\nHeadmaster Souta Yamato"
    return
