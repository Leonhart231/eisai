##----------------------
## Act 0: Prologue
## Scene 3: Walking Home
##----------------------

label a0s3:
    scene bg black with bg_change_seconds
    window show
    play ambient "580527_tosha73_big-city-noise-15072021"
    "Being alone doesn’t bother me like it used to. Over time, I became numb to the sting of loneliness at school."

    scene bg shun_house_street with bg_change_seconds
    play sound "234263_fewes_footsteps-wood"
    "Hell, being alone at school is better than what’s waiting for me at home."

    scene bg shun_house_door with bg_change_seconds
    Mom "You never listen! It’s always important when you need to vent, but I’m not allowed to?"
    Dad "{em}I{/em} never listen?! How can you say that?!"
    "I can hear my parents shouting at each other before I even go inside. The neighbors are kind enough to pretend that they don’t hear them."
    Mom "Stupid!! That’s a lie! Take that back!"
    Dad "If I took it back, then I’d {em}really{/em} be a liar!"
    "They’re really going at it today."
    "What is it this time? Another dispute over money? Maybe dad spent too much time out with his friends last night and forgot to call."
    "Maybe it’s about me. They {em}love{/em} fighting about me."
    "What’s best for their only boy? Where should Shun be spending his time? What is Shun going to do after he finishes school?"

    nvl_narrator "My father wants me to go to a university."
    nvl_narrator "My mother doesn’t want to take a loan because …"
    nvl_narrator "… The reasons don’t matter."
    nvl_narrator "It took me a long time to figure out they used my future as an excuse to yell at each other."
    nvl_narrator "If this is what I have to endure to get to a university, it isn’t worth it."
    window hide
    nvl hide
    nvl clear

    stop music
    play sound "217214_goldkelchen_slap-in-the-face"
    scene bg shun_house_door with hpunch
    window show
    Dad "You … you little … how dare you? How dare you!?"
    Mom "S-stay away from me! Take some time to calm down!"
    play music "contemplative_loop.mp3"
    Dad "{em}I{/em} need to calm down!? {em}You’re{/em} the one who hit {em}me!{/em}"
    call headache_start
    "… {em}Great{/em} another migraine I have to ignore."
    call headache_stop

    scene bg shun_house_street with bg_change_seconds
    "I can’t deal with this right now. Homework can wait."
    return
