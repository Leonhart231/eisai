##-----------------------------
## Act 0: Prologue
## Scene 7: Today was Different
##-----------------------------

label a0s7:
    scene bg shun_house_street night with bg_change_seconds
    play sound "80928_bennstir_door-slam-1"
    play ambient "580527_tosha73_big-city-noise-15072021"
    nvl_narrator "I feel my heart beating fast."
    nvl_narrator "I’m alone."
    nvl_narrator "It’s like any other day.{w} But at the same time … it’s not."
    nvl_narrator "This is a dream. This has to be a dream. I will open my eyes, and I will hear my alarm clock ring. I will come downstairs, and my Mom and Dad will both be there."
    nvl_narrator "There will be no lawyer on the answering machine. No divorce papers. This will all be a dream."
    nvl_narrator "I count to ten with my eyes closed, desperately trying to calm myself down."
    nvl_narrator ". {w=1}. {w=1}. {w=1}. {w=1}. {w=1}. {w=1}. {w=1}. {w=1}. {w=1}. {w=0}{nw}"

    "I reach ten. I open my eyes."
    nvl clear
    "I’m still here."
    "This isn’t a dream."
    "I slump down, sitting on the side of the street, bury my face in my sleeves, and just let myself cry."

    window hide
    scene bg black with bg_change_seconds
    nvl show bg_change_seconds
    nvl_narrator "If this were a book or a show, someone would come by to comfort me."
    nvl_narrator "Higa-sensei would suddenly appear."
    nvl_narrator "My father would come and try to make me feel better."
    nvl_narrator "…"
    nvl_narrator "No one does."
    nvl hide bg_change_seconds
    nvl clear

    scene bg shun_house_street night with bg_change_minutes
    window show
    stop ambient fadeout config.fade_music
    "When I’m done crying, it’s late at night … probably past midnight. It’s cold. I feel cold."
    "I think a cup of coffee at the convenience store will help warm me up. I reach into my pocket for my wallet …"
    "… I feel the letter still sitting in my jacket pocket, crumpled slightly."
    "I pull it out and look at its golden trim. I feel an angry, dark urge to throw it away right now."
    "What point is there in sending it? I’m my father’s son after all. What’s the point in wasting a slot at Inou University on a useless person like me?"
    "I hold the letter in my hands. I try to work up the nerve to tear this stupid letter to shreds."
    "But I can’t bring myself to do it."
    "This letter is the only thing in my life right that proves someone cares about me."

    show higa happy at center with config.say_attribute_transition:
        alpha 0.5
    Higa "I believe in you. Even if you don’t believe in yourself."
    hide higa with hpunch
    play sound "146932_crashoverride61088_wind-gust"
    "A gust of wind picks up. The letter is tugged out of my hand. Panicking, I rush after it. It doesn’t get far, but I hold it tightly close to my chest when I get it back in my hands."

    scene bg black with bg_change_seconds
    "I suddenly realize how much I want the letter."
    "And like my mom … I’m tired."
    "I can see Higa-sensei’s house from here, at the corner of the road. The streetlight flickers by his front door step. And on the corner, under the cone of light thrown by the lamppost, a mailbox."

    show cg act0_letter with Dissolve(2)
    "I’m tired of things being the way they are. {p}I’m tired of pretending everything is okay. {p}Just once … just this once …"
    "I’m going to try."
    "If Higa-sensei thinks I can get in, if he thinks there is a shot …"

    "… Then I maybe can get out of here. And the sooner, the better."

    "I don’t care if my parents disagree with my decision. I don’t care if I don’t know what is waiting for me when I get to Inou or if I even get there at all. And I don’t care if I have to deal with my parents for a few more months while they go through their divorce."
    "I don’t care anymore."
    "As Higa-sensei said, this is my choice.{w} And I choose to leave. To get away from my home, to start somewhere fresh … to be someone else."

    scene bg shun_house_street night with bg_change_seconds
    "Today was not just another day after all."

    return
