##-------------------------
## Act 0: Prologue
## Scene 4: Seban Ichi Ichi
##-------------------------

label a0s4:
    window hide
    scene bg convenience_store with bg_change_minutes
    nvl show bg_change_seconds
    nvl_narrator "Another cheap cup of instant ramen for dinner in the parking lot of the convenience store."
    nvl_narrator "My backpack is filled with assignments, though they are ones I can breeze through with ease."
    nvl_narrator "I’ve never needed to read something twice; the information always stays in my brain the first time."
    nvl_narrator "I can’t hear Mom and Dad fighting from here, but know it goes on. I can predict how it will play out. Mom will lose her temper over the slightest issue and Dad will deny everything until he is red in the face."

    nvl_narrator "More yelling."
    nvl_narrator "More arguing."
    nvl_narrator "More of the same old thing."
    nvl_narrator "I can’t remember a time when they didn’t argue."
    nvl_narrator "They’ve gotten progressively worse lately. As my graduation day approaches, their arguments get louder and louder."
    nvl_narrator "… Another day of wondering if all of this is my fault."

    nvl hide
    nvl clear
    window show
    Higa "Hello Mr. Takamine."
    "A man’s soft voice greets me. I respond without looking at him."
    Shun "Hello, Higa-sensei."

    show higa happy with config.say_attribute_transition
    Higa "Eating out again?"
    Shun "Yes sir."
    "He sounds like he’s trying to be nice to me. Again."

    Higa sad "How are your parents?"
    "Higa-sensei lives on the same block as me, a few houses down. He knows about my parents’ problems.{w} Hell, everyone within earshot of my house knows."
    Shun "They’re the same as usual."

    show higa sad crossed at sitting with config.say_attribute_transition
    "He holds a steaming cup of coffee and looks at me softly."
    Higa crossed "What about you, Shun?{w} Are {em}you{/em} okay?"
    Shun "I’m fine."
    Higa happy "Doing anything special tonight? Maybe spending the evening with friends?"
    Shun "I have some homework to do later, but that’s it."
    Higa talking "Ha. Homework …"
    Higa "It feels like yesterday that I was in school and doing homework. It’s weird how much time has gone by."

    "He takes a sip of his coffee. His eyes get that far-away look adults get when they reminisce about the past. The aura of nostalgia around him is palpable."
    Higa happy "I used to spend time with my friends at this very store, you know."
    Shun "Used to? You don’t see your friends anymore?"
    "I’m cranky. He can tell. He ignores it."

    Higa "I do, occasionally. Mostly during summer and holidays."

    Higa sad "Unfortunately, people often go their separate ways after high school. Real life makes it hard to keep in touch."

    Higa talking "Speaking of which …"
    "Here it comes."

    Higa "… Have you thought about what you are going to do after graduation?"
    Shun "It’s still a little while away."

    Higa happy "Ha. I said that to Mr. Ueda once. He was just as annoying as you probably think I am."

    Higa sad "… Damn. It’s been twenty years."
    Higa "Mr. Ueda passed away almost nine years ago. And now I have his job. Can you believe that?"
    Higa "Things change so quickly. Opportunities pass by before you know it. {em}People{/em} pass by before you know it."

    "Higa-sensei sounds somber, but only for a moment. {w}He has one of those faces that’s usually happy even when he’s stressed or busy. It’s like he doesn’t know how to be disappointed."
    "I hate that."

    Higa happy "You know Shun, your father and I were both in Mr. Ueda’s homeroom. Technically the same homeroom you’re in, same age group."
    Shun "I know sir. You’ve told me."
    Higa "But your father never has?"
    Shun "He doesn’t like to talk about his past."

    Higa sad "How is he? I can’t remember the last time we actually spoke."
    Shun "He’s been looking for a new job. So … you know, busy."

    Higa happy "We were good friends for a while. He had a lot of fun in school. So did I. But your father put off making important decisions."

    Higa sad "I think he regrets not reaching his true potential when he was younger."

    nvl_narrator "Some days I want to hit this man."
    nvl_narrator "He knows how to find my weak points and poke at them."
    nvl_narrator "Talking about my dad as if we are in the same sinking boat."
    nvl_narrator "He has no idea what I have to deal with at home! He doesn’t know what it’s like!"
    nvl_narrator "And yet …"
    nvl_narrator "… Higa-sensei is the only one who hasn’t given up on me. He won’t leave me alone."
    nvl_narrator "He’s not getting paid to coddle me after school ends, yet here he is."
    nvl_narrator "He doesn’t get overtime because of {em}my{/em} issues. Why does he insist on pretending to care about me?"

    "Higa-sensei shakes his head. He probably has papers to grade, or friends to see, or his wife to be with, or a million others things that are more important than checking up on a student."
    nvl clear
    "Why choose me over any of that? What’s he getting out of it?"

    show higa at center with move
    "Higa-sensei stands up and pats the dirt off his slacks. He hesitates for a moment before he speaks."
    Higa talking "Shun, you get perfect grades … almost all of the time."
    Shun "I don’t really think that’s a big deal."

    Higa happy "It’s a big deal to some people. You learn things fast and retain them. Your academic record suggests you’ve always had a good head on your shoulders."
    Higa "Imagine what you could do if you would only apply yourself a bit …"
    Shun "I have a good memory. So what?"

    show higa sad with config.say_attribute_transition
    "He looks discouraged with my response. I don’t show it, but I feel bad for brushing off his compliments."
    Higa "Shun …"
    Shun "Yes sir?"

    Higa happy "You never answered my question. Have you thought about life after school? Do you want to go to university?"
    Shun "My family doesn’t have the money for that."
    Higa "But you do want to go? If you could, would you?"
    Shun "I guess. But it’s not in the cards right now. I’ll probably just get a job somewhere, doing something."

    show higa sad with config.say_attribute_transition
    "Higa’s perpetual peacefulness takes a leave of absence, and frustration quickly flashes on his face."
    Higa "Have you ever heard the expression “apathy is the enemy of greatness”? Or perhaps “There’s nothing more tragic than wasted talent”?"
    Shun "I don’t know what you’re talking about, sir."
    "Another silent staring contest. He’s trying to read my mind again, trying to understand what I really mean when I said those things."

    Higa "… I’m going to grab a cup of coffee. Would you mind waiting for me so we can walk back home together?"
    Shun "Yeah. Sure."

    hide higa with config.say_attribute_transition
    "He goes inside and I think about he said.{w} He’s implying I’m wasting my “gifts” …{w} What gifts? A big brain and good grades and what else exactly?"

    "Okay, so I’m not an idiot. What does it matter? Even if I had the money, I don’t know what I want to do with my life."
    "In the end, it simply doesn’t matter. It’s just another day."

    show higa happy with config.say_attribute_transition
    "Higa-sensei walks out of the store with a fresh cup in hand and that irritating smile of his."
    Higa "Ready when you are, Shun."

    "I jump up, throw my heavy backpack over my shoulders, and we start to walk."
    return
