##-----------------
## Act 3: Maryanne
## Scene 5: Workout
##-----------------

label a3s5:
    scene bg class_okada
    show okada neutral
    with bg_change_days
    "Ending his lecture, Okada stands in front of the class and clears his throat. He glares at the empty seat in the back, where Risa used to sit."

    Okada talking "So, by now you have all realized how difficult this class is. There were some who could not handle it … and some who are merely skimping by. But the ones in this room are all passing. That is something to be proud of."
    Okada "But it does not mean you can slack off! We will have another test this Friday! Study hard!"

    hide okada with config.say_attribute_transition
    "Okada slowly lurches across the room, handing back papers the way an undertaker would hand out death certificates."
    "I hear muted groans from half the class and sighs of contentment. This was the paper after midterms, and the fifth we’ve had so far this semester."
    "I get mine back, upside down, the grade facing my desk. I gulp."
    "… A 92 in red ink decorates the top of the page!"
    "My grades have gone up and up, bit by bit with every passing assignment. I am almost at an A- average now. The study sessions with Maryanne … with my girlfriend … have paid off big time."
    "Heh … even in my head, saying that I have a girlfriend sounds strange. A good kind of strange."

    scene bg hall_school with bg_change_seconds
    stop music fadeout config.fade_music
    "I hurry over to the lab, eager to show Maryanne my passing grade, the grade she helped me earn."
    "The lab’s … locked up. I look through the door window and see … no one inside."
    "I text her quickly, just saying hi."

    stop music
    scene bg inou with bg_change_minutes
    play music "happy_hangout_loop.mp3"
    "I exit the science wing, deciding to go to her dorm. If she was there, I could brag and get some of those cookies!{w} … If there were any left."
    "Shione walks down the twisted campus road, holding a magnifying glass over one eye, her tongue sticking out as she waddles around. She spots me and waves me down, her whole arm flailing."

    show shione finger excited with config.say_attribute_transition
    Shione "Hey, Shun! Hi! Hi! Hiiiii!"

    "She doesn’t come any closer. Shione bounces on the balls of her feet, smiling and blinking a lot, like I was standing next to her."
    "I take a step forward and she flinches."

    Shione confused "Whoa-wee! You sure look big!"

    Shun "Hey Shione … what are you doing?"

    Shione excited "I’m looking at the world through supervision! My magnifying goggles broke so I had to use this do-hicky so I could explore sea life up close and far away at the same time! But then I started looking at stuff close up on land and all the time and it’s so cool!"
    Shione "They make everything look like it’s in 3D!"

    Shun "… We already live in 3D."

    Shione wonder "Yeah, but this is extra-3D. 4D … 6D? Sixty … Hehe, I can see your nose-hair from here."

    "She reaches out and gropes the air in front of her, still several feet away from actually touching me. I jump towards the loopy girl and she yelps."

    Shun "I don’t think it is safe to walk around like that."

    Shione excited "And I don’t think it is safe to move so close to me when I am on extra-3D-mode! Bzzzz! Zoom … out!"

    "She takes three steps backwards, her flip flop twisting on the curb as she nearly falls."

    Shione exclaim "Hehe, Maryanne looks weird from here. Her hair’s all bouncy and twisty."

    Shun "Maryanne? Where?"

    "Shione awkwardly points up past my head. She is looking out at the forest trail around the edge of campus. I see a bouncing blonde ponytail, going up and down and up and down and up and down."

    Shun "Uh, yeah, that is probably her."

    Shione "Yeeeeah. She likes running so she must be on the forest trail. Hehe … hahaha! She likes running …"

    "Shione slaps her knee. I don’t see what is so funny."

    Shione wonder "Oh, by the—oh! Toooo close … there! Okay, better. Tell your Shark-friend-Kevin that we are a-good-to-go this weekend! He’ll know what I mean. And if he doesn’t, tell him bad! Bad Kevin! He really should know."

    Shun "Uh … okay. You got it."

    Shione excited "Okay! Good. Now I gotta go see what birds look like on land … in extra-3D! OooOOoooOOOOoo!"

    hide shione with config.say_attribute_transition
    "She heads towards the forest walkway, giggling as she continues to grab the air in front of her eyes. I’m somewhat envious. I will probably never enjoy anything as much as Shione enjoys … anything."

    scene bg forest with bg_change_minutes
    stop music fadeout config.fade_music
    "I am embarrassed that I have yet to step foot in this forest, even though it is a beautiful and quiet escape that I wanted when I first came here."
    "I’m not sure how the school’s architects did it, but the sounds of the city and the school are practically non-existent here. Not that they were all that loud to begin with, but I can hear chirping and the wind blowing and … not much else. The city sounds like it’s far away now."
    "The sign near the entrance says the dirt path circles around and comes back to campus, so I wait a minute for Maryanne to reappear … which she does, in a few moments."
    "Maryanne’s bright gold hair makes her stand out as usual. Her red tank top is covered in sweat, like her forehead. She doesn’t notice me as I stand by the side of the road. In fact … she passes by me without even looking my way."
    "I jog after her, holding my test up over my face to get her attention as I try to match her pace."
    "She doesn’t flinch. Her eyes are filled with determination and drive, the look of someone who wants to get something done."
    "I love when she looks like that, when there is nothing but her and her goals, when she is so focused that nothing will distract her … oh, she’s wearing headphones. Never mind."
    "I tap her shoulder. She jumps with a shout, almost tripping."

    show maryanne uncomfortable circles with config.say_attribute_transition
    play music "romantic.mp3"
    Maryanne "SHUN! YOU SCARED—"

    "She takes her headphones off."

    Maryanne neutral "You scared me."

    Shun "Sorry. I saw you and … look! I got a 92 on my test."

    Maryanne neutral "That’s … great."

    "She responds calmly, like she is just barely interested. She catches her breath as she wipes sweat off her forehead."

    Shun "I didn’t know you jogged."

    Maryanne happy "Eh, I try to come at least twice a week. Five miles at least a time. Usually an hour run. I needed it today. This feels too good. Another thirty minutes today. And then maybe more if I feel like it."

    "Finally, the mystery of Maryanne’s missing caloric excess is revealed."

    Maryanne "Sorry, I’m in the zone right now. Congrats on your test. But I want to finish this. We can hang out later."

    Shun "Oh. Uh, sure. But … do you want to talk ab—"

    Maryanne "Nope. No. I’m fine. Completely fine."

    Shun "… Alright. But if you need me—"

    Maryanne "I know."

    "She nods slowly. A voice in my head tells me she probably needs space and that I need to be patient."

    menu:
        "Say goodbye and leave.":
            $ asked_if_free = False
            Shun "Alright, well … goodbye. I won’t bother you anymore if you are busy."
            Maryanne "You’re not bothering me. I just need to … I …"
            "She puts her hand on my shoulder and kisses my cheek. She smells like a used, sweaty sock, but the gesture is still nice. She puts her headphones back on and smiles before running off again."
            hide maryanne with config.say_attribute_transition
            "I fold my test paper up gently and, with nothing else to do, put it in my back pocket as I walk back to the dorm."
        "Ask if she is free this weekend.":
            $ asked_if_free = True
            Shun "Okay … um, well are you doing anything this weekend?"
            show maryanne mad with config.say_attribute_transition
            "She suddenly looks … annoyed. She shakes her head and puts her headphones back on before hurrying off. Without looking at me."
            hide maryanne with config.say_attribute_transition
            "Mildly offended, maybe a little hurt, I go back to campus. And shove the test paper into my pocket without folding it."

    stop music
    scene bg dorm with bg_change_minutes
    play music "decompression_loop.mp3"
    "I throw myself on the couch, laying on it face-down like a second bed, the way Kevin does all the time. I can hear Hideki tinkering around in his room and briefly wonder what pointless thesis he could be formulating this time."
    "Then my thoughts go back to Maryanne. Something was obviously bothering her … and that stupid package was probably to blame."
    "My mind jumps from casually frustration to anxiety. I wonder if there was a fight, or worse … if someone was unhealthy."
    "Family issues with family members you like … that must be nice. Frankly, my life had only improved since I left home … and my parents behind. I try not to think about them too much; the guilt was a bit hard to swallow some days."
    "But I understood enough to know Maryanne must have felt scared and mad and helpless … if family was the problem."
    "{em}knock knock knock knock knock knock knock knock knock!!!{/em}"

    Shun "Who is it?!"

    "I yell from the couch, not wanting to get up."

    Kevin "It’s me! Open up, I don’t have my key! Hurry!"

    "I slowly pull myself to my feet, but Kevin keeps knocking madly."
    "Kevin flies in the moment the door opens, pushes me aside, and slams the door shut. He locks it immediately. His hair is messy and he breathes like he just ran a mile. Even worse, he smells like he hasn’t showered in a week."

    show kevin frown sides open with config.say_attribute_transition
    play music "bickering_loop.mp3"
    Shun "Whoa! What happened to you?!"

    Kevin "Uuuuh … you know, heading around here and there and … and uh … is Hideki here? I need to see Hideki."

    hide kevin with config.say_attribute_transition
    "He points to Hideki’s door and rushes inside without knocking."

    Hideki "Gallagher! I told you to—whoa! What happened to you?!"

    "The door shuts before I can hear more."

    Shun "… I need another vacation."

    scene bg dorm with bg_change_hours
    "I finish some homework in the living room, well into the evening."
    "Eventually Hideki escorts Kevin out of his room, smiling smugly. Hideki pulls a house key out from his back pocket and hands it to Kevin with an evil smile. He then pulls out Kevin’s cell phone from his back pocket and gives him that as well."
    "Hideki cackles maliciously as he goes back into his lair. I can hear him slooowly close his door, laughing the entire time until it finally seals shut."
    "Kevin has calmed down. His shoes and hair are … cleaner. Still a little hyperactive, he pulls a beer out of the fridge and sits on the floor. He finishes the drink in four large gulps."

    show kevin neutral crossed closed with config.say_attribute_transition
    Kevin "So … my day … not very good."

    Shun "Gee, really? I had no idea."

    Kevin "Yep. But at least I learned my lessons."

    Shun "What lessons?"

    Kevin open "Lesson one, the cuter she is, the more likely she has a boyfriend. And lesson two, never pick a fight with someone you can’t outrun."

    "He puts the cold beer can on his ribs and hisses."

    Kevin frown "Ow. That’s going to leave a bruise. How’d your day go?"

    Shun "No fights."

    Kevin neutral "Lucky. But we can’t all be “Shun”. Some of us lose bets and have to do dares without our house keys or cell phones."

    Shun "You’re both morons."

    Kevin "You can’t prove that. Ow! God, all this running around takes a lot out of a guy. You having a steady girlfriend almost makes me jealous."

    Shun "Do you want some pain killers or to go to the nurse or something?"

    Kevin smirk "Nah, trust me, I deserve this."

    "He climbs up to the couch and sits next to me with a sigh. He looks up at the ceiling as he rests. He still smells bad but his pain makes it kind of worth it."

    Shun "… Maryanne is acting a little odd lately."

    Kevin neutral "What happened? What’d you do?"

    Shun "She … that package she got. I have a feeling that something isn’t right."

    Kevin "Why don’t you talk to her about it? I mean, if she needs her space, she needs her space. But I wouldn’t let her dwell on anything for too long. Not on her own. You know how she gets."

    if asked_if_free:
        Shun "Yeah. Yeah, I guess. I just hope I didn’t make things worse."
        Shun "I may have pried a bit too much. I meant well, but she looked mad. I mean, all I did was ask her what she was doing this weekend. I just wanted to …"

        show kevin serious with config.say_attribute_transition
        stop music fadeout config.fade_music
        "Kevin’s eyes widen and his jaw drops. He stares at me in a way that makes me think I really messed up."

        Shun "… What?"

        Kevin "… Dude … her birthday’s this weekend."

        Shun "… WHAT?! Why didn’t you tell me?!"

        play music "bickering_loop.mp3"
        Kevin frown crossed open "You’re dating her! I thought you knew!"

        Shun "No one told me!! No one ever tells me anything!"

        Kevin "How the hell do you always hear about this stuff last?!"

        Shun "Don’t blame me for that! I have to figure this stuff as the school year goes along just like everyone who comes here!"

        Kevin neutral "Okay, okay, chill out. There’s plenty of time to fix this."

        Shun "When?! When is it?! Sunday or Saturday?"

        Kevin "Sunday! Don’t panic."

        Shun "Oh my god, she’s probably furious at me!"

        "Kevin grabs my shoulders and shakes me. Hard! Hard enough to make my head bob back and forth."

        Kevin "{em}DON’T!{w=1} FREAK!{w=1} OUT!{w=1} OKAY!?{/em}"

        Shun "O—kay—stop—it—Kev—in."

        Kevin "Good. Now listen. There is still plenty of time to get a gift and make plans and stuff. Shione and I have been talking. We’ve a little something worked out. You’re welcome."

    else:
        Shun "I don’t know. What if I messed up? I don’t think I did."

        Kevin "Don’t dwell on it man. She’s … a lot more sensitive than she lets on. Just make it up to her this weekend."

        Shun "I don’t know, like how?"

        Kevin "Haha. … … … {w}Oh. Oh dude, wait, are you serious?"

        Shun "No … yes … why, what’s happening?"

        stop music fadeout config.fade_music
        Kevin "… Are you serious? Dude, her birthday. It’s Sunday."

        Shun "… What?!"

        Kevin laugh "You didn’t know? How could you not have know? You’re dating her."

        play music "bickering_loop.mp3"
        Shun "Yeah, because no one told me! No one ever tells me anything! I have to figure this stuff as the school year goes along just like everyone who comes here!"
        Shun "I need to get her a gift! Is it Sunday or Saturday?"

        Kevin smirk "Sunday, it’s Sunday. Don’t worry. There is still plenty of time to get a gift and make plans and stuff. Shione and I have been talking, and we’ve got an idea."

    Shun "Shione? Ye—yeah. I saw her earlier. She said to tell you something about this weekend."

    Kevin neutral "She did? Did she say everything was good?"

    Shun "Yeah … she also said you’d know what that meant."

    Kevin smile "Alright! Cool! You are so lucky to have a friend like me, you know that? Seriously, you better remember my damn birthday."
    Kevin "Shione and I knew her birthday was coming, so we rented out a table at Yamada’s. Top shelf stuff."
    Kevin "A bottle of sake, gourmet dishes, maybe a little singing if they have one of those karaoke machines. We’re doing it Saturday. That’s plenty of time to get a present. Oh, and another thing."
    Kevin "Did you ask her to the big dance, the gala? Cause that is coming in like … a month I think."

    Shun "She already said yes."

    Kevin "Awesome. So awesome. So just focus on something good to buy her for her b-day. We’ll take care of the sushi."

    Shun "Thanks. I’ll pay you back when I can."

    Kevin smirk "You’re damn right you will. But for now, just keep on top of everything and do what you can. You know, that’s all anyone can do."

    Kevin "Just promise me that if you ever get rich—"

    Shun "I’ll pay you back, yeah, in full."

    Kevin laugh "Deal."

    hide kevin with config.say_attribute_transition
    "Kevin laughs and pulls out his phone. He goes into his room and closes the door as I hear him say “Shione”."
    "I gulp and start stressing about what to buy Maryanne. Something affordable, something meaningful, something … in town!"

    return
