##------------------
## Act 3: Maryanne
## Scene 4: Package
##------------------

label a3s4:
    scene bg room_maryanne
    with bg_change_minutes
    "I gently place the package on Maryanne’s chair. She starts fiddling with something on her desk, ignoring the parcel."
    "She looks … unwell. Almost sick. She says nothing, only glancing at me a few times as she paces, hoping I’ll open it instead of her."
    "I stay quiet … until eventually Maryanne gently moves to the box. She pulls the piece of tape off the cardboard."
    "Then another."
    "And another … until the lid finally pops open."
    "The box is filled to the brim with packing peanuts, topped off with a small, beautifully embroidered envelope covered with calligraphy."
    "Maryanne open the letter and reads silently to herself, not saying a word or looking away from the folded page. She barely blinks."
    "After a few minutes or reading in total silence, she puts the note to the side and reaches into the box. The peanuts puff out, making a mess that Maryanne is indifferent towards."
    "She pulls out a long plastic container stuffed with candies and cookies. They look homemade. A pink bow is wrapped around its center …"

    Shun "What’s th—"

    "Maryanne carelessly and with total indifference drops the jar to her side, like it was an insult rather than a gift. Thankfully, it doesn’t break."
    "I gesture to the jar, but stop myself before I can speak. She reaches inside the box again, her nails clawing at the noisy cardboard edges."
    "Out comes a framed picture of a happy elderly couple on a boat. The man wears a fishing hat and the woman has faded blonde hair that is almost, but not quite, white. They both wear dark sunglasses colorful shirts and big smiles."

    Shun "Are those your grandparents?"

    show maryanne neutral with config.say_attribute_transition
    Maryanne "…"

    "Maryanne just nods, and puts the framed picture on the floor carefully, pulling out the stand and positioning it so their faces point at her. She looks at the picture and sighs softly."
    "She pulls another envelope out of the box. This one is smaller, blue and wrapped less cordially than the other. Maryanne slides it into her pockets without a word."

    show maryanne confused with config.say_attribute_transition
    "She reaches in and pulls out a red silk ribbon at least a meter long. It looks a little worn in, though it still maintains a modest shine. It glides out of the box like a crimson river. Maryanne looks at the ribbon for a few minutes with a quizzical expression, then gasps."

    Maryanne uncertain "Oh my god …"

    Shun "What? What’s wrong?"

    play music "contemplative_loop.mp3"
    Maryanne "This is … mine. When I was a little girl. My grandma found this when she was cleaning the basement."

    Shun "Whoa, how old is that?"

    Maryanne "Over a decade … wow …"

    "Maryanne glides her fingers over the soft fabric, then drops it to the floor softly by the picture frame."
    "She once again goes back to exploring the package, sliding her hand around the bottom until she bumps into something solid."
    "She pulls out a smooth, oval shaped rock and that fits in her palm easily. She is able to wrap all of her thin fingers around it and squeeze … which she does. For a few moments."
    "She gulps. And with her hand firmly gripping the stone, she starts to clean up, plucking up the Styrofoam piece by piece."

    Shun "… Everything okay?"

    Maryanne "… Sigh … yeah."

    "She puts the packaging back in the box by the fistfuls, without looking at me."

    Shun "… You sure?"

    Maryanne "It’s … fine. I just … it was … something about my grandpa. Grandma sent me a few gifts to say hi. Look! She sent me cookies!"

    show maryanne happy with config.say_attribute_transition
    "She picks up the jar and shakes it, smiling."

    Maryanne ecstatic "Grandma probably baked them herself. And a bunch of other stuff too. She sent a handwritten letter … good ol grandma, right?"

    "She raises her tone, mocking the simplicity of the gift, sounding angry even."

    Shun "… Did the letter say … something bad?"

    Maryanne uncertain "I just … need …"

    Maryanne happy "… Can we hang out later? I need to make a phone call. And I need to be alone. For … a bit. I don’t know, can I just—"

    "I understood what she was asking. She needed time to recoup, just like I did sometimes. Whatever it was that bothered her, prying her might make it worse."

    Shun "Yeah, yeah, sure. Say no more. I’ll call you tomorrow. Or you can call me. Whenever."

    Shun "Bye … uh, Maryanne."

    "She bites her lip and looks away, and I hope she’s thinking about asking me to stay after all."

    Maryanne "… Bye."

    scene bg inou overcast with bg_change_minutes

    "The wind had gotten stronger since this morning. The sky is darker. I hurry back to my dorm, hoping to get back before the storm hits."

    stop music
    scene bg dorm with bg_change_seconds
    play music "decompression_loop.mp3"
    Kevin "{em}… zzzzzz … … zzzzzzz … … zzzzzz …{/em}"

    "Kevin drools, his long arms sprawled out in all directions over the couch and a blanket covering barely half of his chest. A girly-magazine shields the top half of his face."
    "I tap his foot. When that doesn’t work, I push his leg off the armrest."

    show kevin frown sides open with config.say_attribute_transition
    Kevin "{em}… zzz—zz—z cough—cough{/em} Back off Old Man. I’m awake … huh? What happened?"

    Shun "You fell asleep."

    show kevin neutral closed with config.say_attribute_transition
    "I sit down next to him as he yawns, and unpack the lunch I bought from the cafeteria on my way back to our dorm."

    Kevin open "What time is it?"

    Shun "Almost 2 in the afternoon."

    Kevin frown crossed "Oh my god!"

    Shun "It’s Sunday."

    Kevin neutral sides closed "Oh thank god!"

    show kevin smirk crossed open with config.say_attribute_transition
    "Kevin turns the TV on and starts up the DVD player. He skips through the beginning of whatever is on, to a scene where two men in suits point their guns at another, sheepish man protecting a suitcase."

    Kevin smile "Where were you all day?"

    Shun "With Maryanne. We finished homework together."

    Kevin smirk "Cool dude. Did you have fun, or were you actually doing homework?"

    "I roll my eyes and don’t answer. He doesn’t need to know anything. Still, he laughs. And laughs. Just as it gets annoying, he pulls on my shirt tag. My shirt is … inside-out. Oops. "

    Kevin smile "You’re pulling off this boyfriend thing off pretty well man. Smooth sailing in the beginning is a good sign."

    "I nod quietly. Kevin pats my back like he’s a proud older sibling."

    Shun "… Hey, how long have you known Maryanne?"

    Kevin "Technically, more than a decade, but we became friends-friends about three years ago. A little less maybe."

    Shun "What are her grandparents like?"

    Kevin neutral "Her grandparents? No idea. Never met them. Why?"

    Shun "She got a package from them today. And a postcard. It was weird … when she got the postcard from her grandma, she was really happy. But when she got a package from her grandfather, she got … moody. Like, she suddenly stopped talking. And she looked really upset."

    Kevin laugh closed "Oh no! What’s in the boooox?"

    Shun "Nothing weird. Cookies—"

    Kevin serious open "Yet another wasted reference on a man who knows too little. Whatever … but Grandparent-cookies are the best kind! Why would she upset?"

    Shun "There was a photo … and a letter, and envelope … there was a rock too."

    Kevin neutral "A rock? What do you mean a rock?"

    "I describe the stone for Kevin."

    Kevin "… Not ringing any bells. But none of it sounded like something worth getting upset over. Except for the rock I guess. Cause who wants a rock for a gift? Did you ask her what those things meant? Or if she was on her period or something?"

    Shun "I am not asking her that!"

    Kevin smirk "Good, cause girls really get pissed if you ask them that. That how I got that scar on my thigh. Which, ironically, also answered the question for me."

    Shun "Anyway, she asked to be alone … so I left. I’ll check on her tomorrow if it comes up. But … I just have a feeling something’s wrong."

    Kevin neutral "Man, cookies sound pretty good right about now … alright man, you got this. Just tread lightly."

    "Kevin keeps watching his movie as I quietly try to interpret what happened. I try not to dwell, knowing that Maryanne will tell me when she is ready, but my imagination starts to comes up with all kinds of bad scenarios as I pretend to watch the film."
    return
