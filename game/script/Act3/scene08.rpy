##----------------------
## Act 3: Maryanne
## Scene 8: I’m Okay
##----------------------

label a3s8:
    scene bg black with bg_change_days
    stop music fadeout config.fade_music
    "I have a dream, one that I start to forget when I wake up. I think there was an empty building and an earthquake. Whatever it was, it felt … lonely."

    "My clothes still smell like her."

    scene bg room_shun with bg_change_minutes
    "I am half dressed by the time my alarm goes off. I shut it off with a fist, mad at the inanimate object for interrupting my train of thought."

    stop music
    scene bg dorm with bg_change_seconds
    play music "bickering_loop.mp3"
    "Kevin is still at his laptop. Black circles hang under his eyes like skid marks in a cul-de-sac. He looms over the table like a tree swaying on a windy day; back and forth gently, with very little control over himself."
    "His flurry of keyboard strokes has slowed to a drizzle, with one{w=1} hard{w=1} tap{w=1} every{w=1} second{w=1} or{w=1} so.{w=1} {em}tap{w=1} tap{w=1} tap.{/em}"

    show kevin neutral sides closed with config.say_attribute_transition
    Kevin "… … … … Done."

    "He stops typing, and stands up with the grace of a man with a broken leg, almost tumbling down twice as he walks by me."

    Shun "Oh, geez! You need a shower. You smell like coffee and whatever that thing was you were smoking."

    Kevin open smirk "Hehe … heHEHEHEhe. Yeah. I’ll do that. But later. I gotta print … this."

    "He holds up a flash drive, blinking several times as his loony grin twitches."

    Kevin neutral "…"

    Shun "… Uh, yeah. So, I have to go to class now."

    Kevin "Okay. Me too. I think. Is it 10 yet?"

    Shun "It’s 9."

    Kevin closed "Okay. I’ll start walking now. HeheheHHEHEHhe."

    "Exhausted, Kevin struggles to put his left foot in his right shoe. I roll my eyes and grab something small to eat."

    Kevin smirk open "Are you going to see Maryanne today?"

    Shun "I am going to try to. She’s been … upset lately."

    Kevin "Oh. Okay. Well, tell her we’ve got a surprise Saturday for her. Shione got the sushi restaurant to book us a party room. It’ll be fun. Maybe she’ll have fun … fun. Hehe. Did I tell you that yet? … Wow, I’m really tired."

    Shun "I can tell."

    scene bg class_okada with bg_change_minutes
    "Odaka’s lectures, which were once filled with fire and fury and … math, have gradually morphed into mild discussions focused on pure scientific thought. He still laid down the law when it came time to test us, but the diminished class had proven they were not slackers."
    "Odaka rewarded us with the occasional anecdote and modern theory in the current scientific community. Usually at the end of class time."
    "He talked about white holes last week and string theory this week. He seemed to regard both theories as “silly”, but if you watched him carefully … you can see the young scientist he used to be enjoying the incredible “what if” of both ideas."
    "He gets lost in his rambling, like he often did after he was done with his main lecture. and the bell rings before he can finish."

    show okada talking with config.say_attribute_transition
    Okada "Oh, already? Fine. Everyone remember to study and prepare yourself for the next pop quiz that may or may not be on Monday. And be safe this Friday night."
    show okada neutral with config.say_attribute_transition
    "He hobbles over to his desk and starts to methodically put his papers away, while everyone else shuffles out of the class."
    "I wonder what he did with Kevin’s essay, but don’t ask. I’m sure I’ll find out later. And right now, I have more pressing things to worry about."

    scene bg hall_school with bg_change_seconds
    stop music fadeout config.fade_music
    "The east lab is open. And the door is unlocked and ajar. Good!"

    scene bg lab_maryanne sunset with bg_change_seconds
    "I step in and see Maryanne, sitting in her usual spot as I expect her. Her eyes are glued to a giant book, one that looks too heavy for a normal person to lift."
    "I step up behind her. Her head moves very slightly to the left, then back to the center of the book. Her hands move to her lap. I say nothing."

    play music "contemplative_loop.mp3"
    Shun "…"

    show maryanne mad coat circles with config.say_attribute_transition
    Maryanne "… What do you want?"

    "The lab is empty besides us, but she whispers like we might be caught by the teacher."

    menu:
        "Demand an explanation.":
            $ demanded_explanation = True
            Shun "Uh, excuse me? But I think you owe me an explanation for last night."
            Shun "I would have listened if you wanted to talk. But, you just … left me there. Alone."

            "Maryanne’s shoulders push inward. Her head lowers. She look smaller."

            Maryanne worried "… I didn’t want to bother you."

            "Still speak softly. Meekly. Still not looking at me."

            Shun "What’s going on?"

        "Ask if you did something wrong.":
            $ demanded_explanation = False
            Shun "… Did I do something wrong? Are you mad at me?"

            "She lifts her head up and looks at me out of the corner of her eyes. Her big hazel eyes, still heavy with stress, glance at me for only a few seconds."

            Maryanne sad "… No."

            "Still she whispers. Still I feel like she is a million miles away."

            Shun "Then what’s going on?"

    Maryanne worried "… I—I …"

    "Her fingers grab the edges of her coat, tugging the fabric over her knees as she gulps."

    Maryanne "… I’m just …"

    "The room gets so silent. I swear I can hear her heart beating. She turns her head suddenly, flicking her hair around. And smiles."

    Maryanne mad "It’s nothing. I’m just being stupid, I’m sorry. That’s all. It’s … uh, don’t worry about."

    Shun "Don’t worry? Maryanne, you’re doing nothing {em}but{/em} making me worry."

    Maryanne happy "I know, I know, I know, and I’m sorry. I didn’t mean to push that … bit of stress on you and … ugh! Stupid, stupid, stupid."

    "She taps the side of her head with her knuckle and giggles. Then smiles wider than before and stands up."

    Maryanne ecstatic "Thank for worrying about me. But I can handle … I mean … you can relax. I just needed to get that out of my system. I didn’t know that it would bother you so much."

    show maryanne happy with config.say_attribute_transition
    "She smiles. And the smiles stays. Her lips sealed."

    Shun "Uh, alright. Well … if you are sure …"

    Maryanne "Good. You’re too nice to me, you know that?"

    "She ruffles the top of my head and giggles again."

    "She closes the hefty book and struggles to pick it up. I help her lift it and move it to the other side of the room. She thanks me and I suspect that she pretended to need my help to make me feel useful."

    Maryanne "So, is that all? Were you here just to see if I was alright? Or did you need help with work again?"

    Shun "Well, I wanted to tell you to keep Saturday night free."

    Maryanne "Oh? Okay, sure thing. What for?"

    Shun "It’s a surprise."

    Maryanne "Really? What …"
    Maryanne "… A surprise for what?"

    Shun "It’s … you know. For your birthday."

    Maryanne uncomfortable "…"

    "Her smiles evaporates. Suddenly, she looks full of … dread. Like she just got stabbed in the heart."

    Maryanne "… Okay! That would be really nice!"

    "Her breathing gets … louder … faster. And she sits down, gently scratching her chest a few times."

    Maryanne "I … I just … didn’t think …"

    "She raps her fingers on the table a few times."

    Maryanne uncertain "I was … worried everyone forgot about my birthday. That would have been awful, right?"
    Maryanne "I’ll keep Saturday night open. Thank you Shun. I won’t ask questions. I’ll just show up and we’ll have a great time!"

    "Maryanne look at me as I look back, both of us looking for a signal the other is actually okay or not."

    Maryanne happy "… Aww, you’re still worried? That’s so sweet. Don’t worry, don’t worry. I’m fine. Come Saturday, I’ll be back to 100\% without a doubt. And we’ll have fun. Whatever you are planning, we’ll have fun."

    "She gently touches my elbow and smiles again. I want to believe her, so I just nod."

    Shun "… Are you busy now?"

    Maryanne "I have to finish one more thing, then clean up."

    "We stare at each other for a minute. One fake smile to another. Her eyes dart to the door, then back. I take the hint."

    Shun "I’ll leave you to it then."

    Maryanne "Thank you."

    "She hurries back to her stool and turns on her laptop."

    if demanded_explanation:
        scene bg hall_school with bg_change_seconds
        "I leave, not turning my head to see if she watches me go. And hurry down the hallway, feeling more frustrated than I did before walking in."
    else:
        hide maryanne with config.say_attribute_transition
        "I leave slowly. And I look back at her before stepping out of the lab."
        "She does not look back at me."
        scene bg hall_school with bg_change_seconds
        stop music fadeout config.fade_music
        "I drag my feet down the hallway, feeling more frustrated than I did before walking in."

    "My concern turns into flat out fear. Watching her try to hold it together makes all kinds of scenarios run through my head … god, overthinking is a curse!"

    scene bg inou_gate sunset with bg_change_seconds
    "I walk outside, head hanging low … right into a cloud of smoke that blinds me for a moment."

    show okada talking with config.say_attribute_transition
    Okada "Hmm … excuse me young man. I did not see you there."

    show okada neutral with config.say_attribute_transition
    Shun "{em}cough cough{/em}, no problem sir."

    "Okada takes another drag from the smoke-stick, looking over my face, judging me like he does to everyone."

    play music "decompression_loop.mp3"
    Okada talking "You seem stressed Mr. Takamine."

    show okada neutral with config.say_attribute_transition
    Shun "N—no sir!"

    Okada talking "Oh. Right. Because you can handle it by yourself, yes? You don’t need help or advice or anything. Young people always think they can handle it by themselves."

    "He turns his eyes, grumbling about something. I hear “cell phones” and “skirts” I think."

    Okada "You seem like a smart man Mr. Takamine. I know {em}you{/em} at least have a good head on your shoulders. I hope you know that too. Really. Too many of your peers don’t, or think they do."

    show okada neutral with config.say_attribute_transition
    Shun "Uh … thank you sir."

    Okada talking "So … get to it then. What’s the girl doing to you?"

    show okada neutral with config.say_attribute_transition
    Shun "Wh—what? You know about—"

    Okada talking "The blonde, yes I know! Everybody knows."

    show okada neutral with config.say_attribute_transition
    Shun "They do?!"

    Okada talking "Hmph. Maybe I misjudged that head of yours."

    show okada neutral with config.say_attribute_transition
    "He inhales deeply again and grumbles. Looking at the ground with … anger. Watered-down anger, like he was more frustrated than genuinely upset. His shoe kicks the dirt and I can see that even that takes some effort. For some reason, I think of my dad."

    Okada talking "… I asked you a question."

    show okada neutral with config.say_attribute_transition
    Shun "Wh—what question sir?"

    Okada angry "Just because we are not in class, that does not mean I am not your teacher. So … answer me! What is this girl doing to you? If she’s stressing you out so much, why even bother? Hmmm?"

    "He barks, almost yelling. My spine shivers as I try to find my words. I think of how long the last two days were, how difficult it was just to get an honest answer from her. How I hadn’t felt this stuck since … maybe I never felt this stuck honestly."

    show okada neutral with config.say_attribute_transition
    "Then I remember what her hair felt like on my neck when she rested her head on my shoulder. And her laugh. And how she was … just … so …"

    Shun "… Well … to be honest sir …"

    Okada talking "… Hmph. Look at you. You thought of a good answer, hmm? You calmed down right on the spot."

    show okada neutral with config.say_attribute_transition
    "I sigh softly, as Okada fiddles the cigarette between his fingers."

    Okada talking "Remember that feeling son …"
    Okada "If there is any lesson you take from my class after you leave it, it is this: Hard work pays off. In one way or another."
    Okada "You need to know when to back away, sure. But hard work is a virtue, not an event. It’s not something you do sometimes. It is a lifestyle choice. It is your duty if you want things worth having."
    Okada "Do you follow me young man?"

    menu:
        "Yes sir.":
            show okada neutral with config.say_attribute_transition
            Shun "Yes sir."

            Okada talking "Hmph … I wish more young people would know that."
        "No sir.":
            show okada neutral with config.say_attribute_transition
            Shun "No sir. I am afraid I don’t."

            Okada talking "… You will Mr. Takamine. You will."

            "He says ominously, looming over me like a wrinkly ogre."

            Okada "But some never learn …"

    Okada "Like that friend of yours … that Gallagher fool. He doesn’t understand. He’s smart … but he doesn’t understand that he is only letting himself down by not truly applying himself."
    Okada "I gave him an assignment recently. A 30 page paper. Just like you. And just like you, I knew it was unfair."

    show okada neutral with config.say_attribute_transition
    "He looks at me, like he’s waiting for me to ask him something. Then smirks condescendingly."

    Okada talking "I wanted to see how you’d do when pushed against the wall. Both of you. For him … it was to test his patience. Test his tolerance of me. And test his arrogance. Ha!"
    Okada "I knew he’d be too stubborn to not do it. Of course he finished it. It was wonderful to toss it in the garbage without so much as looking at it, and giving him an A on the spot."
    Okada "The poor boy couldn’t process it. It was wonderful. Looked like he was going to have a heart attack because I didn’t even read it and he worked hard on it … so mission accomplished I guess."

    show okada neutral with config.say_attribute_transition
    Shun "Um, sir … with all due respect … you did that just to teach him a lesson?"

    Okada talking "Two lessons. The second one was {em}now{/em} … Mr. Gallagher knows he is not smarter than me."

    show okada neutral with config.say_attribute_transition
    "Okada smiles and puts out his cigarette."

    Okada talking "Well then … good evening Mr. Takamine. Off I go, to where I go next."

    hide okada with config.say_attribute_transition
    "He shambles towards the front gates, slightly hunched over as always. I watch him leave campus. Then I go on my way."
    "I look at the window to the science lab as I pass it, and wonder what Maryanne is hiding from me. If anything. Maybe I am pestering her too much. Maybe she was just stressed out for a bit … I hope anyway. But her birthday party would make her feel better … right?"
    "I consider calling her, but frustration and loneliness still have the better of me."
    "Maybe some time alone will help {em}me{/em} too."

    scene bg dorm with bg_change_minutes
    "Kevin is on the couch, laying face-first into the cushions, his feet over the edge."

    Shun "Hey."

    Kevin "…"

    Shun "Rough day?"

    Kevin "…"

    Shun "… So, the paper went well then?"

    "Kevin rolls onto his back."

    show kevin frown open crossed with config.say_attribute_transition
    Kevin "… I really really hate that old man."

    "I pat his shoulder, then go to my room to finish my homework."

    scene bg room_shun sunset with bg_change_seconds
    "My assignments take longer than usual. I have gotten used to doing them with a partner. But, motivated by the prospect of being completely free Saturday, I push on and eventually finish."
    "I check the order form for Maryanne’s gift and remind myself to pick it up as soon as I’m able to."
    return
