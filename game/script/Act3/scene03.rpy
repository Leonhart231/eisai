##------------------
## Act 3: Maryanne
## Scene 3: Postcard
##------------------

label a3s3:
    scene bg dorm with bg_change_days
    "The next day, Maryanne types and raps and furiously taps on her keyboard as she tries to finish her notes on time, while I watch a video lecture. I take notes, to help with the upcoming “surprise” pop quiz that Okada will surely give next class."
    "Working in our dorm weekly was a great way to keep her out of her lab and simultaneously make sure we both stayed on top of her workload. Months later, I’m still annoyed no one warned me about the homework load in college."
    "The room was starting to smell like her a little. Hideki hated it, but I looked forward to laying down on the couch after a long school day and smelling flowers. Few days went by when this welcoming scent didn’t motivate me to text her immediately after."
    "Almost twice a week, when we weren’t going out, we’d study together … usually when Hideki was not around or when he and Kevin were out drinking. Kevin knew what he was doing when he’d offer to buy the first round, for me and Hideki."
    "Hideki had been extra agitated lately. Weeks had gone by with still no word on why he was so upset."
    "Regardless, Maryanne and I had our time together. We’d usually cuddle on the couch after studying. More frequently, we’d spend time in my room. But even when we weren’t doing that, just being around her was like being around a magnet; there just an urge to be next to her now."
    "I wanted to make her laugh. I wanted to see her happy. I had a scholarship that was dependent on my GPA staying up, but I kept my grades mainly because I didn’t want to disappoint her."
    "And then one day, among these feelings of motivation, of contentment, the desire to impress someone else who I had presumably already impressed … I had this realization that took me by surprise."
    "I was happy."
    "That loneliness was gone. The aching emptiness of going to my room and spending my days alone after school … was gone."
    "Maryanne, my roommates, all of my new friends … they were a part of my life now. It was … freeing."
    "My audio lecture finished and Maryanne is still typing away. I cough a few times to get her attention. She doesn’t notice."
    "Finally, I crumple up a blank sheet of paper from my book and throw it at her keyboard. That gets her attention. She playfully scoffs and throws it back."

    show maryanne happy with config.say_attribute_transition
    Shun "What’cha workin’ on? More science?"

    Maryanne "Oh yeah, lots of science. I’m science-ing so much."

    "I walk up to her swivel chair and hug her from behind. Her corn-colored hair tickles my chin a little. My phone buzzes softly in my pocket."

    Shun "Wanna go stretch your legs?"

    Maryanne uncertain "Well …"

    Shun "Just for a bit. Promise."

    Maryanne happy "Okay, for a bit. I have to mail something anyway."

    "She puts her laptop away, knowing her notes will be there when she gets back. Finally, she realizes that."

    scene bg inou with bg_change_minutes
    "We walk around campus holding hands. I feel taller when she holds my hand, even though we’re about the same height."

    "My phone buzzes again. This time I check it."

    Mom "{tt}Shun. Hi. Wanted to check if—{/tt}"

    "I close my phone before reading the rest."

    show maryanne uncertain with config.say_attribute_transition
    Maryanne "… Spam again?"

    Shun "… Yep."

    show maryanne neutral with config.say_attribute_transition
    "She’s started asking me that question when I ignored my texts. And I’d keep answering with a less-than-excited “yep”, not ready to talk to her about everything. Not yet."
    "Maryanne stops walking. And does this thing with her eyes … not puppy-dog eyes, but it was the way she looks at me. A way that made me feel guilty for no reason."

    Shun "It was my mom."

    stop music fadeout config.fade_music
    "She says nothing. That look gets more intense."

    play music "eisai.mp3"
    Shun "She … and my dad … stopped calling me. They send me a text every now and then."

    show maryanne sad with config.say_attribute_transition
    "Her eyes get sadder. Still, she says nothing. It gets quiet and stays quiet until I nervously speak up again."

    Shun "Before I left, they said they were going to call me at least once a week. And they did. For a while. Then they missed a day. Then two, or called late. Then that became a week … I don’t want to talk about, I’m sorry."

    "Maryanne knew that my parents were splitting up, but that was all. I never went into greater detail. I couldn’t. What would she think? I didn’t want to ruin a good times. And to be fair … I didn’t really know what was happening either."
    "I stopped talking to them altogether. I had the vaguest idea of where they were in the divorce process, but I tried to stay away from that issue like it was quarantined."
    "Still, here we are, talking about it again. And again, the mood was sullied. Maryanne stares at me for a moment. Then she rubbed her thumb across my knuckle."

    Maryanne "When my mom left … my dad became a different person."

    Maryanne "He stopped smiling for a really long time. It hurt him, but he carried that hurt with him. Quietly."

    Maryanne "I know why, cause I was a kid. I carried it too. And he carried it alone … for a lot longer than he should have."

    Maryanne "You don’t have to talk about it. But I’d listen if you wanted to."

    "I don’t know why her words hurt. I don’t feel guilty or ashamed or angry, or pressured into talking now. She doesn’t make me feel like I am doing something wrong by telling me she knows something is bothering me."

    show maryanne neutral with config.say_attribute_transition
    "But before I can swallow the lump in my throat, she kissed my cheek. And I say …"

    Shun "…"

    "… I can’t."

    Shun "… Thank you."

    show maryanne sad with config.say_attribute_transition
    "I plainly respond. Maryanne nods without another word."
    "The envelope in her hand trembles. I can practically see the weight of the world on her shoulders again. Maybe it’s something you get used to over time."

    stop music
    scene bg inou_gate
    show maryanne
    with bg_change_minutes
    play music "decompression_loop.mp3"
    "The campus “mail room” is a small hole-in-the-wall next to the admission office, a barely noticeable door that I must have passed a dozen times without seeing it. There isn’t even a sign on the front door."

    Maryanne "Hang on. This’ll only take a minute."

    hide maryanne with config.say_attribute_transition
    "Maryanne ducks inside the tiny room, envelope in hand. I wait, sighing softly as I see the sky get darker. Clouds were starting to roll in."

    scene bg inou_gate with bg_change_minutes
    "Minutes later, Maryanne comes out with a skip in her step, smiling ear-to-ear."

    show maryanne ecstatic with config.say_attribute_transition
    Maryanne "{em}Yes!{/em} Look! My grandma sent me a postcard!"

    show maryanne happy with config.say_attribute_transition
    "She eagerly pushes the small card toward me, pointing to the image on the front. I’ve seen it before, white letters on top of a hill, like in the movies. The other side has scribbles of bad handwriting in black ink, with several Xs and Os dotted at the bottom."

    Shun "What does it say?"

    Maryanne ecstatic "“Dear Maryanne. Everyone in Trinity misses you and is very proud of you. Hope to see you soon. Maybe Christmas? Grandpa says he wants to see Japan next! Love Grandma Mabel.”"

    "Beaming like a spotlight, Maryanne holds the card to her chest and hugs it like it was her grandmother."

    Maryanne "Dad told me they went to LA last week, they probably bought the card then! Oh, grandpa Ralph loves John Wayne … he probably went to his hand prints in the walk of fame … hehe, sorry. You probably don’t know who he is."

    Shun "Your grandpa? No, sorry."

    Maryanne happy "Haha, I used to go up to his house every weekend and he’d play an old western almost every night. I sat on his knee and he’d tell me what things were like when that movie came out."
    Maryanne "And sometimes he’d let me stay up extra late so I could I watch PG-13 films without grandma knowing."

    Shun "That’s too adorable! Did you ever get into trouble?"

    Maryanne "Honestly, I think my grandma knew every time we did that but never said anything."

    Maryanne neutral "I saw them once before coming to Inou. It was … the last time I saw them actually. Wow, it’s been years already. And …"

    "Her hazel eyes glimmer as she trails off."

    Shun "… You miss them a lot, huh?"

    Maryanne uncomfortable "… Only a little. I miss all of it a little. Who doesn’t miss being a kid?"

    Maryanne happy "But I have a good life here too! My dad thought I should go to school in California but I wanted to stay here. And Grandma’s proud that I took a chance like that."
    Maryanne "I wanted to be like her when I was a kid. But she told me …"

    show maryanne sad with config.say_attribute_transition
    stop music fadeout config.fade_music
    "Maryanne stops and rubs the soft spots under her eyes."

    play music "contemplative_loop.mp3"
    Maryanne sad "… Okay. You got me. I miss them a lot."

    show maryanne happy with config.say_attribute_transition
    "She exhales loudly and smiles again."

    Shun "Did you ever go back to see them?"

    Maryanne "What, back in Trinity? I mean … I haven’t been back to America in a long time. I’d like to, sure. They’d be the first people I go see."

    Shun "Do you at least call them?"

    Maryanne "Every week, every single week."

    Shun "Good, good. I bet that means the world to them."

    show maryanne worried with config.say_attribute_transition
    "She squeezes my hand, firmly grasping the postcard with her other."
    stop music fadeout config.fade_music
    "The door to the mail room opens up."

    Anon "Excuse me ma’am, is your last name “Sawyer”?"

    Maryanne uncertain "Hmm? Yes. Did I forget to sign something?"

    Anon "We noticed you have a package here. We just sorted through it."

    play music "decompression_loop.mp3"
    play sound "338958_podsburgh_rummaging-through-cardboard-boxes"
    Maryanne happy "Oh, great. Thank you for letting me know."

    "He quickly goes back into the mail room, and promptly returns with a thick brown box wrapped in clear tape. It is a bit bigger than my head and draped in stickers and warning labels in bright letters."
    "“FRAGILE”, “DELICATE”, “PRIVATE” are written on one side and I assume their English translations on the other. There’s an assortment of red, white and blue stamps on the top."
    "Maryanne signs for the box."

    Maryanne neutral "Thank you— Whoa! Oh, it’s heavier than it looks!"

    "Maryanne pushes the box in my hands. I try not to grunt or move back, but the weight of whatever’s inside is all uneven and makes it hard to balance it."

    Shun "Oof! Yeah, it’s got some heft to it. Were you expecting something?"

    Maryanne "No, this is—"

    show maryanne uncomfortable with config.say_attribute_transition
    "Her eyes widen, suddenly remembering important something."

    Maryanne neutral "… Uh, it’s probably … I’m not sure. It’s probably nothing."

    "She looks at the return address in the corner, her fingers running over the black marker."

    Maryanne uncertain "Oh! It’s from Grandpa Ralph … oh … great."

    Shun "Isn’t that a good thing? You got a package from your grandparents, right?"

    Maryanne worried "I—it’s n—nothing. Let’s get it back to … uh, would you mind carrying it back to my room?"

    Shun "Sure. But …"

    "She looks worried. Maybe scared. Something tells me to zip my lip and not pry right away."

    show maryanne happy with config.say_attribute_transition
    "I walk back with her to her dorm room. She forces a smile … again, looking less nervous as we walk back, hopefully calming down."

    return
