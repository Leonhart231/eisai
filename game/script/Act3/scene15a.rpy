##----------------------
## Act 3: Maryanne
## Scene 15a: Bad Ending
##----------------------

label a3s15a:

    Maryanne mad "Well … maybe I don’t want you to be here. Did you ever think of that?"

    Shun "… I don’t get it Maryanne. I’m trying to be patient, I’m trying to be understanding … I’m trying to be a good boyfriend. Why won’t you let me?"

    Maryanne pissed "Because, I don’t want to be helped. Why is that {em}so{/em} hard for you to understand?"

    Shun "Because … I … I know that being alone all the time … you …"

    "I stumble over my words as my exasperation turns to irritation. I couldn’t believe her! After all the good times we had, after all I have done for her so far, she was still pushing me away. And now, she was being flat out aggressive."

    Shun "Why are you being like this?! For god sakes, I want an actual answer! What did I do to make you hate me?"

    Maryanne "You think it’s about you? I don’t hate you, I just want you to … back off!"

    Shun "You’re messed up, you know that?"

    Maryanne "I’m messed up?! You can’t even tell when someone doesn’t want to be coddled! Or talked down to! Or treated like …"

    Shun "Like what?"

    Maryanne "Like … this! This isn’t fair. None of this is fair. I just want to be okay. I just want to not have to feel like this all the time."
    Maryanne "And you’re just making this all worse! I can’t get my work done, I haven’t spent that much time with Shione and … and I just want to be alone! But you won’t take a hint! Go away!"

    Shun "You know what, fine! Do whatever you want? And do it alone! I don’t care anymore. This isn’t worth it."

    hide maryanne with config.say_attribute_transition
    "I turn, furious at her, unable to walk away fast with the mud under my shoes. I grab my broken umbrella up and hurry away."

    Maryanne "Wait! Shun! Wait, I didn’t … I …"

    "She says something at last, but I am not in any mood. I can barely see straight. I can’t remember being so mad at … anyone before, let alone someone who I thought cared about me."
    "I leave her under the bridge, not caring if she stays there."

    scene bg dorm with bg_change_minutes
    call stop_weather()
    "I hurry back to my dorm. Alone! The way I prefer it, I tell myself."
    "Kevin and Mr. Sawyer are not in the living room. I slam my door shut and leave my lights off."
    scene bg room_shun overcast with bg_change_seconds
    "I pull her stupid birthday present out of my pocket and throw it in the garbage without a second thought, then throw myself onto my pillow and bury my face in it."

    "{snd}knock knock{/snd}"

    Kevin "Hey!"

    Shun "Go away! I want to be alone."

    Kevin "Dude, where is Maryanne? You didn’t tell us where she—"

    Shun "She wants to be alone! That’s what she said, that’s how it left off! Okay!? She doesn’t want to see me or anyone!"

    Kevin "Did you guys break up?"

    Shun "KEVIN!!! Leave me the fuck alone!!"

    "Kevin goes quiet. And he stays quiet. For the rest of the night."

    scene bg black with bg_change_minutes
    "The next day feels like hangover, the kind that can’t be slept off. I look out the window and see the storm clouds. Fitting."
    "I grab my phone. Several missed calls, three new messages. I turn it off, not caring."
    "I stay wrapped in my blankets. They don’t have the warmth I want; they feel nothing like the cozy feeling of Maryanne grabbing my waist."
    "I can see the present sticking out of my trash can. Shimmering brightly through the rest of the tissues and scrap paper."
    "I roll over and ignore it."

    scene bg room_shun with bg_change_minutes
    play sound "194365_macif_door-knocking-angry"
    "{snd}BANGBANGBANG{/snd}"

    Shun "Go away!"

    Kevin "Shun … you’ve been in there for almost two days."

    Shun "Just leave me alone Kevin!"

    Kevin "Maryanne went missing again. We’ve been looking for her, Shione and I and … come on buddy. We could really use your help. Everyone’s worried sick about you both."

    "I think about it. About her. I think of what her hair smells like when it is wet."

    Shun "Kevin … please leave me alone."

    Kevin "… A—alright."

    "Kevin gives up. I hear him open the front door and leave."
    "…"
    play sound "256513_deleted_user_4772965_knock-on-the-door"
    "{snd}knock knock knock{/snd}"

    Shun "Go away Hideki …"

    Hideki "You are alone in there …"

    Hideki "… This is a hard truth, for anyone to realize."

    "And then he leaves."
    "And then I think about it."
    "And then … I close my eyes."

    scene bg dorm with bg_change_minutes

    "Monday comes. I somehow find the strength to put on a decent set of clothes. I’m tempted to turn on my phone … but I don’t. I can’t. My pride lingers in my bones and it makes the phone too heavy to lift."
    "My dorm is empty. Kevin’s shoes are gone."

    scene bg inou with bg_change_minutes
    "It feels like the road to the science building is an extra mile long. And less beautiful. My books feel heavier than they did last week."
    "There are people whispering as I walk towards Okada’s classroom. I block the sounds of their rumors out as best as I can, but I notice that they rise and fall according to where I go. I wonder if they are about me and my … breakup."

    scene bg hall_school with bg_change_minutes
    stop music fadeout config.fade_music
    "I see several girls staring at me as I walk by, whispering to each other again. I hear her name spring up. Now I am sure it was a breakup. I don’t look at them."
    "On the way to Okada’s class, like every other day, I pass by that atrocious lab that Maryanne claimed to own. And, like every other day, she was there. Working."
    "But not today."
    "I look inside and don’t see the usual blonde head hunched over a microscope, not do I hear heels clicking on the tiles as she rapidly scurries from one end of the lab to the other."
    "I see an empty stool."
    "The light spills over it, trying to fill the empty space that she left behind. The stool, her throne, looks like it is waiting for her to come back. In vain."

    Shun "She’s not coming back."

    "I remind myself of that fact."
    "I pull my backpack over my shoulder, and go to class."
    return
