##----------------------------
## Act 3: Maryanne
## Scene 15b: Lost (continued)
##----------------------------

label a3s15b:

    Maryanne "Why?"

    stop music fadeout config.fade_music
    Shun "… Because … I love you Maryanne."

    show maryanne mad smalltears with config.say_attribute_transition
    "She stares at the flowing water, looking … angry. So angry that she could explode. Then she starts to cry again, pushing her face on her forearm, refusing to look at me."
    hide maryanne with config.say_attribute_transition
    "I sit down next to her. My pants get ruined."

    Maryanne "I don’t want you to see me like this. I don’t want to {em}feel{/em} like this. All the time. But it hurts so bad and I don’t know why. If it doesn’t matter, if it is all pointless … why does this hurt so bad?"

    Shun "Are you really mad at me?"

    play music "<from 71.57>romantic.mp3"
    Maryanne "I’m just mad! I was so mad before you showed up. God, for hours before you showed up … all I wanted was for someone to come get me. And you did. And then I was … the way I was …"

    "She tapers off. We stay under the stone arc in silence for a few minutes. The sound of the howling wind echoes through the underpass."
    "I reach into my pocket and pull out the gift. I figure now is as good of a time as any. The wrapping paper is completely destroyed, hanging on by only the ribbon on it. The cardboard box is crumpled up too, partially wet."
    "I shudder when I see the mess and gulp as I slowly hand it to her slowly."

    Shun "I … never got a chance to give you this. Happy … uh, happy birthday."

    show maryanne circles tangled neutral with config.say_attribute_transition
    "She sighs and takes it from me, exhausted."

    Maryanne "Can I open it later?"

    Shun "Sure, it’s yours. Open it whenever you want."
    show maryanne sad with config.say_attribute_transition
    Shun "… I really don’t know what to say. Or if you even want me to say anything. But I’m here. And I want to be here. I don’t think that all of this is pointless, or that we don’t matter. You matter."
    Shun "To me. And your dad and Shione and Kevin … you matter to a lot of people. I don’t want you to feel like you’re nobody. Cause that’s not true."

    show maryanne sad smalltears with config.say_attribute_transition
    "Maryanne bites her lip. Her eyes squint. A tear goes down her face and she inhales deeply."

    Shun "… I’m … just trying."

    Maryanne "I … I think I want to leave now."

    "I help her up. Her fingers are damp and pruny. We both slosh through the mud, using her umbrella and forgetting mine altogether as we walk up the sloppy hill."

    Maryanne notears neutral "My dad is here?"

    Shun "Yeah. At my place."

    Maryanne sad "Is he worried?"

    Shun "Uh … yeah. A little. I mean, we all were."

    hide maryanne with config.say_attribute_transition
    "Still not looking at me, she gulps."

    Maryanne "I’m sorry I ruined my own birthday party."
    Maryanne "I’m sorry I ruined everything. I didn’t mean to."

    "I rub her shoulder and walk her down the road. The rain dies down a little. We say nothing as we walk back. I don’t know what I {em}can{/em} say …"

    return
