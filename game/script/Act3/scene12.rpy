##--------------------
## Act 3: Maryanne
## Scene 12: Breakdown
##--------------------

label a3s12:
    scene bg hall_dorm_girls with bg_change_minutes
    stop music fadeout config.fade_music
    "I knock on Maryanne’s door, softly at first. No answer."
    "I knock again a little harder. A few times in succession. Not a peep from the other side."
    "I wait a few minutes, thinking she is asleep. … {em}Hoping{/em} she is asleep."
    "A girl walks by as I wait outside her door."
    "I wait. And wait. And wait. And knock again louder a few times … before leaving."

    scene bg inou night with bg_change_minutes
    "I sprint out of the girl’s dorm, towards the science building."

    scene bg hall_school night with bg_change_seconds
    "Dashing through the halls, I hurry. The building is cold and lifeless now, with no students in the classes. The only sound I hear is a series of clomping echoes as my feet bang on the floor."

    stop music
    scene bg lab_maryanne night with bg_change_seconds
    play music "unrest_loop.mp3"
    "I let myself in the lab. The lights are off. The moon shines through the open window, down on a head of glimmering yellow hair."
    "Maryanne leans over a set of beakers, black rings drooping over her barely open eyelids. She sways gently, seemingly unable to sit up. A few empty disposable cups rest next to her, as well as a half-empty to-go boxes from one of the diners in town."

    Shun "Maryanne?"

    "She meekly turns as I say her name. She blinks once, then smiles."

    show maryanne coat tangled happy circles with config.say_attribute_transition
    Maryanne "… Hi. Shun, hi. Uh, what … what are you doing here?"

    Shun "What are {em}you{/em} doing here? It’s really late."

    Maryanne "I’m working. Haha, you know me!"

    "She giggles and rubs the dark skin under her eyes. Still smiling, even though her face looks so worn out, her body so tired. She looks like a factory worker coming off a double shift."

    Shun "Do you mind if I stay and help?"

    show maryanne neutral with config.say_attribute_transition
    "She says nothing at first, becoming suddenly grumpier."

    Maryanne "You didn’t answer me. Why are you here? Did you just come to ask if you could help?"

    Shun "Something like that."

    Maryanne "Don’t worry about it Shun. I have everything completely under control. I’m okay by myself."

    "She slides her hand to her right and knocks over empty coffee cups stacked by her wrist. She rubs her temple and sighs loudly, growing frustrated for a moment … then she smiles again."

    show maryanne happy with config.say_attribute_transition
    Shun "I can get you another cup if you’d like."

    Maryanne "That … would be nice. If you could."

    "She relents, letting me help in at least {em}this{/em} way."
    "I leave, not closing the door, and go to the teacher’s lounge."

    scene bg lab_maryanne night with bg_change_minutes
    "When I return, Maryanne’s face is planted on her thickest textbook as a pillow. She looks peaceful, despite being so exhausted. I sit on the stool next to hers, putting the coffee to my side, and rub her back gently. She mumbles something. Her leg twitches."

    Shun "Maryanne …"

    "I whisper softly, and she wakes up. She springs off the book fast, almost falling off her stool."

    show maryanne coat tangled stressed circles with vpunch
    Maryanne "… What time is it?!"

    "Maryanne frantically grabs her phone and checks the time when I do not answer right away. She sighs and calms down when she sees only a few minutes have passed."

    Maryanne neutral "… Oh good. I must have just … yaaaaaaawwwwnn … nodded off for a second."

    hide maryanne with config.say_attribute_transition
    "She turns back to her books, like I am not even there. I say nothing, waiting a full minute in silence as she studies. Then two minutes. She keeps reading, either ignoring me or totally cloistered off in her studies."
    "I glance to the right and see the metal cot. It is tied up, unused, but it is under the window like she is plans to unwrap later."

    Shun "How long have you been awake?"

    Maryanne "Only a few seconds technically."

    Shun "… You know what I mean."

    Maryanne "… Uh … well, what does that matter?"

    Shun "Of course it does. This isn’t … I don’t know, healthy?"

    "Her fingers grip the edges of the table. Tightly. She stares at her book as she huffs out of her nose."

    Shun "You look very tired. How about you leave this for tomorrow, huh?"

    Maryanne "I’m fine Shun. Thank you."

    Shun "You know, Kevin said …"

    show maryanne coat tangled mad circles with config.say_attribute_transition
    Maryanne "Oh, Kevin said this, Kevin said that! Why don’t you just go bother Kevin if you think what he says is so important?!"

    "The clock in the room ticks loudly."

    hide maryanne with config.say_attribute_transition
    "Maryanne turns back to her book, embarrassed. Not saying another word. The clock ticks again."
    "I push the cup of coffee towards her."

    Maryanne "Thank you."

    Shun "You’re welcome."

    "Another long pause drifts between us."
    "I watch the small hand on the clock tick … tick … tick. Seconds pass. Minutes pass. I hear paper turning as she pretends to study."

    Shun "Am I really bothering you?"

    Maryanne "I didn’t mean that. I just … I … would prefer to be alone right now."

    Shun "Kevin {em}is{/em} worried about you."

    Maryanne "I know."

    Shun "I am too … a little."

    Maryanne mad "You don’t have to be. There is nothing to worry about."

    Shun "Then why haven’t you slept?"

    Maryanne pissed "I told you. I have work to do!"

    Shun "Why can’t you do it tomorrow?"

    show maryanne coat tangled worried circles with config.say_attribute_transition
    Maryanne mad "Shun, stop. Please. Just stop! You wouldn’t understand."

    "She looks at me this time, granting me the privilege of eye contact as she insists this is not something I could know."

    Shun "What? What wouldn’t I understand?"

    Maryanne "I don’t want to get into it. I just want to work."

    hide maryanne with config.say_attribute_transition
    "She stands up and walks to the one of the cabinets on the other side of the room. She opens it and starts looking around, not finding what she is looking for. Pretending to work. I see through her farce and shake my head."

    Maryanne "I’ll talk to you tomorrow about this, okay?"
    Maryanne "But tonight, I just want to be alone."

    "My own frustration grows. I don’t want to hear excuses anymore. Maybe I didn’t understand, maybe I never would, but I didn’t want to go."

    Shun "I don’t really feel like leaving."

    Maryanne pissed "Fine Shun. Don’t leave. Just stay here all night."

    Shun "If I need to stay all night, fine. I will."

    "I stamp my foot, trying to be dramatic, but I think it just made me look silly."

    Maryanne mad "Fine. Just stay."

    "That irritating “do-what-you-want” attitude hits a nerve. This was not the Maryanne I was used to. Condescending, patronizing, a little mean. I missed the cheerful Maryanne. I didn’t like this one. She sounded like she was really ready to rip my head off."
    "She sits down at a different desk, away from me, and continues to pretend to work. She reads over a notepad, yawning every now and then between scribbling nonsense on yellow pages."
    "While trying to keep her eyes open."
    "Eventually, she comes near me to get her coffee, refusing to look at me when she does."

    Shun "I’ll listen if you want to talk."

    Maryanne mad "I don’t want to talk."

    Shun "Then what do you want?"

    Maryanne "I want to just finish my work and be alone."

    Shun "What are you even working on?"

    Maryanne "Just work."

    Shun "Why are you acting like this?"

    Maryanne "Because I am busy and I don’t want to be bothered."

    Shun "Will you please just talk to me?"

    show maryanne coat tangled mad circles with config.say_attribute_transition
    Maryanne pissed "Oh for crying out … can you just go!?"

    Shun "I don’t want to go. I want to know what is bothering you. Ever since you got that package—"

    Maryanne "Are you still worried about that?!"

    Shun "And you almost got run over on your birthday …"

    Maryanne pissed "I got a little drunk! It’s not a big deal! For god’s sake! Shun, your heart is in the right place. But your head is not getting the message."
    Maryanne "I. Do. {em}Not{/em}. Want to talk!"
    Maryanne "It doesn’t matter. None of this matters. None of it."

    return
