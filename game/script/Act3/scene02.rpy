##---------------------------
## Act 3: Maryanne
## Scene 2: Empty Apartment 2
##---------------------------

label a3s2:
    scene bg room_shun with Fade(1,2,1)
    "An hour passes. Then two. We lose track of the time. Maryanne snuggles her soft head under my arm, both of us looking at the sky from my window’s small view."
    "She points to the rollings clouds and says what shape she thinks they look like, and I play with her golden hair, listening to her like she was my favorite show."

    Maryanne "That one looks like a hand, see? But with four fingers. And that one … I don’t know, maybe a bottle if you turn your head a little."

    "My cellphone buzzes. I grab it, knowing Kevin wouldn’t show up again without warning me."

    Dad "{tt}Hey son. Hope midterms went well. Sorry I didn't call this week. Work has been killer. Talk to you later. Love dad.{/tt}"

    "I scowl as I stare at the message. Then turn the phone off."

    show maryanne uncertain hairdown with config.say_attribute_transition
    Maryanne "Who was that?"

    Shun "… No one. Just a spam message."

    show maryanne happy with config.say_attribute_transition
    "She hugs my waist again. I push the painful memories down. It was oddly easy this time. Those bad memories from home were still there, but they seemed … lighter. Like a dull knife, like it couldn’t hurt me like it used to."

    scene bg dorm with bg_change_minutes
    "More time passes. We’re back on the couch, watching TV. Maryanne’s head rests on my tummy and a bowl of popcorn sits on my lap. We share a blanket, and try to share the popcorn … Maryanne eats most of it."

    Shun "Hey … do you know that dance thing? The gala or whatever it was …"

    "Just as the show switches to a commercial, the front door knob rattles, unlocking."

    Hideki "… And they completely changed the timing on her Rolling Thunder! The only reason why anyone used it was because it provided a bit of frame advantage over Twisty’s Diva Slash, but now there’s really no reason to play Fiora any more …"

    Kevin "Dear god, you really are hopeless."

    "My roommates walk inside. Both stop when they see us. Kevin smiles, Hideki does not."

    show kevin smirk crossed open at leftish
    show hideki formal crossed pissed at rightish
    with config.say_attribute_transition
    stop music fadeout config.fade_music
    Kevin "Awww, such a cutesy-wutsey couple! All grown up and dating. Just like Hideki, my baby boy! All grown up and dating too, little Hideki!"

    Hideki "Do. Not. Touch me!"

    Kevin closed "Hey lovebirds, have a good day?"

    Shun "Yeah … pretty good. You?"

    "Hideki scowls at the three of us as Kevin sits down on the other end of the couch."

    Kevin smirk crossed "You alright Goblin-san? Wanna tell us about your day, or do you need space or … drinks? Yeah, want to drink?"

    play music "bickering_loop.mp3"
    Hideki formal armsup argue "No. I’m putting my foot down right now! This is our room! Our living room. Not just Shun’s and not just yours!"

    Kevin frown "Come on dude, don’t be like that. Shun is—"

    Hideki "{em}No!{/em} Do you remember how many times I had to stay out late last year because you put that damn necktie around the goddamn doorknob?! Too many nights were spent sitting in the school library waiting for my roommates to finish— "

    Kevin neutral "Alright, alright, calm down! You’ve had a bad day, they didn’t. No need to make theirs worse. We’re all friend here, no need to—"

    "And their bickering once again escalates. Maryanne looks up at me and we both shrug. I turn up the volume on the TV."
    hide kevin
    hide hideki
    with config.say_attribute_transition
    "After about ten solid minutes of mindless bickering, they’re still going at it. Maryanne sighs as she checks the time."

    show maryanne worried hairdown with config.say_attribute_transition
    Maryanne "… Oh wow! I actually need to go!"

    Shun "You’re leaving?"

    Maryanne neutral "Yeah, something I need to do."

    "She leans in and whispers in my ear."

    Maryanne uncomfortable "Plus it’s … a bit crowded here all of a sudden."

    show maryanne neutral with config.say_attribute_transition
    Shun "Wanna get ice cream?"

    Maryanne happy "Hehe … kinda. But it’s already evening. I gotta go."

    "She kisses my cheek, putting her hand on her neck … covering the mark I left."

    Hideki "In conclusion, that is why it is unfair to let her, or any other female, stay here whenever she—"

    Maryanne "Bye guys. See you around."

    hide maryanne with config.say_attribute_transition
    "Hideki shuts up as Maryanne closes the door."

    show kevin neutral crossed open at leftish
    show hideki formal glasses adjust at rightish
    with config.say_attribute_transition
    Hideki "… Good! She understood my point."

    Kevin "Yeah. I am sure she did. You sure showed her."

    Shun "… Wait, hang on! I almost forgot."

    scene bg hall_dorm_boys with bg_change_seconds
    "I jump off the couch and into my shoes, hurrying after Maryanne."

    Shun "Maryanne, wait! One sec!"

    "She’s just about to go down the stairwell when I catch her attention."

    show maryanne happy hairdown with config.say_attribute_transition
    Shun "I don’t want to forget … did you hear about that dance thing."

    Maryanne ecstatic "Yes! Why?"

    Shun "Well, it’s coming up and … I don’t know a lot about it, but—"

    Maryanne happy "Oh, you’d love it! It’s so much fun! You get to dress up and dance all night and they serve {em}so{/em} much food! I’ve always wanted to go to it!"

    "Her smile beams as she talks about the party and I pick up on that … but something else clicks too. Maryanne’s a grad student but, for some reason, she’s never gone to the gala? I’m sure she had her reasons but … never mind, I tell myself. Focus, don’t overthink things again!"

    Shun "Well … would you like to go with me?"

    Maryanne ecstatic "Yes! Yes yes yes! I’d love to!"

    hide maryanne with config.say_attribute_transition
    "She jumps and hugs me hard enough to hurt my neck a little, quickly letting go as she pulls her phone out."

    show maryanne ecstatic hairdown with config.say_attribute_transition
    Maryanne "I have the perfect dress at home. Maybe my dad can bring it next time he comes to visits. And he is visiting in two weeks too! This is perfect!"

    hide maryanne with config.say_attribute_transition
    "She runs down the stairs, face buried in her screen. She’s so happy that’s she practically skipping down the—"

    show bg hall_dorm_boys with vpunch
    Maryanne "… {w=1}… {w=1}… {w=1}… {w=1}I’m okay!"

    Shun "You sure?"

    Maryanne "Yes. Fine. Just … I might need a new phone."

    scene bg dorm
    show kevin smirk crossed open at leftish
    show hideki formal crossed glare at rightish
    with bg_change_minutes
    "Splayed out on the couch, Kevin watches the TV and finishes what was left of our popcorn. Hideki sits with his arms crossed."

    Kevin "So, how did today go?"

    Shun "It went …"

    "Hideki glares at me. I put my smile away."

    Shun "… It went okay."

    show hideki formal crossed pissed with config.say_attribute_transition
    "Hideki growls and gets up."

    Hideki "I’m going to study. No more guests."

    hide hideki with config.say_attribute_transition
    "He slams his door as he slinks away. Nether Kevin or I flinch, both of us very used to that by now."

    Kevin "Details man, details. I’ll share mine for yours, deal?"

    Shun "Um … it was … very good. Very, very good. Let’s leave it at that."

    show kevin smile closed with config.say_attribute_transition
    "Kevin playfully punches my shoulder and calls me The Man. I smile, trying not to wince."

    Shun "Is … Hideki ok?"

    Kevin serious open "Yeah, yeah. I guess. His thing went exactly as I said it would. But his dad is off his case for a while, so there’s that."

    Kevin "He’ll be fine though … I mean, he’ll be “fine”. You know?"

    Kevin smile "Aw, don’t worry about it man. You know how he gets. Let him unwind in his own way, he’ll be back to his lovable … um … his tolerable old self in no time."
    return
