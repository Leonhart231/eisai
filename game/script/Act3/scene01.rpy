##-------------------------
## Act 3: Maryanne
## Scene 1: Empty Apartment
##-------------------------

label a3s1:
    stop music
    scene bg dorm
    show kevin smirk crossed open at center
    show hideki crossed pissed at leftish
    with bg_change_minutes
    "Finally … midterms are over!"
    "The chaos of the last eight days have passed, with one or two remaining tests finishing this afternoon."

    play music "bickering_loop.mp3"
    show hideki at rightish with move
    Hideki "Grrr … where is it?"

    "Everyone is either sleeping in or out exploring town, recharging their batteries in their own way. "

    show hideki at leftish with move
    Hideki "Where did … it … has to … grrrrr!"

    "Campus is quiet again … most of campus anyway."
    "Kevin stands silently with a smirk as Hideki scurries around the room. I quietly skim through my book, trying to ignore him, though Kevin is enjoying Hideki’s usual antics."

    show hideki armsup argue at leftish with hpunch
    Hideki "Dog! Where is my tie?!"

    Kevin frown "What?! What tie? What the hell are you talking about?"

    Hideki "My international klein-blue silk tie with no details on it! It is my favorite tie! It’s my only tie of that specific color! And you stole it!"

    Kevin "Where the hell do you get the nerve man?! I’ve never worn a tie in my life and you know it! Why on earth would I take yours?!"

    Hideki "Don’t you lie to me!"

    Kevin "I’m not lying, you freakish little Goblin! I’ve been here with Shun for like an hour watching a shark visiting Martha’s Vineyard for summer vacation! He’s not bad, I swear, he’s just misunderstood!"

    Hideki crossed pissed "Bleh! A man with taste as bad as you shouldn’t be allowed to watch films!"

    "They have another aggressive staring contest, scowling at each other until one of them blinks."

    Hideki crossed glare "… I am going to iron my suit. When I am done, I need my tie! So give{w=1} it{w=1} back{w=1} {em}Dog{/em}!"

    hide hideki with config.say_attribute_transition
    Kevin "Don’t hold your breath Four-Eyes. I can’t give you what I don’t have!"

    "Hideki slams his door louder than usual. The room falls silent. I try to stay calm as I flashback to the fights my parents had when they were both drunk, the kind that made our neighbors ask me if I was alright the next day."

    Kevin "… Can you believe that? He gets stressed out for his big date and he acts like it’s the end of the world."

    Shun "Y—yeah. He’s got a lot of … wait, date?"

    Kevin serious "Hmm? Date? Um, did I say date?"

    Shun "Hideki has a date? Like, with a person?"

    Kevin "No He … doesn’t. I know I said “date”, but I meant … not-a-date."

    Shun "…"

    Kevin "… Look man, if Hideki finds out I told you he’s going out, he’ll yell at you too."

    Shun "Noted. But still … he shouldn’t just assume you stole his tie and yell at all."

    Kevin smirk "Oh, I totally stole his stupid tie. He can eat a dick."

    Shun "…"

    scene bg dorm
    show kevin neutral crossed open at center
    stop music fadeout config.fade_music
    with bg_change_minutes
    "The movie rolls on, ending with an explosion caused by a gun and a gas canister. Kevin’s attention is divided between the film and little slips of paper that he writes on carefully. The written notes have Kevin’s usual subtlety."
    "“Don’t look like you are looking at her boobs.” “Tell her she looks sensational.” “Be subtle for once.” … Things like that."
    "He ends up writing over two dozen of them before Hideki comes back out … with his klein blue tie in his hand. He throws his blazer at Kevin, covering his face with an abrupt {snd}fhump{/snd}."

    Kevin serious "Ack! Watch it!"

    show hideki formal crossed glare at rightish with config.say_attribute_transition
    play music "bickering_loop.mp3"
    Hideki "I excavated my tie. It had mysteriously fallen behind my computer desk."

    Kevin neutral "See, told you it wasn’t me. Clearly your computer was the culprit."

    Hideki "I swear to whatever Creator Force there is out there that one day … I will end you … but for now … may I borrow your cologne? I … do not … own a bottle."

    Kevin "… Fine. Whatever. It’s in my room, in my sock drawer."

    Hideki "Thank you."

    hide hideki with config.say_attribute_transition
    "Hideki moves into Kevin’s room, promptly gagging as he steps on one of Kevin’s dirty shirts. Kevin uses that distraction to stuff the little pieces of paper into various pockets on Hideki’s black jacket."

    Shun "So … … … a living girl is involved with this?"

    Kevin "Shhh."

    show hideki formal hips bored at rightish with config.say_attribute_transition
    "Hideki hands Kevin a black bottle. I can smell it from where I am. It’s powerful stuff, but not unpleasant."

    Hideki "… Gratitude …"

    Hideki formal crossed glare "New person! I will be gone for the day. Neither of you enter my domicile!"
    Hideki "Dog! I’ll let you know if I need a ride later. You are relieved of that duty for now. But that may change, given … history."

    show kevin smirk
    show hideki formal crossed pissed
    with config.say_attribute_transition
    "Kevin chuckles and Hideki snarls back for a response. Hideki puts his blazer on carefully, buttoning all of the buttons one at a time from bottom to top. Then he stiffly walks out of the dorm. We both hear him angrily grumbling to himself from behind the closed door."

    hide hideki with config.say_attribute_transition
    Shun "So … what is happening?"
    stop music fadeout config.fade_music
    Kevin neutral "I don’t know … it’s kind of a personal for Hideki. I’m not sure if he would want me to run my mouth."

    Shun "…"

    Kevin "…"

    Shun "…" (multiple=2)
    Kevin "…" (multiple=2)

    Shun "… I won’t tell." (multiple=2)
    Kevin smile closed "Okay good, here it goes!" (multiple=2)

    play music "bickering_loop.mp3"
    Kevin smirk open "Hideki’s parents set him up with someone."

    Shun "Like a blind date?!"

    Kevin "Kinda. Uhhh … so … Hideki doesn’t like talk about his old folks. It’s complicated."

    Shun "Complicated how?"

    Kevin serious "… This stays between us, right?"

    Shun "Cross my heart and hope not to die."

    Kevin "…"
    Kevin "Okay, cool … so … Hideki’s parents essentially force him to go out with someone every now and then. Like once a month or something."
    Kevin "If he doesn’t go on the dates, they get real upset. They cut off his allowance, and then threatened to pull him out of school one time."

    Shun "Whoa, really?! Why? That seems extreme."

    Kevin "All I know is his parents have some pull over the Eisai society, and Hideki has a lot of trouble saying “no” to them. Especially when they expect him to do something."
    Kevin "Thankfully, they leave him alone for most of the year. As long as his grades stay up and he stays out of trouble."

    Shun "Oh man. That’s … wait … once a month?"

    Kevin "Yeah, give or a take a week."

    Shun "… Could Hideki be bombing them deliberately? Does he sabotages the dates to spite his parents?"

    Kevin "No, he fucks them up naturally. I’ve seen it once. He’s really that bad."
    Kevin serious crossed open "… Yeeeah, sorry dude. I don’t think I should say more. This is all personal stuff. It involves parents and baggage and … just, a lot."

    "I start to breathe heavily, as the terrifying idea that I might have more in common with Hideki than I thought plants itself in my mind."

    Kevin "I mean … this is what he told me. He could be lying. He could be an escort for lonely women in their 80s."

    Shun "I really didn’t need {em}that{/em} image in my head, Kevin."

    Kevin smile "Look, the point is he’s gone. We have most of the weekend off from his classic attitude. You have plans? I’m thinking … take-out, maybe hit the bar after, what do ya say?"

    Shun "Heh. You know, that actually sounds kind of—"

    "Kevin’s cell phone buzzes. He snatches his phone up lightning fast and reads a text. Then he jumps to his feet."

    Kevin serious "I’m out."

    Shun "… W—what? "

    Kevin smile "I’m out. I’ve been working on this girl for a few weeks and she’s finally got a free night. Sorry man. If you saw her, you’d get it."

    hide kevin with config.say_attribute_transition
    "Kevin grabs the black bottle of cologne, dive-bombs into his room, and slams the door with his usual level of restraint."
    "A minute later, he bolts out of the dorm with a leather jacket under his arm, texting his mystery girl as he hurries outside."
    stop music fadeout config.fade_music
    "I’m left alone in the dorm. A dorm that’s quiet, for once. After the last few hectic weeks of studying, running around from test to test, having people over randomly, it was strange to see home look so empty."

    Shun "… Home?"

    "I run my hands over the worn-in hand-me-down couch. And smile. Finally …"

    play music "decompression_loop.mp3"
    Shun "… This feels like home."

    "The room, this school … once a strange place filled with weird people I barely wanted to know … I’ve gotten used to it. Used to Hideki rambling about math equations. Used to that weird fish smell coming down the hall when Fusao cooks."
    "I know where Kevin hides his snacks and what hours of the night the guards change shifts in case I need to sneak out and buy more. I know what it feels like to have people living with me instead of just dwelling in the same house."
    "Even by myself in my apartment, I feel like I belong …"
    "… Alone in my apartment …"
    stop music fadeout 0
    "… Alone in my apartment?"
    play music "decompression_loop.mp3"
    "Hey wait! I’m alone in my apartment! And I have a girlfriend!"
    "I text Maryanne right away, then I continue to watch what is left of the movie."
    "She responds back in a moment or two."

    Maryanne "{tt}Hey, what’s up?{/tt}"

    Shun "{tt}Guess what?{/tt}"

    Maryanne "{tt}give me a hint?{/tt}"

    Shun "{tt}I’m in my dorm alone and no one is coming back here till night.{/tt}"

    Maryanne "{tt}haha. That’s a really obvious hint.{/tt}"

    Shun "{tt}Can you come over?{/tt}"

    Maryanne "{tt}I'd love to, but I can't. I have some work to do.{/tt}"

    "More work? After midterms are over? What more could she have to do, everyone was relaxing right now."

    Shun "{tt}play hooky{/tt}"

    Maryanne "{tt}You mean again?{/tt}"

    Shun "{tt}Yes!{/tt}"

    "It feels like days pass, but after ten minutes she responds."

    Maryanne "{tt}Give me an hour.{/tt}"

    Shun "{tt}You got it. I’ll see you in an hour.{/tt}"

    "I then text Kevin immediately."

    Shun "{tt}Have fun on your date. I need the room the rest of the day please.{/tt}"

    Kevin "{tt}My man. Google the F spot and you’ll be golden{/tt}"

    Kevin "{tt}*H spot{/tt}"

    Kevin "{tt}*B spot{/tt}"

    Kevin "{tt}u no what I meant.{/tt}"

    stop music
    scene bg dorm_door with bg_change_minutes
    play music "happy_hangout_loop.mp3"
    "I juggle the grocery bags in my arms whilst trying to open the door and maintain my balance."
    scene bg dorm with bg_change_seconds
    "I neatly place all of the ingredients out before getting started. Some of the supplies were bargain-bin grade, the kind that were made for a college student’s budget. But the cheapness allowed me to buy a lot of food."
    "I had the ingredients, the online recipe, and the tools! I was ready to cook!"
    "… I had never done this before. Not for someone I wanted to impress."

    Shun "How hard can this be?"

    scene bg dorm with bg_change_minutes
    "It’s not easy! It’s not easy! Everything happens at once!"
    "Half of the veggies burn and steam fumes all over the kitchen. I can barely see. I rush around, telling myself that I was multitasking and it only looks like struggling."
    "A plate of overcooked vegetables on runny noodles is what I have left. It smells … well, it smells cooked."

    "{em}Knock knock knock{/em}"

    "I check the time. Maryanne is early. Great."

    Shun "Um … just a minute."

    "I sprinkle some salt on the “meal”. I’m sure that’s fine, I think."

    show maryanne uncertain hairdown at center with config.say_attribute_transition
    Maryanne "Hi … did you cook something?"

    Shun "I … tried."

    "Maryanne looks at the … thing … I made for her. She tries to smile, but the burnt smell makes her wince."

    Maryanne neutral "… I’m sure it tastes fine."

    "She bravely picks up a fork and stabs at the most solid thing she can grab."

    Maryanne "See? This … carrot? Looks delicious."

    Shun "That’s a little corncob."

    Maryanne embarrassed blush "… Uh … oh, yeah. I see that now."

    "She puts it in her mouth and chews. Slowly. She nods a few times before gulping."

    Maryanne confused "Mmm. It’s … honestly not that bad. You know, it’s … crunchy."

    Shun "… Take-out?"

    Maryanne noblush happy "Hehe … sure."

    scene bg dorm
    show maryanne happy hairdown at center
    with bg_change_minutes
    stop music fadeout config.fade_music

    "The delivery guy came over fast. Maryanne tries to give me 1000 yen to split the bill and I insist that she keeps her money."
    "By the time we’re both settled down, half of Maryanne’s meal is gone."

    play music "romantic.mp3"
    Shun "You brought your appetite."

    Maryanne ecstatic "I always do. I … might have skipped breakfast this morning. This makes up for it."
    Maryanne happy "I know your noodle-dish-thing didn’t work out. But it was really sweet. Next time, I’ll cook."

    Shun "You can cook?"

    Maryanne ecstatic "Of course! Cooking is a science after all. Like everything else, it just takes a bit of research and some practical application."

    Shun "That’s probably why I screwed up. Since I need help in my science classes. I’m not good at cooking …"

    Maryanne happy "And I say you just need to keep practicing. Dibs!"

    "She calls dibs on the first dumpling and devours half of it in one bite."

    Shun "Where did you learn how to cook?"

    Maryanne "From my grandparents. And my dad. My grandma used to cook for me every day."

    "She sits down, resting her back on my chest a little. I put my hand around her waist as I take my own bite. She doesn’t stop me. In fact … she smiles. "

    menu:
        "Apologize for ruining the meal.":
            Shun "Sorry about the meal again."
            Maryanne "It’s fine! Don’t worry, I told you. I’ll get it next time. I’ll even show you how to cook. You know I’m a good teacher."
        "Ask her about her grandparents.":
            Shun "What are your grandparents like?"
            Maryanne "They are both really sweet. Grandpa’s really funny. And grandma is … kind of quiet but she …"
            show maryanne sad
            "She trails off and looks away from me."
            Maryanne "I’d … rather not talk about home right now."
            Shun "Oh. Okay, if you don’t want to."

    hide maryanne with config.say_attribute_transition
    "She picks up the remote and turns on the TV. Then casually, puts her head on my shoulder."
    "I kiss her forehead. It makes her giggle."
    "A weird game show is on. A flamboyant man with a bowl cut yells at players as they try to open boxes in the correct order, or whatever. I didn’t care. Being {em}here{/em} mattered, right where I was."
    "A “{snd}sproing{/snd}” noise pops out of the TV, as a game show contestant launches off a spring and into the air."

    show maryanne ecstatic hairdown with config.say_attribute_transition
    Maryanne "Hahaha … that was perfect tim—timing! Haha {em}snort{/em}!"

    show maryanne embarrassed blush with config.say_attribute_transition

    "Maryanne quickly covers her nose with both hands and blushes so heavily that her face started glowing."

    Shun "You just snorted like a pig!?"

    Maryanne worried "No! I—I don’t know, I just inhaled too hard!"

    Shun "Haha. That was cute."

    Maryanne "Shut up! I hate when I laugh like that!"

    "She blushes and pouts, annoyed with herself. I kiss her forehead and rub the back of her head. That makes her blush harder."

    Shun "I won’t tell anyone. Promise."

    Maryanne neutral "… Thank you."

    "It gets quiet. The show … boring, it doesn’t matter."

    Maryanne "Do you like this show?"

    Shun "Not really."

    Maryanne uncertain "Do you want to keep watching it?"

    "She rubs my stomach as she asks."

    Shun "I don’t care."

    "I say and my hand goes slightly lower."
    "She sits up and her hand falls on my lap, exploring down there."
    show maryanne happy with config.say_attribute_transition
    "My hand gently cups her ample chest. She sighs softly, and keeps touching me. She is also gentle, but her grip gets harder the more she enjoys what I do to her."

    "I gently brush my lips across hers. She reciprocates, and pushes herself on me."

    scene bg black with bg_change_minutes
    "I kiss her neck … her weak spot, which I learned … and a few soft moans pop out of her mouth as she pushes on me more eagerly than a moment ago."
    "She started rubbing my waist, which prompts me to be more daring. After a few moments, Maryanne pushes me off her, her cheeks red as a rose."

    scene bg dorm
    show maryanne happy hairdown blush at center
    with bg_change_minutes
    Maryanne "…"

    "She says nothing, expecting me to make the move. This was another thing she taught me, a lesson I learned very quickly."

    Shun "Let’s go to my room."

    "She bites her lip softly and, without looking at me, stands up. I help her to her feet and guide her into my bedroom without turning the TV off."
    return
