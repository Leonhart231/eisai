##--------------------
## Act 3: Maryanne
## Scene 11: Countdown
##--------------------

label a3s11:
    stop music
    scene bg takuya_bridge night with bg_change_days
    play music "contemplative_loop.mp3"
    "I walk into Takuya. Thinking. Strolling by the side of the river that Maryanne liked to visit. I pick up a rock and throw it at the water."
    "It skips once, then sinks. I watch the ripples and listen to the sound of the calming wind."

    Shun "… I don’t know what I am doing."

    "Whatever Maryanne is feeling or thinking, it feels like I am making things worse or not understanding the real problem when I try to help."
    "Maybe … {em}I’m{/em} the problem. Maybe it {em}is{/em} me. Maybe it’s my fault … or she doesn’t want me to help?"
    "I try to stop myself going down that spiral, but it’s hard. It’s easy to be down on myself. Like I was back in high school. I thought being miserable was normal. Now that I’m here and I know things can be better, I know that’s not true."
    "But that black pit is still there, still very able to pull me in. I thought it was gone. It is just waiting for me to come back."

    scene bg takuya_bridge with bg_change_hours
    "By the time the sun is finally up, I am so tired and hungry that I can barely stand. I leave the lake and go back to the dorm."

    scene bg dorm with bg_change_minutes
    "Kevin says nothing as I walk past him. I go to my room and lay down, needing more rest."

    scene bg room_shun night with bg_change_hours
    "Hours later, I wake up from my nap. I don’t know what time it is. I don’t care."
    scene bg dorm with bg_change_seconds
    "Kevin again says nothing as I step out of my room. I am still wearing my clothes from last night. I grab something small from the fridge, devour it quickly without talking, and start up the shower."
    scene bg dorm with bg_change_minutes
    "I barely glance at Kevin as I sit down next to him. I know I’m in the black pit again. I know I’m letting those bad thoughts mess with my head. I don’t care right now."

    show kevin crossed open serious with config.say_attribute_transition
    Kevin "… Rough night."

    Shun "Yeah."

    Kevin "Is she okay?"

    Shun "… I don’t know. She got scared. I don’t know what else I can do."

    Kevin frown "You could … uh … well … yeah, I don’t know either."

    Shun "Maybe this isn’t going to work out."

    Kevin frown "Whoa, whoa, whoa, back up there little buddy. You’re thinking of ending it?"

    Shun "No! Of course not. It’s just … maybe this is a sign. Maybe it’s not going to work. I don’t know, this is … it’s hard man."

    Kevin serious "Uh, yeah, relationships are. But like …"

    scene bg dorm
    show hideki crossed glare at rightish
    show kevin crossed open serious at leftish
    with hpunch
    stop music fadeout config.fade_music
    "{em}slam{/em}"
    "Hideki’s door flies open, slamming against the wall hard enough to crack the paint. He marches over to me like he’s about to punch me, thankfully stopping right in front of me. I still flinch."

    Shun "Jesus Hideki, you almost gave me a heart attack!"

    play music "bickering_loop.mp3"
    Hideki "Implying you have a heart … what is your problem, you pathetic little insect!"

    Kevin "Hey back off man, he’s—"

    Hideki armsup argue "He’s the one backing off!  That is what {em}he{/em} is doing and that is what {em}you{/em} will do right now while I am talking!"

    Kevin "…"

    Shun "Hideki, I really don’t have the patience for this. I am tired, I haven’t eaten in maybe twelve hours. Get a clue!"

    Hideki crossed pissed "I have! I have found plenty of clues! I, of all people, know when something is not adding up! And you, whelp, are missing the most obvious of conclusions."

    Kevin mad crossed open "What the hell are you talking about? This isn’t a math problem! It’s a relationship. They’re stressful, duh!"

    Hideki armsup argue "But the stress is supposed to be worth it in the end, correct? That is the point of it all! Temporary issues do not create an overall dysfunction. It is the greater yield of positives that must be the focus of all profitable endeavors!"

    Shun "…"

    Kevin "…"

    Kevin "… The fuck are you talking about dude?"

    Hideki crossed glare "{em}sigh{/em} Morons. Once again I am forced to leave my sanctuary to straighten you out. So, for your sakes, I will speak as if I also had a fourth grade reading level."
    Hideki chin think "Shun … if this is the worst it’s been so far, then that is not that bad. You do not know what to do. Okay. Fair enough. But that doesn’t mean it’s a lost cause."
    Hideki crossed pissed "All evidence points to you and this girl being … good for each other."
    Hideki "And a bad time here and there amidst an otherwise decent relationship is not cause of alarm. It is normal."

    "He glances at Kevin as he says that last part … well, he glares angrily at Kevin as he says it."

    Hideki hips bored "You are not about to let your own shortcomings become failings, are you? Will you let this become a problem you caused? A bad memory that you yourself will blame yourself forever?"

    stop music fadeout config.fade_music
    Shun "Y—yeah … okay, sure but … what do I do? This doesn’t feel like something I can fix."

    Hideki crossed pissed "Again you disappoint! What makes you think there is something to fix? This might just be who she is."

    show hideki glasses adjust with config.say_attribute_transition
    "Hideki fixes his glasses and grunts, his anger subsiding for a moment."

    play music "contemplative_loop.mp3"
    Hideki "I … am aware that … I am … difficult. I know what I am. But I don’t feel ashamed of it. I don’t need to be. You two are … grrr, you are horrible imbeciles but you’re {em}my{/em} horrible imbeciles. And you don’t make me feel like … you don’t …"

    show kevin neutral with config.say_attribute_transition
    Hideki "{size=-5}… You don’t treat me like I’m wrong for being me. You … both … put up with me.{/size}"
    Hideki armsup argue "As for Maryanne … she might be the same way. You don’t know and I do not know and neither does this idiot."

    Kevin serious "Up yours!"

    Hideki crossed pissed "Bite me, dullard! This female might just need someone to listen to her. Or at the very least, she needs to know someone will be there regardless of what she is experiencing. But you know where you will {em}not{/em} find the answer Shun?"
    Hideki "Here. In this dorm."
    Hideki armsup argue "Get! Out! Go find her and figure out if she is also worth putting up with. For god sakes, stop being such a pushover."

    "There is a very weird feeling that comes when a hard truth is thrown at you in a time of need. Especially when you thought the person didn’t like you in the first place. It is bittersweet, difficult to swallow. Higa-sensei conjured up that same feeling back home before I left."
    "I felt so lost back then and simultaneously insisted that I didn’t need anyone else’s help. I remember being mad and testy and confused … and I was acting very similar to the way Maryanne is acting now. And how I was feeling just before Hideki yelled at me."

    Shun "… You’re right. I’m going to go try something. Thank you."

    Hideki glasses adjust "Thank me {em}after{/em} you have fixed your own mess."

    stop music
    scene bg room_shun night with bg_change_seconds
    play music "decompression_loop.mp3"
    "I hurry to my room to grab my keys, motivated to find her."

    Kevin "So … what happened to all that stuff about dating being stupid?"

    Hideki "It {em}is{/em} stupid. But even the most logical and reasonable minds must make room for … error."

    Kevin "Is that your way of admitting that you made a mistake about something?"

    Hideki "Blow it out your ass!"

    scene bg dorm
    show kevin neutral open crossed at leftish
    show hideki armsup argue at rightish
    with bg_change_seconds
    Shun "Bye guys. Don’t wait up."

    Kevin smile "Good luck."

    Hideki glasses adjust "Being wrong on a very rare occasion is not a sin, not a crime against humanity … it only serves as a lesson to improve oneself as—"

    Shun "Yeah, bye Hideki. Thanks again."

    return
