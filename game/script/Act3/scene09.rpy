##-----------------------
## Act 3: Maryanne
## Scene 9: Vanishing Act
##-----------------------

label a3s9:
    scene bg room_shun night with bg_change_days
    stop music fadeout config.fade_music
    play sound "194365_macif_door-knocking-angry"
    "There’s a loud banging on my door. It’s dark. I roll over and hear Kevin’s voice through the shadows."

    Kevin "Shun! Wake up man."

    "I rub my eyes, not even bothering to check the time. Kevin opens the door just slightly."

    show kevin frown sides open with config.say_attribute_transition
    Kevin "It’s 2am, sorry dude. But is Maryanne here?"

    Shun "What? No, what … what’s going on?"

    play music "very_sad.mp3"
    Kevin "She’s not with you?"

    "My stomach sinks like a rock."

    Shun "No … Kevin, what happened?"

    Kevin frown "Shit … okay, don’t freak out dude."
    Kevin "Her dad called me about an hour ago. She’s not answering her phone and he hasn’t heard from her in like a few days. Shione said she didn’t come home last night. She’s not in the lab, the classrooms … nowhere."

    "I grab my cell phone and call her right away. It rings five times before going to voicemail."

    Shun "She’s probably just asleep or something. I don’t know. Maybe she lost her phone?"

    Kevin serious "Yeah you’re right … that’s probably it. All of those things, at the same time. That’s all."

    "I try not to overthink, but … how could I not? I think about all of the places she could be and how she’s been acting lately, how she’s seemed … distant."

    Shun "Okay, okay, just … stay calm. Maybe she went out for a walk? To go eat somewhere …"

    scene bg shodo_beach sunset
    show maryanne crying nonecklace circles hairdown
    show flashback
    with bg_change_seconds
    Maryanne "{flashback}One day, my mom came to my school in the middle of the day. She pulled me out of class, took me in the hallway alone … and she hugged me. {em}Reeeally{/em} tightly.{/flashback}"
    Maryanne "{flashback}She said she loved me and that she always would. And …{/flashback}"
    Maryanne "{flashback}… That was it. She just … she …{/flashback}"
    Maryanne worried "{flashback}… Didn’t come home.{/flashback}"

    scene bg room_shun night
    show kevin frown sides open
    with bg_change_seconds
    Kevin "What’s the matter? You look like you saw a ghost."

    Shun "… I think … forget it. I’m just … thinking too much."

    Kevin "Shun, if there’s anything you know, you gotta tell us man. Is there anywhere she might be? Anywhere you might remember?"

    "{em}Ring{/em}"

    stop music fadeout 0
    "My phone rings. It’s Maryanne."

    Shun "Oh thank god. It’s her."

    show kevin neutral with config.say_attribute_transition
    "Kevin leaps onto my bed and sits next to me."

    Maryanne "Hello?"

    Shun "… Maryanne? You’re okay?"

    Maryanne "… Of course I am okay. Why do you sound … what is it?"

    Shun "N—nothing. Your dad called us. He said you didn’t answer."

    Maryanne "O—oh. Yeah. I left my phone in my room."

    Shun "Where were you?"

    Maryanne "…"

    Shun "Uh, were you working again? Is that it?"

    Maryanne "No!"

    "She barks angrily at me."

    show kevin frown with config.say_attribute_transition
    Maryanne "I was just going for a walk. That’s {em}it{/em}! I went by the riverside. I don’t need to check in with you every time I go somewhere!"

    Shun "I’m not saying you need to! We were just worried? No one—"

    Maryanne "Who is “we”?"

    "I look at Kevin. He nods twice."

    Shun "Kevin, me, your dad."

    Maryanne "… Goodnight Shun."

    "She hangs up. Kevin hisses with his teeth, able to hear the conversation."

    Kevin frown sides open "That could have gone better."
    Kevin serious "Sorry I woke you man. Her dad got worried and that freaked me out and then I started overthinking too … sorry."

    hide kevin with config.say_attribute_transition
    "Put my phone down and turn my back to Kevin. Mildly upset with him causing this mini-fight, I go back to sleep without another word."

    "The door shuts. I lay back down in bed."

    return
