##----------------------------
## Act 3: Maryanne
## Scene 13b: Breakdown Choice
##----------------------------

label a3s13b:
    play music "very_sad.mp3"
    Shun "How could you say that? It matters to me! How can you not act like you don’t care?!"

    Maryanne "If it matters to you, then you’ll take a hint and leave me alone!"

    "The clock on the wall ticks again. Her words slam on me like a ton of bricks."
    "I don’t have the energy to say anything else. I wanted her to come with me. I wanted her to be okay. But Maryanne made it very clear she doesn’t want my help."
    hide maryanne with config.say_attribute_transition
    "I wait another moment. Then stand up without saying a word. I go to the door …"

    Maryanne "Shun …"

    "She starts to say something. I wait for her to say it. To say anything. I wait for a minute. Then another."
    "But she says nothing."
    "I leave, closing the door behind me."

    scene bg hall_dorm_boys with bg_change_minutes
    "I could hear some of the other students in the other dorms playing music and laughing. Some with their girlfriends."
    "It makes my stomach turn."

    scene bg dorm with bg_change_seconds

    "Kevin is there waiting for me. He looks worried. And tired."

    show kevin serious sides open with config.say_attribute_transition
    Kevin "… Maryanne didn’t leave the lab, did she?"

    Shun "No. She’s still there. She told me to just leave."

    Kevin frown "Yeah … sorry man. I figured she’d do that."

    Shun "Is she gonna be okay?"

    Kevin "Do you want me to tell you the truth or what you want to hear?"

    "That’s not a good response."

    Kevin serious "We just have to be patient. And be here for her when she is ready."
    Kevin "I guess."
    Kevin "When she wants to talk, she’ll talk. A lot of people get defensive when they have problems."

    Shun "Yeah … I know …"

    "He pats my shoulder, trying to comfort me."

    Kevin "Wanna talk about it? Play video games? Drink a bit?"

    Shun "Not tonight Kevin."

    "Kevin nods and says nothing more. I go to my room."

    return
