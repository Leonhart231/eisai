##-------------------------
## Act 3: Maryanne
## Scene 6: Birthday Gift 1
##-------------------------

label a3s6:
    stop music
    scene bg takuya with bg_change_hours
    play music "happy_hangout_loop.mp3"
    "I rush into Takuya, my hand in my pocket to make absolutely sure my wallet was with me. I figured some quick window shopping now, buy the gift I wanted later … when I had more cash to spare. In three days. My parents were scheduled to send me more money soon."
    "I hadn’t told my parents that I had a girlfriend. Or very much else, not in a while. Or my old classmates, who all went to their own colleges. Not even Higa-sensei now that I think about it … but he was probably busy taking care of his new daughter."
    "I shrug those thoughts off and focus on my special mission. Objective “save myself from my own stupidity”."

    scene bg mall with bg_change_minutes
    "I walk through the commercial part of town, past the movie theater where Maryanne and I had our first date, hoping to get at least an idea of what to buy her before the day ends. Maybe something sentimental of our first day out, I think …"
    "An hour flies by. I look through a dozen and spent more time alone in the mall than I had since 10th grade. I found plenty of decent would-be presents, but not great ones. Not ones that would really leave an impression."
    "I mean, there were plenty of great gifts but … most were too expensive."
    "On my way back, I see a small jewelry vendor near the end of the street. So small that I must have passed it a few times by now without noticing it."
    "A small old woman with tiny eyes sit by the booth, with a red scarf over her head. She punches numbers into a register with one hand and holds long receipts with the other, mumbling to herself as she works. Ginger and vinegar emanate off her."

    Shun "E—excuse me?"

    "She looks up, squinting like a mole. She pulls down a pair of comically big glasses from her head, which magnify her eyes to the size of golf balls."

    Woman "Hello young man. May I help you?"

    Shun "Uh, yes. I am looking for something. A gift. For my g—girlfriend."

    Woman "Oooh, young love. She is lucky to have such a handsome young gentleman. I remember when I was young … there were so many suitors back in my day. So much happiness!"

    Woman "A-hem … yes … I have many things to choose from and I am sure you will find what you are looking for. Let me know if you see anything you like."

    "I look over her tiny cart. There are knickknacks sprawled here and there, of all shapes and sizes. It doesn’t look very organized. Some picture frames are crooked and some earrings come in packs of threes."

    "Among the odd knickknacks and bubbles, I see a stylish bracelet with gold link. It’s shiny and well kept compared to the other wares, and looks a bit like Maryanne’s necklace. The center bares the letter “A”, without looking gaudy."

    Woman "Ah! I see you like the custom bracelets. Those come in gold or silver. And you can get the size adjusted to your liking."

    Shun "I think she’d like that. Do you have one in gold, with the Letter “M” on it?"

    Woman "Hmmm … no, this is the last gold one we have here. But I can send out for one just like that. It should arrive in about five … maybe six days."

    Shun "Great! … … … So … h—how much?"

    Woman "They come at 6500 yen."

    Shun "O—oh."

    "A tumbleweed blows through my wallet."

    Woman "… Buuut … maybe I can go for 5000."

    "I think I hear that tumbleweed laughing at my poverty."

    Woman "And you don’t have to pay until it arrives."

    "The tumbleweed shuts up."

    Shun "Yes! That would be great! I am getting some money in a few days just before her birthday!"

    Woman "Splendid! Oh, she is going to be so happy when she sees it. My, my, she’s going to think she’s so lucky to have such a thoughtful gift. I wish someone bought this for me when I was young. Among the other gifts, o—hohoho."

    "She rambles as she passes me an order form. I fill out everything as neatly as I can, nodding as she talks about herself."
    "I thank her several times and she tells me I look like her husband used to look. I mention I have a class to get to and leave as fast as I can."

    return
