##----------------
## Act 3: Maryanne
## Scene 14: Lost
##----------------

label a3s14:

    scene bg room_shun overcast with bg_change_days
    stop music fadeout config.fade_music
    call weather(1, 2)
    "A thud wakes me up. I groggily pull my head off my pillow. The clock says 8:04 am, though it takes me a minute to see it through my crusty eyes. The sky is heavy with pouring rain; it makes my room darker than usual."
    "I groan and close my eyes again. Then I hear someone yelling in the living room. It’s not Kevin."

    scene bg dorm with bg_change_seconds
    "I step out and see Mr. Sawyer sitting on our couch. Kevin is by his side. Neither of them look like they have gotten any sleep. Insomnia seemed to be going around."

    show george panic at rightish
    show kevin serious sides open at leftish
    with config.say_attribute_transition
    Shun "Mr. Sawyer? What are you—"

    George "Is she here!?"

    play music "very_sad_loop.mp3"
    "My heart sinks to my stomach."

    Shun "… Wh—what happened?"

    George "Is my daughter with you?!"

    Shun "N—no sir. She’s … what happened?"

    Kevin frown "… Uh, Maryanne is … she’s …"

    Shun "Kevin! Where is she?"

    Kevin "… We don’t know."

    "Her father pulls a fresh cigarette out from his back pocket, dropping it before it can touch his lips. His trembling hands struggle to pick it off the ground."

    George "I caught an early flight in. Came to campus to surprise her. I was … I was going to surprise her."

    "Kevin moves in close to me and whispers so Mr. Sawyer doesn’t hear."

    Kevin "She won’t answer her phone. Facilities went to her room but there was no answer. She is not in the lab either. We’ve been trying to get in touch with her all morning."

    Shun "Did … you call the police?"

    Kevin "Not yet. We wanted to see if she was with you first."
    Kevin "Mr. Sawyer, now might be a good time to call someone professional."

    "Mr. Sawyer nods, the unlit cigarette between his teeth. His trembling hands cause him to drop his lighter next."

    George "I knew she’s been upset. I didn’t … I didn’t …"

    Kevin closed "Do you have any idea where she might be?"

    Shun "I don’t … I don’t know. She was really upset last—"

    "I have a sudden flashback to our first date and remember what she told me."

    Shun "… There is one place. Maybe."

    Kevin open "Really?"

    George "Oh thank god. Where?"

    Shun "There’s a river in town. She goes there when she is upset. I can go check. It’s a short walk from here. I’ll call you if I find her."

    Kevin "Okay. I’ll … take care of the old ma—, uh, I mean, our guest. I’ll calm him down a bit. We’re gonna call the cops if we don’t hear from you."

    George "Please hurry. I just need to know that’s okay. Geez, I can’t worry like this."

    "I jump into my shoes, I throw my coat on … and feel something poke my side. Maryanne’s birthday gift is still here with me. The wrapping paper has been crumpled up badly, but it is still intact."
    "I take it with me and hurry outside."

    scene bg takuya_bridge rain with bg_change_minutes
    call weather(5, 10)
    "The wind is strong enough to make my umbrella near useless. And the rain pours down in gusty waves, some heavy enough to push me back a few feet. Eventually make it to the bridge over the river."
    "No one is there."
    "I look around but see that no one is by the bank. The water is too high to safely stand on the grass, and the wind makes the water look angry. Dangerous."

    Shun "MARYANNE!!!"

    "I yell. A thunderclap answers back."

    Maryanne "Go away!"

    "I hear her clear as a bell, even though the wind, like she’s right next to me. I look behind me, to my right, to my left, but she is not there."
    "I grab the railing and push my head far over the side of the bridge as I can. Under the bridge, I see a patch of matted grass and a closed pink umbrella resting on the stone wall."
    "I hurry down around the bend, trudging through the mud."
    "Maryanne is there, sitting under the stone arc. Her knees are pressed against her chest, her nose pokes over her knees, her eyes fixated on the aggressive running water. Her clothes are wet but clean, like she’s been there before the dirt turned to mud."

    Shun "Jesus, Maryanne. You scared the hell out of us."

    "I step under the bridge. Everything gets darker. The world is quieter under the stone archways. I hurry next to her to get out of the rain, and send a quick text to Kevin, letting him know I found her."
    "She inches away as I get close. She looks mad. That was … a kind of improvement over being sad … I guess?"

    show maryanne circles tangled mad with config.say_attribute_transition
    Shun "Your dad is here."

    Maryanne "I don’t want him to see me like this."

    "She {em}sounds{/em} mad too. Her eyebrow is crumpled up and once again she does not look at me."

    Shun "He looked really worried."

    Maryanne "…"
    Maryanne worried "Why can’t you just leave me alone?"

    Shun "I don’t want to."

    return
