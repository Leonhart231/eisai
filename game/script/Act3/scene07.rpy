##----------------------
## Act 3: Maryanne
## Scene 7: Overzealous
##----------------------

label a3s7:
    scene bg inou sunset with bg_change_minutes
    "I get back to Inou just as the sun begins to fall. The order form safely tucked away in my wallet, my hand in my pocket to make sure I don’t lose it."
    "The look on Maryanne’s face when she’d get the bracelet, what she’d say to me, if she’d let me put it on her wrist for her … I get lost in all the ways this could go right, for once."
    "As I think of the best way to give her the gift, speak of the devil … a head of shimmering yellow hair moves across campus coming my way."
    "Maryanne walks slowly, one hand on her elbow, a forced smile under tired eyes. I noticed an envelope in her hand but say nothing of it."

    show maryanne happy circles with config.say_attribute_transition
    play music "romantic.mp3"
    Shun "Hey … um—"

    Maryanne neutral "Hi. Shun, hi. I … um …"
    Maryanne happy "Where’d you come from?"

    Shun "Oh, just … out for a walk."

    Maryanne "Oh … the same."
    Maryanne uncertain "You’re … not mad at me are you?"

    Shun "Mad? No, why would I be?"

    Maryanne happy "I just wanted to ask. I’m not mad at you either, okay?"

    Shun "I … o—okay. I didn’t think you were. I just thought you needed …"

    Maryanne "I’m fine."
    Maryanne "Uh … congratulations on your test again. That was … you know, great. I’m happy you … did it."
    Maryanne "I’m … really happy … I could help."

    "She runs her hand over her hair, straightening out a few split ends. She doesn’t look me in the eye for long, darting from her shoes to my face every few seconds."

    Shun "Uh … are you … I mean, you’re okay but … are you okay?"

    Maryanne worried "I’m tired. I needed to … rest … and just … I don’t want you to worry when I … need “me” time. Okay?"

    Shun "Okay. Sure, no problem."

    Maryanne happy "I’ll call you later."

    hide maryanne with config.say_attribute_transition
    "She hurries by me, heading towards the mail office, rubbing her arms like she was freezing."

    scene bg dorm with bg_change_minutes
    stop music fadeout config.fade_music
    "Kevin is on the phone. I nod to him and he waves, speaking English to someone on the other line."
    "A large stack of papers rests on our table, in between Kevin’s laptop and a pile of restaurant menus. An empty coffee mug sits on top of a coaster along with a bottle of whiskey."

    scene bg room_shun sunset with bg_change_seconds
    "I quickly put the invoice in my room, safely hiding in my desk."

    scene bg dorm with bg_change_seconds
    "I go back out to the living room, smiling ear-to-ear as I keep thinking of Maryanne’s reaction."
    "Kevin ends the call just as I sit down."

    show kevin smile crossed open with config.say_attribute_transition
    Kevin "Mom says hi."

    Shun "I haven’t met her."

    Kevin smirk "Yeah, but … she still says hi. Geez, rude much?"

    Shun "… What’s all this on the table?"

    "I gesture towards the mess in front of me. Kevin rubs his hands together and smiles villainous as he pats the paper stack."

    play music "bickering_loop.mp3"
    Kevin smile "Well … after my little fiasco this morning, Old Man Okada tried to push my buttons. The fossil says I’m not applying myself as much as I should, so he gave me a 30 page paper to finish."

    Shun "Sounds familiar. When is the paper due?"

    Kevin closed "He didn’t say. I think he wants to spring a surprise deadline on me later. So I’m going to finish it all tonight! Oh man, I can just see the look on his wrinkly old face. “Wha—what?! You did it already!?” “Yes sir, I guess I really am better than everyone!” Hehehehe."

    "Kevin gleams a smile dripping with spite and shakes his balled up hand to the ceiling."

    Shun "You’re gonna do 30 pages in one night? And you’re drinking?"

    Kevin smile open "Yes and yes!"

    Shun "This will not go well at all. You realize that, don’t you?"

    Kevin "What I realize is my life is awesome and I do awesome shit. As often as possible. This will be one of those things."
    Kevin "I got everything needed for the classic college all-nighter. First, a shot of whiskey to relax. Insta-coffee later. I have a wide variety of late-night take-out numbers so I do. Not. Move! From this spot!"
    Kevin "I even got one of those energy drinks for backup just in case. Hideki knows I’m busy, my phone is on silent mode … and most importantly, I have a thirst for vengeance in my heart!"

    "He slides off the couch, hunches over his laptop. He cracks his knuckles, puts his fingers on the keyboard, and clicks the buttons with deliberate, focused rigor."

    Kevin smirk "Page … one. Let the game begin."

    "I decide not to tell him he is insane, knowing very well how futile such an action would be. Instead, I stare at him judgingly the way a younger brother would to an obnoxious older sibling during yet another impulsive moment."

    Shun "Can I watch something?"

    Kevin neutral "Sure, sure, just keep the volume low. I am crafting art. An Okada-bomb … and my target is his soul!"

    hide kevin with config.say_attribute_transition
    "I pick up the remote and flipped through channels as Kevin hammers out sentences. I glance over at the computer and scoff in amazement as I see him write at the header of the essay “This is why I’m right — Working Title”."

    scene bg dorm with bg_change_minutes
    "{snd}ring ring{/snd}"
    stop music fadeout config.fade_music
    "The ringing of my phone jolts me from an accidental nap. I’m on the couch, though it takes me a minute to realize where I am."
    "Kevin is still typing away, still smiling manically, still greatly motivated by the hopeful shock of an elderly man, who was only trying to teach him a lesson."
    "I rub my eyes and pull my phone out of my pocket. Maryanne is calling, and it’s almost midnight."

    Shun "Hello?"

    play music "romantic.mp3"
    Maryanne "H—hi."

    Shun "Hey."

    Maryanne "… D—do … do you wanna come over? For a while?"

    "Her offer would be more appealing if her voice didn’t sound so … nervous, like she is worried that I would not want to come."

    Shun "Sure. Yeah, uh, give me a few."

    Maryanne "Grea—great! I’ll … uh, yeah. Good. Okay. I’ll … I’ll be here."

    Shun "Are you feeling better?"

    "{snd}click{/snd}"
    "She hangs up. And I get worried again."

    Shun "Hmm … well, later Kevin. I’m going to see Maryanne."

    show kevin smile crossed open with config.say_attribute_transition
    play music "bickering_loop.mp3"
    Kevin "High-five!"

    "I raise my hand, but Kevin does not. His eyes are glued to his screen, still typing out the 30 page monster."

    Kevin "Must … finish … revenge-essay."

    Shun "You’re hopeless."

    Kevin serious "Hopeless? Far from it! Hope is a suckers game, for those who need to ask God for help!"

    "He gives another hearty shake to the heavens, costing him two seconds of typing … which he makes up for with a blitz of finger movements that blur over the keyboard."

    Shun "Alright. Well … bye. Good luck with your hysteria."

    scene bg hall_dorm_boys with bg_change_seconds
    "As I close the door, Kevin cackles like a super-villain, a troubling sound that’s so loud I can hear even as I close the door."

    Hideki "SHUT THE HELL UP!!!!!"

    "The laughter stops abruptly."

    scene bg hall_dorm_girls with bg_change_minutes
    stop music fadeout config.fade_music
    "I knock on Maryanne’s door twice. I don’t hear anyone inside. Shione must be out on another trip or whatever she’s always running off too."
    "The door is unlocked for me … and the dorm is empty. Very quiet. I hesitate, reminded of horror movies and how this usually turns out."

    stop music
    scene bg dorm_maryanne with bg_change_seconds
    play music "romantic.mp3"
    Shun "Hello?"

    show maryanne sad circles with config.say_attribute_transition
    "Maryanne walks out of her room, looking wistful and distant. She walks over to me, not saying a word and barely looking at me."
    "Her hands wrap around my waist. Then a soft ruffle as her head rests on my chest. I can feel her warm bare skin on me."

    Maryanne "I’m sorry I’ve been acting weird."

    menu:
        "It’s okay.":
            $ true_end_available = True
            Shun "It’s … okay."
            "She hugs me as tight as she ever has, tight enough to hurt a little."
            Maryanne "Do you want to stay for a while?"
            scene bg black with bg_change_minutes
            "My lower half answers for me. Maryanne nuzzles in closer."

        "I hadn’t noticed.":
            Shun "I hadn’t noticed …"
            Maryanne "… Yeah you did."
            Maryanne uncomfortable "I’m sorry … I just … get that way … and I … I didn’t mean to …"
            show maryanne sad with config.say_attribute_transition
            "She coughs, trying not to cry. Or perhaps having done too much of that already."
            Maryanne "Do you want to stay for a while?"
            scene bg black with bg_change_minutes

    "I lean in to kiss her. And she kisses back, but harder than I do, pulling me in by grabbing behind my head. I try to be gentle, but Maryanne does not. She touches me with a different energy than usual. She trembles as I run my hands over her hips, as her nails dig into my arms."
    "We move back to her room and quick fall on her bed, our clothes flying everywhere. Even now, as her tan skin reflects the light from the window subtly, as she practically claws at me to get on top of her, her eyes look … different. They are heavy again. Full of worry, of stress."
    "She kisses me again before I can ask her if something is wrong, and puts my hand on her chest to keep me quiet. I know what she is doing. She’s in control and she knows it. My conscience would have stopped me if my body wasn’t calling the shots."
    "She does not let me go, does not detach her lips from mine or her hand from the back of my head. And I get lost in the excitement of touching her. She puts my hand lower, between her bare legs."
    "She grips my skin like she’d fall off the edge of the earth if she lets go."
    "She sighs and pants as we slow down, trembling. She kisses me deeply again and I reciprocate. She hugs me tightly, pushing her head behind mine so I cannot see her eyes."
    "Not knowing what to say, I just hug her back. And lay with her. In silence."

    scene bg room_maryanne night with bg_change_hours
    "…"
    "A rustling wakes me. I open my eyes to a dark room again. A quiet room."
    "I don’t see a second pair of toes next to mine. Nor do I feel weight on the part of the bed where a girl should be. The space next to me is empty, only an indent in the sheets remains."

    Shun "Maryanne?"

    Maryanne "Right here."

    "A voice comes from the darkness. I hear her moving around, but cannot see her."

    Shun "What time is it?"

    Maryanne "3am. Go back to sleep. I was just going to go out for a bit."

    Shun "… Where are you going?"

    Maryanne "Just … out. For a walk. I can’t sleep. Walking helps me sometimes."

    Shun "Let me get dressed. I’ll come with you."

    Maryanne "No!"

    "She barks quickly, like she was … not asking."

    Maryanne "Don’t. I … just need to clear my head."

    Shun "Are you sure you’re okay? We didn’t talk about—"

    Maryanne "I’m fine!"

    Shun "…"

    Maryanne "I just want to be alone. For bit."

    "She opens her door … and leaves. Leaves me alone."
    "I’m tired … I do not push it. And regret that right away but don’t want to argue."
    "Maybe I did something wrong. Maybe I was too nice … or not nice enough?"
    "Maybe I did something to make her need space. Or perhaps I accidentally kicked her when I was asleep."
    "I lay back, my eyelids not closing. And I replay the night, trying to look for any little detail I missed, wondering if it was something I did to push her away."
    "That stupid package, I think. I blame that rotten hunk of folded cardboard for all of the bad things that I’m feeling. I remember the picture of her grandparents. And that letter."
    "I’m sure that’s the reasons … but Maryanne’s silence and dodginess leaves me second guessing."
    "I watch the blinking red digits on her digital clock change from 3:05 … to 3:10 … to 3:35 …"
    "She still doesn’t come back."
    "By 3:45, I have had enough. Crabby and tired, I begrudgingly put my clothes back on, silently cursing whatever was really in that dumb stupid package, and let myself out."

    scene bg inou night with bg_change_minutes
    stop music fadeout config.fade_music
    "I walk through the dead-quiet campus. Not so much as a cricket chirps. The sky is full of moonlight, but no wind blows. The world feels dead!"
    "I grumble my way back to my dorm as a cool breeze slaps my back. Frustration replaces my previous concern as I grow more and more physically exhausted."
    "I look up at the science wing on top of the hill. I think I see a light in the east laboratory. It makes me stop walking for a moment. I keep walking, telling myself that it was just the moon’s reflection on the glass. That’s all it was. Just the moon."

    scene bg dorm_door with bg_change_minutes
    play sound "104533_skyumori_door-open-02"
    "I push my key in very slowly, and open the door as quietly as I possible can. I afraid to wake Hideki or … well, just Hideki."

    scene bg dorm with bg_change_minutes
    "I step into a cloud of smoke that smells like a fruit basket. A laptop glows on the living room table, beaming onto Kevin’s stressed face and his hunched shoulders."
    play music "bickering_loop.mp3"
    "Kevin is still rapidly typing words on his laptop, huffing and puffing a metal stick that lights up as he inhales. One of his eyes twitches as he exhales a thick white cloud."
    "Two empty bottles of emergency energy drinks are by his side on the floor, as well as an empty pot of coffee on the table."

    show kevin laugh sides open with config.say_attribute_transition
    "I walk in. Kevin does not move. He scratches his neck for about ten seconds non-stop, then laughs softly."

    Shun "… Hey Kev … ya’ okay?"

    Kevin "Haha, so, I made a mistake in the topic and didn’t catch it until page twelve, so I had to revise everything everything. … {em}Every{/em}thing. … But I did it … I did it … I did-did-did it."
    Kevin "And I am on page twenty-one now and it’s going good and we ran out of coffee after the ninth cup so I pull out an electronic hookah stick for a good old nicotine fix but it’s {em}noooooot{/em} working!"

    "His smile stretches far enough for it to look painful. I pick up the empty energy bottle and the ingredients tab says it has more sugar than a candy factory."

    Shun "You had only two of these right??"

    Kevin "One more page … one more page … that’s all … hehehhaaa."

    menu:
        "Just walk away.":
            "I back away and go to my room. Kevin’s eyes still glued to his computer. He starts mumbling to himself, and scratching his neck again."
        "Tell him to take a break.":
            Shun "Kevin,  maybe you ought to lay down.I think it is time to take a break. You’ve done enough for now."
            Kevin laugh crossed open "Hahaha, you’d like that wouldn’t you Okada, you wrinkly old prune! No, no, no, no, I am on to you! I’m {em}so{/em} on to you. You’re just trying to get me to stop because you want to win! Haha, hahaha. Not gonna work. No way!"
            "He says without looking at me. His metal stick light up again, and he coughs a little."
            "I put the empty bottle down and go to my room. I’ve fought enough futile battles for one day."

    return
