##-------------------------
## Act 3: Maryanne
## Scene 10: Birthday Night
##-------------------------

label a3s10:
    scene bg room_shun with bg_change_days
    stop music fadeout config.fade_music
    "Friday passes like a bad dream; I don’t remember much about it, but remember I didn’t enjoy myself."
    "Saturday morning comes. I don’t call or text anyone. Nor do I expect someone to call me."
    "I sleep in."
    play music "contemplative_loop.mp3"
    "Noon passes by. Then 1. I notice that the tiny specks of dried paint on the ceiling look like faces if I squint my eyes the right way."
    "I look at my phone. Nothing. I let it fall to the ground, fed up with the waiting. Being shut out was worse than being alone. Having no friends wasn’t as bad as having a girlfriend who was mad at you."
    "Maybe I won’t even go to her birthday, I think. That will show her … somehow. That’ll make her wish she wasn’t mad at me. Maybe if {em}I{/em} go missing for a few hours, she’ll be worried about {em}me{/em}. Who needs her? …"
    "{snd}Ring-ring!{/snd}"
    "I jump out of bed and snatch the phone before the first ring stops."

    Shun "Hello?"

    Anon "Is this Mr. Shun Takamine?"

    "An old woman says my name and my heart sinks."

    Shun "Yes. It is he."

    Anon "Hello sir! I am pleased to inform you that your order is waiting for you to pick up. The bracelet, gold and red. Remember?"

    Shun "Oh. Oh! Ye—yeah. Thank you for calling me."

    Anon "I’ll hold onto it for you. You can come pick it up anytime you’d like."

    "The lady rambles on about the quality of the bracelet. I pretend to listen until she hangs up."
    "Great … now I {em}have{/em} to go to this lame party."
    "Hanging upside-down over my bed, looking under my desk as my hair dangles downward. My frown is literally turned upside-down. I get a smirk out of that. I wish Maryanne was next to me so I could tell her that stupid joke."
    "I sigh again and pull myself up. Even if I am frustrated with the girl … I miss her. And I want to see her again."
    "I find the order receipt in my desk. And double check my bank account on my phone before grabbing the debit card dad gave me."
    "Then I go. Still mad. Still lonely."

    scene bg dorm
    show kevin smile sides open
    with bg_change_hours
    Kevin "Tonight will be fun. Sushi, sake, gifts, friends. The works. It’ll be great."

    "I wait for him to finish getting ready. In silence. He knows I am upset. Worried. He knows nothing can undo the drama of the past week."

    Kevin "I think you both just need to kiss and make up. You know, move past it. Pretend it never happened and just have fun ag—"

    Shun "Kevin."
    Shun "Please."

    Kevin crossed serious "… Sorry."

    Kevin "… At least promise you will try to have a fun night. For her? And me too! I’ve been looking forward to this. And Shione? Forget it! You know how much she likes seafood."

    Shun "Fine, fine. I’ll be good. I’m not holding a grudge. I’m just … not happy."

    Kevin neutral "Okay, fair enough. Maybe that will change tonight."

    "Kevin grabs a large envelope waiting for him on top of the TV."

    Kevin "Ready when you are."

    "Maryanne’s gift is in a box under my arm, wrapped in a nice bow. The old lady took the liberty of gift wrapping it for me."

    Kevin smile "Chin up Champ. This too shall pass. And after it’s over, you’ll be fine. I read that somewhere."

    Kevin neutral "HIDEKI! WE’RE GOING OUT! DON’T STAY UP TOO LATE!!"

    Hideki "GO OUT TO HELL!!! AND DON’T COME BACK!!!"

    "Hideki yells through his door. Then opens his door and slams it shut again, adding flair to his retort."

    Kevin smile "Don’t be surprised if I get drunk tonight. I need it."

    Shun "I’m not carrying you home."

    Kevin smirk "You couldn’t if you wanted to."

    Shun "Exactly. So keep that in mind."

    show kevin smile crossed open with config.say_attribute_transition
    "Kevin holds the door open for me and makes a stupid face. It does not cheer me up."

    scene bg birthday_sushi
    show kevin open crossed smirk at rightish
    with bg_change_minutes
    stop music fadeout config.fade_music
    Waitress "Hello. Table for two?"

    Kevin "No thanks sweetie. We’re here for the party. Should be four total."

    Waitress "Right this way. Your other guests have arrived already."

    Kevin laugh "Great! Let the good times roll."

    Shun "Yeah, great."

    "The waitress guides us towards the back, to the more secluded side of the restaurant."

    stop music
    scene bg birthday_sushi
    show kevin open crossed laugh at rightish
    show shione hips excited
    with bg_change_minutes
    play music "happy_hangout_loop.mp3"
    Shione "Sharkie! You’re here!"

    "Shione jumps out of her seat when she sees Kevin. As bubbly and joyful as ever, she pulls him down next to her, smiling brightly as her green hair flutters about."
    "Kevin pats her head, and Shione immediately gets mad at that and starts waving her arms around while Kevin laughs at her."

    Shione finger pompous "Don’t touch my hair! Just cause you’re tall and I’m … not tall, that doesn’t mean I can have my head touched!"

    show kevin smirk with config.say_attribute_transition
    "Kevin responds by smirking and quickly ruffling her hair with both hands before stopping."

    Shione hips exclaim "UUUFH! Never mind! I didn’t want to talk to you anyway. I can talk to Shun if I want to."

    show kevin laugh with config.say_attribute_transition
    "Shione turns to me, pretending to ignore Kevin as she fixes her messy hair. The waitress closes the door behind us."
    "The table in the room is big enough to seat eight people, and is nice and private as promised. With only the three of us, it feels even more spacious. There is a small pit under the table for our feet to hang, giving us even more room."

    Shun "Where’s Maryanne?"

    Shione "She had to buy something or make a phone call. She mumbled it out and went away for a bit. But there is food coming so she’ll be back. Soon."

    Kevin smile crossed open "Yeah, no kidding. I saved up for this. Mama Gallagher sent me some extra money last month so I am buying a big bottle of dry sake. And maybe more than that, if Maryanne wants to share."

    Shione "Drinking would be more fun next to the fishie tank."

    Kevin "Man, you and the fishes."

    show maryanne happy hairdown at leftish with config.say_attribute_transition
    "The door slides open again. The waitress escorts Maryanne inside. She’s smiling. Smiling at me. She sits down by my side and looks at me. And only me. Her necklace dangles over her collar bone. And the matching earrings off her ear."
    "And she is wearing something that smells really, really good. It hits my nose like a kiss hits my cheek."

    Maryanne "Hey."

    Shun "… Hi. You look really nice."

    "She smiles and grabs my hand. Only then does she finally breaks eye contact to look at the other two guests."

    Maryanne ecstatic "Thanks for coming everyone. I really appreciate you all being here."

    Shione "Happy Birthday Maryanne!" (multiple=2)
    Kevin "Happy Birthday Maryanne!" (multiple=2)

    show maryanne neutral with config.say_attribute_transition
    "She squeezes my palm as they say that. For a moment, her smile wavers."

    show maryanne happy with config.say_attribute_transition
    Kevin "Waitress! Which would you recommend for a birthday party? The Honjozo or the house Ginjo?"

    Shione finger wonder "You’re sharing?"

    Kevin laugh sides open "Of course! It’s a party after all. Besides, drinking alone is sooo laaame."

    show shione hips exclaim with config.say_attribute_transition
    Maryanne "We’ll all put some money to pay for a bottle if you are going to share if that will help."

    Kevin closed smile "I will hear nothing of that birthday girl! This is my treat. You wanna pitch in, pitch in when we get to the enormous portions you are going to eat."

    Maryanne "Oh. Don’t worry. I don’t plan on eating that much tonight."

    show shione confused
    show kevin open serious
    with config.say_attribute_transition
    Shun "…" (multiple=3)
    Shione "…" (multiple=3)
    Kevin "…" (multiple=3)

    Maryanne worried "… I mean it!"

    Kevin laugh "Haha. Okay. We’ll see. So, one bottle of each please. And Gyoza for an appetizer for me."

    Shun "I’ll take Yakitori please."

    Shione exclaim "I am gonna wait. No app-i-tiz-ers for me yet."

    Maryanne "Me neither. Thank you."

    show shione confused
    show kevin open serious
    with config.say_attribute_transition
    Shun "…" (multiple=3)
    Shione "…" (multiple=3)
    Kevin "…" (multiple=3)

    Kevin neutral "… O—okay. If that is what you—"

    Maryanne "It is."

    "Maryanne says quickly, bluntly. Nodding politely, the waitress leaves us."

    Shun "Uh, so, did you eat already?"

    Maryanne uncertain "Hmm? No. Why do you ask?"

    Shun "… No reason."

    Shione exclaim "Are you going to do anything with your dad this year Maryanne?"

    Maryanne ecstatic "Yep. He’s coming over in a couple of days. Not sure what we are doing though."
    Maryanne "Oh! He’ll get to meet you as my boyfriend this time!"

    Shun "Oh yeah, that’s right! Do I need to worry?"

    Maryanne embarrassed "I doubt it. He liked you last time. He thinks you’re … a good pick."

    "She looks at Kevin, who gulps down his glass of water with a few obnoxiously loud gulps, followed by a very loud “ahhh”."

    Maryanne happy "… My dad trusts my judgment."

    "The waitress returns with four glasses and two tall, chilled bottles ready to drink. Kevin claps his hands and rubs them together as the waitress pours his glass first."

    Kevin closed laugh "Mm-mm-mmmm! I’ve been looking forward to this all week."

    "He reaches for his glass when it is half full, but Shione aggressively slaps his hand."

    show kevin neutral open with config.say_attribute_transition
    Shione pompous "Bad Kevin! Bad! No!"

    Kevin "Hey, what’d I do?"

    Shione "We have to have a birthday toast first."

    Kevin frown crossed "First of all, ouch! Second of all, I knew that. I was going to give the toast."

    Shione "… No you weren’t."

    Kevin mad "Well, you already slapped me so I guess you’ll never know for sure."

    "They glare at each other … then turn back to the table. We all grab our glass as it’s filled."

    Kevin smile "Allow me to give a toast … which I was totally going to give and still intend to, if I have permission and you promise not to slap me."

    Shione exclaim "You may proceed, Sharkie."

    Kevin "Thank you. Now then …"

    show kevin neutral with config.say_attribute_transition
    "Shione raises her hand, as if to slap Kevin, but doesn’t. Kevin flinches."

    Kevin smile "To … to Maryanne! Not just a great girl, but the best girl."

    Shun "Here-here!" (multiple=2)
    Shione excited "Here-here!" (multiple=2)

    Maryanne "Thank you. I can’t tell you how … how great it feels to … have …"

    show maryanne sad with config.say_attribute_transition
    "She trails off. And looks down at her glass. She gulps and her happy face crumbles for an instant. She looked like the loneliest person on earth, like everyone had forgotten her birthday and she was eating a microwave dinner by herself."
    show kevin neutral
    show shione confused
    with config.say_attribute_transition
    "And as soon as the expression appears, it goes away again, replaced by her glowing smile. I say nothing, but everyone sees it; I can tell by how their own expressions change to worry for an instant."

    Maryanne happy "… I’m lucky you have you all here."

    show kevin smile
    show shione excited
    with config.say_attribute_transition
    "We all take a drink. The sake is bitter, then smooth, then warm even though it is chilled. I remind myself not to drink more until I have eaten something, as I can feel how strong it is just from the first sip."
    "But Maryanne finishes hers very, very fast. And she fills up a second before Kevin can."

    Maryanne ecstatic "Ahhh! Oh, this is really good! Thank you Kevin. This will be a fun night."

    Kevin "You’re wel—"

    show maryanne happy with config.say_attribute_transition
    "She pours a third glass."

    Kevin smirk "Whoa, you better slow down girl. This is the strong stuff."

    Maryanne worried "Hey, who’s birthday is it?"

    Kevin "Good point! Then I’ll have another drink with you."

    show maryanne ecstatic with config.say_attribute_transition
    "She finishes her third drink … quickly … then smiles."

    Shione "…" (multiple=3)
    Kevin "…" (multiple=3)
    Shun "…" (multiple=3)

    scene bg birthday_sushi
    show maryanne blush hairdown happy at leftish
    show shione hips confused at center
    show kevin smile crossed closed at rightish
    with bg_change_minutes
    Kevin "So then … so then, the old man takes my essay … and he throws it out. Just like that! Doesn’t even read it!"

    Maryanne "Eheeehehe … that shounds like shomething he’d do."

    Kevin open "Yeah. And I got an A anyway. Joke’s on him, I phoned that essay in. I did! I didn’t even lose a wink of sleep on it! Ha! Guess he isn’t as smart as he thinked he was."

    "Kevin leaves out some key details in his tale, ones I do not bring up. But the girls enjoy listening, laughing at his shortcomings."

    scene bg birthday_sushi
    show maryanne blush hairdown ecstatic at leftish
    show shione hips confused at center
    show kevin smile crossed open at rightish
    with bg_change_minutes
    "The sake might be helping, but I haven’t laughed this hard in a while. Maryanne has yet to let go of my hand. When my hand was busy grabbing appetizers or snatching sushi from the platter Shione ordered, her hand rested on my knee."
    "After her fourth or fifth drink, she leaned her head on my shoulder. And brushed her hair by my neck, I think intentionally, and that fragrance kept assaulting my nostrils. I forget why I was upset with her."

    Shun "How many have you had now?"

    Maryanne "I don’t … I didn’t … you … more than you! Okay? And I wants you to cccatch up!"

    Kevin laugh "I think ssshe has had four or—or—or five now. Look at that bottle. It is lower than a river in the middle of Ju-ly! {em}cha-pitooie{/em} Ain’t that right little lady?"

    Shione exclaim "Don’t pet my head “sweetie”! Officer Shark! Shark-man! We’ll … she’ll … I’ll … make sure she gets home safe and sound."

    Maryanne ecstatic "I’m never going home!! Weee! You guys! … You guys … okay {em}now{/em} I’m hungry. Okay?"
    Maryanne "We need to go get food. I think … I think there are a few places open now. If we can gets outta here, we can go get food."

    Shun "We’re in a restaurant!"

    Maryanne "I know. I know. I know. I know. But listen … shhhh, listen. What if we … oh. Oh wait, we can order food {em}hhhhere{/em}! Ooookaaay, haha. I’m gonna get the … big … the big … Shione! Shi-o-ne. What was that thing I liked that they service here?"

    Shione confused "Um … I think it was … everything?"

    Maryanne "Everything? Aight. I’m gonna get that."

    "I reach over her shoulder and grab the sake. Maryanne thankfully says nothing as I place it by my side."

    scene bg birthday_sushi
    show maryanne blush happy hairdown at leftish
    show shione hips excited at center
    show kevin smile crossed open at rightish
    with bg_change_minutes
    "Maryanne’s appetite returns with a vengeance, and soon she is happily munching away at, like she said, everything."
    "She ordered a birthday platter of sushi rolls and sashimi … for two. And devours a quarter of it only as I start on my katsudon."

    Maryanne "Mmm … mmm! Thiff if soo goo!"

    "She says with a partially full mouth and a still prevalent slur. She swoons like she is in love with the dishes."

    Maryanne ecstatic "This is the best party ever! I … hey, I’m empty! Fill ’er up boyfriend!"

    "She pushes the glass just under my nose and taps the edge."

    Shun "You don’t think you have been drinking too fast?"

    Maryanne "I don’t think anything right now. I am druuuuuunk! Put more drinkables in the cup please."

    Shun "I think you might want to slow down."

    Maryanne worried "Nnno! More drinks!"

    Shun "Maryanne, I really think—"

    hide maryanne
    show shione hips wonder
    show kevin smirk crossed closed
    with config.say_attribute_transition
    "She grabs my shirt firmly, pulls me in and kisses me. Really, really passionately. I can taste sushi and drinks as my face lights up. Kevin coughs and Shione … stares awkwardly."

    Shun "Umm … I du … ye …"

    show maryanne happy hairdown blush at leftish with config.say_attribute_transition
    "She pulls the bottle away from me and giggles, letting go of my shirt when she gets what she wants."

    Shun "That … was … a cheap trick."

    Maryanne ecstatic "Yeah. But it worked. Ha!"

    Shione finger excited "Gimme! I want the other bottle!"

    Kevin laugh sides open "Yeah, go nuts. Not as nuts as Maryanne, but nuts enough to enjoy your night."

    Shione "Oooo-kaaay! Hehe, you two are really cute together."

    Maryanne uncomfortable "I know, right?"

    show shione exclaim
    show maryanne happy
    with config.say_attribute_transition
    "She leans on me again and spills her drink. She snickers harshly, then ruffles my hair."

    Shione finger confused "Yeah, he’s a good. Kevin, how come you don’t have a … I mean, I don’t have … you know, cause … ugh, I drank too fast."

    "Kevin, still relatively sober, pats Shione’s head. She simply waves her hand in the hair to bat him off, missing."

    Kevin smile "I think … it is gift time."

    "Kevin reaches behind his back and pulls the nice envelope out from behind his seat."

    Maryanne ecstatic "Than—thank you Kev. I’ve alwaysss wanted an envelope."

    "Maryanne meticulously opens the parcel, careful to not rip it. An easy job for anyone who did not drink almost half a bottle of sake. Eventually, she pries the fold open and gently slides out a certificate."
    "It is embroidered in shiny gold calligraphy, with a gorgeous banner that shimmers with light."

    show maryanne neutral with config.say_attribute_transition
    Kevin "I got that at a stand at a festival in town. It says “Maryanne: Best girl ever!”. Nice huh?"

    Shione "Oooh, pwetty! Is so shinny!"

    Maryanne "…"

    Kevin laugh "Yes! Speechless! I knew you’d be! Shione, you’re up."

    "Kevin passes the metaphoric baton to Shione before Maryanne can speak up, but she doesn’t look too happy with the present."

    Shione "Ooo-ke-dooo-ke!"

    show kevin smile with config.say_attribute_transition
    "Shione reaches into her bag, head first, almost falling into it as she digs around. Eventually, a pair of tickets pop out."

    show maryanne sad
    show shione finger excited
    with config.say_attribute_transition
    Shione "Check it out! I got us movie tickets to see anything you want ever! We can go tomorrow or the next day or the next day or the next day, but only once for each of us! And it comes with free food that isn’t sushi. Cool right?"

    Maryanne ecstatic "Coool. I can’t … can’t wait … oh wow, more sake please!"

    show shione exclaim with config.say_attribute_transition
    "She asks but takes her own, chugging half a cup and smacking her lips loudly. And she does this, I put my wrapped present in front of her and smile."

    Maryanne worried "… Nnnnoo!"

    "She pushes it back to me and fake-pouts."

    Maryanne worried "Give it to me lahter. I want it … lahter. Okay?"

    Shun "Uh, o—okay. If you want to."

    Maryanne happy "Cus … cus … I wanna give you a gift too."

    "She “whispers” loud enough for everyone to hear. Kevin covers his mouth, trying not to laugh. I blush again."

    scene bg birthday_sushi
    show shione hips excited at center
    show kevin smile crossed open at rightish
    with bg_change_minutes
    Waitress "Here is your bill. Thank you for coming tonight."

    "Kevin pays with his card, and we all give Kevin what we owe him with much resistance from the intoxicated organizer of the night."

    "By now, Maryanne’s eyes are closed and her head rests on my lap, giggling and mumbling every few seconds."

    Waitress "Is she alright?"

    Kevin smile sides closed "Oh yeah. She had a bit too much fun. We’re gonna take her home now."

    "The waitress looks worried."

    Shun "I’m her boyfriend."

    show kevin neutral crossed open with config.say_attribute_transition
    Waitress "Oh, okay! Good. You get home safely."

    Kevin "We will. Thanks doll … alright, get her up then."

    hide shione
    hide kevin
    with config.say_attribute_transition
    "I try to lift her, but Maryanne is far too heavy for me to carry on my own. I make a note to never, ever tell her that. Kevin stumbles to my side and puts her arm over his shoulder. Together, we can do it."

    Maryanne "Hmmmmm … why is this room sideways."

    Kevin "Because it’s your birthday."

    Maryanne "Coooool!"

    scene bg birthday_sushi_outside night with bg_change_seconds
    "We made it outside before we realized … we lost Shione."

    Kevin "Hang on, I’ll get her."

    "Kevin ducks back into the restaurant, leaving me to hold onto Maryanne on my own. She groans, her arm wrapped around the back of my neck, her head hanging downwards."

    Shun "You okay?"

    show maryanne blush hairdown neutral with config.say_attribute_transition
    Maryanne "I’m … great."

    Maryanne sad "You are … you are … sush a good boyfriend. Yo—oo—at? I am soooo sorry for … uh, for … all the … all of dat … stuff."

    Shun "It’s … really no problem."

    "She pushes her temple on my forehead and smiles way too wide."

    Maryanne happy "… Shun … I gotta tell you something …"

    Shun "S—sure. What is it?"

    Maryanne sad "… I gotta … I gotta … {em}hurk{/em}"

    hide maryanne with config.say_attribute_transition
    "She pushes me to the side and runs, hand over her mouth."

    Maryanne "{em}hurraaack!{/em}"

    "Maryanne vomits into a nearby storm drain."
    "She goes on for a minute. I go to hold her hair back for her, trying not to look or smell it."
    "When she’s finally finished, she shyly wipes her mouth, not looking up at me."

    show maryanne blush sad hairdown with config.say_attribute_transition
    Maryanne "Uhhh … thank you. I think I drank too much."

    Kevin "I found her! She was, go figure, by the fish tanks."

    Shione "I named some of them!"

    Shun "Hey. Give us a minute."

    Maryanne mad "I’m okay! Don’t worry!"

    "She stands up, wiping the last bits of the punk off her chin. She looks mad, maybe at herself, and pushes me away gently."

    Maryanne "Excuse me. I can got … I can get home. Myself."

    Shione "You tell them Maryanne!"

    "She turns and marches across the street, crossing the road with her chin her up, overplaying her posture and stamping her feet on the crosswalk."
    stop music fadeout 0
    play sound "217542_medartimus_car-breaking-skid-01"
    "{snd}SCREEEEEECCCCHH!{/snd}"

    hide maryanne with config.say_attribute_transition
    Maryanne "Ahh!"

    scene bg white with Dissolve(3)
    "Everything slows down. Two bright headlights veer towards Maryanne as she stand in the middle of the street. The car horn blaring muffles her voice. Kevin curses. Shione gasps. I instinctively step forward but she’s too far from me to reach."

    stop music
    scene bg birthday_sushi_outside night
    show maryanne worried blush hairdown at leftish
    with Dissolve(3)
    play music "unrest_loop.mp3"
    "The car stops inches from Maryanne’s trembling knees. Maryanne’s frozen, aside from the erratic shaking at her joints. Her eyes, wide as I have ever seen them, stare at the reflection of herself in the windshield."
    "The driver sighs in relief, then pulls away quickly. Breathing sporadically, Maryanne watches the car drive off."

    Shun "Maryanne, come on. Come on!"

    "I hurry to her, grab her elbow and pull. Twice. She moves after the third tug. Barely. Her feet drag on the pavement as I tug her away. She shivers and gasps deeply, holding her elbows."

    show kevin open sides frown at center
    show shione hips pompous at rightish
    with config.say_attribute_transition
    Kevin "Jesus girl! You scared the hell out of me."

    Shione finger "That was really scary."

    Maryanne "…"

    Kevin "I think it is time to call it a night. You want help getting back?"

    "I look at Maryanne, who holds onto both of her elbows and stares at the ground as a cold sweat drips down her temple. I nod and take Kevin up on his offer."

    Kevin "Okay. Shione, you carry her birthday gifts."

    Shione hips "Why do I have to … I mean, yeah. Sure. No problem."
    hide kevin
    hide shione
    with config.say_attribute_transition

    "Shione goes to the crosswalk and gathers up the birthday bag Maryanne dropped."
    "Maryanne puts her hand over her mouth and stares, still in silence. She mumbles something to herself, too softly for me to decipher. I rub her shoulder and guide her back to her dorm. We take the long way, away from busy streets."

    scene bg hall_dorm_girls
    show maryanne worried hairdown at leftish
    show kevin frown sides open at center
    show shione hips pompous at rightish
    with bg_change_minutes
    Kevin "Get some sleep, okay girl? It’ll be alright. You’re safe and sound, and that’s what matters."

    Shione exclaim "Thanks Sharky. It was a fun night. Up until the car-part."

    Kevin mad crossed "Shione! Don’t listen to her, we had a great time Blondie. And it’ll be even better next year."

    Maryanne "…"

    Shun "I’ll get her to bed. You guys can go home. Kev, I’ll … see you."

    show kevin neutral crossed open with config.say_attribute_transition
    "Kevin slowly nods, knowing that what I mean is I may or may not be back tonight."

    scene bg dorm_maryanne with bg_change_seconds
    show maryanne worried hairdown
    "Kevin leaves. Shione goes into her room, clumsily falling onto her bed. I guide Maryanne into hers and close the door."
    "I sit her down on her bed and rub her back softly. She trembles, not looking at me."

    Shun "You’re okay. You just had a fright. But you’re—"

    Maryanne crying "I almost died."
    Maryanne "I almost died. I turned 20, and I almost died."
    Maryanne smalltears "I was drunk with my friends and I was happy and having fun and … and …"
    Maryanne bigtears "… I didn’t see my dad. I didn’t see him. I almost died without seeing my dad again!"

    "She covered her eyes and starts weeping into her palms. First quietly. Then loudly, loud enough for Shione to probably hear."
    hide maryanne with config.say_attribute_transition
    "I hug her, tight as I can. She screams into my chest, then goes back to crying loudly. Her nails dig into my arm. She lets it all out and I have to force myself not to cry with her. But it gets close. I think of my dad and how I haven’t even texted him in so long … what if I was in the street?"
    "But I don’t cry. I … can’t. I didn’t have it in me."
    "Before long, she is either too tired or too drunk to keep her eyes open. Sniffling, she lays down on a soft pillow and crawls under the sheets. Tears run down her face like little streams, her eyes too strained to stay open."

    Maryanne "Please don’t go."

    "She begs and I agree without another word. She hugs me, like she’d fall if she let go. I stroke her back gently, nervously, not sure what I can do besides this."
    "Maybe it’s enough. Maybe it’s nowhere near enough; I don’t know. But I think of where I would be if that driver hadn’t stopped. And my own dad."
    "I look at the ceiling as she sobs softly, as she drifts off gradually. The weeping slows and gets softer until eventually she goes quiet. I close my eyes only when I am sure she is asleep."

    scene bg room_maryanne night with bg_change_minutes
    "The sound of her door closing wakes me up."
    "Once more, I am alone in her room. Once more, I feel left behind."
    "My head throbs and my feet are sore. I contemplate going after her but … I don’t think she’d listen to me. Or I wouldn’t say something that would get through to her. Or maybe I’m just tired, from last night and … everything. My shoulders feel heavier than they did yesterday."
    "I see the sun is starting to rise. And my present, still wrapped up, on her coffee table. I put it back in my pocket."

    scene bg inou night with bg_change_seconds
    "I put my shoes on a leave. I am not mad. It’s worse. I am apathetic. Maryanne maybe doesn’t need me, I think. She’s off on her own right now, not a word before leaving."
    "I look at my phone and no messages from her. I scroll through the messages and find the last thing dad sent me, one of many messages I ignored."

    Dad "{tt}Study hard and let me know if you need anything. We're fine over here in case you were wondering.{/tt}"

    "I get so mad when I read that message. Why did I {em}have{/em} to be concerned with how things were there? Why couldn’t I just be left alone or worry about my own problems. Why couldn’t {em}he{/em} worry about {em}my{/em} problems, why didn’t he ask about that?"
    "I begin to type something … but stop. I delete the message and put my phone back in my pocket. I grumble, trying to not think about this."
    return
