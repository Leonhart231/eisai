##----------------
## Act 3: Maryanne
##----------------

label a3:
    call act_transition("3")
    call a3s1
    call a3s2
    call a3s3
    call a3s4
    call a3s5
    call a3s6
    call a3s7
    call a3s8
    call a3s9
    call a3s10
    call a3s11
    call a3s12
    menu:
        "Of course it matters!":
            $ bad_ending = False
            call a3s13a
        "How could you say that?":
            $ bad_ending = True
            call a3s13b
    call a3s14
    if bad_ending:
        call a3s15a
        call ending('bad')
    else:
        call a3s15b
        call a4

    return
