##----------------------------
## Act 3: Maryanne
## Scene 13a: Breakdown Choice
##----------------------------

label a3s13a:
    play music "very_sad.mp3"
    Shun "Of course it matters!"

    "I raise my voice, shouting loud enough to make her flinch."

    Shun "It matters to me! How can you not act like you don’t care?! Of course I care! I care about you! I don’t like seeing you like this. You matter to me! You are—"

    show maryanne sad smalltears with config.say_attribute_transition
    "Maryanne’s eyes quickly water up. She gulps and inhales quickly a few times before burying her eyes in her palms. I freeze. The sound of the ticking of the clock is drowned out by muffled weeping."

    Maryanne crying bigtears "It matters to you? To you? Who cares!?"

    "Abruptly and loudly, Maryanne throws her hands to her side. Her notepad falls to the ground."

    Maryanne "Who cares if it matters to you? Who cares if it matters to me? Or anyone?! It doesn’t matter at all Shun. None of it does! Not a single bit of it!"
    Maryanne "This is all we are! Cells and organs and chemical reactions and nothing more. That’s it. There’s a brain and blood and bones and … nothing! There’s nothing there."
    Maryanne "There’s no great mystery, no big secret, and no point to it!  We’re just a bunch of organic puzzle pieces neatly packed on top of each other."
    Maryanne "Accidents. We’re just random happenstances. None of this matters!"

    Shun "Maryanne, calm down—"

    Maryanne "It’s all just a big … waste of time. I’m only twenty and I make my dad proud, my teachers proud … I even have job offers. Did you know that? People want me to work for them already! But it doesn’t matter! I don’t want any of it."
    Maryanne "I just want to feel safe again. I want to feel like something matters again. I’m … oh god …"
    Maryanne "One day, tomorrow or forty years from now … I am going to die. I can’t stop it. No matter how many books I read or how hard I work or how many people like me, it doesn’t matter. I’m a ticking clock. A countdown."
    Maryanne "I might as well have not wasted my time studying these stupid books in the first place! It wouldn’t matter! It wouldn’t change anything!"

    "The little rivers running down her cheek turns into little waterfalls. Her voice cracks. She started to choke on her own breath. Her legs shake, barely able to hold her up anymore."

    Maryanne "… I … I don’t want to die. I don’t want to get older. It keeps me awake, it makes me want to scream. This isn’t fair!"

    hide maryanne with config.say_attribute_transition
    "Not knowing what else to do, I hug her as tight as I can. She hugs me back so tight that it hurts a little."

    Shun "It’s … it’s … it’s okay."

    Maryanne "It’s not. It’s not okay! It’s wrong! All of it! Why does this have to happen? Why does anyone have to go through this?! It’s not fair! It’s … not … faiiir."

    "She buries her face into my chest as the last piece of her mental armor breaks. She falls to her knees, taking me to the floor with her."
    "She screams into my chest like I was a pillow, her nails digging into my shoulders hard enough to pinch."
    "This is what I wanted. A chance to be someone Maryanne needed, for her to finally tell me what was bothering her … and I had no clue what to say."
    "So I hold her and let her cry. And I do not let go. Not for one second."

    show maryanne crying bigtears coat tangled with config.say_attribute_transition
    Maryanne "I’m sorry."

    Shun "You haven’t done anything wrong."

    Maryanne "I just … don’t want it to happen. I’m … I’m scared … one day, it’s all going to go away. So why do anything at all?"

    "What could I say? What could anyone say? I try to think of something to make her feel better but nothing comes."

    Maryanne "I wish I wasn’t … like this. I wish I wasn’t so helpless. I just … want to be okay again."
    Maryanne "I … lied before. I didn’t want you to leave. I wanted you to stay. I hate being alone. But I like it too … I don’t know. It just feels like I hate everything now. I hate all of this."
    Maryanne "I don’t know what I’m supposed to do. I just want answers. I want someone to tell me what to do. I want someone to tell me I’m going to be okay and … and …"
    Maryanne "… I miss my mom."
    Maryanne "I miss her so much Shun. I’d give anything to see her again. I still remember one of the songs she used to sing to me, and that she hated spiders but … not what made her happy, or even what she looked like or why she …"
    Maryanne "… Or why she left. Why … why didn’t she want to stay and watch me grow up?"
    Maryanne "I wish she was here. God, I’d give anything to see her again."

    hide maryanne with config.say_attribute_transition
    "She shivers and sobs for I don’t know how long, occasionally mumbling “It’s not fair” or “I don’t want to die” into my shirt, to which I would respond only with a tighter hug and then gently rub her back."
    "Lord knows how long she had kept this to herself. This wave of misery felt like it was bottled up for years, like she had carried it with her for a long, long time. She keeps her head on my chest and refuses to look at me."
    "She sniffs and rubs her nose a few times. Her face is covered in tears and running makeup. I wipe the underside of her eyes with my thumbs as she trembles. It takes her a while for her to calm down, but eventually she does."
    "She doesn’t say anything, only breathing loudly through her nose and refusing to look at me."

    Shun "I think … it’s time to go. For now. You could use a night’s rest. If you can sleep. At the very least, you don’t need to work right now."

    "She sniffles, then nods weakly. She lets go and I gather her books, handing some to her. I put one arm over her shoulders and guide her out of the lab."

    scene bg inou night with bg_change_seconds
    "A roll of thunder bellows softly over our heads as we leave the science building. Maryanne keeps her eyes on her feet as I guide her back to her room."
    "At one point, Maryanne trips. I catch her."

    scene bg dorm_maryanne with bg_change_seconds
    "By the time we get to her dorm, she barely has enough strength to keep her eyes open. I have to force her to give me her dorm key so I can open the door for her myself."

    scene bg room_maryanne night with bg_change_seconds
    "I gently lay her down onto her bed. And just as I put a blanket over her, the sky starts to pour."
    "I watch the rainfall through the window for a moment as Maryanne falls to sleep. Emotionally and physically exhausted, she is out like a light in mere moments."
    stop music fadeout config.fade_music
    "I wonder if Maryanne had ever vented like that to someone else, or if I had the privilege of being her first time."
    "I send her a text message now, so she can see it when she wakes up."

    Shun "{tt}I'm here when you need me. Please feel better.{/tt}"

    "Part of me wants to stay with Maryanne. But she’s tired. I’m tired. She needs to sleep. I need to … not be here."
    "I think I hear her sigh as I walk out the door, feeling helpless."

    scene bg black with bg_change_seconds
    "That was the best word to describe this all of this. Helpless."

    return
