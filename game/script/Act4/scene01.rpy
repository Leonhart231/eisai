##---------------------
## Act 4: Maryanne
## Scene 1: Wet and Dry
##---------------------

label a4s1:
    stop music
    call stop_weather()
    scene bg dorm with bg_change_minutes
    play music "very_sad.mp3"
    "The second Maryanne is in the dorm, her father embraces her. He yells at her in English, and she sheepishly replies back in the same language. I only catch every other word … Mr. Sawyer mentions her mother and says he can’t go through that again."
    "Maryanne reassures her father that everything is alright, but can’t look at him. Kevin stays quiet but looks relieved, and slinks over to me while the family hugs it out."

    show kevin frown crossed open with config.say_attribute_transition
    Kevin "All good?"

    Shun "… For now."

    "Kevin nods. He says nothing else. Maryanne comes over to us as her father gathers up his belongings."

    show kevin at leftish
    show maryanne circles tangled crying smalltears at rightish
    with config.say_attribute_transition
    Maryanne "Dad wants me to … spend the day with him. To make up for missing my birthday. And I need to talk to him about … some things."
    Maryanne "Will you be around tonight?"

    Shun "Absolutely."

    "She nods, trembling a bit, still damp from the river. Her father holds the door open for her, wanting to leave right away it seems. He whispers something to Maryanne in English and she hurries back out to the hallway."

    hide maryanne
    show george smile at rightish
    with config.say_attribute_transition
    George "Thanks kid. I … {i}phew{/i}, glad that’s over. Thanks."

    hide george with config.say_attribute_transition
    stop music fadeout config.fade_music
    Kevin serious "Man … that sucked."

    Shun "You’re telling me …"

    Kevin "So, like, uh, do you need … to talk or something? About this?"

    Shun "I don’t think so Kev. It’s … kind of … a lot."

    Kevin frown crossed open "Right, right …"
    Kevin smile "Lunch? My treat."

    Shun "… I’m not hungry."

    show kevin serious with config.say_attribute_transition
    "Kevin huffs softly, nods once, and grabs his coat."

    play sound "237729_flathill_rain-and-thunder-4" fadein 1 fadeout 10
    hide kevin with config.say_attribute_transition
    "A soft rolling thunder bolt echoes outside. I sit down, sighing, too exhausted to do anything but lay down."
    "I hoped she was okay. More than anything else, I hoped she would be okay. Through all my own stress, that really was the only thing that truly worried me."
    return
