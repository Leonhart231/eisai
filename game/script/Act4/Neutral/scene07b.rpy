##---------------------------------
## Act 4: Maryanne
## Neutral route
## Scene 7b: Night before — neutral
##---------------------------------

label a4ns7b:
    scene bg inou night with bg_change_minutes
    "We run out of rocks to throw a little while after the sun goes down. Then we go back. Not talking."
    scene bg hall_dorm_girls with bg_change_seconds
    "She looks like she’s about to cry as we make it back to her dorm."
    "And I … say nothing."
    "I’m too tired."

    show maryanne sad with config.say_attribute_transition
    Maryanne "I … need to pack for the night."

    Shun "Yeah, okay."

    Maryanne neutral "I’m leaving tomorrow morning at 7."

    "She says with a weak smile, rubbing her arm, not looking me in the eye for more than a second."

    Maryanne uncertain "Will you see me off?"

    Shun "Of course."

    "I say half-heartedly. I wanted to say goodbye, but also … didn’t. I wanted to beg her to stay and tell her to leave right now at the same time."
    "It didn’t matter now I suppose."
    hide maryanne with config.say_attribute_transition
    "Maryanne kisses my cheek awkwardly, then turns quickly, rushing inside. I watch her leave, watch the door close behind her, then turn away myself."

    scene bg dorm with bg_change_minutes
    "Kevin is up, watching something on the TV, munching on a bowl of pretzels. He stares at me with a mouth full of snacks and waits for me to sit down next to him."

    show kevin open crossed frown with config.say_attribute_transition
    play music "unrest_loop.mp3"
    Kevin  "{i}gulp{/i} … …"

    Shun "… She’s going to leave."

    "Kevin nodded. Of course he knew. He probably knew before I did."

    Shun "… I don’t feel great."

    "Kevin pats my shoulder. Then offers me a pretzel. I shake my head … and he eats it."

    Kevin "… It was good while it lasted. For however long it lasted."

    hide kevin with config.say_attribute_transition
    "He says that nonchalantly before going to his room."
    "I’m alone."
    "Even if Kevin had stayed, I would have felt alone."

    scene bg room_shun night with bg_change_minutes
    "I don’t sleep. I try to. But I can’t help but look through pictures of her, scrolling through a few favorites. Maryanne’s status stays “Active” almost all night too. I resist the urge to message her, wanting her to talk to me first."
    "She doesn’t."
    "Not all night."

    scene bg dorm with bg_change_days
    "I think I got an hour of sleep, two at most. My head hurts. I’m starving, but I don’t want to eat."
    "I finally get a text … from Kevin, of course. Politely asking me to meet him and Maryanne at the bus station."
    "I get my shoes on and leave. Slowly."

    return
