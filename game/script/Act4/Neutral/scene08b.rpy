##----------------------------
## Act 4: Maryanne
## Neutral route
## Scene 8b: Goodbye — neutral
##----------------------------

label a4ns8b:
    stop music
    scene bg inou_bus overcast with bg_change_minutes
    play music "contemplative_loop.mp3"
    "The sky is dark and cloudy. Cold. Lonely. I walk out of campus, sighing as I look at the bench Maryanne and I sat the day before."
    "Kevin waits by the bus stop, scratching the back of his head as I step next to him. There’s a few suitcases by his side, all pink or covered with stickers from America. He doesn’t say anything, only patting my back as I overlook the bags."
    "We wait, in silence."
    "And we wait."
    "And wait."

    show kevin serious crossed open with config.say_attribute_transition
    Kevin "Maryanne is saying goodbye to Shione. She should be here … you know, soon."

    Shun "… Yeah."

    "More silence. More cold wind. The sky rumbles very softly."

    Kevin "… I’ll load up her bags when she comes, then give you both privacy."

    Shun "Thanks Kev."

    Kevin "And I’ll be home all day. All night. Don’t be alone today. Do yourself a favor, don’t be alone today."
    Kevin "And … well …"

    "He trails off. I know he’s trying to help. And maybe it will help, later. But what else could we say right now? What else {em}can{/em} be said?"
    "He kicks a pebble away from us and I sit motionless. Barely able to feel anything, except the imaginary hole in my chest."

    Kevin "… It was nice while it lasted."

    Shun "I guess. But it’s really not nice now."

    Kevin closed "Yeah. The End … always sucks. They never show that part in movies, you know? Like, we get a title card that says “The End” but never actually see the final parts. I guess it’s cause the story is what matters, when you wanna feel good."

    Shun "Kevin … movies aren’t real."

    Kevin open "…"

    Shun "It’s {em}not{/em} real. It’s not even an edited version of “real”. They chop out the boring or bad parts so you can feel like everything isn’t awful for a few hours."
    Shun "But things {em}are{/em} awful. And they’ll be awful for a while. There’s nothing you can do about it."

    Kevin frown "Yeah but—"

    Shun "But nothing Kevin. Sometimes, things just suck and there’s no way to talk your way out of feeling terrible."

    Kevin "There’s always another movie to watch. You know?"

    Shun "Yeah, sure, there’s always another fake story to listen to. But {em}this{/em} story is over."
    Shun "… And it hurts. Even if it’s not that terrible in the grand scheme of things, it hurts. Endings hurt. Even happy ones."

    Kevin serious "…"

    Shun "You know what I thought of the other day? When Hideki was whining about … something, I don’t know. He’s such a headache! He never has anything nice to say. He drives you nuts. But one day, he’s going to graduate and leave school and we’ll never see him again."
    Shun "And when he’s gone, when {em}that{/em} story is over, we’ll miss that too. It’s not about a story being good or bad … endings hurt because you lose something, no matter what happened before the final part."
    Shun "I’m honestly not that sad Maryanne is going. She {em}has{/em} to go. She has to do what’s right for her. I get that. But I’m losing whatever it was we {em}could{/em} have been and that feels worse somehow."

    Kevin "…"

    "Kevin stays silent and lets me say my piece … thank god."
    "The sun starts to peak up over the hills. Just a bit. It’s still dark, still cold out."

    Kevin "Do you remember when we first met?"

    Shun "Do I remember?! Will I ever forget! You dragged me across town and yelled in my ear all day about how great you were."

    Kevin smirk "Heh … yeah. Yeah, I guess I did do that."
    Kevin "I’m sorry. I was just nervous."

    Shun "Ne—nervous? You?"

    Kevin serious "Well, we didn’t know each other and it’s always weird meeting a new roommate …"

    Shun "Why are you talking about yourself again?!"

    Kevin "Because I get it Shun. I know what “alone” means."

    Shun "You don’t get this …"

    Kevin frown "Shun, I crossed half a planet and left everything I’ve ever known to come here."
    Kevin "Okay?"
    Kevin "My family and old childhood friends … that’s like 15,000 miles that way. When I came to Inou, I was completely alone. Alone-alone. It sucked!"
    Kevin serious "I don’t know, I guess I didn’t want anyone else to feel that way if I could help it."
    Kevin frown "You’re not {em}that{/em} alone Shun. Even if it hurts right now."

    "Another moment of silence passes. Neither of us look at the other. For a long time."
    "Eventually I calm down a bit, but don’t say another word. Neither does Kevin."

    "The bus pulls up. The driver opens the door and politely greets us."

    Bus "Hello young sirs. We’ll be leaving in fifteen minutes on the dot. Do you need help loading those bags onboard?"

    hide kevin with config.say_attribute_transition
    "Kevin shakes his head and lifts Maryanne’s bag up."

    scene bg inou_bus overcast with bg_change_minutes
    "Eventually, Maryanne appears, with Shione nowhere to be seen. Kevin turns his head towards her as she walks on the bus."

    hide kevin with config.say_attribute_transition
    "He slowly walks over to Maryanne then. They talk in English. I pick up on a few words. “Lonely”, “travel”, “miss you”, “please call” … then they switch languages again. I assume they were previously talking about me."

    show kevin neutral sides open at leftish
    show maryanne sad at rightish
    with config.say_attribute_transition
    Kevin "Call when you land. Call before you leave. And be careful. I hear California is a dangerous place with all of those free drugs and great schools. Don’t, uh, don’t pick a fight with anyone you can’t outrun. And if you see a guy in a trench coat, he’s not selling candy."

    Maryanne happy "Oh Kevin …"

    Kevin "…"

    Maryanne crying smalltears "… Oh … Kevin …"

    "They hug each other {em}very{/em} tightly. Kevin rubs the skin under his eyes as she pulls away."
    "Before he steps off the bus, Kevin pats my shoulder and sighs softly."

    Kevin "I’ll leave you to it then …"
    Kevin frown "Don’t be alone tonight."

    hide kevin
    hide maryanne
    with config.say_attribute_transition
    "They say nothing for a minute. Then they hug. Tightly. So tightly that he lifts Maryanne off the ground for a second. He puts her back down, and quietly leaves, rubbing the underside of his eye."
    "Maryanne approaches me, her last bag underarm."

    show maryanne with config.say_attribute_transition
    Maryanne crying smalltears "Well … I’m leaving."

    Shun "I know."

    "The sky rumbles softly. Maryanne’s head hangs with guilt."

    Maryanne sad notears "I …"

    Shun "You don’t have to say it. Really. I get it."

    "And I do. Well, my brain does. My heart still feels like it’s being ripped in two. It doesn’t make sense. I shouldn’t feel like this. But … I do. And even though all I want to do is beg her to stay, I can’t find the energy to say anything to her."
    "Maryanne stands and looks at me, waiting for me to say something. I feel that I’m hurting her. I don’t want to. I want to help her, or at the very least I don’t want her to be mad at me."
    "… But I am mad at her. I don’t mean to be. But I am."
    hide maryanne with config.say_attribute_transition
    "She moves towards the empty seat near the front. My heart leaps in its chest."

    Shun "Wait!"

    show maryanne worried with config.say_attribute_transition
    "She looks at me, clutching her bag, trying not to cry as I try to speak."

    Shun "… I’m sorry."

    Maryanne uncertain uncertainsmalltears "Don’t be sorry. You didn’t—"

    Shun "No, let me say this. Please."
    play music "eisai.mp3"
    Shun "I’m sorry I can’t say more. Or do more. I’m sorry if there was anything I needed to say and didn’t. I’m sorry that you’re going through all of this. I’m sorry that you had to carry all of this with you for so long."
    Shun "I’m sorry you need to do this to feel okay again; I know it’s a hard choice for you too."
    Shun "And …"
    Shun "… I’m sorry I wasn’t enough."

    show maryanne crying bigtears with config.say_attribute_transition
    "Maryanne covers her eyes and cries into her palm. She moves close to me and I hold her, really tightly. I try, and fail, to hold back my own tears. We hold each other as the rain starts to gently fall."
    call weather(1, 0)
    "The bus driver coughs softly after a moment."

    Bus "I am sorry ma’am, but we have to keep on schedule. We need to leave soon."

    "Maryanne puts her bag aside and hugs me so hard that it hurts my neck. I do the same. It feels like every second moves much faster than it should."
    "We could have held each other much longer before this … why didn’t we? Why didn’t we just spend every minute we could with each other? Why didn’t we hold each other this tightly every day?"
    "Maryanne is the one who lets go. She trembles, crying, barely able to pick up her bag as the shivers. I help her, and hug her again, one last time."

    Bus "I’m … sorry ma’am. But …"

    Maryanne "I know. I know, sorry."
    Maryanne "… Bye Shun. I love you."

    Shun "I love you too."

    scene bg inou_bus overcast with bg_change_seconds
    "The bus door closes and I step back, not looking away from her through the window. She looks back, crying so much that I can see her face turn red."
    "The bus lurches forward and I still watch. She looks at me for as long as she can. I watch the heavy vehicle trudge down the road, to the corner, and watch it turn … disappearing from my sight."
    "I stay put. Watching the corner. Like the bus would come back. Like it would break down and she couldn’t leave until tomorrow. Or that she’d order the bus to stop and she’d run back here, and pick me over … {em}herself{/em}. I realize how silly and selfish that thought is."
    "Still … it’s the only thing I want right now."
    "I consider going back to the dorm, but knowing Kevin would be there … I leave the corner and go through town."

    scene bg takuya overcast with bg_change_minutes
    "I take a long time, I think. I walk slowly. I feel old. Like my body is never going to ever be what it once was."
    "I pass by the spots Maryanne and I went to every now and then. Past the mall, the bridge, the fountain. They all feel … damaged. Like cracked mirrors. Broken reflections of what they’re supposed to be."

    scene bg takuya with bg_change_minutes
    "I make it about half a mile, as the light starts to fill the sky, before having enough of it. And I just stand. With nowhere to go, or rather nowhere I’d want to go. Not alone. Not like this."
    "{snd}Bzzz{/snd}"

    Maryanne "{tt}I'm at the airport. I'll text you when I land. Please don't be mad at me. I'll  miss you every day.{/tt}"

    "I hesitate to write something back, staring at the message for a while as my heart sinks. It feels very, very real now as I read this message."

    Shun "{tt}I'm not mad. I just wish it was different.{/tt}"

    "I delete that and start over."

    Shun "{tt}I miss you already.{/tt}"

    "I don’t send that either."
    "Nothing I can say will fix this. Not in Maryanne. And not in me. I feel small. Smaller than I’ve ever felt before. I stare at the message more and more, thinking if any of my feelings even mattered to her. Or if I should even bother talking to her at all."

    Shun "{tt}Have a safe flight. :) You'll be okay.{/tt}"

    "I press send. Then I turn my phone off. And go home."

    scene bg dorm with bg_change_minutes
    "Kevin is waiting for me. He has a pile of movies on the coffee table, all unopened."
    "I look at them, then at him … and shake my head. He nods."
    "I go to my room. Before I can close the door, Kevin stands up."

    show kevin frown crossed open with config.say_attribute_transition
    Kevin "Don’t be—"

    Shun "Alone tonight, yeah. I know. I know."
    Shun "But I want to be alone now."

    scene bg room_shun overcast with bg_change_seconds
    "I close the door before I can hear Kevin’s reaction. And lay down on my bed."
    "I look at my phone. It’s off. I toss it across the room."
    "I see a picture or Maryanne on my dresser. Unable to take it for more than 5 seconds, I stand up and put it away, stuffing it behind my bookshelf."

    Shun "She’s gone …"

    "I say out loud, verbalizing the finality of it all. I needed to hear it. I feel such a weight on my shoulders that I can barely keep my head up."
    "I lay down again, the heaviness too much to bare, and close my eyes. I can smell Maryanne on my sheets. I drift off to a dream about her still being there. With me."
    return
