##--------------------------
## Act 4: Maryanne
## Neutral route
## Scene 3: Talk with part 2
##--------------------------

label a4ns3:
    "I say nothing. I let her say what she needs to and I listen. Carefully. The most I do is nod softly."

    Maryanne crying "It’s not that I don’t like it here. I do. I have friends, and a future. But this is my past, my home … it’s everything to me. It’s where I come from. It’s what I am. It’s who I come from."
    Maryanne bigtears "And they’re just going to sit there and get older and older until one day … they just vanish? Just like that? I can’t take it! They’re good people; they don’t deserve to …"

    "She gulps and stops herself from getting too worked up again."

    Maryanne worried "… I want to see them. More than anything."
    Maryanne "I want to know they’re happy and okay. And I want to see them before … anything else happens."
    Maryanne "I hate that there’s an ocean between us, I hate that I need to fly for 15 hours just to be in the same timezone as them, I hate that they can’t come see me every and now then like everyone else’s grandparents …"

    Maryanne "My dad gives me free plane tickets sometimes. I could have gone years ago! I could have gone every summer! But I never did! I always put it off for one reason or another. I always had work or school or friends or some other excuse."
    Maryanne "It’s not too late … but it feels too late, somehow. All of that time … gone. That stupid photograph showed me everything I missed."

    hide maryanne with config.say_attribute_transition
    "She stands up and paces, energized as her frustration with herself for her past choices grows. She looks like she needs a cigarette, or a vacation. Or maybe just sleep."

    Shun "You’re carrying a lot on your shoulders. Again."

    Maryanne "I … need … to rest …"
    Maryanne "… I’m too tired to think right now."

    "She sighs softly, calming herself down. Her hands tremble and she’s barely able to keep her eyes open."

    show maryanne circles tangled sad with config.say_attribute_transition
    Maryanne "I’m gonna … lay down. We’ll talk later, okay?"

    scene bg hall_dorm_girls with bg_change_seconds
    "I nod silently again, and stand up. I leave, and close the door behind. I wait a moment, hearing Maryanne shut her bedroom door."
    "The hallway is quiet. Very quiet. Filled only with the sounds of my feet walking to the front door."

    return
