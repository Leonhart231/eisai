##--------------------------------
## Act 4: Maryanne
## Neutral route
## Scene 6: A nothing-special date
##--------------------------------

label a4ns6:

    scene bg dorm with bg_change_minutes
    "The morning rolls by uneventfully. I check my phone so many times, each time my heart sinking deeper into my stomach when I see she hasn’t called or messaged me or anything."
    show hideki formal crossed pissed at left with moveinright
    pause 0.5
    hide hideki with moveoutright
    "As I check for the 27th time, Hideki hurries out of his room, his recently-ironed blazer on. He sneers at me and leaves without another word."
    "Kevin peaks out of his own room the moment Hideki leaves. He sneaks into the kitchen like I don’t see him."

    show kevin sides neutral open with config.say_attribute_transition
    Shun "Where’s he going?"

    Kevin frown "Uh …"
    Kevin "… Second date I think."

    Shun "Wow, really?"

    Kevin serious crossed "I think. He’s not exactly talking about it. I’m just assuming."

    Kevin neutral "So … any word from the Egghead?"

    Shun "Not yet …"

    Kevin "Well … it’s one of those things, I guess … hey, maybe after you two work it out, you both … you know, night on the town?"

    Shun "Thanks Kev. I—"

    "My phone buzzes. My wrist flashes towards it, snatching it off the coffee table."

    Maryanne "{tt}Hey. Can I see you?{/tt}"

    "I respond with an emphatic “{tt}yes{/tt}” immediately. Kevin shakes his head. Not wanting to press the issue, I hurry over to the girl’s dorm."

    scene bg inou with bg_change_minutes
    "Maryanne sits on the bench near the center of campus. She finally looks rested, at least physically. But she also looks very pensive. Calm. She doesn’t turn her head towards me as I sit down."

    Shun "Hi …"

    show maryanne happy with config.say_attribute_transition
    "Then she turns. And she smiles … not saying hello back. She pats the bench softly, shifting her position a bit. I sit down slowly. I think I smell cigarettes on her under her usual fruity perfume. She gently rests her hand on top of mine."

    Maryanne uncomfortable "It’s been a rough …"

    "She stops herself, then looks away again."

    Maryanne uncertain "… I can’t remember the last time it wasn’t rough honestly."

    Shun "… I—"

    Maryanne sad "It’s not you … really. It’s not."
    Maryanne worried "I’m … just … not happy, Shun. I can’t remember the last time I was happy. And it’s really not you, I mean that."
    Maryanne crying "I’m … broken."

    return
