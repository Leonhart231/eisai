##---------------------------------------------------
## Act 4: Maryanne
## Neutral route
## Scene 6b: A nothing-special date — part 2: neutral
##---------------------------------------------------

label a4ns6b:
    Maryanne "You confused me long enough to think I was happy, for a little while."

    "Too tired to feel anything but a soft apathy, she lets out a massive sigh, like she just put down a very heavy weight."

    Shun "What are you saying?"

    Maryanne "I …"

    "Another glance away, another soft sigh like she hasn’t any energy left."

    Maryanne "… I don’t know what to do … but I know what I want to do. I’ve been thinking about it for a while now."
    Maryanne crying smalltears "Shun … I don’t think I can stay here anymore."

    "I feel a small knife sliding into my heart. The sensation was practically physical, and slow. It made me gulp."

    Shun "You mean … leave? Forever?"

    Maryanne notears "… I don’t know if it will be forever-forever … but … yeah."

    Maryanne sad "I want to go home. I love it here, I do. But I miss my home. I miss my grandparents. I miss my cousins, and the hills and the cars and the food and … I miss it so much!"
    Maryanne "I talked to my dad about it. He’s for it. He’s being supportive. He …"

    Shun "You’re … serious. This is happening?"

    Maryanne uncomfortable "… It has to. I’m so sorry I didn’t ask you but—"

    Shun "But what? You think that I wouldn’t mind?"

    "I feel my brow furl. But then I sigh, trying to calm down as I shake my head …"

    Maryanne sad "… I’ve … wanted to go back home for a while now. I’ve been talking with my dad and we got things … ready. Just in case. I hadn’t planned to leave until I was ready. Until my work was done, and I could transfer safely or maybe even finish early."
    Maryanne "Then … you showed up. And I had a reason to stay."
    Maryanne sad "I would go to bed crying at least once a week. Listening to songs my grandpa would play or looking at old pictures from when I was little and they were young."
    Maryanne "After I met you, I went to bed smiling every night. And for a while … I forgot why I was so sad."
    Maryanne worried "But then I saw that picture and saw how old my grandparents got … I have to go Shun, I have to. Please tell me you understand … please!"

    "Maryanne’s energy returns to her as she starts to nervously fidget. I cover my mouth, trying to process this. I wanted her to be happy, obviously. But … I also knew that what I was doing was not working. Sometimes it made things worse."
    "Maybe I really couldn’t make her happy and it was just how it was. Maybe I wasn’t ready, or didn’t have the ability to make any different. Maybe, maybe, maybe. My head felt heavy. And honestly, no matter I say, it doesn’t matter. She’s going …"

    Shun "… Yeah Maryanne. Of course I understand."

    show maryanne sad with config.say_attribute_transition
    play music "very_sad.mp3"
    "I say with a sigh. I get it, I really do. But it still means {em}I’m{/em} the one left behind. It doesn’t feel like she’s asking me for permission at all, merely getting my blessing, so why even bother asking if she’ll ever come back?"
    "Was it to make me {em}feel{/em} like I had a say in it? Of so that I wouldn’t be mad and she could leave without feeling guilty? … Why am I getting upset?"
    "Maryanne nods softly. She can obviously tell I’m not 100\% okay with it. There’s a very long silence between us. She anxiously plays with the seam on her pants while I look at the clouds overhead."
    "I wonder how long she’s been planning to go back in actuality. She must have at least spoken to her dad before this. Maybe her dad brought it up first. Maybe they had conversations in English right in front of me and I missed it."
    "So what was I then? Just a fling? A temporary boyfriend? An accident she didn’t plan on? I hoped I was the last one instead of the others. I don’t think she’d be mean enough to just use me, but now that I’m left high and dry I can’t help but think … was this ever serious?"

    Shun "When are you leaving?"

    Maryanne uncertain "… Tomorrow afternoon."

    Shun "… Was that planned in advance?"

    Maryanne "… I was going to fly out in a month. But after the … my dad bought me an earlier flight. I can’t wait anymore."

    "I bite my lip and nod twice. What she said and what I heard was different. I heard that I was not {em}really{/em} a factor in this decision. She had time to come to terms with this, she made this choice. And I’m only now comprehending it."
    "I’m only now starting to come to terms with being without her when she’s had weeks."
    "My chest started to hurt. Badly. I felt an undeniable loss for the time we {em}could{/em} have spent together. The what-if’s, the plans we made, the rest of the semester, the school year … this relationship wasn’t ending because I messed up or because she did something wrong."
    "It was just … ending. It felt like a death more than a breakup, like a terminal illness thrown into an otherwise happy year."

    Shun "Do you want to … I don’t know, get something to eat, my treat, one last time?"

    "Maryanne nods and stands up, and we both head to our usual spot."

    scene bg yamada with bg_change_minutes
    "The pain in my chest gets heavier. My body feels heavier. Maryanne can barely look at me as she stirs her chopsticks in her ramen bowl. She doesn’t eat, aside from a few weak bites every few minutes."
    "We’re not talking, listening to the sounds of the many customers working and patrons eating. I wonder what she was thinking but don’t have the energy to ask."
    "I wonder if she ever needed me, or if she was just using me to help get her through hard times. I wonder if she cared about me as much as she let on, or if I was just something to help her forget her problems for a while."
    "No, I’m sure she cared. “How much” was questionable."
    "Maybe I was just mad that she couldn’t be what I expected her to be …"
    "… Or maybe I was justified in feeling mad at her, for making me feel like we had a future and then ripping that from under my feet like it wouldn’t bother me."
    "But even though all of the frustration, the disappointment, and feeling like I was used … I still wanted her to stay."
    "Why now? Why did she have to leave {em}just{/em} as I was getting comfortable being myself? I had just started to really let her in, to let my guard down and allow myself to care about someone else."
    if called_someone:
        "I called my parents because of her. I got better grades because of her. My friendships were better because of her."
    else:
        "I was forgetting about everything from my old life because of her."
    "{em}I{/em} was better because of her."
    "I wanted to go back in time and enjoy the little moments more. If I had known she was going to up and leave without warning, I would have spent every second I could with her."
    "And yet, here I am, saying nothing … but what can I say? What could I {em}possibly{/em} say? “Don’t go, stay with me”, when we’ve only been dating for a few months. “Are you sure?” when she clearly is very sure she wants to cross an ocean."
    "Was she running away again? Was she secluding herself even more so to avoid things she doesn’t want to deal with? Was she acting too impulsively, or did she take too much time deciding to go?"
    "At a loss, I ask the only thing that truly worries me."

    Shun "Will I ever see you again?"

    Maryanne "… I don’t know."

    "That was the answer I expected. It wasn’t the one I hoped for."

    scene bg yamada_street with bg_change_minutes
    "I pay for the meal, though a petty part of me wanted to split the check. She rubs her elbow. She looks … smaller. Ashamed. And while I kind of want to encourage her out of instinct, I can’t find the strength to do it."
    "I’m tired. So tired. Tired of the stress and second-guessing. Tired of wondering when this would be easier."
    "Maybe it’s best she goes, my head says, only to be metaphorically slapped by my heart who screams “{em}Ask her to stay!{/em}” I wonder what I did wrong. If I was too nice, or not nice enough. If I was supportive or if I said something really dumb before."
    "Maybe we should have just stayed friends, or maybe this had to happen no matter what we did. Either way, it felt like it was partially my fault. It felt like everything was wrong and, because I couldn’t fix it, part of that was on me."
    "I wanted to magically turn off all of her problems, her worries, her anxiety would go away and she’d realize she was fine and wanted to stay with me … what I wouldn’t give for something that simple."
    "I wish she was what I thought she was before I got to know her."

    scene bg takuya with bg_change_seconds
    "Maryanne walked and I followed, staying behind her by a few feet. We went where she wanted to go, though we didn’t talk aside from a few short phrases to keep us on the right path."
    "Even as she was about to leave me alone with my frustration and loneliness, I wanted to spend every minute I could with her while I could."

    scene bg takuya_bridge with bg_change_seconds
    "The wind picked up as we walked over to the bridge in the middle of town, the one she hid under mere days ago in her darkest hour. She looked at the dark pit without moving for a moment, then walked up the street."
    "Leaning over the bridge, looking at her shadowy reflection next to mine in the cold river, she said nothing still."
    "For a second, I felt so angry that I wanted to scream. I was there for her darkest moment … and now she was going to say nothing in mine? This wasn’t fair. Not because she owed me, but didn’t I deserve to have someone help me when I needed it too?"
    "I was there for her every time she needed me. And it all went nowhere I guess."
    "I say nothing. My anger stays put. My lips stay sealed. I struggle to remind myself that she needs to leave for herself, fighting off any urge to talk badly to her or judge her … though it was very hard not to do that."

    show maryanne happy with config.say_attribute_transition
    Maryanne "Remember when you got us lost that one time?"

    "She smiled, trying to break the tension with a happy memory."

    Shun "I think it was {em}you{/em} who got us lost."

    Maryanne ecstatic "No! I remember you being the confused one."

    hide maryanne with config.say_attribute_transition
    "She snickers and shoves me playfully. I smirk softly, but don’t think it’s that funny. Maryanne’s smile disappears and she goes back to staring at the dark reflection of herself in the running water."
    "I take a few rocks from the side of the bridge and hand her one. She holds it weakly as I try to skip mine. She does the same."
    "We don’t say anything. We just skip rocks. Over the bridge where we found each other."

    return
