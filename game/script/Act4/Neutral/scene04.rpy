##--------------------
## Act 4: Maryanne
## Neutral route
## Scene 4: Phone home
##--------------------

label a4ns4:
    scene bg inou overcast with bg_change_seconds
    "A cool breeze flows out from behind the gloomy gray clouds. The sky drips … drips … drips softly, every few seconds a drop or two falling down."
    "I think about how worried Mr. Sawyer was, how worried I was … and how now everything feels … stagnant. Nothing is moving. Nothing is making a noise. Like we’re waiting for an ending we won’t like, that we have no control over."
    "I take out my cell."
    play music "very_sad_loop.mp3"
    menu:
        "Call Mom.":
            $ called_someone = True
            "I stop at “Mom”."
            "I look at the contact, my thumb hovering over the number for a minute or two. My stomach twisting a bit before finally pressing “Call”."
            "{snd}ring ring{w=2} ring ring{/snd}"

            Mom "Hello? Shun, is that you?"

            Shun "… H—hi … mom."

            Mom "Are you alright?"

            Shun "Yeah, I’m fine mom. I’m okay. I’m just calling to say hi."

            Mom "Well … good. I haven’t … I didn’t know what was wrong, you haven’t called in a while."

            Shun "Yeah. Things have been … busy here."

            Mom "Sure, sure."

            "A long pause bookends the awkwardness."

            Mom "… Your father and I haven’t spoken in a while either."

            Shun "I can imagine."

            "Another awkward pause."

            Mom "… Are you mad at me?"

            Shun "… Yeah mom, a little. I’m a little mad at you. And dad."

            "She says nothing for a minute."

            Mom "… I understand. That’s normal."
            Mom "… You’re welcome to come home any time you want. For just a visit, if you’d like."

            Shun "Ye—yeah. I know mom. I just have a lot to do here right now."

            Mom "… Of course you do. You’re a college man now. You have …"
            Mom "… You’re a man now."

            "I try very, very hard to not get emotional, but I have to pull the phone away from my mouth for a second."

            Shun "… I miss you Mom."

            Mom "I miss you too. Son."

            Shun "… Why did things happen the way they did? Why did any of this need to happen, Mom?"

            Mom "… I don’t know hun. I’m sorry they did. I’m sorry for everything …"

            Shun "Please don’t drink tonight. Please Mom."

            Mom "I won’t. I’m sorry."

            "I sit down near the dorm entrance and cover my eyes, trying to keep it together. For a brief moment, I wanted to hug my mother so badly that my stomach hurt."
            "Eventually, we both calm down enough to talk a little. I tell her about my roommates, my classes, my grades."
            "She asks if I was dating anybody, I tell her “no”."

            Mom "… I’m glad you called me. Thank you. But please, don’t wait so long next time. I miss hearing your voice."

            Shun "… I’ll talk to you again, soon mom."

            Mom "Yes, okay. Thank you Shun. I love you."

            Shun "… I love you too mom. I gotta go, we’ll talk later."

            "I hang up … smiling. I’m exhausted again, in a way that has nothing to do with my body being tired. But I’m happy. Happier than I was before the call."

        "Call Dad.":
            $ called_someone = True
            "I scroll through the contacts, past Mom, stopping at “Dad”."
            "My thumb hovers over the number for a minute or two. I put the phone away and grunt. I think about what I’m going to say. And when I can’t think of anything, I bite the bullet and press call as fast as I can, without thinking."
            "The phone rings."
            "And rings."
            "And rings."
            "I hear his voicemail and hang up. I’m partially relieved … mostly angry."
            "Seconds later, I get a call back and I feel a rock in my stomach."
            "I look at my phone for a few seconds. It felt like I was looking over the edge of a cliff. But I answer."

            Dad "Shun?"

            Shun "… Hi dad."

            Dad "Hello, son. H—how are you?"

            Shun "Um … busy. You know?"

            Dad "Good. Good. Busy is good. Keeping your grades up?"

            Shun "Y—yeah. All good this semester."

            Dad "Good. Excellent."

            Shun "…"

            Dad "…"

            Shun "… So … how have you been?"

            Dad "Oh, you know. The same. Busy too."

            Shun "Right. Of course. You know, busy."

            Dad "Yes … Shun … I wanted to ask you if you’ll come home between semesters?"

            Shun "Uh … not sure yet dad. I haven’t really thought about that."

            Dad "Okay. Sure. But we can figure that out if you’d like."

            Shun "I’ll … let you know."

            "After all this time of not talking, I don’t know what to say to this man. He knew why I was so defensive; at least I hoped he understood."

            Dad "It was good to hear you talk again. Son."

            Shun "… Yeah. Yeah dad. Same."
            Shun "… I gotta go. Sorry."

            Dad "Well, yes. Okay. I … I miss you."

            Shun "Yeah dad … I … I know."

            "I hear him clear his throat. I want to tell him that I don’t owe them anything after all that happened, and that I was mad at both of them. Instead … I hang up. I’m just tired. Tired in a way that had nothing to do with my body."

        "Call no one.":
            "I scroll through the contacts. I see dad. And mom. I look at the glowing screen for a moment, my thumb firmly planted on the side of the phone and not moving towards them."

            Shun "…"

            "I put my phone away. I don’t have anything left to say, to anyone."
            "I’m tired too from this. From all of this. I deserve better than feeling like … this."

    return
