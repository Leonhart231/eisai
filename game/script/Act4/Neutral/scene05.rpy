##-----------------------
## Act 4: Maryanne
## Neutral route
## Scene 5: Early morning
##-----------------------

label a4ns5:
    stop music
    scene bg room_shun night with bg_change_minutes
    play music "contemplative_loop.mp3"
    "I go back to my room and lay down. I feel like I’ve been awake for weeks."

    scene bg room_shun with bg_change_days
    "I wake up at 5am. Restless and only slightly rested."

    scene bg inou sunrise with bg_change_minutes
    "I go for a walk through campus."
    "Then a jog around it."
    "Then I run. I just run."

    scene bg takuya sunrise with bg_change_minutes
    "I take off across campus, into town."

    "I go as far as I can before needing to catch my breath, which is not very far. I take my time walking back to campus, looking at the shadowy windows and glimmering rooftops. I don’t think I ever took the time to look at them before."
    "I think about my parents. I think about things I said to them. And how everything feels … better now that they’re not here. I immediately feel terrible for thinking that. But it’s true. I’m not in that place anymore, even though this week has been hard to bear."
    "I think about Maryanne. And what’s going to happen. To her. And us … as if I didn’t know. I don’t think about it for too long. I want to look at how pretty the town looks in the early morning. For a minute. Just a minute, I want to just … enjoy how the world looks."
    "The day is going to be a long day. I can feel it."

    scene bg inou with bg_change_minutes
    "I see Kevin on the bench up on the hill, staring off into the distance like he usually does. He sees me walking over to him and sits up straight."
    "He sees me walking and nods in my direction. I nod back and walk towards him."

    show kevin serious crossed open with config.say_attribute_transition
    Kevin "Hey man. Up early?"

    Shun "Yeah, I guess. You too?"

    Kevin laugh closed "Ah, I can’t find the time to sleep. All those hotties are coming home from one-night stands they regret and I’ll be there to—"

    Shun "Yeah, yeah, okay."

    show kevin smile open with config.say_attribute_transition
    "He snickers and tosses something at me. I catch it before I see what it is … a small bag of candy, the kind Maryanne likes."

    Kevin neutral "Shit’s getting stressful …"

    Shun "Yeah … it is …"

    Kevin "… But … something’s happening."
    Kevin closed "You’re not in your room alone or wasting time on something pointless. You’re not thinking about shit that doesn’t matter. Something is happening. It’s better than Nothing happening, right?"

    Shun "… Um … I guess? I don’t know, Nothing at all might be better than Something bad. Something bad just makes things worse. I think I’d rather have Nothing."

    Kevin serious open "But then you’d never know what you were made of."

    "Kevin’s endless optimism was … annoying sometimes. I’m trying to feel bad about myself and he won’t let me!"

    Kevin frown sides "… So, uh, you gonna see Maryanne today?"

    Shun "Probably. I haven’t heard from her, but I’ll call her later."

    Kevin serious crossed "Cool. Cool."

    "He nods and, for the first time since I met him, bites his lip softly like he’s trying to not say something."

    hide kevin with config.say_attribute_transition
    "After a moment of hissing between his teeth and scratching the back of his head, Kevin stands up and nods. Half-smiling."

    Kevin "You’ll be okay man."

    Shun "Uh … thank you?"

    "He nods again, then heads out, going back to the boy’s dorm. There was no need to leave … I can’t help but feel he’s avoiding me."
    "I let him get ahead of me, then wait a few minutes before going back to the dorm myself."

    scene bg dorm with bg_change_minutes
    "Kevin’s door is closed. I hear him moving around behind, talking to someone in English. Probably a phone call."
    "I sit on the couch and put the TV on. Low volume. Not wanting to disturb either of my friends."

    Shun "… Hmm … friends …"

    "I say as I flip the channel, putting my feet up to relax. The bitterness is … less palpable."
    "I’m among friends."
    "I’m home."

    return
