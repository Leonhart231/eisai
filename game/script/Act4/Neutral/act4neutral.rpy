##----------------
## Act 4: Maryanne
## Neutral route
##----------------

label a4n:
    call a4ns3
    call a4ns4
    call a4ns5
    call a4ns6
    call a4ns6b
    call a4ns7b
    call a4ns8b
    call ending('neutral')
    return
