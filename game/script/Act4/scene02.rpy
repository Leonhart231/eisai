##---------------------
## Act 4: Maryanne
## Scene 2: Talk with …
##---------------------

label a4s2:
    stop music
    scene bg dorm with bg_change_minutes
    play sound "237729_flathill_rain-and-thunder-4" fadein 1 fadeout 10
    play music "contemplative_loop.mp3"
    "I check my phone. Many times. And half-heartily watch a dry comedy about a sarcastic old man visiting Tokyo flirting with a red-haired woman half his age."
    "I guess a movie like that would {em}have{/em} to be artsy. But it was good … it felt like the right thing to watch considering the circumstances."
    "Kevin stays away for a long time, it seems. Maybe giving me my space? Or maybe needing his own … I don’t have the energy to overthink it right now. I watch my movie, nibbling on a small bowl of carrots as it ends."

    show hideki crossed begrudging with config.say_attribute_transition

    Hideki "…"

    Shun "…"

    "Eventually, Hideki slinks out of his room. He stares at me for a few seconds in silence as he slowly turns the corner. When I say nothing, he cautiously pours himself a glass of water."
    "He stands in the living for another few seconds. I can hear him breathing, scowling softly as he decides if he wants to speak up."

    Hideki "…"

    Shun "…"

    Hideki chin think "… You …"

    Shun "… ?"

    Hideki "… You … are … not … as bad as … I first assumed you were."
    Hideki crossed pissed "… Of course, the  improvement between then and now is marginal. Practically microscopic. But … noteworthy. For example, there are many microorganisms that—"

    show hideki crossed glare with config.say_attribute_transition
    Shun "Thank you Hideki. I know that was hard for you."

    Hideki "…"

    hide hideki with config.say_attribute_transition
    "He turns sharply and goes back to his room. He doesn’t slam the door."
    "I sigh and pick the next movie in Kevin’s collection. Just as I narrow the decision down between a muscle bound man with a gun fighting an alien or muscle bound man with a gun fighting a robot, my phone buzzes."

    Maryanne "{tt}Hi. Free now. Can you come by?{/tt}"

    "I respond “{tt}yes{/tt}” and turn off the TV as I step out the door."

    scene bg inou overcast with bg_change_minutes
    "I see Mr. Sawyer walking out of the girl’s dorm, his head hanging low, his eyes heavy. He looks up at me and waves weakly."

    show george neutral with config.say_attribute_transition
    George "Shun … hi."

    Shun "Hello sir. Are you … leaving?"

    George "Not quite. I’m heading out tomorrow. I need to get back to the hotel for now. My sleep schedule … hasn’t been great lately. I’m not used to being up this “late”."

    "He pauses, looking down at the rain puddles. He rubs the spot under his eyes and forces a smile."

    George "… Thank you for watching out for her so far."
    George "Please call me. If anything happens. I mean, you know … if you need anything."

    hide george with config.say_attribute_transition
    "I nod. He walks away slowly, down the walk way, very deep in thought. After a moment, I see him answer his phone and his voice changed from exasperated to professional instantly."

    scene bg hall_dorm_girls with bg_change_seconds
    "Shione walks down the hallway, frowning, scratching her head a bit."

    show shione hips pompous with config.say_attribute_transition
    Shione "Shunie … Maryanne’s all upset. But she told me not to tell you anything yet because … well, um … because. You know?"

    Shun "Yeah, I know. I’m on my way in now."

    Shione "You …"

    "She points to my forehead."

    Shione finger pompous "… I like you. So don’t not be a friend later."

    show shione hips wonder with config.say_attribute_transition
    "She pouts, stamps her tiny foot down softly, and gives me a look. I guess she’s trying to look threatening. Still, her warning fills me with anxiety. I don’t like where this is going."

    Shun "I’ll be around, sure."

    hide shione with config.say_attribute_transition
    "Shione huffs and marches away, her flip-flops clapping on the floor as she scurries away."

    scene bg dorm_maryanne with bg_change_seconds
    "The door is left partially open for me. I slip inside, and  Maryanne is sitting in her living room. She has that accursed box from California open next to her."

    show maryanne circles tangled happy with config.say_attribute_transition
    Maryanne "Hi …"

    "She’s vastly calmer now, though still looking like she’s been through hell. At least she’s smiling again."

    Shun "Hi."

    Maryanne embarrassed "… I’m … really tired."

    Shun "I can … come back. You know, later."

    Maryanne uncomfortable "I’m okay. For now. We really need to talk."

    "Trying not to gulp, I close the door behind me and sit next to her."

    Maryanne worried "I … owe you an explanation …"

    Shun "No. You don’t. I think I know what’s going on."

    menu:
        "Your grandparents are not well.":
            Shun "It’s your grandparents, right?"

            show maryanne uncertain with config.say_attribute_transition
            "Her eyes widen a bit, and she moves away from me slightly."

            Maryanne "How did … wait, what do you mean?"

            Shun "They’re not well, right? Are they sick?"

            Maryanne "Well … no."

            Shun "No?"

            Maryanne "They’re not … sick … they’re fine. Happy, even."

        "You got bad news from home.":
            Shun "You got bad news from home."

            "Maryanne stares at the box for a moment, sighing softly once before looking at me again."

            Maryanne uncomfortable "No. Not exactly."

        "You’re homesick.":
            Shun "You’re homesick."

            show maryanne happy smalltears with config.say_attribute_transition
            "Maryanne smiles and turns her head. She tries not to cry, I can see it. I think I hit the nail on the head there."

        "Worried about passing this year.":
            Shun "You’re worried about not passing this year."

            Maryanne mad "No, not that. I mean, I am, but … not. That’s not … really what this is about."

            "She gets noticeably frustrated and sighs with a soft growl."

    hide maryanne with config.say_attribute_transition
    "Maryanne reaches into the box and pulls out the photograph of her grandparents. She picks it up delicately, like it’s made of glass."

    show maryanne tangled circles worried with config.say_attribute_transition
    Maryanne "My grandparents … look at them. They’re … old."

    Shun "Uh … yeah?"

    Maryanne "They didn’t look like that the last time I saw them. It didn’t feel like it was that long, you know? They were so much … healthier … before. They’re getting older. A lot older … without me."
    Maryanne "I could be spending time with them. But instead, I’m working halfway around the planet. I kept saying I was going to see them one day again. I kept saying I would go see them eventually. And that was years ago."
    Maryanne crying "Some days it makes me feel a little guilty. And some days … it eats me up inside. But … what can I do? Really, it’s not like I can just go visit for a weekend."

    "I get the urge to give her advice, or to pass on advice Higa-sensei gave me. Or Okada. Or even Kevin. But I also remember that I sometimes say more than I should, or the wrong thing. Maryanne looks so exhausted, weak … she needs something."

    return
