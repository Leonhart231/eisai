##----------------
## Act 4: Maryanne
##----------------

label a4:
    call act_transition("4")
    call a4s1
    call a4s2
    menu:
        "Say nothing.":
            call a4n
        "You should go soon as you can.":
            call a4g

    return
