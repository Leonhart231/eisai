##-------------------------
## Act 4: Maryanne
## Good route
## Scene 9: Goodbye - true
##-------------------------

label a4gs9:
    "She carries her last bag onto the bus. We don’t look away from each other, and she sits down by the window nearest to the front."
    "I gulp. I look at her through the window and think of everyone waiting back for me at the dorm. In my classes. My parents who were expecting me to call them at some point."
    "Then, quickly, I knock on the door. The bus driver looks annoyed at me."

    Bus "Sir … I’m sorry, but we have to keep schedule."

    Shun "How much is a ticket?"

    scene bg bus airport
    show maryanne uncertain
    with bg_change_seconds
    Maryanne "What are …"

    "Maryanne watches as I pass the driver my credit card. The one my parents gave me for emergencies. The driver warns me about over-charges due to the short notice, and I nod."

    Shun "Do you mind?"

    Maryanne "I … but … the airport is hours away."

    Shun "So?"

    Maryanne "…"

    show maryanne happy with config.say_attribute_transition
    "Maryanne sits back down and moves over. I sit down next to her as the bus doors close and the heavy metal box lurches forward. I’ll have to eat instant ramen for … a while. Until dad sends me more money. But I’ll get to spend a few more hours with her."

    scene bg bus
    show maryanne happy
    with bg_change_minutes
    "We pull away from campus, then out of town. Then far down the road, past the city’s limits, among the trees and mountains."
    "We don’t say much; we just look out the window, at everything going by."
    "We pass over a bridge, the bus bouncing up and down softly. She squeezes my hand and I put my arm over her shoulder."

    Maryanne neutral "I wish it all wasn’t … so far away."

    Shun "Yeah. I’d come visit if I could."

    Maryanne uncertain "… I know."

    Shun "Maybe I could."

    Maryanne happy "Heh. Sure. Maybe."

    Shun "Yeah. Just visit for a week or two, just to catch up."

    Maryanne ecstatic "And I’d show you California!"

    Shun "And your family. And we could be together."

    Maryanne crying "Just for a bit longer … you know that right? No matter what, it will always be for only a bit longer."

    Shun "True. But we’d still have that time."

    Maryanne happy happybigtears "If only …"

    "The trip goes on and on and on, for hours. The metal box stops every twenty minutes, letting more passengers on or off before heading onward again."
    "We talk about the things we said we were going to do. The things we’d go see together, when she would come back … we talk about times when everything will be okay … and what we would do without each other."
    "We talk about “later” like it’s only a few weeks away."
    "She tells me her dad really liked me. And how she thought about me in her classes. I told her about the lady I bought her bracelet from. And my home town. And, at last, I told her about my mom and dad. Not a lot, but more than I did before."
    "We let ourselves talk about what it all would have been like …"

    Bus "Attention passengers. We will be arriving at our final destination in 5 minutes …"

    "… And then … it’s over. Reality comes right back."

    Maryanne sad "Well … this is it."

    Shun "Yep …"

    "A moment of silence passes between us …"

    "For just a moment, I think about the credit card I still have …"

    show maryanne crying bigtears with config.say_attribute_transition
    "Then she throws herself at me, her arms wrapping around mine and mine hers. She mumbles apologies, weeping madly again. I cry too. I hug her so tightly that I worry I’ll make her burst. She does the same thing back."
    "She pulls away after a few moments, wiping her eyes dry."

    Maryanne "I need to leave."

    Shun "I know."

    hide maryanne with config.say_attribute_transition
    "She picks up her bag, and walks away, looking back my way as she slowly moves towards the bus’ door."

    scene bg inou
    show maryanne coat happy
    show flashback
    with Fade(0.5, 0, 0.5, color="#FFFFFF")
    "Suddenly, I’m back to my first day on campus, when we first met. She’s smiling, coming towards me from the science building."
    show maryanne ecstatic with config.say_attribute_transition
    "She was just some stranger … who would have known?"

    scene bg bus
    show maryanne crying smalltears
    with Fade(1.0, 0, 1.0, color="#FFFFFF")
    Shun "Maryanne, wait!"

    show maryanne confused with config.say_attribute_transition
    "She turns back to me, standing in the middle of the closing doors."
    "I think of the credit card again …"
    "I gulp, and ask her something …"

    return
