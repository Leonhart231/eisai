##-------------------------
## Act 4: Maryanne
## Good route
## Scene 8a: Goodbye - good
##-------------------------

label a4gs8a:
    stop music
    scene bg inou_bus overcast with bg_change_minutes
    play music "contemplative_loop.mp3"
    "I wanted to go with her. And I wanted her to leave and never come back at the same time. I was mad at her, even though I knew that was “wrong” of me. And I wanted her to be safe. To be okay."
    "I couldn’t help but feel … that I had made things worse. A nagging feeling that she should have left a long time ago. That I was the reason it got this bad."
    "But if she did … I wouldn’t have had these memories. One I would keep forever. Of her."
    "The sky is dark and cloudy. Cold. Lonely. I walk out of campus, sighing as I look at the bench Maryanne and I sat the day before."

    show kevin serious crossed open with config.say_attribute_transition
    "Kevin is already waiting by the bus stop. When he sees me carrying Maryanne’s bags, he runs over to help, not saying a word. He quickly gathers the pink and sticker-clad luggage in a pile, then pats my back once."
    "And we wait."
    "And wait."

    Shun "Maryanne is saying goodbye to Shione. She should be here … you know … soon."

    Kevin "Cool dude. When she gets here, I’ll give you both your privacy."

    Shun "Thanks Kev."

    "More silence. More cold wind. The sky rumbles very softly."

    Kevin serious "And I’ll be home all day. All night. Don’t be alone today. Do yourself a favor, don’t be alone today."
    Kevin "And … well …"

    "He trails off. I know he’s trying to help. And maybe it will help, later. But what else could we say right now? What else {em}can{/em} be said?"
    "He kicks a pebble away from us and I sit motionless. Barely able to feel anything, except the imaginary hole in my chest."

    Kevin neutral "… It was nice while it lasted."

    Shun "… Yeah. It really, really was."
    Shun "I … was happy."

    Kevin "You had every right to be. Don’t tell yourself you didn’t."

    Shun "Thanks Kev …"
    Shun "… It still … sucks."

    Kevin serious closed  "Yeah. The End … always sucks. They never show that part in movies, you know? Like, we get a title card that says “The End” but never see the actual final parts. I guess it’s cause the story is what matters, when you wanna feel good."

    Shun "You watch too many movies."

    Kevin neutral open "Maybe you watch too little. The story’s not over yet, Shun. It’s just … The End. Of this one. For now."

    "I go silent for a minute to digest what he said. He was being an optimist I suppose. I guess I needed it."
    "There is another long silence between us. Kevin, thankfully, knows to not talk so much right now."
    "The bus pulls up. The driver opens the door and politely greets us."

    Bus "Hello young sirs. We’ll be leaving in fifteen minutes on the dot. Do you need help loading bags onboard?"

    scene bg inou_bus overcast with bg_change_minutes
    "Eventually, Maryanne appears, with Shione nowhere to be seen. Kevin turns his head towards her as she walks on the bus."
    "He slowly walks over to Maryanne then. They talk in English. I pick up on a few words. “Lonely”, “travel”, “miss you”, “please call” … then they switch languages again. I assume they were previously talking about me."

    show kevin neutral sides open at leftish
    show maryanne sad at rightish
    with config.say_attribute_transition
    Kevin "Call when you land. Call before you leave. And be careful. I hear California is a dangerous place with all of those free drugs and great schools. Don’t, uh, don’t pick a fight with anyone you can’t outrun. And if you see a guy in a trench coat, he’s not selling candy."

    Maryanne happy "Oh Kevin …"

    Kevin "…"

    Maryanne crying smalltears "… Oh … Kevin …"

    "They hug each other {em}very{/em} tightly. Kevin rubs the skin under his eyes as she pulls away."
    "Before he steps off the bus, Kevin pats my shoulder and sighs softly."

    Kevin "I’ll leave you to it then …"
    Kevin frown "Don’t be alone tonight."

    hide kevin
    hide maryanne
    with config.say_attribute_transition
    "They say nothing for a minute. Then they hug. Tightly. So tightly that he lifts Maryanne off the ground for a second. He puts her back down, and quietly leaves, rubbing the underside of his eye."
    "Maryanne approaches me, her last bag underarm."

    show maryanne crying with config.say_attribute_transition
    stop music fadeout config.fade_music
    Maryanne "Well … this is it."

    Shun "… Yeah. I guess so."

    "The sky rumbles softly. We can’t look at each other. Part of me wants to beg her to stay. I smile, knowing that it wouldn’t do anything at this point. Maryanne waits for me to say something, nervously fiddling with her fingers."

    menu:
        "Wish her luck, tell her she’s brave.":
            play music "eisai.mp3"
            Shun "You’re really brave for doing this."
            Shun "I’m going to miss you a lot but … this is a big step. It’s a huge step. And it’s for you. And it’s the right thing to do. You’re really brave for doing it."

            show maryanne worried with config.say_attribute_transition
            "Maryanne gulps, struggling to hold back her tears."

            Maryanne "… Thank you."

            hide maryanne with config.say_attribute_transition
            "She slowly moves closer to me, and I open my arms. I hold her softly, her hands on my chest as she sniffles and breathes heavily. I hug her as hard as I can."

        "Tell her you’ll miss her everyday.":
            play music "eisai.mp3"
            "I put my hand on her shoulder softly."

            show maryanne bigtears with config.say_attribute_transition
            Shun "I’m gonna miss you every day."

            hide maryanne with config.say_attribute_transition
            "Maryanne quickly pushes herself on me, hugging me so tightly that it hurt a little. I hug her back, sighing as she squeezes my ribs."

    "I hold onto her and memorize every little detail of this moment. The exact smell of her perfume. The softness of her hair. How warm she felt. Every second is counted, each dragging into the next far too quickly."
    "We could have held each other much longer before this … why didn’t we? Why didn’t we just spend every minute we could with each other? Why didn’t we hold each other this tightly every day?"
    "Her fingers squeeze my shoulders softly. I can hear the thunder rumbling up above."

    Bus "I am sorry ma’am, but we have to keep on schedule. We need to leave soon."

    "Maryanne lets go and picks up a bag, then hugs me again, her slender arm wrapping around my neck tight enough to hurt."
    "She wipes the tears out of her eye and tries to pull herself together. Her classic forced smile flashes my way. I’d miss those too."
    return
