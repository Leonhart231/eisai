##------------------------------
## Act 4: Maryanne
## Good route
## Scene 7a: Night Before - good
##------------------------------

label a4gs7a:
    scene bg inou night
    show maryanne sad
    with bg_change_hours
    "We ran out of rocks to throw a little while after the sun went down. Then we go back. Our fingers laced. Her head on my shoulder."
    "She’s smiling. I’m fine."

    Maryanne worried "I … need to pack for the night."

    Shun "Yeah, okay. Long day tomorrow."

    Maryanne "I’m leaving tomorrow morning at 7."

    "I’m fine I’m fine I’m fine I’m fine I’m fine I’m fine I’m fineI’mfineI’mfineI’mfine{i}I’mfineI’mfineI’mfine!{/i}"

    Maryanne uncertain "Will you see me off?"

    Shun "Of course. I wouldn’t miss it."
    Shun "Go, rest up. I’ll see you tomorrow."

    Maryanne happy "… Thank you. For today. It was the best date we’ve had."

    hide maryanne with config.say_attribute_transition
    "I hold both of her hands and kiss her forehead. She sighs and moves in close. And kisses me. Slowly. Passionately. I embrace her and kiss her back, drowning in the sensation of connecting with the girl I love. She breaks the kiss but stays in my arms."

    Maryanne "Do … you want to … come inside? For the night?"

    "Without a word, I follow her to her dorm."

    scene bg black with bg_change_minutes
    "Her bags were already packed."
    "The lights stay off."
    "We can’t stop touching each other. I’m on top of her one minute, and she’s climbing on top of me the next. Our lips stay locked as we both hold each other, like we needed to, like every second and every sensation matters more than anything."
    "For an eternity, for an instant, we were there. Connected. One."
    "There was so much heat. And sweat. And music coming from both of our open mouths as we shared each other’s breath. And it was perfect."
    "She was perfect."
    "I stay in her bed, in silence, with her nestled under my arm. We both looked out the window, not saying a word. Every half hour, we’d kiss again. And make love again."
    "We were the only two people in the world for a little while, holding each other in stillness and in red hot passion, until we both fell asleep on top of each other."
    "And it was perfect."

    scene bg room_maryanne with bg_change_hours
    "We’re both tired the next morning. For more than one reason. But now, as we both get dressed without a word or even a glance, that perfect night feels like it happened months ago. The day of her departure is here."
    "She’s leaving."
    "Hugging her from behind, I kiss her cheek lovingly. She holds my forearm and kisses me back, leaning her head past her shoulder."
    "I hold her to try to get her to stay longer. She pulls away, getting her things. I help her, silently dragging each bag into the living room for her."

    show maryanne happy happybigtears with config.say_attribute_transition
    Maryanne "… I love you."

    "She says softly as I pull my shirt back on. I kiss her, and say it back in her ear as softly as I can."
    return
