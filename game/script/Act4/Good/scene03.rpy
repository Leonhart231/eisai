##--------------------------
## Act 4: Maryanne
## Good route
## Scene 3: Talk with part 2
##--------------------------

label a4gs3:
    show maryanne worried with config.say_attribute_transition
    Shun "Maryanne … if you’re worried about lost time … if it’s worrying you {em}this{/em} much … you might need to do something about it and …"
    Shun "Even if they are fine, health-wise, but you just want to see them … maybe you should go. Like right away, even if it’s only for a week or two."

    "Maryanne looks down at the package again. She says nothing for a minute. Then gulps."

    Maryanne uncomfortable "That’s not … exactly what I’m saying …"
    Maryanne uncertain "I … have been thinking about going back. A lot. I spoke to my dad about it."

    Shun "There you go, see? So what did he say?"

    Maryanne "He said that, if I wanted to go, I could. He can pay for a flight. He even gets me free tickets every now and then so it wouldn’t be a problem … god, he gets free tickets. I could have gone anytime before. I just didn’t. Things kept getting in the way."

    Shun "Work will be here when you get back, you know? And school. And me!"

    show maryanne worried with config.say_attribute_transition
    "She pauses again, then clears her throat."

    Maryanne "Shun … I don’t think you understand what I’m saying. I want to go back."

    Shun "Yeah, I know. You need to see them and—"

    Maryanne "No. Shun. Listen."
    stop music fadeout 0
    Maryanne "I want to go back. And stay there."

    "My stomach sinks. The pieces start to come together rapidly. Nights where she felt torn between two worlds, days when she panicked to get all of her work done on time, hurrying to finish this and that."

    play music "contemplative_loop.mp3"
    Maryanne "This isn’t impulsive. I’ve been thinking about it for … months. Maybe longer. Like … before we met."
    Maryanne uncomfortable "I was almost ready to leave, honestly. But … something … made me … drag my feet, for a while."

    "Her eyes dart at me, back and forth a bit, and she scratches the back of her head. I get it. She has a boyfriend now. All the way over here, on this side of the planet. I had made things more complicated."
    "I try not to panic. My stomach swells with fears of being alone, memories of all the good times we had, and the bad times, which seem suddenly smaller."
    "I think of myself … and then I think of her and all the stress, the nights she cried, the times she pulled away and got mad. She wasn’t mad at me. Now I see it, clear as day. She was ready to leave … then I showed up."

    show maryanne uncertain with config.say_attribute_transition
    "I lean forward slightly. Maryanne nervously stares back. Terrified of what I was going to say next."

    Shun "… Maryanne … if you {em}need{/em} to do this, then you need to do this. You’re not going to feel better if you just stay here. This means a lot to you. If it’s something you want to do, you might have to do it, for yourself."

    Maryanne crying smalltears "I’m so, so sorry!"

    hide maryanne with config.say_attribute_transition
    "She hugs me, her tears flooding out like they’ve been waiting to pour down. I try to stay strong, try to hold back everything I’m feeling. And I do. For now. For Maryanne."
    "After a minute, she rests her head on my shoulder, weeping softly as she calms down."

    Shun "When are you leaving?"

    Maryanne "… Two days."

    "An invisible knife plunges in my heart."

    Maryanne "We … moved up the move date after the party. I’m going to stay with my grandparents for a bit, and finish my classes online or at a school nearby. I only need another semester and a few courses so I’ll be done in … no time."

    "I nod, struggling to hold everything back. Part of me wants to yell at her, scream “this isn’t fair”. And it {em}isn’t{/em} fair. None of this is fair. But just when I find the nerve to talk back to her, I think about how incredibly difficult this decision must have been for her."
    "How much she clearly suffered because she struggled to tell me. We talked about everything, except this."
    "I put my hand on hers. She squeezes my fingers gently. Her skin is so smooth and warm … I don’t know why I’m noticing it now, instead of every blissful second I was with her before this."
    "She leans her golden head on my shoulder. I stroke her hair. There’s not much to say right now, I guess."

    Shun "You’re carrying a lot on your shoulders. Again."

    show maryanne sad tangled circles with config.say_attribute_transition
    Maryanne "I … need … to rest …"
    Maryanne "… I’m too tired to think right now."

    Shun "Go rest. Lay down. We’ll talk later, okay?"

    scene bg hall_dorm_girls with bg_change_seconds
    "She nods, then stands up. I leave, and close the door behind me. I wait a moment, listening for Maryanne to shut her bedroom door. When I hear it, I leave."
    "The hallway is quiet. Very quiet. Filled only with the sounds of my feet walking through the front door."

    return
