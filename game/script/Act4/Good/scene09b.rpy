##--------------------------
## Act 4: Maryanne
## Good route
## Scene 9: Goodbye - good 2
##--------------------------

label a4gs9b:
    Bus "I’m … sorry ma’am. But …"

    show maryanne crying with config.say_attribute_transition
    Maryanne "I’m sorry … goodbye. I love you."

    Shun "I love you too."

    hide maryanne with config.say_attribute_transition
    "The bus door closes and I step back, not looking away from her through the window. She looks back, crying so much that I can see her face turn red."
    "The bus lurches forward and I still watch. She looks at me for as long as she can. I watch the heavy vehicle trudge down the road, to the corner, and watch it turn … disappearing from my sight."
    "I stay put. Watching the corner. Like the bus would come back. Like it would break down and she couldn’t leave until tomorrow. Or that she’d order the bus to stop and she’d run back here, and pick me over … {em}herself{/em}. I realize how silly and selfish that thought is."
    "After a while … I wasn’t sure how long … I went into town. For a walk. A long walk."

    scene bg takuya_far night with bg_change_minutes
    "I pass the spots Maryanne and I used to go to. I pass the mall, the bridge, the fountain. They all feel incomplete, like a puzzle someone gave up on."
    "I don’t feel … broken as I thought I would. I miss her so much that it hurts, but … she’ll be okay. I believe that, now. I have to believe that."

    scene bg takuya_far with bg_change_minutes
    "{snd}Bzzz{/snd}"

    Maryanne "{tt}I'm at the airport. I'll text you when I land. I'll text you every day!{/tt}"

    "I smile … and it surprises me. I {em}can{/em} smile. I feel okay. Or rather, that I’ll be okay, later."

    Shun "{tt}Have a safe flight!{/tt}"

    "I hesitate before pressing “Send”. I wonder if there is anything else I want to say to her. Part of me does. Part of me is mad that everything ended this way, or that she had to leave. Was I bitter? Maybe a bit. But I wasn’t broken."

    Shun "{tt}Have a safe flight! Take pictures in the air! Be well, love you.{/tt}"

    "And I meant it. I {em}did{/em} love her. Even if it wasn’t going to work out. Maybe it was doomed from the start, I wonder."
    "But it still happened. I still got to know her. She was, for a moment, part of my life. And even though the feeling of loss was there, I wouldn’t trade those memories for that feeling to go away."
    "I walk back home. To my friends. I feel lighter, and heavier, at the same time somehow."

    scene bg dorm with bg_change_minutes
    "Kevin is waiting for me. He has a pile of movies and video games on the coffee table, all unopened."
    "I look at them, then at him."

    show kevin serious sides open with config.say_attribute_transition
    Kevin "… You okay?"

    Shun "… I mean, no. But, yeah. You know?"

    Kevin frown closed "Yeah, I know. I know."

    Kevin open "Do you remember when we first met?"

    Shun "Yeah, you dragged me across town and yelled in my ear all day about how great you were."

    Kevin smirk "Haha … yeah. Yeah, I guess I did do that. I’m sorry. I was just nervous."

    Shun "Ne—nervous? You?"

    Kevin serious "Well, we didn’t know each other and it’s always weird meeting a new roommate …"

    "The irony of this statement takes me back."

    Kevin "… I get it Shun. I know what “alone” means. I crossed half a planet and left everything I’ve ever known … to come here. My home is like 15,000 miles away."
    Kevin closed "You could visit home if you take a bus ride, but I’m like … alone-alone."
    Kevin open "I don’t know, I guess when you came here, I didn’t want anyone else to feel that way. If I could help it."
    Kevin smirk "You’re not {em}that{/em} alone Shun. Even if it hurts right now."

    Shun "Thanks Kevin."

    show kevin smile with config.say_attribute_transition
    play sound "412157_chancelombardo_high-five"
    "He smirks softly, and holds up his hand. I sigh softly, and high-five him."
    "Kevin clears his throat but before he can say more, he is interrupted by the sound of Hideki’s door opening."

    show kevin neutral open at leftish
    show hideki crossed pissed at rightish
    with config.say_attribute_transition
    "Hideki robotically walks into the living room, glancing at me with his usual grumpy scowl. He looks at the pile of movies briefly and then, sneering, sits down."

    Hideki "… Today would be made slightly less awful with a Skull Girls marathon. And perhaps, a film that isn’t unbearable moronic dribble."

    Shun "… Dibs on Cerebella?"

    Kevin smirk "I got the nurse with the big rack."

    Hideki armsup argue "Of course, of course! Why do anything unique, dog? Of course you pick the most obvious fighter, and based only on your lesser impulses no doubt. Defeating you will be a sweet victory in the name of decency and self-control, which will be the primary tools I will use to best you!"

    Kevin "Sure Lancelot, but it’s your turn to buy the beer."

    Hideki glasses adjust "Hmph. Fine. I shall be back shortly. Both of you give me money!"

    Kevin mad sides "For what? You’re paying for the beer!"

    Hideki "For snacks and dinner and—"

    show kevin neutral with config.say_attribute_transition
    Shun "I got it."

    hide hideki with config.say_attribute_transition
    "I hand Hideki some bills to end the expected bickering. He counts it quickly, then huffs … and leaves in a hurry. It seems that he wanted to get back fast."

    Kevin smile "This’ll be a fun night!"

    Shun "Yeah. I hope so."

    stop music fadeout config.fade_music
    "I sit down and lower my head. Kevin says nothing. The room falls very quiet."
    "And it hits me. It {em}really{/em} hits me. I was fine a second ago but suddenly it feels so … heavy. Like a thousand bricks on my shoulders all at once."

    Shun "… She’s gone Kev."

    show kevin neutral with config.say_attribute_transition
    "I force myself not to get worked up. Kevin says nothing as I turn my head away from him. He pats me on the shoulder, and I quickly get up and go to my room."

    Kevin frown "Don’t be alone tonight!"

    play music "unrest_loop.mp3"
    Shun "I won’t. Just … give me a minute."

    scene bg room_shun with bg_change_seconds
    "Palm covering my brow, I close my door. My room smells a little like her. The tears flow out gently."
    "I look at my phone. Maryanne replied to my last message. She sent me a picture of her on the plane, smiling at me, the window next to her. The caption says “Leaving home for home”."
    "I put my phone down. I don’t want to reply just yet. I see a picture of Maryanne on my dresser, and walk to it. Looking at it makes the pain worse, somehow. I look at her gorgeous hazel eyes. Then, after a moment, place it face-down back on the dresser."
    "I sit on my bed and under my breath, I say her name. And hope that she’ll remember me."
    stop music fadeout config.fade_music

    call ending('true')

    stop music
    scene bg black with Dissolve(3)
    centered "Four months later"
    scene bg dorm with bg_change_minutes
    window show
    play music "happy_hangout_loop.mp3"
    "Kevin paces in the kitchen, reheating a jar of instant ramen and pretending the microwave was a timed bomb. I’m reading on the couch, trying to ignore him."

    Kevin "5 … 4 … 3 … 2 … ooo, stopped it at 1! We did it Shun … we saved the president."

    Shun "Ok Kevin."

    show kevin sides open laugh with config.say_attribute_transition
    "Kevin sits down and blows on the streaming cup in hand. He grabs the remote and flips through the channels."

    Kevin smile "What’s on the agenda tonight?"

    Shun "Nothing. I’m thinking of heading out, I don’t know. Maybe the bar?"

    Kevin neutral "Hideki’s got a cord that connects to his computer. We can stream stuff … I think. I don’t know where he gets anything but it’s an option."

    Shun "… I’ll think about it. I really feel like stretching my legs tonight."

    Kevin "Sure dude. I’d offer to grace you with my presence at said pub, but I need to hit the books tonight."

    Shun "As in, textbooks?"

    Kevin smirk "Oh yes. Okada said he wanted me to work on a project or something, I don’t know. I’ll look at the topic later. Either way, I’m staying in tonight."

    "Hideki’s door opens and shuts quickly. He walks out, not saying a word, not looking at either of us."

    Kevin crossed "Hey dude, can we use your computer?"

    "Hideki tensed up, then sighs."

    show kevin at leftish
    show hideki crossed pissed at rightish
    with config.say_attribute_transition
    Hideki "You … you keep your grease-covered fingers off my … property."
    Hideki "Ahem … please. … I … will … be … unavailable … this evening."

    Kevin serious closed "Ah. Another cult meeting?"

    Hideki armsup argue "Don’t touch my stuff!"

    hide hideki
    show kevin smirk open
    with config.say_attribute_transition
    "He hurries back into his room, slamming the door. He had been attending more of those arranged dates lately, after his parents threatened to cut him off. Hideki had been oddly quiet the last few weeks. Still grumpy, but less hostile."
    "That might be the closest Hideki {em}could{/em} get to being happy … happi{em}er{/em} than usual anyway."

    Kevin smile crossed "That guy thinks we don’t know, but we know."

    Shun "Just don’t mock him."

    Kevin smile "Oh please, when have I {em}ever{/em} mocked that sunlight-intolerant goblin?"

    Shun "Right. What was I thinking … you know what, I’m gonna go out now. I can finish this book later."

    Kevin "Sure man. I’ll see you later."

    scene bg room_shun with bg_change_seconds
    "The past few months felt oddly like mourning. I had found things to fill the empty space she left. Kevin and Hideki were spending more time together, going to bars and meeting new people. I’d tag along sometimes."
    "Risa would come sometimes, often at Kevin’s invitation. She’d get us beer-tickets when we went to her concerts."
    "I had gone hiking alone on weekends. I tried cooking more … with still questionable results, but nothing burned anymore. Shione even lets me come with her on her diving trips! … I haven’t gone {em}in{/em} the water yet, but it’s nice to be out on a boat every so often."
    play music "eisai_loop.mp3"
    "And I started talking to my mom more. Usually once a week. My dad, not as often much … but he’s trying. {em}We{/em} are trying. I say hi to him briefly in text messages every so often."
    "I’m considered going to visit them next time I have off … but if I didn’t do that, that’d be fine too. For me."
    "I still wasn’t … great, when I’m honest with myself. There were nights Maryanne’s absence hurt tremendously."
    "Maybe I’d find something she left behind, scroll through a picture of us on my phone, or Shione would bring up something we all used to do together … and suddenly a brick was pushing down on my chest."
    "Then one day, in the middle of wishing things were different and possibly after a few beers, I had this epiphany."
    "Maryanne did what she did for herself. It wasn’t about me. Ever. I was part of her life, for a bit, like she was to me. But she kept going, not because she wanted to hurt me but because she felt that she needed to do what was right for herself."
    "And the same thing was true from my perspective. She was part of my life, when she was there. She wasn’t here anymore but I still had to go to classes, still had a lot going for me socially. I still had reasons to be happy."
    "And after that realization, things got … easier. Not better, and not right away, but the loss felt … lighter. Easier to carry."
    "So I called my mom. And went on a boat with Shione. And went to more concerts with my friends. It wasn’t the same without Maryanne being there, obviously."
    "But it was still … okay. I was okay. One day, like a storm clearing up over time, I realized … I was okay. And that was enough … for now."
    "The weight of it all was still there. It was heavy. But I was getting used to carrying it. Day by day, it got a little easier. But it wasn’t good. Or bad … it was hard to explain."
    "I felt like a cracked cup that was well repaired; tougher than before, but frankly still damaged. It wasn’t good or bad. It was just what it was."
    "It’s been months now. And the heartache itself doesn’t sting so much anymore. What gets to me is the “what could have been” thing. I get somber when I get the nerve to look at her picture. But only for a few moments."
    "I let myself have those moments, alone, and never for more than a short time. But sooner or later, the framed picture of her goes back face down."
    "Some times for a few hours. Then days. Then, I stopped needed to look at it so often."
    "I pick the picture up this time, my thumb rubbing over her cheek. I smile. The weight is … light now. I put the picture away, in my dresser. Out of sight, out of mind. It felt … right, now. It felt easy, now. Finally, after what felt like a very, very long time, I could let it go."

    scene bg dorm with bg_change_seconds
    "I grab my wallet and keys and leave my room, leaving the bad feelings for now. And taking the good piece of Maryanne that was still there with me."
    "For now. For a time. Tonight, I want to go out and have fun."

    call ending('true', thanksonly=True)

    return
