##----------------
## Act 4: Maryanne
## Good route
##----------------

label a4g:
    call a4gs3
    call a4ns4
    call a4ns5
    call a4ns6
    call a4gs6a
    call a4gs7a
    call a4gs8a
    if not true_end_available:
        call a4gs9
        call ending('good')
    else:
        call a4gs9b
    return
