##-------------------------------------------------
## Act 4: Maryanne
## Good route
## Scene 6a: A nothing-special date — part 2: good
##-------------------------------------------------

label a4gs6a:
    Maryanne crying smalltears "You confused me long enough to think I was happy, for a little while."

    "She makes me smile … and breaks my heart."

    Shun "I was … really, really happy I met you."

    "Maryanne pushes her head onto my shoulder. Her yellow hair bounces softly as she hides her face."

    Maryanne notears "… I just miss home."
    Maryanne sad "I want to go home. I love it here, I do. But I miss my home. I miss my grandparents. I miss my cousins, and the hills and the cars and even the greasy foods … I miss it so much!"
    Maryanne worried "My dad is being supportive. And … my grandparents are excited. And I’m excited … as I can be."

    Shun "… It’s … for the best then."
    Shun "If it’s for you, and for your health … then it’s for the best."

    Maryanne "I have to go Shun, I have to. Please tell me you understand."

    Shun "I do."
    Shun "… But it’s … not fair."

    Maryanne "I know."

    "A cloud passes by the sun."
    "I hold her shoulder, tightly. I try to stay calm. Harder than I ever had before. It {em}is{/em} for the best. It is. I know it is. For her. It’s for the best …"

    show maryanne worried with config.say_attribute_transition
    Maryanne "Do you hate me?"

    "She asks with a defeated whimper."

    Shun "Never. I could never hate you. I hate the bad timing of all of this."

    hide maryanne with config.say_attribute_transition
    "She falls into my arms again, having pulled herself away long enough to ask me how I felt. She trembles as she sobs into my chest. I try so hard to be okay with it … for her."

    show maryanne worried smalltears with config.say_attribute_transition
    Shun "… you need to do what is best for you. And it’s that simple."

    show maryanne sad with config.say_attribute_transition
    "I kiss her forehead. She laughs through her tears, holding onto my wrists. My heart beats heavily, so much that I can feel it in my head. I smile at her, laughing softly with her as my temple throbs with stress."

    Shun "… All of this tragedy makes me kinda hungry."

    Maryanne ecstatic smalltears "Ha … yeah … I’m hungry too."

    Shun "When are you not?"

    Maryanne happy happybigtears "When I’m crying I guess."

    hide maryanne with config.say_attribute_transition
    play music "<from 28.72>romantic.mp3" fadein 5
    "We both laugh and smile … then she starts to cry again. I give her another hug and wait for her to let it out. I do too … softly, not wanting to draw attention to my own stress."
    "As much as I want to, it’s not about me, I tell myself … over and over again, like a mantra made to both keep me sane and torture me slowly."
    "I hold her hand and guide her into town, towards our usual spot."

    scene bg yamada with bg_change_minutes
    "I look at the empty booth and remember taking her before, bumping into her in the rain, eating their specials with her. I pull a chair out for her and order her favorite, a meal I know by memory now. My treat."

    show maryanne sad with config.say_attribute_transition
    Shun "So … are you going right to your grandparents? Like, flying to them directly?"

    Maryanne embarrassed "Uh, not {em}directly{/em} directly. But I’ll take a layover as close as I can."

    Shun "They obviously know you’re coming, right?"

    Maryanne worried "My grandpa does. Grandma Mabel won’t know until I open the front door."

    Shun "Wow! She’s gonna flip out!"

    show maryanne happy with config.say_attribute_transition
    "Maryanne smiles. I do too. For a moment, we forget that this will be the last time we go here. And I’m reminded that, when I’m with her, all the bad stuff in the world doesn’t exist."
    "I’m really going to miss that feeling. That I was safe. That I could be myself around her. I’m going to really, really miss feeling like we were the only two people in the world."
    "Part of me wanted to beg her to stay … like that would work. The tickets were purchased already. She needed to see her family."
    "I didn’t know if I would be anything but a memory to her in a month, a year … but maybe I could be a good memory, I think. Maybe she’ll think about me when she’s sad or when she feels alone."
    "Maybe she won’t be sad anymore. Even if that meant being without me, that would be a good thing."
    "But it would also be very, very sad for me."

    Shun "I better get a postcard or something. Pictures at the very least."

    Maryanne ecstatic "Oh, obviously! You’ll be the first person who knows when I land in LA. And postcards? Please. I’m sending you cookies."

    Shun "Homemade or store-bought that you then pretend are homemade?"

    Maryanne happy "Fine, no cookies and just a postcard … from the airport."

    Shun "Oof, poor me!"

    "I miss her {em}so{/em} much already. I can feel my heart cracking."

    scene bg yamada_street with bg_change_minutes
    "I pay for the meal quickly, and walk out of Yamada’s slowly, savoring the smells and sight. I don’t know if I can come back here after Maryanne leaves. I suspect that it will feel … haunted."
    "I smile at her as we leave. I’m so tired that I can’t believe it. Tired of the stress and second-guessing. Tired of wondering when this would be easier. But still, I smile … exactly the way Maryanne would smile at me when she told me she was fine."
    "She cracks a few mild jokes and I respond, fighting the urge to yell “stay with me”. I wish I could fix it all for her. I wish I could make all of the bad things go away. And instead of begging her or yelling or acting on those selfish impulses, I crack jokes back. Making her smile."
    "I wish I had appreciated her smile more, before I knew it would go away."

    scene bg takuya with bg_change_seconds
    "Maryanne walked and I followed, staying behind her by a mere few inches. We went where she wanted to go, quipping and talking about Los Angeles and Hollywood the whole time."
    "I wanted to spend every minute I could with her while I could, enjoy every casual comment, the sound of her voice, the way her hair smelled when the wind picked up, how she walked, how she looked at me."

    scene bg takuya_bridge with bg_change_seconds
    "We walked over to the bridge in the middle of town, the one she hid under mere days ago in her darkest hour. She looked at the dark pit without moving for a moment, then walked up the street."
    "Leaning over the bridge, looking at her shadowy reflection next to mine in the cold river, she said nothing still."
    "For a second, I felt so sad that I thought my chest was going to cave in. I was there for {em}her{/em} darkest moment … but it was still her moment. Maybe that’s what I was missing. I was there for her because I wanted to be there, for that problem and the next."
    "Because I wanted to help. Because I loved her. And now, in {em}my{/em} darkest moment … she was here. Smiling and looking at her reflection … she really is beautiful."
    "I smile at her, and say nothing else. She doesn’t need more stress. There would be plenty of time for that later. And she has enough things to worry about. I’m fine, I think. I’m fine …"

    show maryanne happy with config.say_attribute_transition
    Maryanne "Remember when you got us lost that one time?"

    "She smiled, trying to break the tension with a happy memory."

    Shun "Uh, excuse me … I think it was {em}you{/em} who got us lost."

    Maryanne ecstatic "No! I remember you being the confused one. And I bravely saved the day by guiding us back!"

    Shun "Suuuure, whatever you say. You only get to be right because you’re leaving."

    show maryanne sad with config.say_attribute_transition
    "Her smile vanishes. So does mine. She goes back to staring at the dark reflection of herself in the running water."
    hide maryanne with config.say_attribute_transition

    "I take a few rocks from the side of the bridge and hand her one. She holds it weakly as I try to skip mine. She does the same."
    "We don’t say anything. We just skip rocks. Over the bridge where we found each other."

    return
