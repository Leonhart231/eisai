##------------------------
## Act 1.5: Shodo
## Scene 1: Arrival/Gaijin
##------------------------

label a1_5s1:
    stop music
    scene bg black with bg_change_days
    play music "decompression_loop.mp3"
    "The sun is barely above the horizon as I drift in and out of consciousness. The boat hits a few waves as it trudges along."
    "We left shore over an hour ago. Shodo Island looked a lot closer on google maps. I hear gulls squawking somewhere, but can’t keep my eyes open."
    "I can smell the ocean though."
    "And taste salt in the air."

    scene bg black with bg_change_hours
    Kevin "Shun. Wake up bro."

    "A hand shoves my shoulder, waking me up. I have to rub the sleepiness out of my eyes."

    scene bg shodo_lobby
    show kevin neutral sides open
    with bg_change_seconds
    Kevin "Get up. We’re on the island."

    Shun "Uuuhh … what time is it?"

    Kevin "It’s like nine, I think. You fell asleep like twice on the way over here."

    "Kevin leans over Hideki, who is barely awake himself as he sits in one of the hotel’s comfy chairs. Kevin, being the epitome of subtlety, slaps Hideki’s forehead."

    Kevin smile "Hey automaton! Turn on already! Activate!"

    show kevin smirk sides open at leftish
    show hideki armsup argue at rightish
    with config.say_attribute_transition
    Hideki "{em}Ow!{/em} What the hell was that …"
    Hideki hips bored "… Oh, we checked in. Good …"

    "Hideki lifts his leather suitcase off the floor. A label with his name is stitched in the exact center of the bag. Kevin slings a decade-old gym bag over his shoulder. His name is sloppily written on the front. They both look at each other and roll their eyes."
    "I brought my backpack, which doubled as my pillow for the boat ride … an awful substitute for comfort."

    scene bg shodo_beach
    show hideki hips bored
    with bg_change_minutes
    "We throw our bags in the room and head back out. The sunlight still hurts a little as I struggle to fully wake up."
    "Shodo Island. … Two days on a scenic beach. Warm sun, a quiet small town to explore, great barbeque."

    Kevin "See ya! I’m going swimming."

    "Kevin rushes by me, throwing his towel on the beach to claim a spot near the shore."

    Hideki "…"

    show hideki crossed glare with config.say_attribute_transition
    "Hideki stares at me, says nothing for a moment …"
    hide hideki with config.say_attribute_transition
    "… And leaves."
    "Not wanting to waste a moment of peace, I decide to head into town."

    scene bg shodo with bg_change_minutes
    "The area around the hotel is very modern, but the older parts of town are just a few blocks south. These homes are made of wood and paper, and there are lots of older people walking around amongst the herds of youth."
    "I smell fish all along the tourist walkway, and fantasize about tasting grilled carp caught fresh that morning."
    "Clusters of people move down each street. Most of them tourists like me. Vendors are open, selling silly souvenirs and snacks to little kids who loudly beg their parents to buy them something."
    "I see fathers carrying their daughters on their shoulders and mothers doting on their overly energetic sons. Seeing the parents with their happy children … as much as I hate to admit it, it makes me uneasy, maybe jealous even."

    scene bg black with bg_change_seconds
    George "Watch it!"
    scene bg shodo
    show george angry
    with hpunch
    $ george_name = "Older Foreigner"
    "Not watching where I’m going, I walk into someone, an older foreigner. He scolds me, but says nothing."

    Shun "Um, I’m sorry …"

    George "It’s … fine, young man. Don’t worry about it."
    George neutral "But you should really watch where you are going."

    Shun "I will. Sorry again."

    "Without waiting for permission, I bend down and pick up the bag he dropped."

    George "Careful. That’s quite delicate. I think. Either way, it is brand new."

    "I hand him his parcel and he takes it. He opens the bag right away to see if the contents were damaged."

    George "Hmmm … well … no harm done then, it seems."

    Shun "Well, um … nice meeting you."

    George "Hm? Oh right. And thank you for being so polite. You’d be amazed how many kids your age don’t have manners these days."

    Shun "Yeah. I know what you mean."

    "I hear my roommates bickering in my head."

    George "Have a good day."
    hide george with config.say_attribute_transition

    "He nods at me and keeps walking, tucking his mysterious parcel under his arm. I go back to exploring the town."

    return
