##-------------------
## Act 1.5: Shodo
## Scene 5: Boat Home
##-------------------

label a1_5s5:
    scene bg shodo_room with bg_change_days
    "A few days later, after a few days of eating expensive tourist food and nights of staying up too late, it is over. The vacation ends and we have to repack our belongings to go back to real life."

    show hideki crossed pissed at leftish
    show kevin neutral sides open at rightish
    with config.say_attribute_transition
    "Hideki meticulously folds each item of clothing and, with great anger, struggles to shut his leather briefcase."

    Hideki "Grrr … a mathematical impossibility. I have less in my luggage than when I arrived. And yet, it is more difficult to repack this time around."

    "Hideki pushes on the suitcase in rhythm, forcing the palms of his hands on the center like he is giving it CPR."

    Hideki "Why. Is. It. {em}Always. Harder. To. Pack. {b}Before. You. Leave?!{/b}{/em}"

    Kevin smirk sides open "Calm down! You’ll pop another blood vessel."

    show kevin at leftish with move
    show kevin at leftish with vpunch
    "Without asking, Kevin jumps on Hideki’s briefcase, butt first, and slams the latches shut."

    show kevin at rightish with move
    Hideki armsup argue "{em}guah!{/em} What are you doing?!"

    Kevin neutral crossed closed "I’m closing it."

    Hideki "Don’t engage in such behaviors again! At least not on my belongings! You’ll break them with your overweight ass!"

    Kevin "It closed, didn’t it?"

    Hideki crossed pissed "That … is not the point! Don’t do that again!"

    hide hideki with config.say_attribute_transition
    "Hideki points his nose up high as he can and leaves."

    scene bg shodo_lobby
    show kevin serious crossed open
    with bg_change_minutes
    "The other Inou students are in the lobby, all of us waiting for shuttle to arrive, each holding onto a bag or souvenirs."

    Kevin neutral sides open "Ah … another great end. Why can’t these vacations last longer? Like … forever. That would be nice."

    Shun "I don’t think I could be on vacation forever. I’d get antsy."

    Kevin "Not me man. I would travel non-stop if I could afford it. Some day bro, some day …"

    Shun "I’ve got to admit, this {em}was{/em} better than staying on campus by myself the whole time."

    Kevin smile crossed "I told you, didn’t I? Imagine being all alone in your room studying and stuff when everyone is having fun. You’d be like Maryanne. Ha!"

    Shun "Does she study {em}that{/em} much?"

    show kevin frown crossed open with config.say_attribute_transition
    "Kevin says nothing, and he looks uncomfortable as he tries to stir up a response. With none to give, he throws his pack over his shoulder."

    Kevin neutral sides open "Let’s go home."

    "Home."
    "Little by little … the dorm was becoming home."

    stop music
    scene bg bus with bg_change_minutes
    play music "romantic.mp3"
    "Everyone hurries to find a seat, all eager to get home as fast as possible. A few take out handheld systems, some books. All look tired from lack of sleep from the wild weekend."
    "Maryanne is in the far end of the bus. She struggles to push her bulging backpack above her head. Shione was passed out on the seat next to her, snoring obnoxiously loud."
    "Kevin shoves me towards her, pretending to not look at me as he nods in her direction. I take the hint and go."
    "I put my hand on her bag and help her push it up over her head."

    show maryanne stressed with config.say_attribute_transition
    Shun "What are you packing? Rocks?"

    Maryanne "… Uh … yeah."
    Maryanne embarrassed "Shione … collects them. Sometimes. She ran out of room in her bag, so I … you know … let her use a little space in mine."

    "The kokeshi doll is in Maryanne’s seat. She picks it up before sitting down herself."
    show maryanne happy with config.say_attribute_transition
    "I look back at my roommates. Kevin nods and smiles at me. Hideki shakes his head and pushes his face into yet another textbook."

    Shun "Did your dad fly out?"

    "I ask as I sit across from her. Maryanne nods softly, and the bus starts moving as she tells me he landed safely."

    return
