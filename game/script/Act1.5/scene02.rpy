##--------------------
## Act 1.5: Shodo
## Scene 2: Meeting up
##--------------------

label a1_5s2:
    scene bg shodo with bg_change_minutes
    "The shops along the shore-side fume with a myriad of scents. Knock-off perfumes, oil bubbling in tin pots, all mixing in with salty air to create a strange aroma that makes me feel like I’m walking through a festival."
    "The smell of boiling noodles makes my stomach rumble as I walk past a ramen shop. I reach for my wallet … and I feel an empty pouch."
    "I must have left my wallet in my hotel room."
    "With no other options, I turn around and head back to the hotel. I carefully navigate through shoppers and tourists, not wanting to bump into anyone else."
    "As I walk back the way I came, I see a head of blonde hair moving my way. Ever the sore thumb, Maryanne stands out."
    "She looks mildly worried as she looks around the crowd of people with little sense of where she was going."

    Shun "Maryanne! Over here!"

    "I wave her down. She turns and sighs with relief when she sees it’s me."

    show maryanne ecstatic with config.say_attribute_transition
    Maryanne "Hey Shun, good to see you! How about that trip in, right?"

    show maryanne happy with config.say_attribute_transition
    Shun "Oh, tell me about it. I’m so tired. I need a vacation from this vacation already."

    Maryanne "Speak for yourself. I’m perfectly fine with this one."

    Shun "I’m glad you’re having fun. But gotta get back to the hotel."

    Maryanne confused "Already?"

    Shun "I left my wallet back at the hotel and I didn’t eat breakfast, so …"

    Maryanne embarrassed "Oh … {w=0.5}well … {w=0.5}do you want to get lunch with me? I’m meeting my dad actually. I’m sure he wouldn’t mind if you showed up."

    Shun "Your dad?! He’s on the … island …"

    "I remember the foreigner from earlier and put two and two together."

    Shun "Um … maybe that’s not a good idea. I wouldn’t want to intrude on your meeting up."

    Maryanne happy "Really, it’s not a big deal. Besides, he’ll want to meet you. He’ll know I’m hanging out with someone other than Kevin and that will … make him feel better, to say the least."

    Shun "Um … I really don’t …"

    Maryanne "He … usually offers to pay for lunch for my friends."

    Shun "Ok!"

    "A college student’s budget doesn’t have room for embarrassment or shame. Regardless of any awkwardness that may occur, I wasn’t going to turn down a free meal."

    Maryanne ecstatic "Great! Let’s go, he’s … um … I think this way."

    "She leads me down the touristy path and we duck around the crowded patches of passersby. Eventually, we’re alone, walking down the much quieter side street together."

    Shun "So, why is he here? Doesn’t he have off for Golden Week?"

    Maryanne happy "Not officially, he’s an international employee so … he gets American holidays. And the Los Angeles office needs him back stateside soon so he’ll be off the island early tomorrow morning."

    Maryanne ecstatic "But I haven’t seen him in months. So he asked his boss if he could fly out here for a day before going back to home."

    "She points to a present under her arm: a small, neatly wrapped box with a yellow bow tied on top. Her smile grows and grows the more she talks about her dad."

    Maryanne happy "And I bought him a tie. A silver one. He likes silver colored things."

    "She is so wrapped up in talking about her father that Maryanne almost bumps into someone else. They should really build sidewalks on these side roads."

    return
