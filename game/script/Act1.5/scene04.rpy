##---------------------
## Act 1.5: Shodo
## Scene 4: Lost Parcel
##---------------------

label a1_5s4:
    scene bg shodo with bg_change_days
    stop music fadeout config.fade_music
    "I spend that morning grabbing a quick bite to eat at a small cart just down the road. Nothing crazy, but enough of a reason to get out of the hotel room."
    "Hideki and Kevin spent a good 20 minutes last night fighting over the remote and regardless of which of them won … I lost."
    "A few packs of young people crowd around the stand, talking softly as I pick up my order."
    "But no sooner do I take my first bite do I see Maryanne shooting around the sidewalk like a scared dog."

    show maryanne nonecklace worried circles hairdown with config.say_attribute_transition
    Shun "Arh ffo o—{em}gulp{/em} … are you okay?"

    play music "very_sad_loop.mp3"
    Maryanne "I—I don’t … I don’t know what to do."

    Shun "Calm down. Are you okay? What happened?!"

    Maryanne uncomfortable "Last night … did you … find …"

    "She gulps in between shallow hyperventilations. I get up and push her into my chair to help ease her mild conniption."

    Maryanne uncertain "I don’t … I can’t … you’re—you’re gonna think I’m silly. Don’t laugh!"

    Shun "I won’t laugh, I promise. But what happened?"

    Maryanne crying "… I lost it."

    "She points to her collarbone. Then I notice … Her ruby necklace, which was usually draped around her neck, was gone."

    Maryanne "I don’t know where it was! I had it yesterday and … I don’t know. What if I dropped it!?"

    Maryanne uncertain "I woke up early to, you know, relax at the beach … and I didn’t have it. I must have … I lost it. And … I’ve been up since 8 looking for it … I tore up my room …"

    Shun "Don’t worry. It’s okay. It’s just a necklace."

    Maryanne worried "No it’s not! I need it! I can’t replace it, okay? I … I … don’t know what …"

    Shun "I’m sorry, I’m sorry. I didn’t mean anything by it. I’ll … help you look for it. But calm down first. Okay?"

    Maryanne "Okay … y—yeah. Okay."

    "She lets herself breathe. Loudly and slowly, letting herself relax for a moment."

    stop music fadeout config.fade_music
    Shun "A—alright. The best thing to do is to retrace your steps. Where was the last place you remember wearing it?"

    Maryanne "The restaurant. Yesterday. Maybe they found it? Can we go? I can’t leave this island without that necklace, okay?"

    Shun "Fine. Okay. Yeah, we’ll … go right now."

    "I hand her half of my breakfast and she inhales it as we hurry through town."

    scene bg shodo_sushi with bg_change_minutes
    "We get back to the restaurant with no issues, though I had to hustle after Maryanne, who got a strong second wind once we got out of the hotel lobby."
    "The head waitress is there, though she is reluctant to let us in. The restaurant was not open just yet, but when she saw Maryanne’s stressed out face, she relented."
    play music "contemplative_loop.mp3"
    "Sure enough, the necklace was there waiting for us, behind the counter in a small container."
    "The massive panic melted away when Maryanne touched that little bubble with a loud sigh. The latch had somehow broken and the necklace was under her seat."

    scene bg shodo_sushi
    show maryanne nonecklace worried circles hairdown
    with bg_change_seconds
    "She gradually calms down, relaxing the more she holds the little piece of jewelry. I stay quiet until it seems like she has calmed down. Only then do I speak up."

    Shun "You were so worried about that little thing."

    Maryanne happy "Oh … heheh … uh, yeah. Um … it matters a lot to me."

    Shun "Why? Is it expensive?"

    "She touches the gemstone. She is about to say something, but pauses. She looks very sad for a moment."

    Maryanne sad "It … was my mother’s."
    Maryanne uncertain "It was her favorite thing in the world. Her mother passed it to her too. She loved it. It meant … uh … it meant a lot."

    "I nod, staying silent, not sure what to say after that."
    "Maryanne doesn’t say anything either, putting on a proud smile instead of talking. A smile that tries its hardest not to waver."

    scene bg shodo_beach sunset
    show maryanne sad nonecklace circles hairdown
    with bg_change_seconds
    "But as we head back, after another period of silence, that smile does begin to quiver a bit."

    Shun "Are you okay?"

    Maryanne "I’m just … thinking …"

    Shun "Do you wanna talk about it? If you want, I mean. It’s not really … I know it’s personal."

    "She scratches the back of her neck softly before softly speaking up."

    Maryanne "My mother … had a hard life. She didn’t know when to get help. Or when something was wrong."
    Maryanne "She wasn’t happy. Not really. She smiled sometimes. She laughed. But … she was …"
    Maryanne confused "Some nights, I’d come home and dad would be reading alone in the living room. Mom would be in the bedroom by herself. Dad said it was because she was watching a sad movie or something. And he’d read me stories while I sat on his lap those night. Until I’d fall asleep."
    Maryanne crying "One day, my mom came to my school in the middle of the day. She pulled me out of class, took me in the hallway alone … and she hugged me. {em}Reeeally{/em} tightly."
    Maryanne "She said she loved me and that she always would. And …"
    Maryanne "… That was it. She just … she …"
    Maryanne worried "… Didn’t come home."
    Maryanne "It’s weird. I remember that day. I remember she was there, at my school. And she had this … heavy fur coat she liked to wear in the winter that smelled like coffee and she wore it that day ’cause it was January."
    Maryanne "But … I … I can’t really remember what she looked like. Dad doesn’t keep pictures of her up anymore."

    "Maryanne puts the ruby necklace back in her pocket, and starts to walk like she is carrying something heavy."
    "I am at a major loss for words, not expecting that kind of thing to be brought up. I figure that the best thing to do was say nothing and wait for her to say something else … for her to open up on her own again."
    "But she doesn’t."
    "She walks next to me without a peep. Eventually, I finally have to break the silence."

    Shun "Do you … I mean … do you tell that story to a lot of people?"

    Maryanne sad "No, I … I don’t really like talking about it."

    Shun "Does Kevin know that story? Or Shione?"

    Maryanne neutral "… No."

    "She goes quiet again, her fist in her pocket, still holding the necklace."

    Maryanne "…"
    Maryanne sad "… I miss her."

    menu:
        "Gently pat her back.":
            jump a1_5s4_pat
        "Tell her that she’s not alone.":
            jump a1_5s4_not_alone

    label a1_5s4_pat:
        "Knowing how she felt, I reach over and pat her back. She smiles softly and looks at me. Still not saying anything, her eyes getting a little watery."
        "She stares at the ocean in silence, the wind blowing through her hair. I don’t know what to say. So, like Maryanne, I say nothing."

        Maryanne "… I want to go back."

        "I nod and we both start walking towards the hotel. Without another word."
    jump a1_5s4_common

    label a1_5s4_not_alone:
        Shun "I … might know what it’s like. To feel like home is … not a great place. You’re not … alone, you know? Even if you feel that way sometimes."

        "She tries not to cry, I can see it. She looks at me and smiles softly."

        Maryanne happy "… Thank you."

        Shun "… So, uh, do you want to check out the beach? I heard Shione was setting up a bonfire party."

        Maryanne "Uh … maybe another time. I’m tuckered out."
        Maryanne "{em}sigh{/em} Maybe I’ll sleep in tomorrow. I didn’t get a chance to do that this morning."

        "She shakes her head softly."

        Maryanne "Thank you for everything today. It … meant a lot to me."

        Shun "I know Maryanne. You know I’d … anytime you need it."

        Maryanne confused "Really? I mean. I’m not … annoying you?"

        Shun "Of course not. I really like being around you."

        "I am standing close to her, closer than I realize. Our feet are almost touching."
        "And then, not really thinking, I lean a little. And so does she. Then, as her head begins to tilt …"

        Maryanne "… {em}Wellgoodnight{/em}!"

        hide maryanne with config.say_attribute_transition
        "… She steps back, turns and runs back to the hotel."
        "I stand still in the empty night road and watch her hurry away."
        "Embarrassed, I wait until Maryanne is out of sight. Then go to the hotel. The bonfire doesn’t sound as exciting as it did a minute ago."

        scene bg black with bg_change_seconds
        "My heart feels weird. A mesh of disappointment and frustration makes me feel like I screwed up badly. Maryanne didn’t just leave, she ran."

    label a1_5s4_common:
        stop music
        scene bg shodo_lobby
        show hideki glasses adjust
        with bg_change_minutes
        play music "bickering_loop.mp3"
        "Hideki is sitting in the hotel lobby, reading a thick textbook called “Mathematics and The Meaning of the Universe: Abridged”. He holds it as if it were a magazine, but looks up from it as he sees me coming his way."

        Hideki "Hello Takamine."

        "He says with a grunt, fiddling through his pages."

        Shun "Hey. Is Kevin around?"

        Hideki hips bored "I don’t know where he is, but he’s not in the room. I am waiting for his return."

        Shun "Oh, okay. Um … yeah. It would be nice if I could talk to someone."

        Hideki "…"

        "Hideki coughs softly."

        Hideki crossed glare "…"
        Hideki armsup argue "Uuugh … alright, fine! Are you okay? You were out all day with that yellow-headed female, weren’t you?"

        Shun "How did you know?"

        Hideki "What else could exhaust a young man in his prime but a woman? And which else is more likely to be on your arm than her?"
        Hideki "What happened?"

        Shun "It’s … a long story."

        Hideki crossed glare "Summarize it in thirty words or less."

        Shun "Um … I … never mind."

        Hideki "Oh, thank god. … I mean, if that is what you wish."

        hide hideki with config.say_attribute_transition
        "He goes back to reading his book. Silently as I wait for Kevin by his side."

        Shun "…"

        Hideki "…"

        Shun "… It’s just …"

        show hideki crossed pissed with config.say_attribute_transition
        "Hideki snarls."

        Shun "… What do you do when you miss an opportunity? I mean, is it … when do you know you mess up versus when you just make a small mistake?"

        Hideki "Rules of war states you must wait and prepare for another chance to strike. Bide your time and move when you see a chance to move."

        Shun "… Thanks, that’s actually not terrible advice. I feel a bit better."

        Hideki "War doesn’t count feelings. It counts bodies."

        Shun "… Yeeeah. Well, thanks anyway."

    return
