##-------------------------
## Act 1.5: Shodo
## Scene 3: Meet the Parent
##-------------------------

label a1_5s3:
    scene bg shodo_sushi
    show maryanne happy at rightish
    with bg_change_minutes
    "We stop at a little restaurant that smells like grilled carp and fried dough. An eager waitress is at the door, ready to greet us."
    "Soon enough, I see the same foreign man I bumped into earlier. He stops sipping his drink when he sees me walk in with his daughter."
    $ george_name = "Mr. Sawyer"
    "I put on my best smile, the prospects of a free meal motivating me to not make this awkward."

    Maryanne ecstatic "Dad, dad! I missed you so much!"

    show george neutral at leftish with config.say_attribute_transition
    "They both hug each other tightly, the father’s hand pats Maryanne’s shoulder."

    show maryanne happy with config.say_attribute_transition
    "She sits down next to him and pulls out the gift from her purse."

    Maryanne "This is Shun. We met a little while back. He’s—"

    George "Nice to see you again young man."

    Shun "Y—yes sir! It is good to … see you again."

    Maryanne "Have you two met?"

    George smile "You could say that."

    "He smiles knowingly, then clears his throat."

    George neutral "Alright, order what you want. It’s on me. Just don’t go too crazy."

    Shun "Thank you sir!"

    "He waves at me to assure me it’s alright."

    George "It’s fine. Besides, I could eat a horse."

    Maryanne "Me too."

    Shun "I bet … I mean, me too!"

    George "Maryanne … is this the boy you were telling me about? The one living with the brat from New York?"

    Maryanne "Yep. Kevin’s new roommate."

    George "You … live with Kevin? Oh. I’m so sorry."

    Shun "Ha. It’s fine. He’s really not that bad."

    George "From what I hear, he’s worse. I’m just glad Maryanne has the sense to stay away from that."

    scene bg shodo_sushi
    show maryanne nonecklace happy at rightish
    show george smile at leftish
    with bg_change_minutes
    "The evening rolls by happily, filled with Mr. Sawyer’s array of stories. Many of them were about his youth and travels around the world. He told me about their family and when Maryanne got her first science trophy, the award that would help her to skip a grade."
    "My own family had a habit of making me feel alienated. Mr. Sawyer made me feel welcomed, like the others I met since I left home. This weird feeling … of being welcomed by strangers … was starting to not feel so weird."

    "As the evening continues, the impressive spread of food vanishes before my eyes."

    George "Well, that was fantastic! Definitely worth the money. Aaand … I guess that’s everything for the night."

    play music "romantic.mp3"
    Maryanne confused "… Oh … um, that’s … do you have to leave now?"

    George neutral "If I want to get the next flight out, yeah. I’m sorry … I—"

    Maryanne sad "It’s okay dad. I know how your job is."

    "They both of look at each other in silence."

    George "Before I forget … I bought you something."

    "He grabs the small bag in the corner, the one that I knocked out of his hands earlier this morning."
    "He takes out a small Kokeshi with a red robe on. It’s very pretty, clearly high quality, with a gentle polished shimmer."

    Maryanne uncertain "Aww, this is adorable! Thanks dad."

    show george smile at leftish with config.say_attribute_transition
    "Maryanne smiles, and hugs her father again. Just as tightly as last time."
    "When he’s free, he holds out his hand and shakes mine, smiling."

    George "It was a pleasure meeting you young man."

    "With the bill paid, he leaves, answering his cell phone as he hurries out."

    scene bg shodo with bg_change_minutes
    "We pick up the change. There was a lot more than needed to pay the bill. I feel a little bad for Mr. Sawyer paying that much, but Maryanne assures me that was her dad’s way of giving a gift."
    "She smiles ear to ear, holding the doll in her hands the entire way back to the hotel."

    scene bg shodo_lobby
    show maryanne nonecklace uncertain
    with bg_change_minutes
    Shun "Thank you for inviting me out tonight. I had a lot of fun."

    Maryanne happy "I’m glad you came Shun. I … like it when you’re around."

    Shun "Since you … you know, you don’t have plans tomorrow … I mean … {em}if{/em} you don’t have plans tomorrow … do you want to hang out?"

    Maryanne uncomfortable "I … honestly, I … I think I need to be alone for a bit."

    Shun "Yeah. I understand."

    Maryanne happy "But we should hang out. More. A—at school. You know, just … because that’d be … fun."

    show maryanne happy with config.say_attribute_transition
    Shun "Yeah, I’d like that! We should."

    hide maryanne with config.say_attribute_transition
    "Maryanne smiles, then goes to her room on the first floor. And I get in the elevator, catching a glance at her before the doors close."

    return
