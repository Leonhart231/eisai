# -------------------------------------------------------
# Declare Custom Transforms here
# -------------------------------------------------------

transform left:
    zoom 1.0
    xalign 0.0 yalign 0.0

transform right:
    zoom 1.0
    xalign 1.0 yalign 0.0

transform leftish:
    zoom 1.0
    xalign 0.2 yalign 0.0

transform rightish:
    zoom 1.0
    xalign 0.8 yalign 0.0

transform midleft:
    zoom 1.0
    xalign 0.35 yalign 0.0

transform midright:
    zoom 1.0
    xalign 0.65 yalign 0.0

transform center:
    zoom 1.0
    xalign 0.5 yalign 0.0

transform p1_4:
    zoom 1.0
    xalign 0.1 yalign 0.0

transform p2_4:
    zoom 1.0
    xalign 0.3 yalign 0.0

transform p3_4:
    zoom 1.0
    xalign 0.7 yalign 0.0

transform p4_4:
    zoom 1.0
    xalign 0.9 yalign 0.0

transform sitting:
    zoom 1.0
    xalign 0.5 yalign 0.0 yanchor -0.2

# -------------------------------------------------------
## NEAR TRANSFORMS ##
# -------------------------------------------------------

transform leftnear:
    zoom 1.5
    xalign 0.0 yalign 0.0

transform rightnear:
    zoom 1.5
    xalign 1.0 yalign 0.0

transform leftishnear:
    zoom 1.5
    xalign 0.2 yalign 0.0

transform rightishnear:
    zoom 1.5
    xalign 0.8 yalign 0.0

transform midleftnear:
    zoom 1.5
    xalign 0.35 yalign 0.0

transform midrightnear:
    zoom 1.5
    xalign 0.65 yalign 0.0

transform centernear:
    zoom 1.5
    xalign 0.5 yalign 0.0
