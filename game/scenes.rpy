## General images
image splash = make_fillscreen("general/splash.png")
image logo = make_fillscreen("general/logo.png")
image red = "#FF0000"
image flashback:
    "#FFFFFF"
    alpha 0.3

## Colors
image bg black = "#000000"
image bg white = "#FFFFFF"

## Prefixes
init -10 python:
    def prefix_overcast(s):
        bg = make_bg(s)
        overcast = im.MatrixColor(bg, im.matrix.saturation(0.85) * im.matrix.tint(0.65, 0.65, 0.8))
        return overcast

    def prefix_night(s):
        bg = make_bg(s)
        night = im.MatrixColor(bg, im.matrix.saturation(0.5) * im.matrix.tint(0.4, 0.4, 0.7))
        return night

    def prefix_sunrise(s):
        bg = make_bg(s)
        sunrise = im.MatrixColor(bg, im.matrix.tint(0.95, 0.95, 0.75))
        return sunrise

    def prefix_sunset(s):
        bg = make_bg(s)
        sunset = im.MatrixColor(bg, im.matrix.saturation(0.9) * im.matrix.tint(0.95, 0.75, 0.75))
        return sunset

    config.displayable_prefix["overcast"] = prefix_overcast
    config.displayable_prefix["night"] = prefix_night
    config.displayable_prefix["sunrise"] = prefix_sunrise
    config.displayable_prefix["sunset"] = prefix_sunset

## Backgrounds
image bg a0_class_history = make_bg("bg/a0_class.png")
image bg a0_hall = make_bg("bg/a0_hall.jpg")
image bg a0_lunch = make_bg("bg/a0_lunch.png")
image bg a0_stairs = make_bg("bg/a0_stairs.png")
image bg airport = Placeholder("bg")
image bg bar night = make_bg("bg/bar.png")
image bg birthday_sushi = make_bg("bg/yamada_night.png")
image bg birthday_sushi_outside night = make_bg("bg/yamada_street_night.png")
image bg bus = "bg/bus.png"
image bg bus airport = "bg/bus_airport.png"
image bg class_okada = make_bg("bg/classroom.jpg")
image bg convenience_store = make_bg("bg/convenience_store.jpg")
image bg dorm = make_bg("bg/dorm.png")
image bg dorm_door = make_bg("bg/dorm_door.jpg")
image bg dorm_door open = make_bg("bg/dorm_door open.jpg")
image bg dorm_maryanne = make_bg("bg/dorm_maryanne.png")
image bg forest = make_bg("bg/forest.png")
image bg hall_dorm_boys = make_bg("bg/hall_dorm_boys.jpg")
image bg hall_dorm_girls = make_bg("bg/hall_dorm_girls.jpg")
image bg hall_school = make_bg("bg/hall_school.png")
image bg hall_school night = make_bg("bg/hall_school_night.png")
image bg inou = make_bg("bg/inou.png")
image bg inou night = make_bg("bg/inou_night.png")
image bg inou overcast = "overcast:bg/inou.png"
image bg inou sunrise = "sunrise:bg/inou.png"
image bg inou sunset = "sunset:bg/inou.png"
image bg inou_bus overcast = make_bg("bg/inou_gate_night.png")
image bg inou_gate = make_bg("bg/inou_gate.png")
image bg inou_gate night = make_bg("bg/inou_gate_night.png")
image bg inou_gate sunset = make_bg("bg/inou_gate_sunset.png")
image bg lab_maryanne = make_bg("bg/lab_maryanne.png")
image bg lab_maryanne night = make_bg("bg/lab_maryanne_night.png")
image bg lab_maryanne sunset = make_bg("bg/lab_maryanne_sunset.png")
image bg lab_shione = make_bg("bg/lab_maryanne.png")
image bg mall = make_bg("bg/mall.png")
image bg room_hideki = make_bg("bg/room_hideki.png")
image bg room_kevin = make_bg("bg/room_kevin.png")
image bg room_kevin night = "night:bg/room_kevin.png"
image bg room_maryanne = make_bg("bg/dorm_maryanne.png")
image bg room_maryanne night = "#000000"
image bg room_shun = make_bg("bg/room_shun.png")
image bg room_shun night = "night:bg/room_shun.png"
image bg room_shun overcast = "overcast:bg/room_shun.png"
image bg room_shun sunset = "sunset:bg/room_shun.png"
image bg shodo:
    make_bg("bg/shodo_full.png")
    xalign 0
image bg shodo sunset:
    make_bg("bg/shodo_full_sunset.png")
    xalign 0
image bg shodo_beach:
    make_bg("bg/shodo_full.png")
    xalign 1.0
image bg shodo_beach sunset:
    make_bg("bg/shodo_full_sunset.png")
    xalign 1.0
image bg shodo_lobby = make_bg("bg/lobby.png")
image bg shodo_room = make_bg("bg/hotel_room.png")
image bg shodo_sushi = make_bg("bg/shodo_sushi.png")
image bg shun_house_door:
    make_bg("bg/shun_house.png")
    xalign 0.55
    yalign 1.0
    zoom 2.5
image bg shun_house_door night:
    make_bg("bg/shun_house_night.png")
    xalign 0.55
    yalign 1.0
    zoom 2.5
image bg shun_house_kitchen = make_bg("bg/shun_house_kitchen.png")
image bg shun_house_street = make_bg("bg/shun_house.png")
image bg shun_house_street night = make_bg("bg/shun_house_night.png")
image bg takuya = make_bg("bg/takuya.png")
image bg takuya night = make_bg("bg/takuya_night.png")
image bg takuya overcast = "overcast:bg/takuya.png"
image bg takuya sunrise = "sunrise:bg/takuya.png"
image bg takuya_bridge = make_bg("bg/takuya_bridge.png")
image bg takuya_bridge night = "night:bg/takuya_bridge_overcast.png"
image bg takuya_bridge overcast = make_bg("bg/takuya_bridge_overcast.png")
image bg takuya_bridge rain = make_bg("bg/takuya_bridge_rain.png")
image bg takuya_far = make_bg("bg/takuya.png")
image bg takuya_far night = make_bg("bg/takuya_night.png")
image bg theater = make_bg("bg/theater.jpg")
image bg yamada = make_bg("bg/yamada.png")
image bg yamada night = make_bg("bg/yamada_night.png")
image bg yamada_street = make_bg("bg/yamada_street.png")
image bg yamada_street overcast = "overcast:bg/yamada_street.png"
image bg yamada_street night = make_bg("bg/yamada_street_night.png")

## CGs
image cg act0_bus = make_cg("cg/act0_bus.png")
image cg act0_letter = make_cg("cg/act0_letter.jpg")
image cg act0_reflection = make_cg("cg/act0_reflection.png")
image cg mom_kitchen = make_cg("cg/mom_kitchen.png")
