init python:
    ## Function to add circles underneath Maryanne's eyes
    def maryanne_add_circles(options):
        if "circles" in options:
            if "neutral" in options or "happy" in options:
                options.update({"type1"})
            elif "ecstatic" in options:
                options.update({"type2"})
            elif "stressed" in options or "embarrassed" in options or "confused" in options:
                options.update({"type4"})
            else:
                options.update({"type3"})
        return options

# Ai
image ai pout = make_sprite("sprites/ai/pout.png")
image ai proud = make_sprite("sprites/ai/proud.png")
image ai smile = make_sprite("sprites/ai/smile.png")
image ai talk = make_sprite("sprites/ai/talk.png")
image ai yell = make_sprite("sprites/ai/yell.png")

# Fusao
image fusao cheerful = make_sprite("sprites/fusao/cheerful.png")
image fusao crossed default = make_sprite("sprites/fusao/crossed default.png")
image fusao crossed smile = make_sprite("sprites/fusao/crossed smile.png")
image fusao crossed talking = make_sprite("sprites/fusao/crossed talking.png")

# George Sawyer
layeredimage george:
    always:
        make_sprite("sprites/george/base.png")
    group emotion:
        attribute neutral default:
            make_sprite("sprites/george/emotion_neutral.png")
        attribute angry:
            make_sprite("sprites/george/emotion_angry.png")
        attribute smile:
            make_sprite("sprites/george/emotion_happy.png")
        attribute laugh:
            make_sprite("sprites/george/emotion_laugh.png")
        attribute panic:
            make_sprite("sprites/george/emotion_panic.png")
        attribute sad:
            make_sprite("sprites/george/emotion_sad.png")

# Hideki Ayanokoji
image hideki armsup argue = make_sprite("sprites/hideki/armsup argue.png")
image hideki formal armsup argue = make_sprite("sprites/hideki/formal armsup argue.png")
image hideki chin gendo = make_sprite("sprites/hideki/chin gendo.png")
image hideki chin think = make_sprite("sprites/hideki/chin think.png")
image hideki crossed begrudging = make_sprite("sprites/hideki/crossed begrudging.png")
image hideki crossed glare = make_sprite("sprites/hideki/crossed glare.png")
image hideki formal crossed glare = make_sprite("sprites/hideki/formal crossed glare.png")
image hideki crossed grin = make_sprite("sprites/hideki/crossed grin.png")
image hideki crossed smile = make_sprite("sprites/hideki/crossed smile.png")
image hideki crossed pissed = make_sprite("sprites/hideki/crossed pissed.png")
image hideki formal crossed pissed = make_sprite("sprites/hideki/formal crossed pissed.png")
image hideki glasses adjust = make_sprite("sprites/hideki/glasses adjust.png")
image hideki formal glasses adjust = make_sprite("sprites/hideki/formal glasses adjust.png")
image hideki hips bored = make_sprite("sprites/hideki/hips bored.png")
image hideki formal hips bored = make_sprite("sprites/hideki/formal hips bored.png")

# Higa-sensei
layeredimage higa:
    group arms:
        attribute sides default:
            make_sprite("sprites/higa/arms_sides.png")
        attribute crossed:
            make_sprite("sprites/higa/arms_crossed.png")
    group emotion:
        attribute happy default:
            make_sprite("sprites/higa/emotion_happy.png")
        attribute talking:
            make_sprite("sprites/higa/emotion_talking.png")
        attribute sad:
            make_sprite("sprites/higa/emotion_sad.png")

# Kevin Gallagher
image kevin frown crossed closed = make_sprite("sprites/kevin/frown crossed closed.png")
image kevin frown crossed open = make_sprite("sprites/kevin/frown crossed open.png")
image kevin frown sides closed = make_sprite("sprites/kevin/frown sides closed.png")
image kevin frown sides open = make_sprite("sprites/kevin/frown sides open.png")
image kevin laugh crossed closed = make_sprite("sprites/kevin/laugh crossed closed.png")
image kevin laugh crossed open = make_sprite("sprites/kevin/laugh crossed open.png")
image kevin laugh sides closed = make_sprite("sprites/kevin/laugh sides closed.png")
image kevin laugh sides open = make_sprite("sprites/kevin/laugh sides open.png")
image kevin mad crossed closed = make_sprite("sprites/kevin/mad crossed closed.png")
image kevin mad crossed open = make_sprite("sprites/kevin/mad crossed open.png")
image kevin mad sides closed = make_sprite("sprites/kevin/mad sides closed.png")
image kevin mad sides open = make_sprite("sprites/kevin/mad sides open.png")
image kevin neutral crossed closed = make_sprite("sprites/kevin/neutral crossed closed.png")
image kevin neutral crossed open = make_sprite("sprites/kevin/neutral crossed open.png")
image kevin neutral sides closed = make_sprite("sprites/kevin/neutral sides closed.png")
image kevin neutral sides open = make_sprite("sprites/kevin/neutral sides open.png")
image kevin serious crossed closed = make_sprite("sprites/kevin/serious crossed closed.png")
image kevin serious crossed open = make_sprite("sprites/kevin/serious crossed open.png")
image kevin serious sides closed = make_sprite("sprites/kevin/serious sides closed.png")
image kevin serious sides open = make_sprite("sprites/kevin/serious sides open.png")
image kevin smile crossed closed = make_sprite("sprites/kevin/smile crossed closed.png")
image kevin smile crossed open = make_sprite("sprites/kevin/smile crossed open.png")
image kevin smile sides closed = make_sprite("sprites/kevin/smile sides closed.png")
image kevin smile sides open = make_sprite("sprites/kevin/smile sides open.png")
image kevin smirk crossed closed = make_sprite("sprites/kevin/smirk crossed closed.png")
image kevin smirk crossed open = make_sprite("sprites/kevin/smirk crossed open.png")
image kevin smirk sides closed = make_sprite("sprites/kevin/smirk sides closed.png")
image kevin smirk sides open = make_sprite("sprites/kevin/smirk sides open.png")

# Maryanne Sawyer
layeredimage maryanne:
    attribute_function maryanne_add_circles
    group hair:
        attribute ponytail default:
            make_sprite("sprites/maryanne/hair_ponytail.png")
        attribute hairdown:
            make_sprite("sprites/maryanne/hair_hairdown.png")
        attribute tangled:
            make_sprite("sprites/maryanne/hair_tangled.png")
    always:
        make_sprite("sprites/maryanne/base.png")
    group emotion:
        attribute neutral default:
            make_sprite("sprites/maryanne/emotion_neutral.png")
        attribute confused:
            make_sprite("sprites/maryanne/emotion_confused.png")
        attribute crying:
            make_sprite("sprites/maryanne/emotion_crying.png")
        attribute ecstatic:
            make_sprite("sprites/maryanne/emotion_ecstatic.png")
        attribute embarrassed:
            make_sprite("sprites/maryanne/emotion_embarrassed.png")
        attribute happy:
            make_sprite("sprites/maryanne/emotion_happy.png")
        attribute mad:
            make_sprite("sprites/maryanne/emotion_mad.png")
        attribute pissed:
            make_sprite("sprites/maryanne/emotion_pissed.png")
        attribute sad:
            make_sprite("sprites/maryanne/emotion_sad.png")
        attribute stressed:
            make_sprite("sprites/maryanne/emotion_stressed.png")
        attribute uncertain:
            make_sprite("sprites/maryanne/emotion_uncertain.png")
        attribute uncomfortable:
            make_sprite("sprites/maryanne/emotion_uncomfortable.png")
        attribute worried:
            make_sprite("sprites/maryanne/emotion_worried.png")
    group blushing:
        attribute noblush default null
        attribute blush:
            make_sprite("sprites/maryanne/blush.png")
    group necklace:
        attribute withnecklace default:
            make_sprite("sprites/maryanne/necklace_withnecklace.png")
        attribute nonecklace null
    attribute coat:
        make_sprite("sprites/maryanne/coat.png")
    attribute circles null
    group circletypes:
        attribute type1:
            im.MatrixColor(make_sprite("sprites/maryanne/circles_type1.png"), boost_opacity(4))
        attribute type2:
            im.MatrixColor(make_sprite("sprites/maryanne/circles_type2.png"), boost_opacity(4))
        attribute type3:
            im.MatrixColor(make_sprite("sprites/maryanne/circles_type3.png"), boost_opacity(4))
        attribute type4:
            im.MatrixColor(make_sprite("sprites/maryanne/circles_type4.png"), boost_opacity(4))
    group tears:
        attribute notears null default
        attribute smalltears:
            make_sprite("sprites/maryanne/tears_small.png")
        attribute bigtears:
            make_sprite("sprites/maryanne/tears_big.png")
        attribute happybigtears:
            make_sprite("sprites/maryanne/tears_happybig.png")
        attribute uncertainsmalltears:
            make_sprite("sprites/maryanne/tears_uncertainsmall.png")

# Okada-sensei
image okada angry = make_sprite("sprites/okada/angry.png")
image okada neutral = make_sprite("sprites/okada/neutral.png")
image okada talking = make_sprite("sprites/okada/talking.png")
image okada yelling = make_sprite("sprites/okada/yelling.png")

# Risa Hayase
image risa neutral = make_sprite("sprites/risa/neutral.png")
image risa happy = make_sprite("sprites/risa/happy.png")
image risa pout = make_sprite("sprites/risa/pout.png")
image risa concern = make_sprite("sprites/risa/concern.png")

# Shione Yamazaki
image shione finger confused = make_sprite("sprites/shione/finger confused.png")
image shione finger excited = make_sprite("sprites/shione/finger excited.png")
image shione finger exclaim = make_sprite("sprites/shione/finger exclaim.png")
image shione finger pompous = make_sprite("sprites/shione/finger pompous.png")
image shione finger wonder = make_sprite("sprites/shione/finger wonder.png")
image shione hips confused = make_sprite("sprites/shione/hips confused.png")
image shione hips excited = make_sprite("sprites/shione/hips excited.png")
image shione hips exclaim = make_sprite("sprites/shione/hips exclaim.png")
image shione hips pompous = make_sprite("sprites/shione/hips pompous.png")
image shione hips wonder = make_sprite("sprites/shione/hips wonder.png")
