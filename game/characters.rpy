## Special
define base_char = Character(color="#FFFFFF", ctc="ctc", ctc_pause="ctc", ctc_position="fixed")
define narrator = Character(kind=base_char)

define unknown_name = "???"
default ai_name = unknown_name
default george_name = unknown_name
default hideki_name = unknown_name
default kevin_name = unknown_name
default maryanne_name = "Western Girl"
default okada_name = unknown_name
default risa_name = "Purple-Haired Girl"
default shione_name = "Strange Girl"

## Named
define Ai = Character("ai_name", dynamic=True, image="ai", kind=base_char)
define Fusao = Character("Fusao", color="#FF9900", image="fusao", kind=base_char)
define George = Character("george_name", dynamic=True, image="george", kind=base_char)
define Hideki = Character("hideki_name", color="#6666FF", dynamic=True, image="hideki", kind=base_char)
define Higa = Character("Higa-sensei", image="higa", kind=base_char)
define Kevin = Character("kevin_name", color="#DFA07D", dynamic=True, image="kevin", kind=base_char)
define Maryanne = Character("maryanne_name", color="#FBEF9E", dynamic=True, image="maryanne", kind=base_char)
define Okada = Character("okada_name", dynamic=True, image="okada", kind=base_char)
define Risa = Character("risa_name", color="#FF33CC", dynamic=True, image="risa", kind=base_char)
define Shione = Character("shione_name", color="#C1F2C4", dynamic=True, image="shione", kind=base_char)
define Shun = Character("Shun", color="#F5C403", kind=base_char)

## Other
define AMach = Character("Answering Machine", kind=base_char)
define Anon = Character(unknown_name, kind=base_char)
define Bus = Character("Bus Driver", kind=base_char)
define Dad = Character("Dad", kind=base_char)
define Mom = Character("Mom", kind=base_char)
define Note = Character("Note", kind=base_char)
define Waitress = Character("Waitress", kind=base_char)
define Woman = Character("Woman", kind=base_char)
