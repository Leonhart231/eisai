## Custom transitions
define bg_change_seconds = Dissolve(0.2)
define bg_change_minutes = Fade(1, 0.5, 1)
define bg_change_hours = bg_change_minutes
define bg_change_days = MultipleTransition([
    False, Fade(1, 0, 1),
    "images/general/logo.png", Pause(1.0),
    "images/general/logo.png", Fade(1, 0, 1),
    True])

define config.say_attribute_transition = bg_change_seconds

## Click-to-continue marker
image ctc:
    "images/general/ctc.png"
    xalign 0.83
    yalign 0.958
    alpha 0
    block:
        linear 3 alpha 1
        "images/general/ctc.png"
        linear 3 alpha 0
        repeat

init python:
    ## Audio channel definitions
    renpy.music.register_channel("music", mixer="music", loop=True, stop_on_mute=True, tight=False, file_prefix='audio/music/', file_suffix='', buffer_queue=True)
    renpy.music.register_channel("sound", mixer="sfx", loop=False, stop_on_mute=True, tight=False, file_prefix='audio/sfx/', file_suffix='.ogg', buffer_queue=True)
    renpy.music.register_channel("ambient", mixer="sfx", loop=True, stop_on_mute=True, tight=False, file_prefix='audio/sfx/', file_suffix='.ogg', buffer_queue=True)
    renpy.music.register_channel("ambient2", mixer="sfx", loop=True, stop_on_mute=True, tight=False, file_prefix='audio/sfx/', file_suffix='.ogg', buffer_queue=True)

    ## Font mapping
    config.font_replacement_map["fonts/DejaVuSans.ttf", False, True] = ("fonts/DejaVuSans-Oblique.ttf", False, False)
    config.font_replacement_map["fonts/DejaVuSans.ttf", True, False] = ("fonts/DejaVuSans-Bold.ttf", False, False)
    config.font_replacement_map["fonts/DejaVuSans.ttf", True, True] = ("fonts/DejaVuSans-BoldOblique.ttf", False, False)

    ## Define tag for English
    def en_tag(tag, value, contents):
        return [
                (renpy.TEXT_TEXT, "<"),
            ] + contents + [
                (renpy.TEXT_TEXT, ">"),
            ]
    config.custom_text_tags["en"] = en_tag

    ## Define tag for emphasis
    def em_tag(tag, value, contents):
        return [
                (renpy.TEXT_TAG, u"i"),
            ] + contents + [
                (renpy.TEXT_TAG, u"/i"),
            ]
    config.custom_text_tags["em"] = em_tag

    ## Define tag for flashbacks
    def flashback_tag(tag, value, contents):
        return [
                (renpy.TEXT_TAG, u"i"),
            ] + contents + [
                (renpy.TEXT_TAG, u"/i"),
            ]
    config.custom_text_tags["flashback"] = flashback_tag

    ## Define tag for sounds
    def snd_tag(tag, value, contents):
        return [
                (renpy.TEXT_TAG, u"i"),
            ] + contents + [
                (renpy.TEXT_TAG, u"/i"),
            ]
    config.custom_text_tags["snd"] = snd_tag

    ## Define tag for text messages
    def txt_tag(tag, value, contents):
        return [
                (renpy.TEXT_TAG, u"font=fonts/DejaVuSansMono.ttf"),
            ] + contents + [
                (renpy.TEXT_TAG, u"/font"),
            ]
    config.custom_text_tags["tt"] = txt_tag

    ## Calculate the scale needed for an image to fill the screen
    def get_img_fill_scale(img):
        img_w, img_h = get_img_size(img)
        need_w_scale = config.screen_width / float(img_w)
        need_h_scale = config.screen_height / float(img_h)
        return max(need_w_scale, need_h_scale)

    ## Calculate the scale needed for an image to fill the screen vertically
    def get_img_fill_vert_scale(img):
        img_w, img_h = get_img_size(img)
        return config.screen_height / float(img_h)

    ## Get the height and width of an image
    def get_img_size(img):
        tmp1 = renpy.loader.load(img)
        tmp2 = renpy.display.scale.image_load_unscaled(tmp1, img)
        return tmp2.get_size()

    ## Upscale an image for use as a background
    def make_bg(img):
        return make_fillscreen(img)

    ## Upscale an image for use as a CG
    def make_cg(img):
        return make_fillscreen(img)

    ## Scale an image to use as a sprite
    def make_sprite(img):
        return make_fillvert(img)

    ## Upscale an image to fill the screen
    def make_fillscreen(img):
        return im.FactorScale(img, get_img_fill_scale(img))

    ## Upscale an image to fill the screen vertically
    def make_fillvert(img):
        return im.FactorScale(img, get_img_fill_vert_scale(img))

    ## Matrix for boosting opacity in an image
    def boost_opacity(val):
        return [1,0,0,0,0, 0,1,0,0,0, 0,0,1,0,0, 0,0,0,val,0]
