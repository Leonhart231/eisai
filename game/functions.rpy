## Audio
label audio_fadeout:
    stop music fadeout config.fade_music
    stop sound fadeout config.fade_music
    stop ambient fadeout config.fade_music
    stop ambient2 fadeout config.fade_music
    return

label audio_stop:
    stop music fadeout 0
    stop sound fadeout 0
    stop ambient fadeout 0
    stop ambient2 fadeout 0
    return

## Global transitions
label reset:
    call audio_fadeout
    scene bg black with bg_change_seconds
    return

## Miscellaneous
label headache_start:
    show red with dissolve:
        alpha 0.2
        linear 2.0 alpha 0.0
        linear 2.0 alpha 0.2
        repeat
    return

label headache_stop:
    hide red with dissolve
    return
