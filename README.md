# Eisai
Eisai is a visual novel about first impressions, how they are often what they first seemed, and how strangers in new places can surprise you. The main character, running from a dysfunctional family, hopes to get a fresh start in college after being accepted to a prestigious honor society program called “Eisai”. But the studious young introvert quickly learns that “fresh starts” require embracing that which you’re not used to and leaving your comfort zone. It’s a comedy, a drama, a romance … a slice of life!
## Downloading
Downloads for Windows, Mac, and Linux can be found on [itch.io](https://shotglass-studios.itch.io/eisai).
