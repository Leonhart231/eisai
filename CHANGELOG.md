# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.1] - 2022-02-11
### Removed
- Removed unused audio assets
### Fixed
- Fixed many minor errors in the text
- Corrected credits

## [1.0.0] - 2022-01-05
### Added
- Initial release
